﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class WorkingGoalsManager : MonoBehaviour
{
    public enum GoalType
    {
        worker_win, extra_vote, employee_review, invalid
    }

    public class Goal
    {
        public GoalType type = GoalType.invalid;
        public int requiredPoints = -1;

        public Goal(GoalType inType, int inRequiredPoints)
        {
            type = inType;
            requiredPoints = inRequiredPoints;
        }
    }

    public class PlayerPoints
    {
        public BirdName player = BirdName.none;
        public int points = -1;
        public string prefixGuess = "";
        public string correctPrefix = "";
        public string nounGuess = "";
        public string correctNoun = "";

        public PlayerPoints(BirdName inPlayer, int inPoints, string inPrefixGuess, string inCorrectPrefix, string inNounGuess, string inCorrectNoun)
        {
            player = inPlayer;
            points = inPoints;
            prefixGuess = inPrefixGuess;
            correctPrefix = inCorrectPrefix;
            nounGuess = inNounGuess;
            correctNoun = inCorrectNoun;
        }
    }

    public bool active = false;
    public float graphWidth = 5f;
    public float graphHeight = 5f;
    public GameObject linePrefab;
    public GameObject bgLinePrefab;
    public GameObject thresholdLinePrefab;
    public Vector3 startingPoint;
    public float timeToShowPoint = 1.0f;
    public List<string> graphingSounds;

    public GameObject extraVoteVisualPrefab, evaluateVisualPrefab, workerWinVisualPrefab;
    public GameObject guessContainer;
    public Text prefixText;
    public Text nounText;
    public Image authorImage;

    private LineRenderer currentLineRenderer;
    private float _timeShowingPoint = 0f;
    private float _heightReached = 0f;
    private int _currentCabinetIndex = 0;
    private List<Goal> _goals = new List<Goal>();
    private List<PlayerPoints> _pointsPerCabinet = new List<PlayerPoints>();

    public void Start()
    {
        //List<Goal> tempGoals = new List<Goal>() { new Goal(GoalType.worker_win, 24), new Goal(GoalType.extra_vote, 8) };
        //List<PlayerPoints> tempPoints = new List<PlayerPoints>() { new PlayerPoints(BirdName.brown, 1, "potato", "","tomato","tomato"), new PlayerPoints(BirdName.purple, 1,"potato", "potato", "tomato", ""), new PlayerPoints(BirdName.green, 3,"potato", "potato", "tomato", "tomato"), new PlayerPoints(BirdName.black, 1, "potato", "", "tomato", "tomato"), new PlayerPoints(BirdName.grey, 1,"potato", "potato", "tomato", ""), new PlayerPoints(BirdName.blue, 3, "potato", "potato", "tomato", "tomato"), new PlayerPoints(BirdName.orange, 0, "potato", "", "tomato", ""), new PlayerPoints(BirdName.red, 3, "potato", "potato", "tomato", "tomato") };
        //initializeWorkingGoals(tempGoals, tempPoints);
        //startShowingWorkingGoals();
    }

    public void Update()
    {
        if (active)
        {
            _timeShowingPoint += Time.deltaTime;
            float widthPerSegment = (graphWidth / _pointsPerCabinet.Count);
            float heightPerSegment = (graphHeight / _pointsPerCabinet.Count / 3);
            Vector3 newPosition = new Vector3(startingPoint.x + widthPerSegment * (_currentCabinetIndex + (_timeShowingPoint / timeToShowPoint)),
                                                _heightReached + (heightPerSegment*(_timeShowingPoint/timeToShowPoint) * _pointsPerCabinet[_currentCabinetIndex].points),
                                                -2);
            currentLineRenderer.SetPosition(1, newPosition);
            if(_timeShowingPoint > timeToShowPoint)
            {
                _timeShowingPoint = 0.0f;
                _currentCabinetIndex++;
                if(_pointsPerCabinet.Count > _currentCabinetIndex)
                {
                    currentLineRenderer = Instantiate(linePrefab,transform).GetComponent<LineRenderer>();
                    currentLineRenderer.material = ColourManager.Instance.birdMap[_pointsPerCabinet[_currentCabinetIndex].player].material;
                    
                    _heightReached += heightPerSegment * _pointsPerCabinet[_currentCabinetIndex-1].points;
                    currentLineRenderer.SetPosition(0, new Vector3(startingPoint.x + widthPerSegment * _currentCabinetIndex, _heightReached,-2));
                    currentLineRenderer.SetPosition(1, new Vector3(startingPoint.x + widthPerSegment * _currentCabinetIndex, _heightReached, -2));
                    foreach(string sound in graphingSounds)
                    {
                        AudioManager.Instance.StopSound(sound);
                    }
                    authorImage.sprite = ColourManager.Instance.birdMap[_pointsPerCabinet[_currentCabinetIndex].player].faceSprite;
                    prefixText.text = _pointsPerCabinet[_currentCabinetIndex].prefixGuess;
                    prefixText.color = _pointsPerCabinet[_currentCabinetIndex].prefixGuess == _pointsPerCabinet[_currentCabinetIndex].correctPrefix ? new Color(0.25f, 0.75f, 0.25f) : Color.red;
                    nounText.text = _pointsPerCabinet[_currentCabinetIndex].nounGuess;
                    nounText.color = _pointsPerCabinet[_currentCabinetIndex].nounGuess == _pointsPerCabinet[_currentCabinetIndex].correctNoun ? new Color(0.25f, 0.75f, 0.25f) : Color.red;
                    AudioManager.Instance.PlaySound(graphingSounds[_pointsPerCabinet[_currentCabinetIndex].points], true);
                }
                else
                {
                    foreach (string sound in graphingSounds)
                    {
                        AudioManager.Instance.StopSound(sound);
                    }
                    guessContainer.SetActive(false);
                    active = false;
                    
                }
            }
        }
    }

    public void initializeWorkingGoals(List<Goal> goals, List<PlayerPoints> playerPoints)
    {
        _goals = goals;
        _pointsPerCabinet = playerPoints;
    }

    public void startShowingWorkingGoals()
    {
        guessContainer.SetActive(true);
        _currentCabinetIndex = 0;
        
        float heightPerSegment = (graphHeight / _pointsPerCabinet.Count / 3);
        int numberOfBGLines = _pointsPerCabinet.Count*3;

        for(int i = 0; i < numberOfBGLines; i++)
        {
            LineRenderer bgLine = Instantiate(bgLinePrefab, transform).GetComponent < LineRenderer>();
            bgLine.SetPosition(0, new Vector3(startingPoint.x, startingPoint.y + (i * heightPerSegment), -1));
            bgLine.SetPosition(1, new Vector3(startingPoint.x + graphWidth, startingPoint.y + (i * heightPerSegment), -1));
        }
        foreach (Goal goal in _goals)
        {
            //Instantiate a threshold line at the corresponding height
            LineRenderer thresholdLine = Instantiate(thresholdLinePrefab, transform).GetComponent<LineRenderer>();
            thresholdLine.SetPosition(0, new Vector3(startingPoint.x, startingPoint.y + heightPerSegment * goal.requiredPoints, -2));
            thresholdLine.SetPosition(1, new Vector3(startingPoint.x + graphWidth, startingPoint.y + heightPerSegment * goal.requiredPoints, -2));
            GameObject selectedPrefab;
            //Instantiate the goal image at the corresponding height
            switch (goal.type)
            {
                case GoalType.employee_review:
                    selectedPrefab = evaluateVisualPrefab;
                    break;
                case GoalType.extra_vote:
                    selectedPrefab = extraVoteVisualPrefab;
                    break;
                case GoalType.worker_win:
                    selectedPrefab = workerWinVisualPrefab;
                    break;
                default:
                    continue;
            }
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(new Vector3(startingPoint.x, startingPoint.y + heightPerSegment * goal.requiredPoints, -2));
            GameObject newObject = Instantiate(selectedPrefab, Vector3.zero, Quaternion.identity, transform);
            newObject.transform.position = screenPoint;
        }
        
        _timeShowingPoint = 0.0f;
        active = true;
        currentLineRenderer = Instantiate(linePrefab, transform).GetComponent<LineRenderer>();
        currentLineRenderer.material = ColourManager.Instance.birdMap[_pointsPerCabinet[0].player].material;
        currentLineRenderer.SetPosition(0, startingPoint);

        authorImage.sprite = ColourManager.Instance.birdMap[_pointsPerCabinet[0].player].faceSprite;
        prefixText.text = _pointsPerCabinet[0].prefixGuess;
        prefixText.color = _pointsPerCabinet[0].prefixGuess == _pointsPerCabinet[0].correctPrefix ? new Color(0.25f, 0.75f, 0.25f) : Color.red;
        nounText.text = _pointsPerCabinet[0].nounGuess;
        nounText.color = _pointsPerCabinet[0].nounGuess == _pointsPerCabinet[0].correctNoun ? new Color(0.25f, 0.75f, 0.25f) : Color.red;
        _heightReached = startingPoint.y;
        AudioManager.Instance.PlaySound(graphingSounds[_pointsPerCabinet[0].points], true);
    }

}
