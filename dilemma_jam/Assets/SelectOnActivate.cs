﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectOnActivate : MonoBehaviour
{
    public Selectable objectToSelectOnActivate;

    // Start is called before the first frame update
    private void OnEnable()
    {
        objectToSelectOnActivate.Select();
    }


}
