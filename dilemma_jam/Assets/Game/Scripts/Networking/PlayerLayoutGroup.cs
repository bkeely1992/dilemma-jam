﻿using Photon.Realtime;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerLayoutGroup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject _playerListingPrefab;
    private GameObject PlayerListingPrefab
    {
        get { return _playerListingPrefab; }
    }

    private List<PlayerListing> _playerListings = new List<PlayerListing>();
    private List<PlayerListing> PlayerListings
    {
        get { return _playerListings; }
    }

    private void Update()
    {
        List<Player> allPlayers = PhotonNetwork.PlayerList.ToList();


        for (int i = PlayerListings.Count - 1; i >= 0; i--)
        {
            if (allPlayers.Where(p => p == PlayerListings[i].PhotonPlayer).ToList().Count != 1)
            {
                if (PlayerListings[i] && 
                    PlayerListings[i].gameObject)
                {
                    Destroy(PlayerListings[i].gameObject);
                }
                
                PlayerListings.RemoveAt(i);
            }
        }
    }

    public void PlayerJoinedRoom(Player photonPlayer, string name)
    {
        if (photonPlayer == null)
        {
            return;
        }

        PlayerLeftRoom(photonPlayer);

        GameObject playerListingObj = Instantiate(PlayerListingPrefab);
        playerListingObj.transform.SetParent(transform, false);

        PlayerListing playerListing = playerListingObj.GetComponent<PlayerListing>();
        playerListing.ApplyPhotonPlayer(photonPlayer, name);

        foreach(KeyValuePair<ColourManager.BirdName,PlayerIdentification> playerID in MenuLobbyButtons.Instance.PlayerIdentificationMap)
        {
            if(playerID.Value.name != "" && 
                playerID.Key != ColourManager.BirdName.none &&
                playerID.Value.playerName.ToString() == playerListing.NameText.text)
            {
                playerListing.PlayerImage.sprite = ColourManager.Instance.birdMap[playerID.Key].faceSprite;
                playerListing.NameText.color = ColourManager.Instance.birdMap[playerID.Key].colour;
                playerListing.PlayerImage.gameObject.SetActive(true);
            }
        }

        PlayerListings.Add(playerListing);
        MenuLobbyButtons.Instance.PlayerListingMap.Add(name, playerListing);
        MenuLobbyButtons.Instance.UpdateCorrectCabinetThresholdMaximum(PlayerListings.Count());
    }

    public void PlayerLeftRoom(Player photonPlayer)
    {
        int index = PlayerListings.FindIndex(x => x.PhotonPlayer == photonPlayer);

        if (MenuLobbyButtons.Instance.PlayerListingMap.ContainsKey(photonPlayer.NickName))
        {
            MenuLobbyButtons.Instance.PlayerListingMap.Remove(photonPlayer.NickName);
        }

        if (index != -1)
        {
            if (PlayerListings[index].gameObject)
            {
                Destroy(PlayerListings[index].gameObject);
            }

            PlayerListings.RemoveAt(index);
        }

        MenuLobbyButtons.Instance.UpdateCorrectCabinetThresholdMaximum(PlayerListings.Count());
    }

    public void Refresh()
    {
        for(int i = PlayerListings.Count - 1; i >= 0; i--)
        {
            Destroy(PlayerListings[i].gameObject);
        }

        foreach(Player player in PhotonNetwork.PlayerList)
        {
            PlayerJoinedRoom(player, player.NickName);
        }
    }


}
