﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviourPunCallbacks
{
    public Player PhotonPlayer { get; private set; }

    public Text NameText;
    private Text PlayerName
    {
        get { return NameText; }
    }

    public Image PlayerImage;

    public void ApplyPhotonPlayer(Player photonPlayer, string name)
    {
        PhotonPlayer = photonPlayer;
        PlayerName.text = name;
    }

    public void ChangePlayerBird(ColourManager.BirdName inBirdName)
    {
        PlayerImage.sprite = ColourManager.Instance.birdMap[inBirdName].faceSprite;
        PlayerName.color = ColourManager.Instance.birdMap[inBirdName].colour;
        PlayerImage.gameObject.SetActive(true);
    }

    public void ResetPlayerText()
    {
        PlayerName.color = Color.black;
        PlayerImage.gameObject.SetActive(false);
    }


}
