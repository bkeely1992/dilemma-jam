﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    public Text RoomNameText;
    public Text AttendeeCountText;
    public string roomName;

    public void Select()
    {
        MenuLobbyButtons.Instance.SelectRoomListing(this);
    }
}
