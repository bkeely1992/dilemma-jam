﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class MeetingBirdArm : MonoBehaviour
{
    public Vector3 startingPosition;
    public float moveDelay = 0.25f;
    public ColourManager.BirdName birdName;
    public float birdHandHeight;
    public float birdHandWidth;
    public Transform birdHand;
    public bool activated;
    public Image birdImage;
    public Image bodyImage;
    public Image birdHandImage;
    public GameObject birdParent;
    public bool isInitialized = false;
    public BirdButton accusationButton;
    public GameObject upForReviewToken;
    public GameObject eotmToken;

    private float timeSinceLastMove = 0.0f;
    private ColourManager.BirdName playerBirdName;
    private Vector3 screenOrigin, screenExtent;
    private float imageSize;
    

    // Start is called before the first frame update
    void Start()
    {
        screenOrigin = new Vector3(0, 0, 0);
        screenExtent = new Vector3(Screen.width, Screen.height,0);
        playerBirdName = SettingsManager.Instance.birdName;

        imageSize = GetComponent<Image>().rectTransform.rect.width;
    }

    public void initialize(ColourManager.BirdName inBirdName)
    {
        if (!isInitialized)
        {
            birdName = inBirdName;
            bodyImage.sprite = ColourManager.Instance.birdMap[inBirdName].accusationBodySprite;
            birdImage.sprite = ColourManager.Instance.birdMap[inBirdName].accusationArmSprite;
            birdHandImage.sprite = ColourManager.Instance.birdMap[inBirdName].accusationHandSprite;

            birdHandImage.gameObject.transform.eulerAngles += new Vector3(0, 0, ColourManager.Instance.birdMap[inBirdName].birdHandRotationAdjustment);
            birdHandImage.gameObject.transform.localScale = ColourManager.Instance.birdMap[inBirdName].birdHandWidthScaling;
            birdParent.SetActive(true);

            accusationButton.parent.SetActive(true);
            accusationButton.playerName.text = GameManager.Instance.playerFlowManager.playerNameMap[birdName];
            accusationButton.playerLabelObject.SetActive(true);
            accusationButton.birdName = inBirdName;
            bodyImage.gameObject.SetActive(true);
            gameObject.SetActive(true);
            isInitialized = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (birdName != playerBirdName ||
            !activated)
        {
            return;
        }

        Vector3 temp;
        if (Input.mousePosition.x < startingPosition.x)
        {
            temp = Input.mousePosition - (transform.localScale.x * birdHandHeight * transform.right + transform.up * birdHandWidth);
        }
        else
        {
            temp = Input.mousePosition - (transform.localScale.x * birdHandHeight * transform.right + transform.up * birdHandWidth);
        }
         


        if (temp.x < screenOrigin.x)
        {
            temp.x = screenOrigin.x;
        }
        else if(temp.x > screenExtent.x)
        {
            temp.x = screenExtent.x;
        }
        if(temp.y < screenOrigin.y)
        {
            temp.y = screenOrigin.y;
        }
        else if(temp.y > screenExtent.y)
        {
            temp.y = screenExtent.y;
        }

        Stretch(transform.position, temp);

        timeSinceLastMove += Time.deltaTime;

        if (timeSinceLastMove > moveDelay)
        {
            timeSinceLastMove = 0.0f;
            if (Photon.Pun.PhotonNetwork.IsMasterClient)
            {
                GameManager.Instance.gameFlowManager.SetAccuseArmStretch(birdName, temp);
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("accuse_arm_position" + GameDelim.BASE + birdName.ToString() + GameDelim.BASE + temp.x.ToString(CultureInfo.InvariantCulture)
                                                                                                                    + GameDelim.SUB + temp.y.ToString(CultureInfo.InvariantCulture) + 
                                                                                                                    GameDelim.SUB + temp.z.ToString(CultureInfo.InvariantCulture));
            }
        }
    }

    public void Stretch(Vector3 startingPosition, Vector3 stretchingPosition)
    {
        Vector3 direction = stretchingPosition - startingPosition;
        direction = Vector3.Normalize(direction);
        transform.right = direction;

        Vector3 scale = new Vector3(1, 1, 1);
        scale.x = Vector3.Distance(startingPosition, stretchingPosition) / imageSize;

        transform.localScale = scale;
    }

    public void Stretch(ColourManager.BirdName inBirdName, string[] messageSegments)
    {
        if (inBirdName == playerBirdName)
        {
            return;
        }

        //Convert the segments and then call the base stretch method
        string[] currentSplit = messageSegments[2].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
        Vector3 stretchingPosition = new Vector3(float.Parse(currentSplit[0], CultureInfo.InvariantCulture), float.Parse(currentSplit[1], CultureInfo.InvariantCulture), float.Parse(currentSplit[2], CultureInfo.InvariantCulture));
        Stretch(transform.position, stretchingPosition);
    }

    public void Stretch(ColourManager.BirdName inBirdName, Vector3 stretchingPosition)
    {
        if (inBirdName == playerBirdName)
        {
            return;
        }

        Stretch(transform.position, stretchingPosition);
    }
}
