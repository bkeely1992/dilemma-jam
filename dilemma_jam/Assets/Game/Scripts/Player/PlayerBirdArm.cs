﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class PlayerBirdArm : MonoBehaviour
{
    public SpriteRenderer sprite;
    public SpriteRenderer outlineSprite;
    public ColourManager.BirdName birdName;
    public float moveDelay = 0.5f;
    public float barrierPosition = 100.0f;
    public LayerMask cabinetLayer;
    public GameObject heldFolderObject;
    public enum FolderState
    {
        receiving, sending, inactive
    }
    public FolderState folderState = FolderState.inactive;

    private bool isInitialized = false;
    private float timeSinceLastMove;
    private Vector3 screenOrigin;

    // Start is called before the first frame update
    void Start()
    {
        screenOrigin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        timeSinceLastMove = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        HandleClick();
        MoveArm();
    }

    private void HandleClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100.0f, cabinetLayer);
            if (hit)
            {
                CabinetDrawer hitCabinet = hit.collider.gameObject.GetComponent<CabinetDrawer>();

                if (hitCabinet)
                {
                    hitCabinet.select();
                    return;
                }
            }
        }
    }

    private void MoveArm()
    {
        Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (temp.x < screenOrigin.x)
        {
            temp.x = screenOrigin.x;
        }

        transform.position = new Vector3(temp.x, temp.y, transform.position.z);

        timeSinceLastMove += Time.deltaTime;
        
        if (timeSinceLastMove > moveDelay)
        {
            if (!isInitialized)
            {
                birdName = SettingsManager.Instance.birdName;
                sprite.sprite = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].armSprite;
                outlineSprite.sprite = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].armOutlineSprite;
                isInitialized = true;
            }

            timeSinceLastMove = 0.0f;
            if (PhotonNetwork.IsMasterClient)
            {
                GameManager.Instance.gameFlowManager.SetBirdArmPosition(birdName, transform.position);
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("drawing_arm_position" + GameDelim.BASE + birdName.ToString() + GameDelim.BASE + transform.position.x.ToString(CultureInfo.InvariantCulture) +
                                                                                                                    GameDelim.SUB + transform.position.y.ToString(CultureInfo.InvariantCulture) +
                                                                                                                    GameDelim.SUB + transform.position.z.ToString(CultureInfo.InvariantCulture));
            }
        }
    }
}
