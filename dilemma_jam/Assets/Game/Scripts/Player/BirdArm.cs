﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdArm : MonoBehaviour
{
    public SpriteRenderer sprite;
    public ColourManager.BirdName birdName;
    public GameObject heldFolderObject;
}
