﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static ColourManager;

public class CabinetDrawer : MonoBehaviour
{
    public int id;
    public SpriteRenderer tab1, tab2, tab3, tab4, tab5;
    public List<SpriteRenderer> colouredElements;

    public Dictionary<int, SpriteRenderer> tabs = new Dictionary<int, SpriteRenderer>();
    public BirdName currentPlayer;

    public Color playerColour;
    public int currentTab = 0;
    public GameObject glowObject;
    public float timePerFlash = 0.85f;
    public Clock clock;
    public bool ready = false;
    public GameObject folderObject;

    public enum ChainState
    {
        waiting, providing, receiving
    }
    public ChainState chainState = ChainState.waiting;

    public ChainData chainData;
    private Animator animator;
    private PlayerFlowManager playerFlowManager;
    private GameFlowManager gameFlowManager;
    private bool isInitialized = false;
    private float totalTimeFlashing = 0.0f;

    public void OnEnable()
    {
        clock.OnClockComplete += handleClockComplete;
    }

    public void OnDisable()
    {
        clock.OnClockComplete -= handleClockComplete;
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    private void Update()
    {
        if(totalTimeFlashing > 0.0f)
        {
            totalTimeFlashing += Time.deltaTime;
            if(totalTimeFlashing > timePerFlash)
            {
                glowObject.SetActive(!glowObject.activeSelf);
                if(currentPlayer == SettingsManager.Instance.birdName)
                {
                    totalTimeFlashing = Time.deltaTime;
                }
                else
                {
                    totalTimeFlashing = 0.0f;
                }
            }
        }
    }

    private void initialize()
    {
        chainData = new ChainData();
        animator = GetComponent<Animator>();
        tabs.Add(1, tab1);
        tabs.Add(2, tab2);
        tabs.Add(3, tab3);
        tabs.Add(4, tab4);
        tabs.Add(5, tab5);
        isInitialized = true;
    }

    public void setDrawerColours()
    {
        Color drawerColour = ColourManager.Instance.birdMap[currentPlayer].colour;

        foreach (SpriteRenderer colouredCabinetElement in colouredElements)
        {
            colouredCabinetElement.color = drawerColour;
        }

        switch (GameManager.Instance.playerFlowManager.drawingRound.currentState)
        {
            case DrawingRound.CurrentState.drawing:
            case DrawingRound.CurrentState.prompting:
                //folderObject.GetComponent<SpriteRenderer>().color = Color.white;
                break;
            case DrawingRound.CurrentState.evaluation:
                //folderObject.GetComponent<SpriteRenderer>().color = ColourManager.Instance.evaluationFolderColour;
                break;
        }

        
    }

    private string getTabVisualMessage(string tabMessagePrefix, ChainData inChainData, bool isInUse)
    {
        string message = tabMessagePrefix + GameDelim.BASE + id.ToString() + GameDelim.BASE + currentPlayer.ToString() + GameDelim.BASE + isInUse.ToString();
        message += GameDelim.BASE + (inChainData.drawings.ContainsKey(1) ? inChainData.drawings[1].author.ToString() : "");
        message += GameDelim.BASE + (inChainData.prompts.ContainsKey(2) ? inChainData.prompts[2].author.ToString() : "");
        message += GameDelim.BASE + (inChainData.drawings.ContainsKey(3) ? inChainData.drawings[3].author.ToString() : "");
        message += GameDelim.BASE + (inChainData.prompts.ContainsKey(4) ? inChainData.prompts[4].author.ToString() : "");
        message += GameDelim.BASE + (inChainData.drawings.ContainsKey(5) ? inChainData.drawings[5].author.ToString() : "");
        return message;
    }

    public void setDrawerVisuals(BirdName inCurrentPlayer, bool useCabinet, BirdName tab1Player, BirdName tab2Player, BirdName tab3Player, BirdName tab4Player, BirdName tab5Player)
    {
        currentPlayer = inCurrentPlayer;
        
        setDrawerColours();

        setTabVisual(tab1, tab1Player);
        setTabVisual(tab2, tab2Player);
        setTabVisual(tab3, tab3Player);
        setTabVisual(tab4, tab4Player);
        setTabVisual(tab5, tab5Player);
    }

    private void setTabVisual(SpriteRenderer tabRenderer, BirdName author)
    {
        if(author != BirdName.none)
        {
            tabRenderer.gameObject.SetActive(true);
            tabRenderer.color = ColourManager.Instance.birdMap[author].colour;
        }
        else
        {
            tabRenderer.gameObject.SetActive(false);
        }
    }


    public void select()
    {
        if (chainData != null &&
            animator.GetBool("Open") &&
            currentPlayer == SettingsManager.Instance.birdName)
        {
            //Turn off the glow
            totalTimeFlashing = 0.0f;
            glowObject.SetActive(false);
            GameManager.Instance.playerFlowManager.drawingRound.currentActiveCabinetIndex = id;
            folderObject.SetActive(false);

            if (PhotonNetwork.IsMasterClient)
            {
                GameNetwork.Instance.BroadcastQueue.Add("hide_cabinet_folder" + GameDelim.BASE + id.ToString());
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("hide_cabinet_folder" + GameDelim.BASE + id.ToString());
            }

            GameManager.Instance.playerFlowManager.drawingRound.loadHeldFolder();

            ready = false;
        }
    }

    public void submit()
    {
        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            playerFlowManager.instructionRound.handleCabinetOpening(id, SettingsManager.Instance.birdName == currentPlayer);
        }

        if (currentTab == playerFlowManager.numberOfCabinetRounds)
        {
            currentTab++;
            foreach (SpriteRenderer cabinetElement in colouredElements)
            {
                cabinetElement.color = Color.white;
            }
            return;
        }
        currentTab++;
    }

    public void setAsReady(BirdName queuedPlayer)
    {
        chainState = ChainState.providing;
        GameManager.Instance.playerFlowManager.hasRunOutOfTime = false;
        GameManager.Instance.playerFlowManager.drawingRound.hasSubmitted = false;
        ready = true;
        

        currentPlayer = queuedPlayer;
        chainData.active = true;

        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        //Set tab visuals
        BirdName author1 = chainData.drawings.ContainsKey(1) ? chainData.drawings[1].author : BirdName.none;
        BirdName author2 = chainData.prompts.ContainsKey(2) ? chainData.prompts[2].author : BirdName.none;
        BirdName author3 = chainData.drawings.ContainsKey(3) ? chainData.drawings[3].author : BirdName.none;
        BirdName author4 = chainData.prompts.ContainsKey(4) ? chainData.prompts[4].author : BirdName.none;
        BirdName author5 = chainData.drawings.ContainsKey(5) ? chainData.drawings[5].author : BirdName.none;
        setDrawerVisuals(currentPlayer, false, author1, author2, author3, author4, author5);

        //Pull the drawer open
        animator.SetBool("Open", true);
        AudioManager.Instance.PlaySound("DrawerOpen");

        if (currentPlayer == SettingsManager.Instance.birdName)
        {
            //Initialize the drawer glow
            glowObject.SetActive(true);
            totalTimeFlashing += Time.deltaTime;
        }
        else
        {
            glowObject.SetActive(false);
            totalTimeFlashing = 0.0f;
        }
    }

    public void close()
    {
        animator.SetBool("Open", false);
        AudioManager.Instance.PlaySound("DrawerClose");
        glowObject.SetActive(false);
        totalTimeFlashing = 0.0f;
        folderObject.SetActive(true);
    }

    private void handleClockComplete()
    {
        //If drawing then submit what's been done up until that point

        //If prompt then submit what's been done up until that point

        //release();
    }

    public bool isFullyLoaded()
    {
        if (!chainData.active)
        {
            return true;
        }

        if(chainData.correctNoun == "" ||
            chainData.correctPrefix == "" ||
            chainData.guesser == BirdName.none ||
            !chainData.promptsAreLoaded ||
            !chainData.drawingsAreLoaded)
        {
            //Debug.LogError("Chain not loaded: cn[" + chainData.correctNoun + "], cp[" + chainData.correctPrefix + "], g[" + chainData.guesser + "], p[" + chainData.prompts.Count + " out of " + chainData.promptsExpected + "], d[" + chainData.drawings.Count + " out of " + chainData.drawingsExpected + "]");
            return false;
        }
        return true;
    }
}
