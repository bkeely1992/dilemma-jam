﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawingButton : MonoBehaviour
{
    public enum DrawingButtonType
    {
        colour, size, invalid
    }
    public DrawingButtonType drawingButtonType = DrawingButtonType.invalid;
    public string identifier = "";
    public Image buttonImage;
    public Image backgroundImage;
}
