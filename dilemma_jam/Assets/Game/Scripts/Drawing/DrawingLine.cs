﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ColourManager;

public class DrawingLine : MonoBehaviour
{
    public int sortingOrder = -1;
    public enum LineType
    {
        Base, Colour, Light, Erase, Invalid
    }
    public LineType lineType = LineType.Invalid;
    public string lineSize = "";
    public BirdName author = BirdName.none;
    public List<Vector3> positions = new List<Vector3>();
}
