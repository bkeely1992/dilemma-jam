﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static DrawingButton;

public class DrawingController : MonoBehaviour
{
    enum CursorVariant
    {
        feather, hand
    }

    public Color unselectedButtonColour;
    public float timeForDotClick = 0.25f;
    public LayerMask drawingLayer;
    public List<GameObject> lineObjects;

    public int currentSortingOrder = 1;
    public GameObject toolbarContainer;
    public float lineZOffset = -1f;

    private GameObject currentLine;
    private List<Vector2> fingerPositions = new List<Vector2>();
    private LineRenderer lineRenderer;
    private float timeSinceMouseDown = 0.0f;
    private bool startNewLine = true;
    private PolygonCollider2D polygonCollider2D;
    private Color currentDrawingColour = Color.black;
    private float currentDrawingSize = 0.05f;
    private GameObject currentDrawingPrefab = null;
    private Material currentDrawingMaterial = null;
    private string currentLineType = "";
    private string currentDrawingSizeName = "";

    public List<DrawingButton> drawingButtons = new List<DrawingButton>();
    private Dictionary<DrawingButtonType, Dictionary<string,DrawingButton>> drawingButtonMap = new Dictionary<DrawingButtonType, Dictionary<string,DrawingButton>>();
    private Dictionary<string, float> drawingSizes = new Dictionary<string, float>();
    public Dictionary<string, string> drawingSizeNames = new Dictionary<string, string>();
    private Dictionary<string, Color> drawingColours = new Dictionary<string, Color>();
    private Dictionary<string, GameObject> drawingPrefabs = new Dictionary<string, GameObject>();
    private Dictionary<string, Material> drawingMaterials = new Dictionary<string, Material>();
    private Dictionary<string, string> drawingTypes = new Dictionary<string, string>();
    private bool isInitialized = false;

    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
        //testLogic();

    }

    public void initialize()
    {
        if (!isInitialized)
        {
            polygonCollider2D = GetComponent<PolygonCollider2D>();
            lineObjects = new List<GameObject>();
            foreach (DrawingButton drawingButton in drawingButtons)
            {
                if (!drawingButtonMap.ContainsKey(drawingButton.drawingButtonType))
                {
                    drawingButtonMap.Add(drawingButton.drawingButtonType, new Dictionary<string, DrawingButton>());
                }

                if (drawingButtonMap[drawingButton.drawingButtonType].ContainsKey(drawingButton.identifier))
                {
                    Debug.LogError("Drawing button map contains multiple instances of drawing button[" + drawingButton.identifier + "] of drawing button type[" + drawingButton.drawingButtonType.ToString() + "].");
                    continue;
                }
                drawingButtonMap[drawingButton.drawingButtonType].Add(drawingButton.identifier, drawingButton);
            }
            isInitialized = true;
        }
    }

    private void testLogic()
    {
        setButtonDrawingSize("Big", GameManager.Instance.playerFlowManager.drawingRound.bigBrushSize, GameManager.Instance.playerFlowManager.drawingRound.bigBrushUIDotSize,"Big");
        setButtonDrawingSize("Medium", GameManager.Instance.playerFlowManager.drawingRound.mediumBrushSize, GameManager.Instance.playerFlowManager.drawingRound.mediumBrushUIDotSize,"Medium");
        setButtonDrawingSize("Small", GameManager.Instance.playerFlowManager.drawingRound.smallBrushSize, GameManager.Instance.playerFlowManager.drawingRound.smallBrushUIDotSize,"Small");
        Bird playerBird = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName];
        setButtonDrawingColour("Colour", playerBird.colour, GameManager.Instance.playerFlowManager.drawingRound.colourLinePrefab, playerBird.material, "Colour");
        setButtonDrawingColour("Light", playerBird.bgColour, GameManager.Instance.playerFlowManager.drawingRound.colourLinePrefab, playerBird.bgLineMaterial, "Light");
        setButtonDrawingColour("Base", Color.black, GameManager.Instance.playerFlowManager.drawingRound.baseLinePrefab, GameManager.Instance.playerFlowManager.drawingRound.baseLineMaterial, "Base");
        setButtonDrawingColour("Erase", Color.white, GameManager.Instance.playerFlowManager.drawingRound.eraseLinePrefab, GameManager.Instance.playerFlowManager.drawingRound.eraseLineMaterial, "Erase");
        changeCurrentDrawingColour("Colour");
        changeCurrentDrawingSize("Big");
    }

    void Update()
    {
        Vector2 temp = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100.0f, drawingLayer);
        bool isInDrawingArea = hit;

        if (startNewLine &&
            Input.GetMouseButtonDown(0) && 
            isInDrawingArea)
        {
            startNewLine = false;
            timeSinceMouseDown = 0.0f;
            CreateForegroundLine();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            startNewLine = true;
            if(timeSinceMouseDown < timeForDotClick &&
                isInDrawingArea)
            {
                CreateDot();
            }
        }
        else if (Input.GetMouseButton(0))
        {
            if (isInDrawingArea)
            {
                if (startNewLine)
                {
                    startNewLine = false;
                    timeSinceMouseDown = 0.0f;
                    CreateForegroundLine();
                }
                else
                {
                    Vector2 tempFingerPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (Vector2.Distance(tempFingerPos, fingerPositions[fingerPositions.Count - 1]) > .05f)
                    {
                        UpdateForegroundLine(tempFingerPos);
                    }
                }
            }
            else
            {
                startNewLine = true;
            }
        }

        if(Input.GetKey(KeyCode.LeftControl) &&
            Input.GetKeyDown(KeyCode.Z))
        {
            undoLastLine();
        }

        timeSinceMouseDown += Time.deltaTime;
    }

    void CreateForegroundLine()
    {
        currentLine = Instantiate(currentDrawingPrefab, Vector3.zero, Quaternion.identity, transform);
        DrawingLine lineData = currentLine.GetComponent<DrawingLine>();
        lineData.lineSize = currentDrawingSizeName;
        lineData.author = SettingsManager.Instance.birdName;
        //Debug.LogError("Current line type["+ currentLineType + "].");
        lineData.lineType = Enum.TryParse(currentLineType, out lineData.lineType) ? lineData.lineType : DrawingLine.LineType.Invalid;

        if(lineData.lineType == DrawingLine.LineType.Invalid)
        {
            Debug.LogError("Invalid line type["+currentLineType+"] set for creating foreground line.");
        }

        lineObjects.Add(currentLine);
        currentLine.transform.position = Vector3.zero;
        lineRenderer = currentLine.GetComponent<LineRenderer>();
        lineRenderer.startWidth = currentDrawingSize;
        lineRenderer.endWidth = currentDrawingSize;
        lineRenderer.material = currentDrawingMaterial;
        lineRenderer.sortingOrder = currentSortingOrder;
        lineData.sortingOrder = currentSortingOrder;
        currentSortingOrder++;

        fingerPositions.Clear();
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        Vector3 temp = new Vector3(fingerPositions[0].x, fingerPositions[0].y, lineZOffset);
        lineRenderer.SetPosition(0, temp);
        temp = new Vector3(fingerPositions[1].x, fingerPositions[1].y, lineZOffset);
        lineRenderer.SetPosition(1, temp);

    }

    private void UpdateForegroundLine(Vector2 newFingerPos)
    {
        if (!lineRenderer)
        {
            CreateForegroundLine();
            return;
        }
        Vector3 temp = new Vector3(newFingerPos.x, newFingerPos.y, lineZOffset);
        fingerPositions.Add(newFingerPos);
        
        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, temp);
    }

    private void CreateDot()
    {
        Vector3 lastPosition = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
        Vector3 temp = new Vector3(lastPosition.x + 0.0075f, lastPosition.y, lineZOffset);
        //fingerPositions.Add(temp);
        //lineRenderer.positionCount++;
        //lineRenderer.SetPosition(lineRenderer.positionCount - 1, temp);
    }

    public void undoLastLine()
    {
        if (lineObjects.Count > 0)
        {
            if(lineObjects[lineObjects.Count - 1] != null)
            {
                Destroy(lineObjects[lineObjects.Count - 1]);
                AudioManager.Instance.PlaySound("EraseLine");
            }

            lineObjects.RemoveAt(lineObjects.Count - 1);
        }

    }

    public void setButtonDrawingColour(string buttonIdentifier, Color drawingColour, GameObject drawingPrefab, Material drawingMaterial, string drawingType)
    {
        if (!drawingButtonMap.ContainsKey(DrawingButtonType.colour))
        {
            Debug.LogError("Colour drawing buttons are not loaded, cannot change colour of button["+buttonIdentifier+"].");
        }
        if (!drawingButtonMap[DrawingButtonType.colour].ContainsKey(buttonIdentifier))
        {
            Debug.LogError("There isn't a colour drawing button with identifier["+buttonIdentifier+"], cannot change the colour.");
        }

        if (!drawingColours.ContainsKey(buttonIdentifier))
        {
            drawingColours.Add(buttonIdentifier, drawingColour);
        }
        else
        {
            drawingColours[buttonIdentifier] = drawingColour;
        }
        if (!drawingPrefabs.ContainsKey(buttonIdentifier))
        {
            drawingPrefabs.Add(buttonIdentifier, drawingPrefab);
        }
        else
        {
            drawingPrefabs[buttonIdentifier] = drawingPrefab;
        }
        if (!drawingMaterials.ContainsKey(buttonIdentifier))
        {
            drawingMaterials.Add(buttonIdentifier, drawingMaterial);
        }
        else
        {
            drawingMaterials[buttonIdentifier] = drawingMaterial;
        }
        if (!drawingTypes.ContainsKey(buttonIdentifier))
        {
            drawingTypes.Add(buttonIdentifier, drawingType);
        }
        else
        {
            drawingTypes[buttonIdentifier] = drawingType;
        }

        drawingButtonMap[DrawingButtonType.colour][buttonIdentifier].buttonImage.color = drawingColour;

    }

    public void setButtonDrawingSize(string buttonIdentifier, float drawingSize, float dotSize, string drawingSizeName)
    {
        if (!drawingButtonMap.ContainsKey(DrawingButtonType.size))
        {
            Debug.LogError("Size drawing buttons are not loaded, cannot change size of button[" + buttonIdentifier + "].");
        }
        if (!drawingButtonMap[DrawingButtonType.size].ContainsKey(buttonIdentifier))
        {
            Debug.LogError("There isn't a size drawing button with identifier[" + buttonIdentifier + "], cannot change the size.");
        }
        if (!drawingSizes.ContainsKey(buttonIdentifier))
        {
            drawingSizes.Add(buttonIdentifier, drawingSize);
        }
        else
        {
            drawingSizes[buttonIdentifier] = drawingSize;
        }
        if (!drawingSizeNames.ContainsKey(buttonIdentifier))
        {
            drawingSizeNames.Add(buttonIdentifier, drawingSizeName);
        }
        else
        {
            drawingSizeNames[buttonIdentifier] = drawingSizeName;
        }

        drawingButtonMap[DrawingButtonType.size][buttonIdentifier].buttonImage.rectTransform.sizeDelta = new Vector2(dotSize, dotSize);
    }

    public void changeCurrentDrawingColour(string buttonIdentifier)
    {
        currentDrawingColour = drawingColours[buttonIdentifier];
        currentDrawingMaterial = drawingMaterials[buttonIdentifier];
        currentDrawingPrefab = drawingPrefabs[buttonIdentifier];
        currentLineType = drawingTypes[buttonIdentifier];

        foreach(KeyValuePair<string,DrawingButton> button in drawingButtonMap[DrawingButtonType.colour])
        {
            button.Value.backgroundImage.color = unselectedButtonColour;
        }

        //Highlight the button?
        drawingButtonMap[DrawingButtonType.colour][buttonIdentifier].backgroundImage.color = Color.yellow;
    }

    public void changeCurrentDrawingSize(string buttonIdentifier)
    {
        currentDrawingSize = drawingSizes[buttonIdentifier];
        currentDrawingSizeName = drawingSizeNames[buttonIdentifier];
        foreach (KeyValuePair<string, DrawingButton> button in drawingButtonMap[DrawingButtonType.size])
        {
            button.Value.backgroundImage.color = unselectedButtonColour;
        }

        //Highlight the button?
        drawingButtonMap[DrawingButtonType.size][buttonIdentifier].backgroundImage.color = Color.yellow;
    }

    public List<DrawingLine> getDrawingLines()
    {
        List<DrawingLine> drawingLines = new List<DrawingLine>();
        DrawingLine currentDrawingLine;
        List<Vector3> points;
        Vector3 pointToAdd;

        for (int i = lineObjects.Count - 1; i >= 0; i--)
        {
            currentDrawingLine = lineObjects[i].GetComponent<DrawingLine>();
            points = new List<Vector3>();

            for (int j = 0; j < lineObjects[i].GetComponent<LineRenderer>().positionCount; j++)
            {
                pointToAdd = lineObjects[i].GetComponent<LineRenderer>().GetPosition(j);
                points.Add(pointToAdd);
            }
            currentDrawingLine.positions = points;
            drawingLines.Add(currentDrawingLine);
        }
        

        return drawingLines;
    }

    public void clearDrawingLines()
    {
        for (int i = lineObjects.Count - 1; i >= 0; i--)
        {
            Destroy(lineObjects[i].gameObject);
        }
        lineObjects.Clear();
    }

    public bool hasDrawingLines()
    {
        return lineObjects.Count > 0;
    }

}
