﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DCManager : MonoBehaviourPunCallbacks
{
    public enum GameScene
    {
        lobby, game
    }

    public GameScene currentGameScene;
    public GameObject DCPrompt;

    public override void OnLeftRoom()
    {
        switch (currentGameScene)
        {
            case GameScene.lobby:
                MenuLobbyButtons.Instance.LeftRoom();
                print("Left room.");
                break;
            case GameScene.game:
                GameNetwork.Instance.Disconnected_ReturnToLobby();
                break;
        }

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        switch (currentGameScene)
        {
            case GameScene.lobby:
                MenuLobbyButtons.Instance.LeftRoom();
                print("Disconnected from room.");
                break;
            case GameScene.game:
                GameNetwork.Instance.Disconnected_ReturnToLobby();
                break;
        }
        
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        switch (currentGameScene)
        {
            case GameScene.lobby:
                MenuLobbyButtons.Instance.PlayerListings.PlayerLeftRoom(otherPlayer);

                if (PhotonNetwork.IsMasterClient)
                {
                    //Update the players
                    string playerJoinUpdateMessage = "PlayerJoinUpdate";
                    foreach (PlayerIdentification playerID in MenuLobbyButtons.Instance.PlayerIdentifiers)
                    {
                        if (playerID.playerName == otherPlayer.NickName)
                        {
                            playerID.playerName = "";
                        }

                        playerJoinUpdateMessage += GameDelim.BASE + playerID.birdName.ToString() + GameDelim.NAME + playerID.playerName;
                    }
                    MenuLobbyButtons.Instance.UpdatePlayerIdentifiers(playerJoinUpdateMessage.Split(new string[] { GameDelim.BASE }, StringSplitOptions.None), 0);

                    LobbyNetwork.Instance.BroadcastQueue.Add(playerJoinUpdateMessage);
                }
                break;
            case GameScene.game:
                GameFlowManager.GamePhase currentPhase = GameManager.Instance.playerFlowManager.currentPhaseName;
                
                if (currentPhase != GameFlowManager.GamePhase.results &&
                        currentPhase != GameFlowManager.GamePhase.reveal)
                {
                    DCPrompt.SetActive(true);
                    GameManager.Instance.playerFlowManager.active = false;
                }

                if (PhotonNetwork.IsMasterClient)
                {
                    if (SettingsManager.Instance.playerNameMap.Where(pn => pn.Value == otherPlayer.NickName).Count() == 1)
                    {
                        SettingsManager.Instance.playerNameMap.Remove(SettingsManager.Instance.playerNameMap.Single(pn => pn.Value == otherPlayer.NickName).Key);
                    }

                    if(currentPhase != GameFlowManager.GamePhase.results &&
                        currentPhase != GameFlowManager.GamePhase.reveal)
                    {
                        GameManager.Instance.gameFlowManager.active = false;
                    }
                    
                }
                break;
        }

    }

}
