﻿using Photon.Pun;
using Photon.Realtime;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;
using static PlayerDrawingInputData;

public class PlayerFlowManager : MonoBehaviour
{
    public InstructionsRound instructionRound;
    public DrawingRound drawingRound;
    public ResponsesRound responsesRound;
    public SlidesRound slidesRound;
    public AccoladesRound accoladesRound;
    public AccusationsRound accusationsRound;
    public RevealRound revealRound;
    public ResultsRound resultsRound;

    public Text timeRemainingText;
    public float currentTimeInRound;
    public GameFlowManager.GamePhase currentPhaseName;
    public MusicButton musicButton;
    public bool active = true;
    public int numberOfCabinetRounds;

    public Transform drawingsContainer;
    public Dictionary<BirdName, string> playerNameMap = new Dictionary<BirdName, string>();

    public bool serverIsReady = false, analyticsAvailable = false;
    public GameFlowManager.PlayerRole playerRole;

    private Dictionary<BirdName, BirdArm> birdArmMap = new Dictionary<BirdName, BirdArm>();
    private bool hasWarnedOnTime = false;
    public bool hasRunOutOfTime = false;
    public Dictionary<string, DrawingLinesPackage> drawingLinePackageQueue = new Dictionary<string, DrawingLinesPackage>();
    private bool isInitialized = false;

    private void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            GameNetwork.Instance.ToServerQueue.Add("player_connected");
        }

        if (!isInitialized)
        {
            initialize();
        }
    }

    private void initialize()
    {
        if (!serverIsReady)
        {
            return;
        }

        active = true;

        instructionRound.deskPlayerImage.sprite = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].faceSprite;
        drawingRound.playerNameText.text = PhotonNetwork.NickName;
        
        Texture2D selectedCursor = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].cursor;
        Cursor.SetCursor(selectedCursor, new Vector2(10, 80), CursorMode.Auto);

        if (!SettingsManager.Instance.GetSetting("music", true))
        {
            AudioManager.Instance.SetMusic(false);
            musicButton.musicButtonStatusImage.sprite = musicButton.offSprite;
        }
        

        drawingLinePackageQueue = new Dictionary<string, DrawingLinesPackage>();
        isInitialized = true;
    }


    private void Update()
    {
        if (!isInitialized)
        {
            initialize();
        }
        if (!active)
        {
            return;
        }
        currentTimeInRound -= Time.deltaTime;
        var ts = TimeSpan.FromSeconds(currentTimeInRound);

        if (currentTimeInRound < 10.0f &&
            !hasWarnedOnTime &&
            (currentPhaseName != GameFlowManager.GamePhase.loading 
            && currentPhaseName != GameFlowManager.GamePhase.instructions 
            && currentPhaseName != GameFlowManager.GamePhase.accolades
            && currentPhaseName != GameFlowManager.GamePhase.slides
            && currentPhaseName != GameFlowManager.GamePhase.reveal))
        {
            hasWarnedOnTime = true;
            if(currentTimeInRound > 0.1f)
            {
                AudioManager.Instance.PlaySound("TimeTick");
            }
            
        }
        else if (currentTimeInRound < 0.0f &&
            (currentPhaseName != GameFlowManager.GamePhase.loading
            && currentPhaseName != GameFlowManager.GamePhase.instructions))
        {
            if (!hasRunOutOfTime)
            {
                hasRunOutOfTime = true;
                timeRemainingText.text = "00:00";
                if (PhotonNetwork.IsMasterClient)
                {
                    if (currentPhaseName == GameFlowManager.GamePhase.drawing)
                    {
                        //Send notification to players that aren't done yet to force them to submit what they have so far
                        foreach (BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
                        {
                            if (!drawingRound.isPlayerReady(player))
                            {
                                drawingRound.forcePlayerToSubmit(player);
                            }
                        }
                    }
                    else if(currentPhaseName == GameFlowManager.GamePhase.responses)
                    {
                        //Send notification to players that aren't done yet to force them to submit what they have so far
                        foreach (BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
                        {
                            int activeCabinetID = -1;
                            int currentCabinetRoundIndex = drawingRound.currentPlayerCabinetRoundIndex;

                            foreach (KeyValuePair<int, CabinetDrawer> drawer in drawingRound.cabinetDrawerMap)
                            {
                                if (drawer.Value.chainData.playerOrder.ContainsKey(currentCabinetRoundIndex) &&
                                    drawer.Value.chainData.playerOrder[currentCabinetRoundIndex] == player)
                                {
                                    activeCabinetID = drawer.Key;
                                }
                            }
                            if (activeCabinetID == -1)
                            {
                                Debug.LogError("Could not find a corresponding cabinet for the player[" + player.ToString() + "] when attempting to force submit response.");
                                return;
                            }

                            if (SettingsManager.Instance.birdName == player)
                            {
                                GameManager.Instance.playerFlowManager.responsesRound.forceSubmit();
                            }
                            else
                            {
                                GameNetwork.Instance.ToPlayerQueue.Add(player.ToString() + GameDelim.BASE + "force_submit_response" + GameDelim.BASE + activeCabinetID);
                            }

                        }
                    }
                }
                
            }
            
            return;
        }
        
        timeRemainingText.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
    }

    public void UpdatePhase(GameFlowManager.GamePhase inPhaseName)
    {
        currentPhaseName = inPhaseName;
        hasWarnedOnTime = false;
        hasRunOutOfTime = false;

        switch (inPhaseName)
        {
            case GameFlowManager.GamePhase.instructions:
                instructionRound.StartRound();
                break;
            case GameFlowManager.GamePhase.drawing_tutorial:
                GameManager.Instance.gameFlowManager.drawingTutorialSequence.startSlides();
                break;
            case GameFlowManager.GamePhase.drawing:
                drawingRound.StartRound();
                break;
            case GameFlowManager.GamePhase.responses:
                responsesRound.StartRound();
                break;
            case GameFlowManager.GamePhase.slides_tutorial:
                GameManager.Instance.gameFlowManager.slidesTutorialSequence.startSlides();
                break;
            case GameFlowManager.GamePhase.accolades:
                accoladesRound.StartRound();
                break;
            case GameFlowManager.GamePhase.accusation_tutorial:
                GameManager.Instance.gameFlowManager.accusationTutorialSequence.startSlides();
                break;
            case GameFlowManager.GamePhase.accusation:
                Cursor.visible = false;
                GameManager.Instance.cursorHider.activated = false;
                accusationsRound.StartRound();

                break;
            case GameFlowManager.GamePhase.slides:
                Cursor.visible = true;
                GameManager.Instance.cursorHider.activated = false;
                slidesRound.StartRound();
                break;
            case GameFlowManager.GamePhase.reveal:
                Cursor.visible = true;
                GameManager.Instance.cursorHider.activated = false;
                revealRound.StartRound();
                break;
            case GameFlowManager.GamePhase.results:
                Cursor.visible = true;
                GameManager.Instance.cursorHider.activated = false;
                resultsRound.StartRound();
               
                break;
            default:
                return;
        }
    }

    public void initializeDrawingLinePackage(BirdName sendingBirdName, BirdName author, DrawingType inDrawingType, int inCabinetID, int inCabinetTab, Coverup inCoverup, string inPackageID, int inNumMessages, string readyMessage, float inTimeTaken)
    {
        DrawingLinesPackage newPackage = new DrawingLinesPackage()
        {
            author = author,
            cabinetID = inCabinetID,
            cabinetTab = inCabinetTab,
            expectedNumberOfMessages = inNumMessages,
            messages = new Dictionary<int, string>(),
            packageID = inPackageID,
            timeTaken = inTimeTaken,
            drawingType = inDrawingType,
            coverup = inCoverup
        };

        if (drawingLinePackageQueue.ContainsKey(inPackageID))
        {
            return;
        }
        drawingLinePackageQueue.Add(inPackageID, newPackage);

        string responseMessage;
        if (PhotonNetwork.IsMasterClient)
        {
            responseMessage = sendingBirdName.ToString() + GameDelim.BASE + readyMessage + GameDelim.BASE + inPackageID;
            GameNetwork.Instance.ToPlayerQueue.Add(responseMessage);
        }
        else
        {
            responseMessage = readyMessage + GameDelim.BASE + SettingsManager.Instance.birdName.ToString() + GameDelim.BASE + inPackageID;
            GameNetwork.Instance.ToServerQueue.Add(responseMessage);
        }
    }

    public void initializeEvaluationLinePackage(BirdName recipient, BirdName author, int inCabinetID, int inEvaluationRound, string inPackageID, int inNumMessages, string inReadyMessage, float inTimeTaken)
    {
        DrawingLinesPackage newPackage = new DrawingLinesPackage()
        {
            author = author,
            cabinetID = inCabinetID,
            cabinetTab = inEvaluationRound,
            expectedNumberOfMessages = inNumMessages,
            messages = new Dictionary<int, string>(),
            packageID = inPackageID,
            timeTaken = inTimeTaken
        };

        if (drawingLinePackageQueue.ContainsKey(inPackageID))
        {
            return;
        }
        drawingLinePackageQueue.Add(inPackageID, newPackage);

        string responseMessage;
        if (PhotonNetwork.IsMasterClient)
        {
            responseMessage = author.ToString() + GameDelim.BASE + inReadyMessage + GameDelim.BASE + inPackageID;
            GameNetwork.Instance.ToPlayerQueue.Add(responseMessage);
        }
        else
        {
            responseMessage = inReadyMessage + GameDelim.BASE + SettingsManager.Instance.birdName.ToString() + GameDelim.BASE + inPackageID;
            GameNetwork.Instance.ToServerQueue.Add(responseMessage);
        }
    }

    public void handleQueuedDrawingLineMessage(string packageID, int lineMessageID, string message)
    {
        string transitionCondition = "";
        if (!drawingLinePackageQueue.ContainsKey(packageID))
        {
            Debug.LogError("Failed to add the queued line message, couldn't find the corresponding packageID:" + packageID);
            return;
        }
        DrawingLinesPackage currentPackage = drawingLinePackageQueue[packageID];

        if (currentPackage.messages.ContainsKey(lineMessageID))
        {
            Debug.LogError("Failed to add the queued line message, the line messageID [" + lineMessageID.ToString() + "] already exists for package ID[" + packageID + "]");
            return;
        }

        currentPackage.messages.Add(lineMessageID, message);
        if (currentPackage.messages.Count == currentPackage.expectedNumberOfMessages)
        {
            //Open the cabinet
            PlayerDrawingInputData currentDrawData;

            if (PhotonNetwork.IsMasterClient)
            {
                switch (currentPackage.drawingType)
                {
                    case DrawingType.cabinet:
                        addDrawingLineData(currentPackage);
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab];
                        currentDrawData.timeTaken = currentPackage.timeTaken;

                        //Create drawing on drawing queue
                        drawingRound.setCabinetDrawingContents(currentPackage.cabinetID, currentPackage.cabinetTab, currentDrawData);

                        BirdName nextInQueue = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.playerOrder[currentPackage.cabinetTab + 1];

                        if (nextInQueue != SettingsManager.Instance.birdName)
                        {
                            transitionCondition = "drawing_receipt:" + nextInQueue;

                            //Broadcast to next in queue
                            drawingRound.prepareLinesForClient(currentDrawData, currentPackage.cabinetID, currentPackage.cabinetTab, nextInQueue, PlayerDrawingInputData.DrawingType.cabinet);
                            GameManager.Instance.gameFlowManager.addTransitionCondition(transitionCondition);

                        }

                        //Send drawing lines to botcher
                        foreach (PlayerData player in GameManager.Instance.gameFlowManager.gamePlayers.Values)
                        {
                            if (player.playerRole == GameFlowManager.PlayerRole.botcher)
                            {
                                if (SettingsManager.Instance.birdName == player.birdName)
                                {
                                    continue;
                                }

                                //Add condition for the receipt of the drawing we're sending to the botcher, we cannot proceed until they have a copy of the drawing 
                                GameManager.Instance.gameFlowManager.addTransitionCondition("botcher_drawing_receipt:" + player.birdName + "-" + currentDrawData.author);
                                drawingRound.prepareLinesForClient(currentDrawData, currentPackage.cabinetID, currentPackage.cabinetTab, player.birdName, PlayerDrawingInputData.DrawingType.to_botcher);
                            }
                        }
                        

                        transitionCondition = "drawing_submitted:" + currentDrawData.author;
                        GameManager.Instance.gameFlowManager.resolveTransitionCondition(transitionCondition);
                        break;
                    case DrawingType.evaluation:
                        addEvaluationDrawingLineData(currentPackage, currentPackage.cabinetTab);
                        currentDrawData = drawingRound.evaluationsMap[currentPackage.cabinetTab].drawings[currentPackage.cabinetID];
                        currentDrawData.timeTaken = currentPackage.timeTaken;

                        foreach (BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
                        {
                            if (player == SettingsManager.Instance.birdName)
                            {
                                continue;
                            }

                            GameManager.Instance.gameFlowManager.addTransitionCondition("evaluation_drawing_receipt:" + player + "-" + currentDrawData.author);
                            drawingRound.prepareLinesForClient(currentDrawData, currentPackage.cabinetID, currentPackage.cabinetTab, player, DrawingType.evaluation);
                        }
                        transitionCondition = "evaluation_drawing_submitted:" + currentDrawData.author;
                        GameManager.Instance.gameFlowManager.resolveTransitionCondition(transitionCondition);
                        break;
                    case DrawingType.botcher_update:
                        addDrawingLineData(currentPackage);
                        
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab];
                        nextInQueue = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.playerOrder[currentPackage.cabinetTab + 1];
                        drawingRound.setCabinetDrawingContents(currentPackage.cabinetID, currentPackage.cabinetTab, currentDrawData);

                        if (nextInQueue != SettingsManager.Instance.birdName)
                        {
                            transitionCondition = "botcher_update_receipt:" + nextInQueue;

                            //Broadcast to next in queue
                            drawingRound.prepareLinesForClient(currentDrawData, currentPackage.cabinetID, currentPackage.cabinetTab, nextInQueue, PlayerDrawingInputData.DrawingType.botcher_update);
                            GameManager.Instance.gameFlowManager.addTransitionCondition(transitionCondition);

                        }
                        break;
                    default:
                        return;
                }

                
            }
            else
            {
                switch (currentPackage.drawingType)
                {
                    case DrawingType.cabinet:

                        addDrawingLineData(currentPackage);
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab]; ;
                        currentDrawData.timeTaken = currentPackage.timeTaken;
                        drawingRound.setCabinetDrawingContents(currentPackage.cabinetID, currentPackage.cabinetTab, currentDrawData);
                        transitionCondition = "drawing_receipt:" + SettingsManager.Instance.birdName;
                        break;
                    case DrawingType.evaluation:
                        addEvaluationDrawingLineData(currentPackage, currentPackage.cabinetTab);
                        currentDrawData = drawingRound.evaluationsMap[currentPackage.cabinetTab].drawings[currentPackage.cabinetID];
                        currentDrawData.timeTaken = currentPackage.timeTaken;
                        transitionCondition = "evaluation_drawing_receipt:" + SettingsManager.Instance.birdName + "-" + currentDrawData.author;
                        break;
                    case DrawingType.to_botcher:
                        addDrawingLineData(currentPackage);
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab]; ;
                        transitionCondition = "botcher_drawing_receipt:" + SettingsManager.Instance.birdName + "-" + currentDrawData.author;
                        break;
                    case DrawingType.botcher_update:
                        addDrawingLineData(currentPackage);
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab];
                        drawingRound.setCabinetDrawingContents(currentPackage.cabinetID, currentPackage.cabinetTab, currentDrawData);
                        transitionCondition = "botcher_update_receipt:" + SettingsManager.Instance.birdName;
                        break;
                    case DrawingType.chain:
                        addDrawingLineData(currentPackage);
                        currentDrawData = drawingRound.cabinetDrawerMap[currentPackage.cabinetID].chainData.drawings[currentPackage.cabinetTab]; ;
                        currentDrawData.timeTaken = currentPackage.timeTaken;

                        //Check if all of the contents of all chains have been loaded
                        if (chainsAreAllLoaded())
                        {
                            transitionCondition = "all_chains_loaded:" + SettingsManager.Instance.birdName.ToString();
                        }
                        else
                        {
                            return;
                        }

                        break;
                    default:
                        return;
                }
                //Send confirmation back to server
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + transitionCondition);
            }
        }
    }

    public bool chainsAreAllLoaded()
    {
        foreach(KeyValuePair<int,CabinetDrawer> drawer in drawingRound.cabinetDrawerMap)
        {
            if (!drawer.Value.isFullyLoaded())
            {
                return false;
            }
        }
        return true;

    }

    public void setChainDetails(int cabinetID, string correctPrefix, string correctNoun, string prefixGuess, string nounGuess, int numberOfPrompts, int numberOfDrawings, BirdName guesser)
    {
        ChainData chain;
        if (!drawingRound.cabinetDrawerMap.ContainsKey(cabinetID))
        {
            Debug.LogError("Could not set chain details, cabinet["+cabinetID.ToString()+"] does not exist.");
        }
        chain = drawingRound.cabinetDrawerMap[cabinetID].chainData;

        chain.correctPrefix = correctPrefix;
        chain.correctNoun = correctNoun;

        chain.prefixGuess = prefixGuess;
        chain.nounGuess = nounGuess;

        chain.promptsExpected = numberOfPrompts;
        chain.drawingsExpected = numberOfDrawings;

        chain.guesser = guesser;

        if (chainsAreAllLoaded())
        {
            GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "all_chains_loaded:" + SettingsManager.Instance.birdName.ToString());
        }
    }

    public void setChainPrompt(int cabinetID, int round, BirdName author, string prefix, string noun, float timeTaken)
    {
        ChainData chain;
        if (!drawingRound.cabinetDrawerMap.ContainsKey(cabinetID))
        {
            Debug.LogError("Could not set chain prompt, cabinet[" + cabinetID.ToString() + "] does not exist.");
        }
        chain = drawingRound.cabinetDrawerMap[cabinetID].chainData;

        if (!chain.prompts.ContainsKey(round))
        {
            chain.prompts.Add(round, new PlayerTextInputData());
        }

        chain.prompts[round].prefix = prefix;
        chain.prompts[round].noun = noun;

        chain.prompts[round].author = author;
        chain.prompts[round].timeTaken = timeTaken;

        if (chainsAreAllLoaded())
        {
            GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "all_chains_loaded:" + SettingsManager.Instance.birdName.ToString());
        }
    }

    public void addDrawingLineData(DrawingLinesPackage drawingLinesPackage)
    {
        if (!drawingRound.cabinetDrawerMap.ContainsKey(drawingLinesPackage.cabinetID))
        {
            Debug.LogError("Cabinet[" + drawingLinesPackage.cabinetID + "] does not exist.");
        }
        ChainData selectedCabinet = drawingRound.cabinetDrawerMap[drawingLinesPackage.cabinetID].chainData;
        PlayerDrawingInputData newDrawing;

        if (selectedCabinet.drawings.ContainsKey(drawingLinesPackage.cabinetTab))
        {
            newDrawing = selectedCabinet.drawings[drawingLinesPackage.cabinetTab];
            newDrawing.author = drawingLinesPackage.author;
        }
        else
        {
            newDrawing = new PlayerDrawingInputData();
            newDrawing.author = drawingLinesPackage.author;
            selectedCabinet.addDrawing(drawingLinesPackage.cabinetTab, newDrawing);
        }

        DrawingLine newLine;
        List<Vector3> allPoints = new List<Vector3>();
        string currentMessage;
        string[] lines;
        string[] points;
        string[] pointSegments;
        float x = 0, y = 0, z = 0;
        Vector3 currentPoint;
        for (int i = 0; i < drawingLinesPackage.messages.Count; i++)
        {
            if (!drawingLinesPackage.messages.ContainsKey(i))
            {
                Debug.LogError("Missing messageid[" + i.ToString() + "] from queued drawing package[" + drawingLinesPackage.packageID + "]");
            }
            currentMessage = drawingLinesPackage.messages[i];


            if (currentMessage.Contains(GameDelim.CONT))
            {
                currentMessage = currentMessage.Replace(GameDelim.CONT, "");
            }
            else
            {
                allPoints = new List<Vector3>();
            }

            //Iterate over all lines in message
            lines = currentMessage.Split(new string[] { GameDelim.LINE }, StringSplitOptions.None);
            foreach (string line in lines)
            {
                newLine = new DrawingLine();
                allPoints = new List<Vector3>();
                string[] lineDetails = line.Split(new string[] { GameDelim.SUBLINE }, StringSplitOptions.None);
                if(lineDetails.Length != 4)
                {
                    Debug.LogError("Invalid number of subline segments in current line segment[" + lineDetails.Length + "]. Cannot create line.");
                    Debug.LogError(line);
                    return;
                }

                newLine.author = newDrawing.author;
                newLine.lineType = Enum.TryParse(lineDetails[0], out newLine.lineType) ? newLine.lineType : DrawingLine.LineType.Invalid;
                if(newLine.lineType == DrawingLine.LineType.Invalid)
                {
                    Debug.LogError("Invalid drawing line type["+newLine.lineType+"] set for adding drawing line data.");
                    return;
                }
                newLine.lineSize = lineDetails[1];
                newLine.sortingOrder = int.Parse(lineDetails[2]);
                points = lineDetails[3].Split(new string[] { GameDelim.POINT }, StringSplitOptions.None);

                for (int j = 0; j < points.Length; j++)
                {
                    pointSegments = points[j].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
                    if (pointSegments.Length != 3 ||
                        !float.TryParse(pointSegments[0], out x) ||
                        !float.TryParse(pointSegments[1], out y) ||
                        !float.TryParse(pointSegments[2], out z))
                    {
                        Debug.LogError("Point["+ points.ToList().IndexOf(points[j]) +"] was in invalid format, line["+lines.ToList().IndexOf(line)+"] contents - " + line);
                    }
                    else
                    {
                        currentPoint = new Vector3(float.Parse(pointSegments[0], CultureInfo.InvariantCulture), float.Parse(pointSegments[1], CultureInfo.InvariantCulture), float.Parse(pointSegments[2], CultureInfo.InvariantCulture));
                        allPoints.Add(currentPoint);
                    }
                }
                newLine.positions = allPoints;
                newDrawing.lines.Add(newLine);
            }
        }
    }

    public void addEvaluationDrawingLineData(DrawingLinesPackage drawingLinesPackage, int evaluationRound)
    {
        if (!drawingRound.cabinetDrawerMap.ContainsKey(drawingLinesPackage.cabinetID))
        {
            Debug.LogError("Cabinet[" + drawingLinesPackage.cabinetID + "] does not exist.");
        }
        
        PlayerDrawingInputData newDrawing;

        if (!drawingRound.evaluationsMap.ContainsKey(evaluationRound))
        {
            drawingRound.evaluationsMap.Add(evaluationRound, new EvaluationData());
        }
        if (!drawingRound.evaluationsMap[evaluationRound].drawings.ContainsKey(drawingLinesPackage.cabinetID))
        {
            drawingRound.evaluationsMap[evaluationRound].drawings.Add(drawingLinesPackage.cabinetID, new PlayerDrawingInputData());
        }
        
        newDrawing = drawingRound.evaluationsMap[evaluationRound].drawings[drawingLinesPackage.cabinetID];
        newDrawing.author = drawingLinesPackage.author;

        DrawingLine newLine;
        List<Vector3> allPoints = new List<Vector3>();
        string currentMessage;
        string[] lines;
        string[] points;
        string[] pointSegments;
        float x = 0, y = 0, z = 0;
        Vector3 currentPoint;
        for (int i = 0; i < drawingLinesPackage.messages.Count; i++)
        {
            if (!drawingLinesPackage.messages.ContainsKey(i))
            {
                Debug.LogError("Missing messageid[" + i.ToString() + "] from queued drawing package[" + drawingLinesPackage.packageID + "]");
            }
            currentMessage = drawingLinesPackage.messages[i];


            if (currentMessage.Contains(GameDelim.CONT))
            {
                currentMessage = currentMessage.Replace(GameDelim.CONT, "");
            }
            else
            {
                allPoints = new List<Vector3>();
            }

            //Iterate over all lines in message
            lines = currentMessage.Split(new string[] { GameDelim.LINE }, StringSplitOptions.None);
            foreach (string line in lines)
            {
                newLine = new DrawingLine();
                allPoints = new List<Vector3>();
                string[] lineDetails = line.Split(new string[] { GameDelim.SUBLINE }, StringSplitOptions.None);
                if (lineDetails.Length != 4)
                {
                    Debug.LogError("Invalid number of subline segments in current line segment[" + lineDetails.Length + "]. Cannot create line.");
                    Debug.LogError(line);
                    return;
                }

                newLine.author = newDrawing.author;
                newLine.lineType = Enum.TryParse(lineDetails[0], out newLine.lineType) ? newLine.lineType : DrawingLine.LineType.Invalid;
                if (newLine.lineType == DrawingLine.LineType.Invalid)
                {
                    Debug.LogError("Invalid drawing line type[" + newLine.lineType + "] set for adding evaluation line data.");
                    return;
                }
                newLine.lineSize = lineDetails[1];
                newLine.sortingOrder = int.Parse(lineDetails[2]);
                points = lineDetails[3].Split(new string[] { GameDelim.POINT }, StringSplitOptions.None);

                for (int j = 0; j < points.Length; j++)
                {
                    pointSegments = points[j].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
                    if (pointSegments.Length != 3 ||
                        !float.TryParse(pointSegments[0], out x) ||
                        !float.TryParse(pointSegments[1], out y) ||
                        !float.TryParse(pointSegments[2], out z))
                    {
                        Debug.LogError("Point[" + points.ToList().IndexOf(points[j]) + "] was in invalid format, line[" + lines.ToList().IndexOf(line) + "] contents - " + line);
                    }
                    else
                    {
                        currentPoint = new Vector3(float.Parse(pointSegments[0], CultureInfo.InvariantCulture), float.Parse(pointSegments[1], CultureInfo.InvariantCulture), float.Parse(pointSegments[2], CultureInfo.InvariantCulture));
                        allPoints.Add(currentPoint);
                    }
                }
                newLine.positions = allPoints;
                newDrawing.lines.Add(newLine);
            }
        }
    }

    public void addToRating(int cabinetID, int tab, string ratingType)
    {
        
        ChainData selectedCabinetData = drawingRound.cabinetDrawerMap[cabinetID].chainData;
        bool isGuesser = false;
        if(tab == -1)
        {
            isGuesser = true;
            tab = numberOfCabinetRounds + 1;
        }
        if (!selectedCabinetData.ratings.ContainsKey(tab))
        {
            selectedCabinetData.ratings.Add(tab, new PlayerRatingData());
        }
        if (!selectedCabinetData.playerOrder.ContainsKey(tab))
        {
            Debug.LogError("Cannot add rating["+ratingType+"] for tab["+tab+"] in cabinet["+cabinetID+"] because player order is missing.");
        }
        if(ratingType == "like")
        {
            selectedCabinetData.ratings[tab].likeCount++;
            selectedCabinetData.ratings[tab].target = isGuesser ? selectedCabinetData.guesser : selectedCabinetData.playerOrder[tab];

            slidesRound.showLikedObject(cabinetID, tab);
            GameNetwork.Instance.BroadcastQueue.Add("slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "like");
        }
        else if(ratingType == "dislike")
        {
            selectedCabinetData.ratings[tab].dislikeCount++;
            selectedCabinetData.ratings[tab].target = isGuesser ? selectedCabinetData.guesser : selectedCabinetData.playerOrder[tab];

            slidesRound.showDislikedObject(cabinetID, tab);
            GameNetwork.Instance.BroadcastQueue.Add("slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "dislike");
        }
    }

    public void addToEvaluationRating(int cabinetID, int round, string ratingType)
    {

        EvaluationData selectedEvaluationData = drawingRound.evaluationsMap[round];

        if (!selectedEvaluationData.ratings.ContainsKey(cabinetID))
        {
            selectedEvaluationData.ratings.Add(cabinetID, new PlayerRatingData());
        }

        if (ratingType == "like")
        {
            selectedEvaluationData.ratings[cabinetID].likeCount++;
            selectedEvaluationData.ratings[cabinetID].target = selectedEvaluationData.drawings[cabinetID].author;

            slidesRound.showEvaluationLikedObject(cabinetID, round);
            GameNetwork.Instance.BroadcastQueue.Add("evaluation_slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + "like");
        }
        else if (ratingType == "dislike")
        {
            selectedEvaluationData.ratings[cabinetID].dislikeCount++;
            selectedEvaluationData.ratings[cabinetID].target = selectedEvaluationData.drawings[cabinetID].author;

            slidesRound.showEvaluationDislikedObject(cabinetID, round);
            GameNetwork.Instance.BroadcastQueue.Add("evaluation_slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + "dislike");
        }
    }



    public void createDrawingLines(PlayerDrawingInputData drawingData, Transform drawingParent, Vector3 position, Vector3 scale, float largeThickness, float mediumThickness, float smallThickness, Canvas canvas)
    {
        Material drawingMaterial;
        GameObject drawingPrefab;
        float lineThickness;
       
        foreach (DrawingLine line in drawingData.lines)
        {
            if (line.lineType == DrawingLine.LineType.Base)
            {
                drawingMaterial = drawingRound.baseLineMaterial;
                drawingPrefab = drawingRound.baseLinePrefab;
            }
            else if(line.lineType == DrawingLine.LineType.Colour)
            {
                drawingMaterial = ColourManager.Instance.birdMap[line.author].material;
                drawingPrefab = drawingRound.colourLinePrefab;
            }
            else if(line.lineType == DrawingLine.LineType.Light)
            {
                drawingMaterial = ColourManager.Instance.birdMap[line.author].bgLineMaterial;
                drawingPrefab = drawingRound.colourLinePrefab;
            }
            else if(line.lineType == DrawingLine.LineType.Erase)
            {
                drawingMaterial = drawingRound.eraseLineMaterial;
                drawingPrefab = drawingRound.eraseLinePrefab;
            }
            else
            {
                Debug.LogError("Invalid line type["+line.lineType+"] provided for line. Cannot create drawing.");
                return;
            }

            if(line.lineSize == "Big")
            {
                lineThickness = largeThickness;
            }
            else if(line.lineSize == "Medium")
            {
                lineThickness = mediumThickness;
            }
            else if(line.lineSize == "Small")
            {
                lineThickness = smallThickness;
            }
            else
            {
                Debug.LogError("Invalid line size["+line.lineSize+"] provided for line. Cannot create drawing.");
                return;
            }


            GameObject newLine = Instantiate(drawingPrefab, position, Quaternion.identity, drawingParent);
            LineRenderer newLineRenderer = newLine.GetComponent<LineRenderer>();

            newLineRenderer.material = drawingMaterial;
            newLineRenderer.positionCount = line.positions.Count;

            newLineRenderer.SetPositions(line.positions.ToArray());
            newLine.transform.localScale = scale;
            newLineRenderer.startWidth = lineThickness;
            newLineRenderer.endWidth = lineThickness;
            newLineRenderer.sortingOrder = line.sortingOrder;
        }

        if(drawingData.coverup != Coverup.none && drawingData.coverup != Coverup.invalid)
        {
            if(canvas == null)
            {
                Debug.LogError("Canvas is null, cannot show coverup object.");
            }
            //first you need the RectTransform component of your canvas
            RectTransform CanvasRect = canvas.GetComponent<RectTransform>();

            //then you calculate the position of the UI element
            //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

            Vector2 viewportPosition;
            Vector2 screenPosition;

            switch (drawingData.coverup)
            {
                case Coverup.bottom_left:
                    viewportPosition = Camera.main.WorldToViewportPoint(drawingParent.position + new Vector3(0,-drawingRound.botchingForm.blockerSize,0));
                    break;
                case Coverup.bottom_right:
                    viewportPosition = Camera.main.WorldToViewportPoint(drawingParent.position + new Vector3(drawingRound.botchingForm.blockerSize, -drawingRound.botchingForm.blockerSize, 0));
                    break;
                case Coverup.top_left:
                    viewportPosition = Camera.main.WorldToViewportPoint(drawingParent.position);
                    break;
                case Coverup.top_right:
                    viewportPosition = Camera.main.WorldToViewportPoint(drawingParent.position + new Vector3(drawingRound.botchingForm.blockerSize, 0, 0));
                    break;
                default:
                    return;
            }
            screenPosition = new Vector2(
                ((viewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
                ((viewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));
            Instantiate(drawingRound.botchingForm.blockerPrefab, screenPosition, Quaternion.identity, drawingParent);
        }

    }

    public void addGuessPrompt(BirdName inBirdName, string prefix, string noun, int cabinetID)
    {
        ChainData chain = drawingRound.cabinetDrawerMap[cabinetID].chainData;
        chain.prefixGuess = prefix;
        chain.nounGuess = noun;
        chain.guesser = inBirdName;

        if (PhotonNetwork.IsMasterClient)
        {
            //Send notification to players letting them know what contents to expect for the chain
            string currentMessage = "chain_details" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + chain.correctPrefix + GameDelim.BASE + chain.correctNoun + GameDelim.BASE + chain.prefixGuess + GameDelim.BASE + chain.nounGuess +
                GameDelim.BASE + ((numberOfCabinetRounds) / 2).ToString() + GameDelim.BASE + (((numberOfCabinetRounds - 1) / 2) + 1).ToString() +GameDelim.BASE + chain.guesser.ToString();
            GameNetwork.Instance.BroadcastQueue.Add(currentMessage);

            //Send all of the prompts
            foreach(KeyValuePair<int,PlayerTextInputData> prompt in chain.prompts)
            {
                if(prompt.Key < 2)
                {
                    continue;
                }
                GameNetwork.Instance.BroadcastQueue.Add("chain_prompt" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + prompt.Key.ToString() + GameDelim.BASE + prompt.Value.author.ToString() + GameDelim.BASE + prompt.Value.prefix + GameDelim.BASE + prompt.Value.noun + GameDelim.BASE + prompt.Value.timeTaken);
            }

            //Send notifications for chain drawings
            foreach(KeyValuePair<int,PlayerDrawingInputData> drawing in chain.drawings)
            {
                foreach(BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
                {
                    if(player != SettingsManager.Instance.birdName)
                    {
                        if(drawing.Value.lines.Count == 0)
                        {
                            drawingRound.sendEmptyDrawingToClient(cabinetID, drawing.Key, player, DrawingType.chain, drawing.Value.author);
                        }
                        else
                        {
                            drawingRound.prepareLinesForClient(drawing.Value, cabinetID, drawing.Key, player, PlayerDrawingInputData.DrawingType.chain);
                        }
                    }
                }
            }

        }
    }

    public void setChainDrawingAsEmpty(int cabinetID, int round, BirdName author)
    {
        if (!drawingRound.cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(round))
        {
            drawingRound.cabinetDrawerMap[cabinetID].chainData.addDrawing(round, new PlayerDrawingInputData() { author = author });
        }
        drawingRound.drawingContainer.addDrawingHolder(cabinetID, round);
        GameNetwork.Instance.ToServerQueue.Add("empty_drawing_confirmation" + GameDelim.BASE + SettingsManager.Instance.birdName.ToString());
    }

    public void addCabinetTimeTaken(int cabinetID, int tab, float timeTaken)
    {
        ChainData selectedCabinetData = drawingRound.cabinetDrawerMap[cabinetID].chainData;
        
        if(tab % 2 == 0)
        {
            if (selectedCabinetData.prompts.ContainsKey(tab))
            {
                selectedCabinetData.prompts[tab].timeTaken = timeTaken;
            }
        }
        else
        {
            if (selectedCabinetData.drawings.ContainsKey(tab))
            {
                selectedCabinetData.drawings[tab].timeTaken = timeTaken;
            }
            else
            {
                PlayerDrawingInputData newDrawing = new PlayerDrawingInputData();
                newDrawing.timeTaken = timeTaken;
                selectedCabinetData.addDrawing(tab, newDrawing);
            }
        }
    }

    public void assignRole(GameFlowManager.PlayerRole inPlayerRole)
    {
        playerRole = inPlayerRole;

        if (playerRole == GameFlowManager.PlayerRole.botcher)
        {
            instructionRound.alignmentText.color = GameManager.Instance.traitorColour;
            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                //instructionRound.traitorPromptsDeskSticky.Place(false);
                instructionRound.roleDeskSticky.Place(false);
                instructionRound.removeDeskSticky.Place(true);
            }
            instructionRound.vultureNoteAnimator.SetBool("Slide", true);
            instructionRound.bossNoteBotcherAnimator.SetBool("Slide", true);
        }
        else
        {
            instructionRound.alignmentText.color = GameManager.Instance.workerColour;
            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                instructionRound.roleDeskSticky.Place(true);
                instructionRound.removeDeskSticky.Place(true);
            }
            instructionRound.bossNoteWorkerAnimator.SetBool("Slide", true);
        }
        instructionRound.active = true;
        instructionRound.alignmentText.text = inPlayerRole.ToString().ToUpper();
        instructionRound.lanyardObject.SetActive(true);
    }

    public void setDrawingPackageExpectedMessageNumber(string inPackageID, int inExpectedNumberOfMessages)
    {
        if (!drawingLinePackageQueue.ContainsKey(inPackageID))
        {
            drawingLinePackageQueue.Add(inPackageID, new DrawingLinesPackage() { expectedNumberOfMessages = inExpectedNumberOfMessages, packageID = inPackageID });
        }
        else
        {
            drawingLinePackageQueue[inPackageID].expectedNumberOfMessages = inExpectedNumberOfMessages;
        }
    }

}
