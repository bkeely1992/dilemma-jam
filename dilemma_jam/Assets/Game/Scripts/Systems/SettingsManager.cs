﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : Singleton<SettingsManager>
{
    public ColourManager.BirdName birdName;
    public Dictionary<ColourManager.BirdName, string> playerNameMap = new Dictionary<ColourManager.BirdName, string>();
    public bool isHostInLobby = false;
    public bool disconnected = false;
    public List<string> settingNames = new List<string>();
    public int winningThreshold = 1;
    public int drawingTime = 300, accuseTime = 120, correctCabinetThreshold = 1;

    public class GameSettings
    {
        public enum GameMode
        {
            botching, boss_rush
        }
        public GameMode gameMode = GameMode.boss_rush;
    }
    public GameSettings gameSettings = new GameSettings();


    private Dictionary<string, int> settings;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        settings = new Dictionary<string, int>();

        foreach(string settingName in settingNames)
        {
            if (settings.ContainsKey(settingName))
            {
                continue;
            }

            if (PlayerPrefs.HasKey(settingName))
            {
                settings.Add(settingName, PlayerPrefs.GetInt(settingName));
            }
            else
            {
                settings.Add(settingName, 1);
            }
            
        }
    }

    public void UpdateSetting(string inSettingName, bool inSettingValue)
    {
        if (!settings.ContainsKey(inSettingName))
        {
            settings.Add(inSettingName, inSettingValue ? 1 : 0);
        }
        else
        {
            settings[inSettingName] = inSettingValue ? 1 : 0;
        }
        PlayerPrefs.SetInt(inSettingName, inSettingValue ? 1 : 0);
    }

    public bool GetSetting(string inSettingName, bool defaultValue = true)
    {
        if (settings.ContainsKey(inSettingName))
        {
            return (settings[inSettingName] == 1);
        }
        else
        {
            return defaultValue;
        }

        
    }
}
