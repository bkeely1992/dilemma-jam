﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public enum GameScene
    {
        lobby, game
    }

    public GameScene currentGameScene;
    public Color workerColour, traitorColour;
    public GameObject linePrefab;
    public HideCursorOnHover cursorHider;
    public GameObject submitBtn;
    public Sprite folderFailSprite, folderSuccessSprite;

    public GameFlowManager gameFlowManager;
    public PlayerFlowManager playerFlowManager;

    // Start is called before the first frame update
    void Start()
    {
        if (!gameFlowManager)
        {
            gameFlowManager = FindObjectOfType<GameFlowManager>();
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (gameFlowManager)
            {
                gameFlowManager.gameObject.SetActive(true);
            }
           
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
