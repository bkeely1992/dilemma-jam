﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyNetwork : MonoBehaviourPunCallbacks
{
    public static LobbyNetwork Instance;

    private const byte LOBBY_BROADCAST_EVENT = 1;
    private const byte LOBBY_FROMCLIENT_EVENT = 2;
    private const byte LOBBY_TOCLIENT_EVENT = 3;

    public List<string> FromClientQueue = new List<string>();
    public List<string> ToClientQueue = new List<string>();
    public List<string> BroadcastQueue = new List<string>();

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    private string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        Screen.fullScreen = false;
        if (Instance == null)
        {
            Instance = this;
        }
        sceneName = SceneManager.GetActiveScene().name;

        AudioManager.Instance.PlaySound("OpeningMusic");

        if (!PhotonNetwork.IsConnected)
        {
            print("Connecting to server..");
            //PhotonNetwork.ConnectToRegion("us");
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {
            PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
        }
    }

    private void Update()
    {
        SendEvents();
    }

    #region Create/Join/Connect Callbacks

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if(Photon.Realtime.ClientState.Leaving == PhotonNetwork.NetworkClientState)
        {
            return;
        }

        //Close the room
        MenuLobbyButtons.Instance.PlayerPrompt.Activate("Host disconnected.");
        MenuLobbyButtons.Instance.LobbyPageObject.SetActive(false);
        LeaveRoom();    
    }

    public override void OnConnectedToMaster()
    {
        
        print("Connected to the server.");
        MenuLobbyButtons.Instance.ConnectedToServer();
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
    }

    public override void OnCreatedRoom()
    {
        print("Created room." + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnJoinedLobby()
    {
        if (!PhotonNetwork.InRoom)
        {
            print("Joined lobby.");
            MenuLobbyButtons.Instance.JoinedLobby();
        }
    }

    public void JoinLobby()
    {
        if (!PhotonNetwork.JoinLobby(TypedLobby.Default))
        {
            Debug.Log("Failed to join lobby.");
        }

    }

    public void JoinRoom(string roomName)
    {
        Debug.Log("Trying to join room.");
        if (!PhotonNetwork.JoinRoom(roomName))
        {
            Debug.Log("Failed to join room.");
        }
    }


    public override void OnJoinedRoom()
    {
        print("Joined room.");

        MenuLobbyButtons.Instance.JoinedRoom();

        Player[] photonPlayers = PhotonNetwork.PlayerList;

        foreach (Player photonPlayer in photonPlayers)
        {
            MenuLobbyButtons.Instance.PlayerListings.PlayerJoinedRoom(photonPlayer, photonPlayer.NickName);
        }

        MenuLobbyButtons.Instance.LobbyStartGameBtn.SetActive(PhotonNetwork.IsMasterClient);

    }

    public bool CreateRoom(string roomName)
    {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 8 };

        PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);
        Debug.Log("Creating room.");
        return true;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        MenuLobbyButtons.Instance.FailedToCreateRoom(message);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        
        if (PhotonNetwork.IsMasterClient)
        {
            if (MenuLobbyButtons.Instance.PlayerListingMap.ContainsKey(newPlayer.NickName))
            {
                if(MenuLobbyButtons.Instance.PlayerListingMap[newPlayer.NickName].PhotonPlayer.ActorNumber == newPlayer.ActorNumber)
                {
                    return;
                }

                string baseName = newPlayer.NickName;
                string newName = newPlayer.NickName;

                for (int i = 0; i < 10; i++)
                {
                    newName = baseName + i.ToString();
                    if (!MenuLobbyButtons.Instance.PlayerListingMap.ContainsKey(newName))
                    {
                        break;
                    }
                }
                newPlayer.NickName = newName;
                
                //Send player their name
                BroadcastQueue.Add("PlayerChangeName" + GameDelim.BASE + newPlayer.ActorNumber.ToString() + GameDelim.BASE + newName);
            }
            MenuLobbyButtons.Instance.PlayerListings.PlayerJoinedRoom(newPlayer, newPlayer.NickName);
            ServerRefreshBirds();

            
        }
        else
        {
            MenuLobbyButtons.Instance.PlayerListings.Refresh();
        }
    }

    private void ServerRefreshBirds()
    {
        string playerJoinUpdateMessage = "PlayerJoinUpdate";
        
        foreach (PlayerIdentification playerID in MenuLobbyButtons.Instance.PlayerIdentifiers)
        {
            playerJoinUpdateMessage += GameDelim.BASE + playerID.birdName.ToString() + GameDelim.NAME + playerID.playerName;
        }

        MenuLobbyButtons.Instance.UpdatePlayerIdentifiers(playerJoinUpdateMessage.Split(new string[] { GameDelim.BASE }, StringSplitOptions.None), 0);

        BroadcastQueue.Add(playerJoinUpdateMessage);
    }

    private void ServerSendBirdsToPlayer(string playerName)
    {
        string playerJoinUpdateMessage = playerName + GameDelim.BASE + "PlayerJoinUpdate";

        foreach (PlayerIdentification playerID in MenuLobbyButtons.Instance.PlayerIdentifiers)
        {
            playerJoinUpdateMessage += GameDelim.BASE + playerID.birdName.ToString() + GameDelim.NAME + playerID.playerName;
        }

        MenuLobbyButtons.Instance.UpdatePlayerIdentifiers(playerJoinUpdateMessage.Split(new string[] { GameDelim.BASE }, StringSplitOptions.None), 1);

        ToClientQueue.Add(playerJoinUpdateMessage);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Dictionary<RoomInfo, bool> rooms = new Dictionary<RoomInfo, bool>();

        foreach (RoomInfo room in roomList)
        {
            rooms.Add(room, !room.RemovedFromList && room.IsOpen && room.IsVisible);
        }

        MenuLobbyButtons.Instance.UpdateRoomListings(rooms);
    }
    #endregion

    #region Leave Callbacks
    public void LeaveLobby()
    {
        PhotonNetwork.LeaveLobby();
        MenuLobbyButtons.Instance.LeftLobby();
    }

    public void LeaveRoom()
    {
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.red);
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.blue);
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.green);
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.purple);
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.grey);
        MenuLobbyButtons.Instance.DeselectPlayerBird(ColourManager.BirdName.brown);
        MenuLobbyButtons.Instance.WaitingForHostPrompt.SetActive(false);

        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region Failure Callbacks
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join room: " + returnCode + "-" + message);
    }
    #endregion

    #region EventHandling
    public void SendEvents()
    {
        object newEventMessage;

        foreach (string queuedEvent in BroadcastQueue)
        {
            newEventMessage = queuedEvent;
            PhotonNetwork.RaiseEvent(LOBBY_BROADCAST_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        foreach (string queuedEvent in FromClientQueue)
        {
            newEventMessage = PhotonNetwork.NickName + GameDelim.BASE + queuedEvent;
            PhotonNetwork.RaiseEvent(LOBBY_FROMCLIENT_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        foreach (string queuedEvent in ToClientQueue)
        {
            newEventMessage = queuedEvent;
            PhotonNetwork.RaiseEvent(LOBBY_TOCLIENT_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        BroadcastQueue.Clear();
        FromClientQueue.Clear();
        ToClientQueue.Clear();
    }

    private void NetworkingClient_EventReceived(EventData obj)
    {
        if(sceneName == "Game")
        {
            return;
        }

        object datas;
        string[] messageSegments;
        switch (obj.Code)
        {
            case LOBBY_BROADCAST_EVENT:
                datas = obj.CustomData;
                messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                switch (messageSegments[0])
                {
                    case ("SelectBird"):
                        MenuLobbyButtons.Instance.SelectPlayerBird(MenuLobbyButtons.Instance.GetBirdNameFromText(messageSegments[1]), messageSegments[2]);
                        break;
                    case ("DeselectBird"):
                        MenuLobbyButtons.Instance.DeselectPlayerBird(MenuLobbyButtons.Instance.GetBirdNameFromText(messageSegments[1]));
                        break;
                    case ("UpdatePlayerID"):
                        MenuLobbyButtons.Instance.UpdatePlayerID(messageSegments);
                        break;
                    case ("PlayerJoinUpdate"):
                        MenuLobbyButtons.Instance.UpdatePlayerIdentifiers(messageSegments, 0);
                        MenuLobbyButtons.Instance.PlayerListings.Refresh();
                        break;
                    case ("LoadLevel"):
                        foreach (PlayerIdentification playerID in MenuLobbyButtons.Instance.PlayerIdentifiers)
                        {
                            if (playerID.playerName == PhotonNetwork.NickName)
                            {
                                SettingsManager.Instance.birdName = playerID.birdName;
                                LoadLevel(messageSegments[1]);
                            }
                        }
                        break;
                    case "HostJoiningLobby":
                        SettingsManager.Instance.isHostInLobby = true;
                        MenuLobbyButtons.Instance.WaitingForHostPrompt.SetActive(false);
                        break;
                    case "PlayerChangeName":
                        
                        if (messageSegments[1] == PhotonNetwork.LocalPlayer.ActorNumber.ToString())
                        {
                            PhotonNetwork.NickName = messageSegments[2];
                            FromClientQueue.Add("PlayerNameChanged" + GameDelim.BASE + messageSegments[1]);
                        }

                        
                        break;
                }

                break;
            case LOBBY_FROMCLIENT_EVENT:
                datas = obj.CustomData;
                messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                if (PhotonNetwork.IsMasterClient)
                {
                    switch (messageSegments[1])
                    {
                        case ("TrySelectBird"):

                            if (MenuLobbyButtons.Instance.TrySelectingBird(messageSegments[2], messageSegments[0]))
                            {
                                ToClientQueue.Add(messageSegments[0] + GameDelim.BASE + "SuccessfulSelect" + GameDelim.BASE + messageSegments[2]);
                            }
                            break;
                        case ("PlayerNameChanged"):
                            //Update the player listings
                            MenuLobbyButtons.Instance.PlayerListings.Refresh();

                            ServerRefreshBirds();
                            break;
                        case "RequestUsedBirdsFromClient":
                            ServerSendBirdsToPlayer(messageSegments[0]);
                            break;
                    }
                }
                break;
            case LOBBY_TOCLIENT_EVENT:
                datas = obj.CustomData;
                messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                if (messageSegments[0] == PhotonNetwork.NickName)
                {
                    switch (messageSegments[1])
                    {
                        case ("SuccessfulSelect"):
                            AudioManager.Instance.PlaySound("scan-beep");
                            break;
                        case ("SuccessfulDeselect"):
                            AudioManager.Instance.PlaySound("ButtonPress");
                            break;
                        case ("PlayerJoinUpdate"):
                            MenuLobbyButtons.Instance.UpdatePlayerIdentifiers(messageSegments, 1);
                            MenuLobbyButtons.Instance.PlayerListings.Refresh();
                            break;
                    }
                }

                break;
            default:
                break;
        }
    }
    #endregion

    public void LoadLevel(string levelName)
    {
        SettingsManager.Instance.isHostInLobby = false;
        SceneManager.LoadScene(levelName, LoadSceneMode.Single);
    }
}
