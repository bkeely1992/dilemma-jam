﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using static BotchingForm.BotcherRoundData;
using static ColourManager;
using static PlayerDrawingInputData;

public class GameFlowManager : MonoBehaviour
{
    public enum GamePhase
    {
        loading, drawing_tutorial, instructions, drawing, responses, accusation_tutorial, accusation, results, slides_tutorial, slides, accolades, reveal, invalid
    }
    public GamePhase currentGamePhase = GamePhase.loading;

    public enum PlayerRole
    {
        worker, botcher, invalid
    }

    public float loadingTimeLimit;
    public GameObject linePrefab;
    public Transform drawingsContainer;
    public int maxRounds = 5;

    public int numberOfPrefixesInChain = 10;
    public int numberOfNounsInChain = 10;
    public int numberOfBotcherPrefixOptions = 5;
    public int numberOfBotcherNounOptions = 5;
    
    public Dictionary<BirdName, Vector3> birdArmPositionMap = new Dictionary<BirdName, Vector3>();
    public Dictionary<BirdName, Vector3> accuseArmStretchMap = new Dictionary<BirdName, Vector3>();

    public Dictionary<BirdName, PlayerData> gamePlayers = new Dictionary<BirdName, PlayerData>();
    public List<BirdName> activeBirdNames = new List<BirdName>();

    public int numberOfPlayers;
    public float timeRemainingInPhase;
    public bool active;
    public float armUpdateFrequency = 0.5f;

    public TutorialSequence drawingTutorialSequence, slidesTutorialSequence, accusationTutorialSequence;

    private List<string> allPrefixCategories = new List<string>();
    private List<string> allNounCategories = new List<string>();
    private Dictionary<string,List<string>> allPrefixes = new Dictionary<string, List<string>>();
    private Dictionary<string,List<string>> allNouns = new Dictionary<string, List<string>>();
    private List<string> allAdjectives = new List<string>();

    private PlayerFlowManager playerFlowManager;
    private float timeSinceLastArmUpdate = 0.0f;
    public int totalPlayersConnected = 1;
    private bool isInitialized = false;

    private bool hasSentAccusationsToClients = false;
    public List<string> activeTransitionConditions = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isInitialized)
        {
            initialize();
            return;
        }
        if (!active)
        {
            return;
        }
        timeRemainingInPhase -= Time.deltaTime;
        
        if (timeRemainingInPhase <= 0)
        {
            TransitionPhase();
        }
        else
        {
            UpdatePhase();
        }
    }

    private void initialize()
    {
        if (totalPlayersConnected != SettingsManager.Instance.playerNameMap.Count)
        {
            return;
        }

        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        playerFlowManager.serverIsReady = true;
        GameNetwork.Instance.BroadcastQueue.Add("server_is_ready");

        active = true;
        timeRemainingInPhase = loadingTimeLimit;

        //Load in the potential prompts
        TextAsset promptsFile = Resources.Load<TextAsset>("prompts");
        string[] lines = promptsFile.ToString().Split('\n');
        string[] fields;
        string currentWord;
        string currentCategory;

        foreach (string line in lines)
        {
            fields = line.Split(',');

            if(fields.Length > 1)
            {
                currentWord = fields[0];
                currentCategory = fields[1];
                if(currentWord != "" && currentCategory != "")
                {
                    if (!allPrefixes.ContainsKey(currentCategory))
                    {
                        allPrefixes.Add(currentCategory, new List<string>());
                        allPrefixCategories.Add(currentCategory);
                    }
                    allPrefixes[currentCategory].Add(currentWord.ToUpper());
                }

                if (fields.Length > 3)
                {
                    currentWord = fields[2];
                    currentCategory = fields[3];
                    if (currentWord != "" && currentCategory != "")
                    {
                        if (!allNouns.ContainsKey(currentCategory))
                        {
                            allNouns.Add(currentCategory, new List<string>());
                            allNounCategories.Add(currentCategory);
                        }
                        allNouns[currentCategory].Add(currentWord.ToUpper());
                    }
                }

                if(fields.Length > 4)
                {
                    currentWord = fields[4];
                    if(currentWord != "")
                    {
                        allAdjectives.Add(currentWord.ToUpper());
                    }
                }
            }
        }

        ChainData newChain;
        EvaluationData newEvaluation;
        int promptIndex;

        string message = "game_initialization";
        string coloursMessageSegment = "";
        string playersMessageSegment = "";

        //Initialize the players
        foreach (KeyValuePair<BirdName, string> player in SettingsManager.Instance.playerNameMap)
        {
            activeBirdNames.Add(player.Key);
            gamePlayers.Add(player.Key, new global::PlayerData() { birdName = player.Key, playerName = player.Value, playerRole = PlayerRole.worker });
            playerFlowManager.playerNameMap.Add(player.Key, player.Value);

            if(SettingsManager.Instance.playerNameMap.First().Key == player.Key)
            {
                coloursMessageSegment += player.Key.ToString();
                playersMessageSegment += player.Value.ToString();
            }
            else
            {
                coloursMessageSegment += GameDelim.SUB + player.Key.ToString();
                playersMessageSegment += GameDelim.SUB + player.Value.ToString();
            }
        }

        numberOfPlayers = gamePlayers.Count;

        playerFlowManager.numberOfCabinetRounds = (gamePlayers.Count % 2) == 0 ? gamePlayers.Count - 1 : gamePlayers.Count - 2;
        if (playerFlowManager.numberOfCabinetRounds > 5)
        {
            playerFlowManager.numberOfCabinetRounds = 5;
        }
        message += GameDelim.BASE + playerFlowManager.numberOfCabinetRounds.ToString() + GameDelim.BASE + coloursMessageSegment + GameDelim.BASE + playersMessageSegment;

        GameNetwork.Instance.BroadcastQueue.Add(message);

        PlayerTextInputData tempPromptData;
        List<string> usedPrefixes = new List<string>();
        List<string> usedNouns = new List<string>();
        List<string> usedAdjectives = new List<string>();
        int iterator;
        BirdName currentPlayer;
        string currentPrefixCategory = "";
        string currentNounCategory = "";

        for (int i = 0; i < gamePlayers.Count; i++)
        {
            usedNouns.Clear();
            usedPrefixes.Clear();
            tempPromptData = new PlayerTextInputData()
            {
                author = BirdName.none
            };
            newChain = new ChainData();
            allNounCategories = allNounCategories.OrderBy(a => Guid.NewGuid()).ToList();
            allPrefixCategories = allPrefixCategories.OrderBy(a => Guid.NewGuid()).ToList();
            currentNounCategory = allNounCategories[0];
            currentPrefixCategory = allPrefixCategories[0];
            allPrefixes[currentPrefixCategory] = allPrefixes[currentPrefixCategory].OrderBy(a => Guid.NewGuid()).ToList();
            allNouns[currentNounCategory] = allNouns[currentNounCategory].OrderBy(a => Guid.NewGuid()).ToList();

            newChain.possiblePrefixes = new List<string>();
            newChain.possibleNouns = new List<string>();
            newChain.active = true;

            iterator = 0;
            while(newChain.possiblePrefixes.Count < numberOfPrefixesInChain)
            {
                currentWord = allPrefixes[currentPrefixCategory][iterator];
                if (!usedPrefixes.Contains(currentWord))
                {
                    newChain.possiblePrefixes.Add(currentWord);
                    usedPrefixes.Add(currentWord);
                }
                iterator++;
            }

            iterator = 0;
            while (newChain.possibleNouns.Count < numberOfNounsInChain)
            {
                currentWord = allNouns[currentNounCategory][iterator];
                if (!usedNouns.Contains(currentWord))
                {
                    newChain.possibleNouns.Add(currentWord);
                    usedNouns.Add(currentWord);
                }
                iterator++;
            }

            iterator = 0;
            while (newChain.botcherPrefixOptions.Count < numberOfBotcherPrefixOptions)
            {
                currentWord = allPrefixes[currentPrefixCategory][iterator];
                if (!usedPrefixes.Contains(currentWord))
                {
                    newChain.botcherPrefixOptions.Add(currentWord);
                    usedPrefixes.Add(currentWord);
                }
                iterator++;
            }

            iterator = 0;
            while (newChain.botcherNounOptions.Count < numberOfBotcherNounOptions)
            {
                currentWord = allNouns[currentNounCategory][iterator];
                if (!usedNouns.Contains(currentWord))
                {
                    newChain.botcherNounOptions.Add(currentWord);
                    usedNouns.Add(currentWord);
                }
                iterator++;
            }

            promptIndex = UnityEngine.Random.Range(0, numberOfPrefixesInChain);
            newChain.correctPrefix = newChain.possiblePrefixes[promptIndex];

            promptIndex = UnityEngine.Random.Range(0, numberOfNounsInChain);
            newChain.correctNoun = newChain.possibleNouns[promptIndex];

            
            List<BirdName> playerColours = gamePlayers.Keys.ToList();

            //Set the player order
            for (int j = 0; j < playerFlowManager.numberOfCabinetRounds+1; j++)
            {
                currentPlayer = i + j >= playerColours.Count ? playerColours[i + j - playerColours.Count] : playerColours[i + j];
                newChain.playerOrder.Add(j + 1, currentPlayer);
            }

            tempPromptData.prefix = newChain.correctPrefix;
            tempPromptData.noun = newChain.correctNoun;
            newChain.prompts.Add(1, tempPromptData);
            newChain.guesser = newChain.playerOrder[playerFlowManager.numberOfCabinetRounds + 1];

            //Put the new chain into the corresponding cabinet
            GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[i + 1].chainData = newChain;
        }

        //Send starting prompts to players
        foreach (KeyValuePair<int,CabinetDrawer> cabinet in playerFlowManager.drawingRound.cabinetDrawerMap)
        {
            ChainData chain = cabinet.Value.chainData;
            if (chain == null || !chain.active)
            {
                continue;
            }
            currentPlayer = chain.playerOrder[1];
            if(currentPlayer == SettingsManager.Instance.birdName)
            {
                playerFlowManager.drawingRound.setPrompt(cabinet.Key, 0, currentPlayer, chain.correctPrefix, chain.correctNoun, 0,false);
            }
            else
            {
                GameManager.Instance.gameFlowManager.addTransitionCondition("initial_cabinet_prompt_receipt:" + currentPlayer);
                GameNetwork.Instance.ToPlayerQueue.Add(currentPlayer.ToString() + GameDelim.BASE + "initial_cabinet_prompt_contents" + GameDelim.BASE + cabinet.Key + GameDelim.BASE + "0" + GameDelim.BASE + chain.correctPrefix + GameDelim.BASE + chain.correctNoun + GameDelim.BASE + BirdName.none.ToString());
            }
        }

        if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.botching)
        {
            int numberOfEvaluationPhases = gamePlayers.Count > 5 ? 3 : 2;

            for (int i = 0; i < numberOfEvaluationPhases; i++)
            {
                allAdjectives = allAdjectives.OrderBy(a => Guid.NewGuid()).ToList();
                allNounCategories = allNounCategories.OrderBy(a => Guid.NewGuid()).ToList();
                iterator = 0;
                currentWord = "";
                currentCategory = allNounCategories[0];

                allNouns[currentCategory] = allNouns[currentCategory].OrderBy(a => Guid.NewGuid()).ToList();
                newEvaluation = new EvaluationData();

                iterator = 0;
                while (newEvaluation.adjective == "")
                {
                    currentWord = allAdjectives[iterator];
                    if (!usedAdjectives.Contains(currentWord))
                    {
                        newEvaluation.adjective = currentWord;
                        usedAdjectives.Add(currentWord);
                    }
                    iterator++;
                }

                iterator = 0;
                while (newEvaluation.noun == "")
                {
                    currentWord = allNouns[currentCategory][iterator];
                    if (!usedNouns.Contains(currentWord))
                    {
                        newEvaluation.noun = currentWord;
                        usedNouns.Add(currentWord);
                    }
                    iterator++;
                }

                //Add drawings for each player
                iterator = 0;
                foreach (KeyValuePair<BirdName, PlayerData> player in gamePlayers)
                {
                    newEvaluation.drawings.Add(iterator + 1, new PlayerDrawingInputData() { author = player.Key });

                    //Message the evaluation round contents to every player
                    if (player.Key != SettingsManager.Instance.birdName)
                    {
                        GameNetwork.Instance.ToPlayerQueue.Add(player.Key + GameDelim.BASE + "evaluation_details" + GameDelim.BASE + newEvaluation.adjective + GameDelim.BASE + newEvaluation.noun + GameDelim.BASE + (i + 1).ToString() + GameDelim.BASE + (iterator + 1).ToString());
                    }
                    iterator++;
                }

                GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap.Add(i + 1, newEvaluation);
            }
            usedNouns.Clear();

            //Set the botcher
            int selectedPlayerIndex = UnityEngine.Random.Range(0, gamePlayers.Count);
            BirdName botcherBird = gamePlayers[activeBirdNames[selectedPlayerIndex]].birdName;

            gamePlayers[botcherBird].playerRole = GameFlowManager.PlayerRole.botcher;

            BotcherMode randomBotcherMode;
            int differentCabinetID = -1;
            int drawingRound;
            ChainData currentChain = null;
            int iterations = 0;
            string currentMessage = "";
            //Initialize the botcher round contents
            for (int i = 0; i < numberOfEvaluationPhases; i++)
            {
                randomBotcherMode = playerFlowManager.drawingRound.botchingForm.getRandomBotcherMode();
                drawingRound = i * 2 + 1;
                iterations = 0;
                differentCabinetID = -1;
                while (differentCabinetID == -1 || (currentChain.drawings.ContainsKey(drawingRound) && currentChain.drawings[drawingRound].author == botcherBird))
                {
                    differentCabinetID = UnityEngine.Random.Range(1, gamePlayers.Count + 1);
                    currentChain = playerFlowManager.drawingRound.cabinetDrawerMap[differentCabinetID].chainData;
                    iterations++;

                    if (iterations > 10)
                    {
                        break;
                    }
                }
                if (SettingsManager.Instance.birdName == botcherBird)
                {
                    playerFlowManager.drawingRound.botchingForm.addBotcherRound(randomBotcherMode, i + 1, differentCabinetID, drawingRound);

                    if (randomBotcherMode == BotcherMode.add_prompts)
                    {
                        currentChain = playerFlowManager.drawingRound.cabinetDrawerMap[differentCabinetID].chainData;
                        playerFlowManager.drawingRound.botchingForm.addBotcherPrompts(i + 1, currentChain.botcherPrefixOptions, currentChain.botcherNounOptions);
                    }
                }
                else
                {
                    GameNetwork.Instance.ToPlayerQueue.Add(botcherBird + GameDelim.BASE + "add_botcher_round" + GameDelim.BASE + randomBotcherMode.ToString() + GameDelim.BASE + (i + 1).ToString() + GameDelim.BASE + differentCabinetID.ToString() + GameDelim.BASE + drawingRound.ToString());

                    if (randomBotcherMode == BotcherMode.add_prompts)
                    {
                        currentChain = playerFlowManager.drawingRound.cabinetDrawerMap[differentCabinetID].chainData;

                        currentMessage = botcherBird + GameDelim.BASE + "add_botcher_prefixes" + GameDelim.BASE + (i + 1).ToString();
                        foreach (string botcherPrefix in currentChain.botcherPrefixOptions)
                        {
                            currentMessage += GameDelim.BASE + botcherPrefix;
                        }
                        GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);

                        currentMessage = botcherBird + GameDelim.BASE + "add_botcher_nouns" + GameDelim.BASE + (i + 1).ToString();
                        foreach (string botcherNoun in currentChain.botcherNounOptions)
                        {
                            currentMessage += GameDelim.BASE + botcherNoun;
                        }
                        GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);
                    }
                }
            }

            List<int> accusationChairPositions = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
            accusationChairPositions = accusationChairPositions.OrderBy(a => Guid.NewGuid()).ToList();
            iterator = 0;
            foreach (PlayerData player in gamePlayers.Values)
            {
                GameManager.Instance.playerFlowManager.accusationsRound.initializePlayerSeat(accusationChairPositions[iterator], player.birdName);
                GameManager.Instance.playerFlowManager.slidesRound.initializeGalleryBird(accusationChairPositions[iterator], player.birdName);
                GameManager.Instance.playerFlowManager.accoladesRound.initializeAccoladeBirdRow(accusationChairPositions[iterator], player.birdName);
                GameNetwork.Instance.BroadcastQueue.Add("accusation_seat" + GameDelim.BASE + accusationChairPositions[iterator].ToString() + GameDelim.BASE + player.birdName);
                iterator++;
            }
        }
        

        isInitialized = true;
    }

    private void TransitionPhase()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        switch (currentGamePhase)
        {
            case GamePhase.loading:
                
                foreach(BirdName player in gamePlayers.Keys)
                {
                    if(player == SettingsManager.Instance.birdName)
                    {
                        continue;
                    }
                }

                if (SettingsManager.Instance.GetSetting("tutorials"))
                {
                    currentGamePhase = GamePhase.drawing_tutorial;
                }
                else
                {
                    currentGamePhase = GamePhase.instructions;
                    drawingTutorialSequence.gameObject.SetActive(false);
                }
                
                break;
            case GamePhase.drawing_tutorial:
                if (drawingTutorialSequence.active ||
                    !isRoundOver())
                {
                    return;
                }
                drawingTutorialSequence.gameObject.SetActive(false);
                currentGamePhase = GamePhase.instructions;
                break;
            case GamePhase.instructions:
                if (!isRoundOver())
                {
                    return;
                }

                currentGamePhase = GamePhase.drawing;
                break;
            case GamePhase.drawing:
                if (playerFlowManager.drawingRound.roundIsInProgress)
                {
                    if (!isRoundOver())
                    {
                        return;
                    }
                    playerFlowManager.drawingRound.transitionRound();
                    UpdatePhase();
                    return;
                }
                if (!isRoundOver())
                {
                    UpdatePhase();
                    return;
                }

                currentGamePhase = GamePhase.responses;

                foreach(BirdName bird in gamePlayers.Keys)
                {
                    //activeTransitionConditions.Add("response_submitted:" + bird);
                    if(bird != SettingsManager.Instance.birdName)
                    {
                        addTransitionCondition("all_chains_loaded:" + bird);
                    }
                }

                break;
            case GamePhase.responses:
                if (!isRoundOver())
                {
                    return;
                }

                if (SettingsManager.Instance.GetSetting("tutorials"))
                {
                    currentGamePhase = GamePhase.slides_tutorial;
                }
                else
                {
                    foreach (BirdName bird in gamePlayers.Keys)
                    {
                        if (bird != SettingsManager.Instance.birdName)
                        {
                            activeTransitionConditions.Add("ratings_loaded:" + bird);
                        }
                    }
                    currentGamePhase = GamePhase.slides;
                }
                break;
            case GamePhase.slides_tutorial:
                if (slidesTutorialSequence.active ||
                    !isRoundOver())
                {
                    return;
                }
                foreach (BirdName bird in gamePlayers.Keys)
                {
                    if (bird != SettingsManager.Instance.birdName)
                    {
                        activeTransitionConditions.Add("ratings_loaded:" + bird);
                    }
                }
                slidesTutorialSequence.gameObject.SetActive(false);
                currentGamePhase = GamePhase.slides;
                break;
            case GamePhase.slides:
                if (playerFlowManager.slidesRound.inProgress)
                {
                    if (isRoundOver())
                    {
                        playerFlowManager.slidesRound.inProgress = false;
                    }
                    return;
                }

                currentGamePhase = GamePhase.accolades;
                break;
            case GamePhase.accolades:
                if (playerFlowManager.accoladesRound.isActive)
                {
                    return;
                }
                if (didWorkersWinResponseRound())
                {
                    playerFlowManager.resultsRound.endgameState = ResultsRound.EndGameState.worker_early_win;
                    currentGamePhase = GamePhase.results;
                }
                else if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.boss_rush)
                {
                    playerFlowManager.resultsRound.endgameState = ResultsRound.EndGameState.worker_early_loss;
                    currentGamePhase = GamePhase.results;
                }
                else
                {
                    if (SettingsManager.Instance.GetSetting("tutorials"))
                    {
                        currentGamePhase = GamePhase.accusation_tutorial;
                    }
                    else
                    {
                        currentGamePhase = GamePhase.accusation;

                        foreach (BirdName bird in gamePlayers.Keys)
                        {
                            if (bird != SettingsManager.Instance.birdName)
                            {
                                activeTransitionConditions.Add("accusations_loaded:" + bird);
                            }
                        }
                    }

                }
                break;
            case GamePhase.accusation_tutorial:
                if (accusationTutorialSequence.active ||
                    !isRoundOver())
                {
                    return;
                }
                accusationTutorialSequence.gameObject.SetActive(false);
                currentGamePhase = GamePhase.accusation;

                foreach (BirdName bird in gamePlayers.Keys)
                {
                    if (bird != SettingsManager.Instance.birdName)
                    {
                        activeTransitionConditions.Add("accusations_loaded:" + bird);
                    }
                }
                break;
            case GamePhase.accusation:
                AccusationsRound accusationRound = GameManager.Instance.playerFlowManager.accusationsRound;
                if (accusationRound.presentationSubround.isActive)
                {
                    timeRemainingInPhase = accusationRound.presentationSubround.savedAccusationVoteRoundTime;
                    GameNetwork.Instance.BroadcastQueue.Add("resume_accusation" + GameDelim.BASE + accusationRound.presentationSubround.savedAccusationVoteRoundTime);
                    accusationRound.resumeFromPresentation(accusationRound.presentationSubround.savedAccusationVoteRoundTime);
                    return;
                }
                switch (accusationRound.roundState)
                {
                    case AccusationsRound.AccusationRoundState.employee_review:
                        GameNetwork.Instance.BroadcastQueue.Add("accusation_change_to_vote");
                        accusationRound.changeToVoteFromReview();
                        timeRemainingInPhase = accusationRound.timeForVoting;
                        return;
                    case AccusationsRound.AccusationRoundState.accusation:
                        if (!hasSentAccusationsToClients)
                        {
                            if (didWorkersWinAccusationRound())
                            {
                                playerFlowManager.resultsRound.endgameState = ResultsRound.EndGameState.worker_win;
                            }
                            else if ((accusationRound.numberOfAccuseRoundsLeft - 1) <= 0)
                            {
                                playerFlowManager.resultsRound.endgameState = ResultsRound.EndGameState.traitor_win;
                            }
                            else
                            {
                                playerFlowManager.resultsRound.endgameState = ResultsRound.EndGameState.incomplete;
                            }

                            accusationRound.sendAccusationsToClients();
                            hasSentAccusationsToClients = true;
                        }
                        else if (!isRoundOver())
                        {
                            return;
                        }
                        else
                        {
                            if (accusationRound.mostAccusedPlayer != BirdName.none)
                            {
                                accusationRound.removeAccusedPlayer(accusationRound.mostAccusedPlayer);
                            }
                            accusationRound.resetRound();
                            hasSentAccusationsToClients = false;
                            GameNetwork.Instance.BroadcastQueue.Add("reset_accuse_round");
                            GameManager.Instance.playerFlowManager.revealRound.hasSentEndGame = false;
                            currentGamePhase = GamePhase.reveal;

                            foreach (BirdName bird in gamePlayers.Keys)
                            {
                                if (bird != SettingsManager.Instance.birdName)
                                {
                                    activeTransitionConditions.Add("accusations_loaded:" + bird);
                                }
                            }
                        }
                        break;
                }
                

                break;
            case GamePhase.reveal:
                if (GameManager.Instance.playerFlowManager.revealRound.isActive)
                {
                    return;
                }

                switch (playerFlowManager.resultsRound.endgameState)
                {
                    case ResultsRound.EndGameState.traitor_win:
                    case ResultsRound.EndGameState.worker_win:
                        currentGamePhase = GamePhase.results;
                        break;
                    case ResultsRound.EndGameState.incomplete:
                        currentGamePhase = GamePhase.accusation;
                        break;
                }
                break;
            case GamePhase.results:
                return;
        }
        playerFlowManager.UpdatePhase(currentGamePhase);
        timeRemainingInPhase = playerFlowManager.currentTimeInRound;

        //Broadcast the phase
        GameNetwork.Instance.BroadcastQueue.Add("update_phase" + GameDelim.BASE + currentGamePhase.ToString());
        
    }

    public void addTransitionCondition(string condition)
    {
        if (activeTransitionConditions.Contains(condition))
        {
            //Debug.LogError("Condition["+condition+"] already found in list of active conditions, could not add.");
            return;
        }
        //Debug.LogError("Adding["+ condition + "] to transition conditions.");
        activeTransitionConditions.Add(condition);
    }

    public void resolveTransitionCondition(string condition)
    {
        if (!activeTransitionConditions.Contains(condition))
        {
            //Debug.LogError("Condition["+condition+"] not found in list of active conditions, could not resolve.");
            return;
        }
        activeTransitionConditions.Remove(condition);
        //Debug.LogError("Resolving transition condition[" + condition + "]. There are still " + activeTransitionConditions.Count + " active conditions.");

        if (isRoundOver())
        {
            //Debug.LogError("Round is over, setting time remaining in phase to 0.");
            timeRemainingInPhase = 0.0f;
        }
    }

    private void UpdatePhase()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        string message;
        switch (currentGamePhase)
        {
            case GamePhase.loading:
            case GamePhase.instructions:
            case GamePhase.drawing:
            case GamePhase.responses:
                timeSinceLastArmUpdate += Time.deltaTime;

                if(timeSinceLastArmUpdate > armUpdateFrequency)
                {
                    message = "drawing_phase_positions";
                    if (birdArmPositionMap != null && birdArmPositionMap.Count > 0)
                    {
                        foreach (KeyValuePair<BirdName, Vector3> birdArmPosition in birdArmPositionMap)
                        {
                            message += GameDelim.BASE + birdArmPosition.Key.ToString() + GameDelim.SUB + birdArmPosition.Value.x.ToString(CultureInfo.InvariantCulture)
                                                                                        + GameDelim.SUB + birdArmPosition.Value.y.ToString(CultureInfo.InvariantCulture) + 
                                                                                        GameDelim.SUB + birdArmPosition.Value.z.ToString(CultureInfo.InvariantCulture);
                            //Update arm positions locally
                            playerFlowManager.drawingRound.SetBirdArmPosition(birdArmPosition.Key, birdArmPosition.Value);
                        }
                       

                        //Broadcast arm positions to all other clients
                        GameNetwork.Instance.BroadcastQueue.Add(message);

                        timeSinceLastArmUpdate = 0.0f;
                    }

                }
                break;
                
            case GamePhase.accusation:
                timeSinceLastArmUpdate += Time.deltaTime;

                if (timeSinceLastArmUpdate > armUpdateFrequency)
                {
                    message = "accuse_phase_positions";
                    foreach (KeyValuePair<ColourManager.BirdName, Vector3> accuseArmStretch in accuseArmStretchMap)
                    {
                        message += GameDelim.BASE + accuseArmStretch.Key.ToString() + GameDelim.SUB + accuseArmStretch.Value.x.ToString(CultureInfo.InvariantCulture) + GameDelim.SUB + 
                                                                                                        accuseArmStretch.Value.y.ToString(CultureInfo.InvariantCulture) + GameDelim.SUB +
                                                                                                        accuseArmStretch.Value.z.ToString(CultureInfo.InvariantCulture);
                    }
                    //Update arm positions locally
                    playerFlowManager.accusationsRound.setMeetingArmStretches(message);

                    //Broadcast arm positions to all other clients
                    GameNetwork.Instance.BroadcastQueue.Add(message);

                    timeSinceLastArmUpdate = 0.0f;
                }
                break;
        }
    }

    public bool isRoundOver()
    {        
        if(activeTransitionConditions.Count > 0)
        {
            return false;
        }

        return true;
    }

    public bool isCurrentAccusationReady()
    {
        AccusationsRound accusationRound = GameManager.Instance.playerFlowManager.accusationsRound;
        if (!accusationRound.accusationMap.ContainsKey(accusationRound.currentVoteRound))
        {
            return false;
        }
       
        foreach (BirdName player in playerFlowManager.accusationsRound.livingPlayers)
        {
            if (!accusationRound.accusationMap[accusationRound.currentVoteRound].ContainsKey(player))
            {
                return false;
            }
        }

        return true;
    }

    public bool didWorkersWinResponseRound()
    {
        ChainData currentCabinetData;
        int totalPoints = 0;

        foreach (CabinetDrawer cabinet in playerFlowManager.drawingRound.allCabinets)
        {
            if (cabinet.currentPlayer == BirdName.none)
            {
                continue;
            }

            currentCabinetData = cabinet.chainData;

            if(currentCabinetData.correctPrefix == currentCabinetData.prefixGuess)
            {
                totalPoints += 1;
            }
            if(currentCabinetData.correctNoun == currentCabinetData.nounGuess)
            {
                totalPoints += 1;
            }
            if (currentCabinetData.correctPrefix == currentCabinetData.prefixGuess &&
                currentCabinetData.correctNoun == currentCabinetData.nounGuess)
            {
                totalPoints+=1;
            }

        }

        return totalPoints >= playerFlowManager.playerNameMap.Count * 2;
    }

    private bool didWorkersWinAccusationRound()
    {
        Dictionary<BirdName, int> accusePlayerMap = new Dictionary<BirdName, int>();
        List<BirdName> currentMostAccusedPlayers = new List<BirdName>();
        BirdName correctAccusation = BirdName.none;
        int mostAccusedAmount = 0;
        AccusationsRound accusationRound = GameManager.Instance.playerFlowManager.accusationsRound;



        foreach (KeyValuePair<BirdName, PlayerData> playerData in gamePlayers)
        {
            if (playerData.Value.playerRole == PlayerRole.botcher)
            {
                correctAccusation = playerData.Key;
            }
        }
        if (!accusationRound.accusationMap.ContainsKey(accusationRound.currentVoteRound))
        {
            Debug.LogError("Accusation map is missing vote round["+(accusationRound.currentVoteRound).ToString()+"].");
            foreach(int round in accusationRound.accusationMap.Keys)
            {
                Debug.LogError("Accusation map contains round["+round+"].");
            }
            return false;
        }
        foreach(KeyValuePair<BirdName,BirdName> vote in accusationRound.accusationMap[accusationRound.currentVoteRound])
        {
            if (!accusePlayerMap.ContainsKey(vote.Value))
            {
                accusePlayerMap.Add(vote.Value, 0);
            }
            accusePlayerMap[vote.Value]++;
        }

        foreach(KeyValuePair<BirdName, int> accusedPlayer in accusePlayerMap)
        {
            if(accusedPlayer.Value == mostAccusedAmount)
            {
                currentMostAccusedPlayers.Add(accusedPlayer.Key);
            }
            else if(accusedPlayer.Value > mostAccusedAmount)
            {
                mostAccusedAmount = accusedPlayer.Value;
                currentMostAccusedPlayers.Clear();
                currentMostAccusedPlayers.Add(accusedPlayer.Key);
            }
        }

        GameManager.Instance.playerFlowManager.accusationsRound.currentVoteRound++;
        playerFlowManager.accusationsRound.mostAccusedPlayer = currentMostAccusedPlayers.Count == 1 ? currentMostAccusedPlayers[0] : BirdName.none;

        return (currentMostAccusedPlayers.Count == 1 && currentMostAccusedPlayers[0] == correctAccusation);
    }

    public void SetBirdArmPosition(BirdName inBirdName, Vector3 currentPosition)
    {
        if (!birdArmPositionMap.ContainsKey(inBirdName))
        {
            birdArmPositionMap.Add(inBirdName, currentPosition);
        }
        else
        {
            birdArmPositionMap[inBirdName] = currentPosition;
        }
    }

    public void SetAccuseArmStretch(BirdName inBirdName, Vector3 currentStretch)
    {
        if (!accuseArmStretchMap.ContainsKey(inBirdName))
        {
            accuseArmStretchMap.Add(inBirdName, currentStretch);
        }
        else
        {
            accuseArmStretchMap[inBirdName] = currentStretch;
        }
    }
}

public class EvaluationData
{
    public int round = -1;
    public string adjective = "";
    public string noun = "";
    public Dictionary<int, PlayerDrawingInputData> drawings = new Dictionary<int, PlayerDrawingInputData>();
    public Dictionary<int, PlayerRatingData> ratings = new Dictionary<int, PlayerRatingData>();
}

public class ChainData
{
    public Dictionary<int, PlayerDrawingInputData> drawings = new Dictionary<int, PlayerDrawingInputData>();
    public Dictionary<int, PlayerTextInputData> prompts = new Dictionary<int, PlayerTextInputData>();
    public Dictionary<int, PlayerRatingData> ratings = new Dictionary<int, PlayerRatingData>();
    public Dictionary<int, BirdName> playerOrder = new Dictionary<int, BirdName>();
    public int currentRound = 1;
    public List<string> possiblePrefixes = new List<string>();
    public List<string> possibleNouns = new List<string>();
    public List<string> botcherPrefixOptions = new List<string>();
    public List<string> botcherNounOptions = new List<string>();

    public string selectedBotcherPrefix = "";
    public string selectedBotcherNoun = "";
    public string correctPrefix = "";
    public string correctNoun = "";
    public BirdName guesser = BirdName.none;

    public string prefixGuess = "";
    public string nounGuess = "";
    public bool active = false;
    public bool drawingsAreLoaded
    {
        get
        {
            bool result = true;

            if (drawings.Count != drawingsExpected)
            {
                return false;
            }

            foreach (KeyValuePair<int, PlayerDrawingInputData> drawing in drawings)
            {
                if (drawing.Value.author == BirdName.none)
                {
                    return false;
                }
            }
            return result;
        }
    }
    public bool promptsAreLoaded
    {
        get
        {
            bool result = true;

            if(prompts.Where(p => p.Key > 1).Count() != promptsExpected)
            {
                return false;
            }
            foreach (KeyValuePair<int, PlayerTextInputData> prompt in prompts)
            {
                if(prompt.Value.author == BirdName.none)
                {
                    return false;
                }
            }
            return result;
        }
    }

    public void addDrawing(int round, PlayerDrawingInputData drawing)
    {
        //Debug.LogError("Adding drawing from author["+drawing.author+"] for round["+round+"].");
        if (drawings.ContainsKey(round))
        {
            drawings[round] = drawing;
        }
        else
        {
            drawings.Add(round, drawing);
        }
        
    }

    public void addToDrawing(int round, PlayerDrawingInputData drawing)
    {
        if (drawings.ContainsKey(round))
        {
            drawings[round].lines.AddRange(drawing.lines);
        }
        else
        {
            drawings.Add(round, drawing);
        }
    }
    public int drawingsExpected = -1;
    public int promptsExpected = -1;
}

public class PlayerTextInputData
{
    public BirdName author;
    public string prefix = "";
    public string noun = "";
    public float timeTaken = 0.0f;
}

public class PlayerRatingData
{
    public BirdName target = BirdName.none;
    public int likeCount;
    public int dislikeCount;

    public override string ToString()
    {
        return likeCount + GameDelim.SUB + dislikeCount;
    }
}

public class PlayerDrawingInputData
{
    public BirdName author;
    public List<DrawingLine> lines = new List<DrawingLine>();
    public float timeTaken = 0.0f;
    public enum Coverup
    {
        top_left, top_right, bottom_left, bottom_right, none, invalid
    }
    public Coverup coverup = Coverup.none;
    public enum DrawingType
    {
        cabinet, evaluation, chain, to_botcher, botcher_update, presentation, invalid
    }
    public DrawingType type = DrawingType.invalid;

    public static DrawingType getDrawingTypeFromCode(string code)
    {
        switch (code)
        {
            case "cabinet":
                return DrawingType.cabinet;
            case "evaluation":
                return DrawingType.evaluation;
            case "chain":
                return DrawingType.chain;
            case "to_botcher":
                return DrawingType.to_botcher;
            case "botcher_update":
                return DrawingType.botcher_update;
            case "presentation":
                return DrawingType.presentation;
            default:
                return DrawingType.invalid;
        }
    }

    public static Coverup getCoverupFromCode(string code)
    {
        switch (code)
        {
            case "top_left":
                return Coverup.top_left;
            case "top_right":
                return Coverup.top_right;
            case "bottom_left":
                return Coverup.bottom_left;
            case "bottom_right":
                return Coverup.bottom_right;
            case "none":
                return Coverup.none;
            default:
                return Coverup.invalid;
        }
    }
}

public class PlayerData
{
    public BirdName birdName;
    public GameFlowManager.PlayerRole playerRole = GameFlowManager.PlayerRole.worker;
    public BirdName nextInOrder;
    public string playerName;
}

public class DrawingLinesPackage
{
    public ColourManager.BirdName author;
    public Dictionary<int,string> messages;
    public int expectedNumberOfMessages;
    public int cabinetID;
    public int cabinetTab;
    public DrawingType drawingType;
    public Coverup coverup;
    public string packageID;
    public float timeTaken;
}

