﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourManager : Singleton<ColourManager>
{
    [SerializeField]
    public enum BirdName
    {
        red, blue, green, grey, brown, purple, none, orange, black
    }


    public Dictionary<BirdName, Bird> birdMap = new Dictionary<BirdName, Bird>();
    public List<Bird> allBirds = new List<Bird>();
    public Color evaluationFolderColour;

    void Awake()
    {
        foreach(Bird bird in allBirds)
        {
            birdMap.Add(bird.name, bird);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[System.Serializable]
public class Bird
{
    public Sprite faceSprite;
    public Sprite armSprite;
    public Color colour, bgColour;
    public ColourManager.BirdName name;
    public Material material,bgLineMaterial;
    public Texture2D cursor, featherCursor, handCursor;
    public Sprite armOutlineSprite;
    public Sprite accusationBodySprite, accusationArmSprite, accusationHandSprite;
    public Sprite peanutGallerySprite;
    public Vector3 birdHandWidthScaling;
    public float birdHandRotationAdjustment;
}
