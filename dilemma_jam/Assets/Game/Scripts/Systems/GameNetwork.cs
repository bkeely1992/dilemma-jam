﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static BotchingForm.BotcherRoundData;
using static ColourManager;
using static GameFlowManager;
using static GameNetwork.BroadcastNetworkMessage;
using static GameNetwork.ToPlayerNetworkMessage;
using static GameNetwork.ToServerNetworkMessage;
using static PlayerDrawingInputData;

public class GameNetwork : Singleton<GameNetwork>
{
    private const byte GAME_BROADCAST_EVENT = 4;
    private const byte GAME_TOPLAYER_EVENT = 5;
    private const byte GAME_TOSERVER_EVENT = 6;

    public List<string> ToPlayerQueue = new List<string>();
    public List<string> ToServerQueue = new List<string>();
    public List<string> BroadcastQueue = new List<string>();

    private PlayerFlowManager playerFlowManager;
    private GameFlowManager gameFlowManager;

    private void Start()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
    }

    public void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    void Update()
    {
        SendEvents();
    }

    public void SendEvents()
    {
        if (PhotonNetwork.InRoom)
        {
            object newEventMessage;

            foreach (string queuedEvent in BroadcastQueue)
            {
                newEventMessage = queuedEvent;
                PhotonNetwork.RaiseEvent(GAME_BROADCAST_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
            }

            foreach (string queuedEvent in ToPlayerQueue)
            {
                newEventMessage = queuedEvent;
                PhotonNetwork.RaiseEvent(GAME_TOPLAYER_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
            }

            foreach(string queuedEvent in ToServerQueue)
            {
                newEventMessage = queuedEvent;
                PhotonNetwork.RaiseEvent(GAME_TOSERVER_EVENT, newEventMessage, RaiseEventOptions.Default, SendOptions.SendReliable);
            }

            BroadcastQueue.Clear();
            ToPlayerQueue.Clear();
            ToServerQueue.Clear();
        }

    }

    private void NetworkingClient_EventReceived(EventData obj)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (!gameFlowManager)
        {
            gameFlowManager = GameManager.Instance.gameFlowManager;
        }
        
        object datas;
        string[] messageSegments;
        

        switch (obj.Code)
        {
            case GAME_BROADCAST_EVENT:
                datas = obj.CustomData;
                messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                processBroadcastEvent(messageSegments);
                break;
            case GAME_TOPLAYER_EVENT:
                datas = obj.CustomData;
                messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                if (getBirdNameFromCode(messageSegments[0]) == SettingsManager.Instance.birdName)
                {
                    processToPlayerEvent(messageSegments);
                }
                break;
            case GAME_TOSERVER_EVENT:
                if (PhotonNetwork.IsMasterClient)
                {
                    datas = obj.CustomData;
                    messageSegments = datas.ToString().Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
                    processToServerEvent(messageSegments, datas.ToString());
                }
                break;
        }
    }

    private void processBroadcastEvent(string[] messageSegments)
    {
        BroadcastMessageType messageType;
        if(!Enum.TryParse(messageSegments[0], out messageType))
        {
            Debug.LogError("Invalid value provided for message type[" + messageSegments[0] + "] for processing broadcast event.");
            return;
        }

        switch (messageType)
        {
            case BroadcastMessageType.host_joining_lobby:
                HostJoiningLobbyBroadcastNetworkMessage hostJoiningLobbyMessage = new HostJoiningLobbyBroadcastNetworkMessage(messageSegments);
                hostJoiningLobbyMessage.resolve();
                break;
            case BroadcastMessageType.server_is_ready:
                ServerIsReadyBroadcastNetworkMessage serverIsReadyMessage = new ServerIsReadyBroadcastNetworkMessage(messageSegments);
                serverIsReadyMessage.resolve();
                break;
            case BroadcastMessageType.game_initialization:
                GameInitializationBroadcastNetworkMessage gameInitializationMessage = new GameInitializationBroadcastNetworkMessage(messageSegments);
                gameInitializationMessage.resolve();
                break;
            case BroadcastMessageType.folder_colour_initialization:
                FolderColourInitializationBroadcastNetworkMessage folderColourInitializationMessage = new FolderColourInitializationBroadcastNetworkMessage(messageSegments);
                folderColourInitializationMessage.resolve();
                break;
            case BroadcastMessageType.update_bird_folder_status:
                UpdateBirdFolderStatusBroadcastNetworkMessage updateBirdFolderStatusMessage = new UpdateBirdFolderStatusBroadcastNetworkMessage(messageSegments);
                updateBirdFolderStatusMessage.resolve();
                break;
            case BroadcastMessageType.hide_cabinet_folder:
                HideCabinetFolderBroadcastNetworkMessage hideCabinetFolderMessage = new HideCabinetFolderBroadcastNetworkMessage(messageSegments);
                hideCabinetFolderMessage.resolve();
                break;
            case BroadcastMessageType.cabinet_drawer_is_ready:
                CabinetDrawerIsReadyBroadcastNetworkMessage cabinetDrawerIsReadyMessage = new CabinetDrawerIsReadyBroadcastNetworkMessage(messageSegments);
                cabinetDrawerIsReadyMessage.resolve();
                break;
            case BroadcastMessageType.evaluation_drawer_is_ready:
                EvaluationDrawerIsReadyBroadcastNetworkMessage evaluationDrawerIsReadyMessage = new EvaluationDrawerIsReadyBroadcastNetworkMessage(messageSegments);
                evaluationDrawerIsReadyMessage.resolve();
                break;
            case BroadcastMessageType.access_drawer:
                AccessDrawerBroadcastNetworkMessage cabinetIsStartedMessage = new AccessDrawerBroadcastNetworkMessage(messageSegments);
                cabinetIsStartedMessage.resolve();
                break;
            case BroadcastMessageType.update_drawing_round_state:
                UpdateDrawingRoundStateBroadcastNetworkMessage updateDrawingRoundStateMessage = new UpdateDrawingRoundStateBroadcastNetworkMessage(messageSegments);
                updateDrawingRoundStateMessage.resolve();
                break;
            case BroadcastMessageType.update_phase:
                UpdatePhaseBroadcastNetworkMessage updatePhaseMessage = new UpdatePhaseBroadcastNetworkMessage(messageSegments);
                updatePhaseMessage.resolve();
                break;
            case BroadcastMessageType.slide_rating:
                SlideRatingBroadcastNetworkMessage slideRatingMessage = new SlideRatingBroadcastNetworkMessage(messageSegments);
                slideRatingMessage.resolve();
                break;
            case BroadcastMessageType.evaluation_slide_rating:
                EvaluationSlideRatingBroadcastNetworkMessage evaluationSlideRatingMessage = new EvaluationSlideRatingBroadcastNetworkMessage(messageSegments);
                evaluationSlideRatingMessage.resolve();
                break;
            case BroadcastMessageType.slide_speed:
                SlideSpeedBroadcastNetworkMessage slideSpeedMessage = new SlideSpeedBroadcastNetworkMessage(messageSegments);
                slideSpeedMessage.resolve();
                break;
            case BroadcastMessageType.slide_player_message:
                SlideMessageBroadcastNetworkMessage slideMessage = new SlideMessageBroadcastNetworkMessage(messageSegments);
                slideMessage.resolve();
                break;
            case BroadcastMessageType.show_summary_slide:
                ShowSummarySlideBroadcastNetworkMessage showSummarySlideMessage = new ShowSummarySlideBroadcastNetworkMessage(messageSegments);
                showSummarySlideMessage.resolve();
                break;
            case BroadcastMessageType.show_prompt_slide:
                ShowPromptSlideBroadcastNetworkMessage showPromptSlideMessage = new ShowPromptSlideBroadcastNetworkMessage(messageSegments);
                showPromptSlideMessage.resolve();
                break;
            case BroadcastMessageType.show_drawing_slide:
                ShowDrawingSlideBroadcastNetworkMessage showDrawingSlideMessage = new ShowDrawingSlideBroadcastNetworkMessage(messageSegments);
                showDrawingSlideMessage.resolve();
                break;
            case BroadcastMessageType.show_final_slide:
                ShowFinalSlideBroadcastNetworkMessage showFinalSlideMessage = new ShowFinalSlideBroadcastNetworkMessage(messageSegments);
                showFinalSlideMessage.resolve();
                break;
            case BroadcastMessageType.reset_slide:
                ResetSlideBroadcastNetworkMessage resetSlideMessage = new ResetSlideBroadcastNetworkMessage(messageSegments);
                resetSlideMessage.resolve();
                break;
            case BroadcastMessageType.show_evaluation_drawing_slide:
                ShowEvaluationDrawingSlideBroadcastNetworkMessage showEvaluationDrawingSlideMessage = new ShowEvaluationDrawingSlideBroadcastNetworkMessage(messageSegments);
                showEvaluationDrawingSlideMessage.resolve();
                break;
            case BroadcastMessageType.show_evaluation_summary_slide:
                ShowEvaluationSummarySlideBroadcastNetworkMessage showEvaluationSummarySlideMessage = new ShowEvaluationSummarySlideBroadcastNetworkMessage(messageSegments);
                showEvaluationSummarySlideMessage.resolve();
                break;
            case BroadcastMessageType.reset_evaluation_slide:
                ResetEvaluationSlideBroadcastNetworkMessage resetEvaluationSlideMessage = new ResetEvaluationSlideBroadcastNetworkMessage(messageSegments);
                resetEvaluationSlideMessage.resolve();
                break;
            case BroadcastMessageType.slide_round_end_info:
                SlideRoundEndInfoBroadcastNetworkMessage slideRoundEndInfoMessage = new SlideRoundEndInfoBroadcastNetworkMessage(messageSegments);
                slideRoundEndInfoMessage.resolve();
                break;
            case BroadcastMessageType.slide_round_evaluation_end_info:
                SlideRoundEvaluationInfoBroadcastNetworkMessage slideRoundEvaluationInfoMessage = new SlideRoundEvaluationInfoBroadcastNetworkMessage(messageSegments);
                slideRoundEvaluationInfoMessage.resolve();
                break;
            case BroadcastMessageType.drawing_phase_positions:
                DrawingPhasePositionsBroadcastNetworkMessage drawingPhasePositionsMessage = new DrawingPhasePositionsBroadcastNetworkMessage(messageSegments);
                drawingPhasePositionsMessage.resolve();
                break;
            case BroadcastMessageType.accusation_seat:
                AccusationSeatBroadcastNetworkMessage accusationSeatMessage = new AccusationSeatBroadcastNetworkMessage(messageSegments);
                accusationSeatMessage.resolve();
                break;
            case BroadcastMessageType.accuse_phase_positions:
                AccusePhasePositionsBroadcastNetworkMessage accuseArmPositionsMessage = new AccusePhasePositionsBroadcastNetworkMessage(messageSegments);
                accuseArmPositionsMessage.resolve();
                break;
            case BroadcastMessageType.accusation_change_to_vote:
                AccusationChangeToVoteBroadcastNetworkMessage accusationChangeToVoteMessage = new AccusationChangeToVoteBroadcastNetworkMessage(messageSegments);
                accusationChangeToVoteMessage.resolve();
                break;
            case BroadcastMessageType.accusation:
                AccusationBroadcastNetworkMessage accusationMessage = new AccusationBroadcastNetworkMessage(messageSegments);
                accusationMessage.resolve();
                break;
            case BroadcastMessageType.update_timer:
                UpdateTimerBroadcastNetworkMessage updateTimerMessage = new UpdateTimerBroadcastNetworkMessage(messageSegments);
                updateTimerMessage.resolve();
                break;
            case BroadcastMessageType.chain_details:
                ChainDetailsBroadcastNetworkMessage chainsDetailsMessage = new ChainDetailsBroadcastNetworkMessage(messageSegments);
                chainsDetailsMessage.resolve();
                break;
            case BroadcastMessageType.chain_prompt:
                ChainPromptBroadcastNetworkMessage chainPromptMessage = new ChainPromptBroadcastNetworkMessage(messageSegments);
                chainPromptMessage.resolve();
                break;
            case BroadcastMessageType.resume_accusation:
                ResumeAccusationBroadcastNetworkMessage resumeAccusationMessage = new ResumeAccusationBroadcastNetworkMessage(messageSegments);
                resumeAccusationMessage.resolve();
                break;
            case BroadcastMessageType.remove_player:
                RemovePlayerBroadcastNetworkMessage removePlayerMessage = new RemovePlayerBroadcastNetworkMessage(messageSegments);
                removePlayerMessage.resolve();
                break;
            case BroadcastMessageType.reset_accuse_round:
                ResetAccuseRoundBroadcastNetworkMessage resetAccuseRoundMessage = new ResetAccuseRoundBroadcastNetworkMessage(messageSegments);
                resetAccuseRoundMessage.resolve();
                break;
            case BroadcastMessageType.start_presentation:
                StartPresentationBroadcastNetworkMessage startPresentationMessage = new StartPresentationBroadcastNetworkMessage(messageSegments);
                startPresentationMessage.resolve();
                break;
            case BroadcastMessageType.start_evaluation_presentation:
                StartEvaluationBroadcastNetworkMessage startEvaluationPresentationMessage = new StartEvaluationBroadcastNetworkMessage(messageSegments);
                startEvaluationPresentationMessage.resolve();
                break;
            case BroadcastMessageType.update_presentation_line:
                UpdatePresentationLineBroadcastNetworkMessage updatePresentationLineMessage = new UpdatePresentationLineBroadcastNetworkMessage(messageSegments);
                updatePresentationLineMessage.resolve();
                break;
            case BroadcastMessageType.endgame:
                EndgameBroadcastNetworkMessage endgameMessage = new EndgameBroadcastNetworkMessage(messageSegments);
                endgameMessage.resolve();
                break;
            default:
                Debug.LogError("Implementation of broadcast message["+messageSegments[0]+"] is incomplete.");
                return;
        }
    }

    private void processToPlayerEvent(string[] messageSegments)
    {
        ToPlayerMessageType messageType;
        if (!Enum.TryParse(messageSegments[1], out messageType))
        {
            Debug.LogError("Invalid value provided for message type[" + messageSegments[1] + "] for processing ToPlayer event.");
            string messageToPrint = "";
            foreach (string messageSegment in messageSegments)
            {
                messageToPrint += messageSegment + ",";
            }

            Debug.LogError(messageToPrint);
            return;
        }

        switch (messageType)
        {
            case ToPlayerMessageType.evaluation_details:
                EvaluationDetailsToPlayerNetworkMessage evaluationDetailsMessage = new EvaluationDetailsToPlayerNetworkMessage(messageSegments);
                evaluationDetailsMessage.resolve();
                break;
            case ToPlayerMessageType.add_botcher_round:
                AddBotcherRoundToPlayerNetworkMessage addBotcherRoundMessage = new AddBotcherRoundToPlayerNetworkMessage(messageSegments);
                addBotcherRoundMessage.resolve();
                break;
            case ToPlayerMessageType.add_botcher_prefixes:
                AddBotcherPrefixesToPlayerNetworkMessage addBotcherPrefixesMessage = new AddBotcherPrefixesToPlayerNetworkMessage(messageSegments);
                addBotcherPrefixesMessage.resolve();
                break;
            case ToPlayerMessageType.add_botcher_nouns:
                AddBotcherNounsToPlayerNetworkMessage addBotcherNounsMessage = new AddBotcherNounsToPlayerNetworkMessage(messageSegments);
                addBotcherNounsMessage.resolve();
                break;
            case ToPlayerMessageType.add_botcher_coverup:
                AddBotcherCoverupToPlayerNetworkMessage addBotcherCoverupMessage = new AddBotcherCoverupToPlayerNetworkMessage(messageSegments);
                addBotcherCoverupMessage.resolve();
                break;
            case ToPlayerMessageType.lines:
                DrawingLinesToPlayerNetworkMessage drawingsLinesMessage = new DrawingLinesToPlayerNetworkMessage(messageSegments);
                drawingsLinesMessage.resolve();
                break;
            case ToPlayerMessageType.expected_lines_notification:
                ExpectedDrawingLinesNotificationToPlayerNetworkMessage drawingLinesPackageNotificationMessage = new ExpectedDrawingLinesNotificationToPlayerNetworkMessage(messageSegments);
                drawingLinesPackageNotificationMessage.resolve();
                break;
            case ToPlayerMessageType.ready_for_lines:
                ReadyForDrawingLinesToPlayerNetworkMessage readyForDrawingLinesMessage = new ReadyForDrawingLinesToPlayerNetworkMessage(messageSegments);
                readyForDrawingLinesMessage.resolve();
                break;
            case ToPlayerMessageType.initial_cabinet_prompt_contents:
                InitialPromptContentsToPlayerNetworkMessage initialPromptContentsMessage = new InitialPromptContentsToPlayerNetworkMessage(messageSegments);
                initialPromptContentsMessage.resolve();
                break;
            case ToPlayerMessageType.drawing_package_confirmation:
                DrawingPackageConfirmationToPlayerNetworkMessage drawingPackageConfirmationMessage = new DrawingPackageConfirmationToPlayerNetworkMessage(messageSegments);
                drawingPackageConfirmationMessage.resolve();
                break;
            case ToPlayerMessageType.role_distribution:
                RoleDistributionToPlayerNetworkMessage roleDistributionMessage = new RoleDistributionToPlayerNetworkMessage(messageSegments);
                roleDistributionMessage.resolve();
                break;
            case ToPlayerMessageType.possible_prefixes:
                PossiblePrefixesToPlayerNetworkMessage possiblePrefixesMessage = new PossiblePrefixesToPlayerNetworkMessage(messageSegments);
                possiblePrefixesMessage.resolve();
                break;
            case ToPlayerMessageType.possible_nouns:
                PossibleNounsToPlayerNetworkMessage possibleNounsMessage = new PossibleNounsToPlayerNetworkMessage(messageSegments);
                possibleNounsMessage.resolve();
                break;
            case ToPlayerMessageType.cabinet_prompt_contents:
                CabinetPromptContentsToPlayerNetworkMessage cabinetPromptContentsMessage = new CabinetPromptContentsToPlayerNetworkMessage(messageSegments);
                cabinetPromptContentsMessage.resolve();
                break;
            case ToPlayerMessageType.show_botcher_correct_option:
                ShowBotcherCorrectOptionToPlayerNetworkMessage showBotcherCorrectOptionMessage = new ShowBotcherCorrectOptionToPlayerNetworkMessage(messageSegments);
                showBotcherCorrectOptionMessage.resolve();
                break;
            case ToPlayerMessageType.force_submit:
                ForceSubmitToPlayerNetworkMessage forceSubmitMessage = new ForceSubmitToPlayerNetworkMessage(messageSegments);
                forceSubmitMessage.resolve();
                break;
            case ToPlayerMessageType.force_submit_response:
                ForceSubmitResponseToPlayerNetworkMessage forceSubmitResponseMessage = new ForceSubmitResponseToPlayerNetworkMessage(messageSegments);
                forceSubmitResponseMessage.resolve();
                break;
            case ToPlayerMessageType.empty_drawing:
                EmptyDrawingToPlayerNetworkMessage emptyDrawingMessage = new EmptyDrawingToPlayerNetworkMessage(messageSegments);
                emptyDrawingMessage.resolve();
                break;
            case ToPlayerMessageType.response_round_correct_guess:
                ResponseRoundCorrectGuessToPlayerNetworkMessage responseRoundCorrectGuessMessage = new ResponseRoundCorrectGuessToPlayerNetworkMessage(messageSegments);
                responseRoundCorrectGuessMessage.resolve();
                break;
            case ToPlayerMessageType.review_player:
                ReviewPlayerToPlayerNetworkMessage reviewPlayerMessage = new ReviewPlayerToPlayerNetworkMessage(messageSegments);
                reviewPlayerMessage.resolve();
                break;
            default:
                Debug.LogError("Implementation of ToPlayer message[" + messageSegments[1] + "] is incomplete.");
                return;
        }
    }

    private void processToServerEvent(string[] messageSegments, string originalMessage = "")
    {
        ToServerMessageType messageType;
        if (!Enum.TryParse(messageSegments[0], out messageType))
        {
            Debug.LogError("Invalid value provided for message type[" + messageSegments[0] + "] for processing ToServer event.");
            string messageToPrint = "";
            foreach (string messageSegment in messageSegments)
            {
                messageToPrint += messageSegment + ",";
            }

            Debug.LogError(messageToPrint);
            return;
        }

        switch (messageType)
        {
            case ToServerMessageType.transition_condition:
                TransitionConditionToServerNetworkMessage transitionConditionMessage = new TransitionConditionToServerNetworkMessage(messageSegments);
                transitionConditionMessage.resolve();
                break;
            case ToServerMessageType.update_bird_folder_status:
                UpdateHeldBirdFolderStatusToServerNetworkMessage updateBirdFolderStatusMessage = new UpdateHeldBirdFolderStatusToServerNetworkMessage(messageSegments);
                updateBirdFolderStatusMessage.resolve();
                break;
            case ToServerMessageType.hide_cabinet_folder:
                HideCabinetFolderToServerNetworkMessage hideCabinetFolderMessage = new HideCabinetFolderToServerNetworkMessage(messageSegments);
                hideCabinetFolderMessage.resolve();
                break;
            case ToServerMessageType.ready_for_lines:
                ReadyForDrawingLinesToServerNetworkMessage drawingPackageConfirmationMessage = new ReadyForDrawingLinesToServerNetworkMessage(messageSegments);
                drawingPackageConfirmationMessage.resolve();
                break;
            case ToServerMessageType.expected_lines_notification:
                ExpectedDrawingLinesNotificationToServerNetworkMessage drawingLinesNotificationMessage = new ExpectedDrawingLinesNotificationToServerNetworkMessage(messageSegments);
                drawingLinesNotificationMessage.resolve();
                break;
            case ToServerMessageType.lines:
                DrawingLinesToServerNetworkMessage drawingLinesMessage = new DrawingLinesToServerNetworkMessage(messageSegments);
                drawingLinesMessage.resolve();
                break;
            case ToServerMessageType.prompt:
                PromptToServerNetworkMessage promptMessage = new PromptToServerNetworkMessage(messageSegments);
                promptMessage.resolve();
                break;
            case ToServerMessageType.access_drawer:
                AccessDrawerToServerNetworkMessage accessDrawerMessage = new AccessDrawerToServerNetworkMessage(messageSegments);
                accessDrawerMessage.resolve();
                break;
            case ToServerMessageType.botcher_prefix_option:
                BotcherPrefixOptionToServerNetworkMessage botcherPrefixOptionMessage = new BotcherPrefixOptionToServerNetworkMessage(messageSegments);
                botcherPrefixOptionMessage.resolve();
                break;
            case ToServerMessageType.botcher_noun_option:
                BotcherNounOptionToServerNetworkMessage botcherNounOptionMessage = new BotcherNounOptionToServerNetworkMessage(messageSegments);
                botcherNounOptionMessage.resolve();
                break;
            case ToServerMessageType.add_botcher_coverup:
                AddBotcherCoverupToServerNetworkMessage addBotcherCoverupMessage = new AddBotcherCoverupToServerNetworkMessage(messageSegments);
                addBotcherCoverupMessage.resolve();
                break;
            case ToServerMessageType.prompt_guess:
                GuessToServerNetworkMessage guessMessage = new GuessToServerNetworkMessage(messageSegments);
                guessMessage.resolve();
                break;
            case ToServerMessageType.accuse_player:
                AccuseToServerNetworkMessage accuseMessage = new AccuseToServerNetworkMessage(messageSegments);
                accuseMessage.resolve();
                break;
            case ToServerMessageType.drawing_arm_position:
                DrawingArmPositionToServerNetworkMessage drawingArmPositionMessage = new DrawingArmPositionToServerNetworkMessage(messageSegments);
                drawingArmPositionMessage.resolve();
                break;
            case ToServerMessageType.accuse_arm_position:
                AccuseArmPositionToServerNetworkMessage accuseArmPositionMessage = new AccuseArmPositionToServerNetworkMessage(messageSegments);
                accuseArmPositionMessage.resolve();
                break;
            case ToServerMessageType.slide_round_message:
                SlidePlayerMessageToServerNetworkMessage slideRoundMessage = new SlidePlayerMessageToServerNetworkMessage(messageSegments);
                slideRoundMessage.resolve();
                break;
            case ToServerMessageType.rate_slide:
                RateSlideToServerNetworkMessage rateSlideMessage = new RateSlideToServerNetworkMessage(messageSegments);
                rateSlideMessage.resolve();
                break;
            case ToServerMessageType.rate_evaluation_slide:
                RateEvaluationSlideToServerNetworkMessage rateEvaluationSlideMessage = new RateEvaluationSlideToServerNetworkMessage(messageSegments);
                rateEvaluationSlideMessage.resolve();
                break;
            case ToServerMessageType.player_connected:
                PlayerConnectedToServerNetworkMessage playerConnectedMessage = new PlayerConnectedToServerNetworkMessage(messageSegments);
                playerConnectedMessage.resolve();
                break;
            case ToServerMessageType.cabinet_time_taken:
                CabinetTimeTakenToServerNetworkMessage cabinetTimeTakenMessage = new CabinetTimeTakenToServerNetworkMessage(messageSegments);
                cabinetTimeTakenMessage.resolve();
                break;
            case ToServerMessageType.review_player:
                RequestReviewToServerNetworkMessage reviewRequestMessage = new RequestReviewToServerNetworkMessage(messageSegments);
                reviewRequestMessage.resolve();
                break;
            case ToServerMessageType.speed_up_slides:
                SpeedUpSlidesToServerNetworkMessage speedUpSlidesMessage = new SpeedUpSlidesToServerNetworkMessage(messageSegments);
                speedUpSlidesMessage.resolve();
                break;
            case ToServerMessageType.drawer_released:
                DrawerReleasedToServerNetworkMessage drawerReleasedMessage = new DrawerReleasedToServerNetworkMessage(messageSegments);
                drawerReleasedMessage.resolve();
                break;
            case ToServerMessageType.empty_drawing:
                EmptyDrawingToServerNetworkMessage emptyDrawingMessage = new EmptyDrawingToServerNetworkMessage(messageSegments);
                emptyDrawingMessage.resolve();
                break;
            case ToServerMessageType.start_presentation:
                StartPresentationToServerNetworkMessage startPresentationMessage = new StartPresentationToServerNetworkMessage(messageSegments);
                startPresentationMessage.resolve();
                break;
            case ToServerMessageType.start_evaluation_presentation:
                StartEvaluationPresentationToServerNetworkMessage startEvaluationPresentationMessage = new StartEvaluationPresentationToServerNetworkMessage(messageSegments);
                startEvaluationPresentationMessage.resolve();
                break;
            case ToServerMessageType.update_presentation_line:
                UpdatePresentationLineToServerNetworkMessage updatePresentationLineMessage = new UpdatePresentationLineToServerNetworkMessage(messageSegments);
                updatePresentationLineMessage.resolve();
                break;
            case ToServerMessageType.resume_accusation:
                ResumeAccusationToServerNetworkMessage resumeAccusationMessage = new ResumeAccusationToServerNetworkMessage(messageSegments);
                resumeAccusationMessage.resolve();
                break;
            default:
                Debug.LogError("Implementation of ToServer message[" + messageSegments[0] + "] is incomplete.");
                return;
        }
    }

    public string getCodeFromBirdName(BirdName inBirdName)
    {
        switch (inBirdName)
        {
            case ColourManager.BirdName.red:
                return "red";
            case ColourManager.BirdName.green:
                return "green";
            case ColourManager.BirdName.blue:
                return "blue";
            case ColourManager.BirdName.grey:
                return "grey";
            case ColourManager.BirdName.purple:
                return "purple";
            case ColourManager.BirdName.brown:
                return "brown";
            case ColourManager.BirdName.black:
                return "black";
            case ColourManager.BirdName.orange:
                return "orange";
        }
        return "";
    }

    public static BirdName getBirdNameFromCode(string inCode)
    {
        switch (inCode)
        {
            case "red":
                return ColourManager.BirdName.red;
            case "blue":
                return ColourManager.BirdName.blue;
            case "green":
                return ColourManager.BirdName.green;
            case "grey":
                return ColourManager.BirdName.grey;
            case "purple":
                return ColourManager.BirdName.purple;
            case "brown":
                return ColourManager.BirdName.brown;
            case "black":
                return ColourManager.BirdName.black;
            case "orange":
                return ColourManager.BirdName.orange;
        }

        return ColourManager.BirdName.none;
    }

    public static GameFlowManager.GamePhase getGamePhaseFromCode(string inCode)
    {
        switch (inCode)
        {
            case "accusation":
                return GameFlowManager.GamePhase.accusation;
            case "responses":
                return GameFlowManager.GamePhase.responses;
            case "drawing_tutorial":
                return GamePhase.drawing_tutorial;
            case "slides_tutorial":
                return GamePhase.slides_tutorial;
            case "accusation_tutorial":
                return GamePhase.accusation_tutorial;
            case "drawing":
                return GameFlowManager.GamePhase.drawing;
            case "results":
                return GameFlowManager.GamePhase.results;
            case "instructions":
                return GameFlowManager.GamePhase.instructions;
            case "slides":
                return GameFlowManager.GamePhase.slides;
            case "reveal":
                return GameFlowManager.GamePhase.reveal;
            case "accolades":
                return GameFlowManager.GamePhase.accolades;
            case "loading":
                return GameFlowManager.GamePhase.loading;
        }

        return GameFlowManager.GamePhase.invalid;
    }

    public static DrawingRound.CurrentState getDrawingRoundStateFromCode(string inCode)
    {
        switch (inCode)
        {
            case "drawing":
                return DrawingRound.CurrentState.drawing;
            case "evaluation":
                return DrawingRound.CurrentState.evaluation;
            case "prompting":
                return DrawingRound.CurrentState.prompting;
            default:
                return DrawingRound.CurrentState.invalid;
        }
    }

    public static GameFlowManager.PlayerRole getPlayerRoleFromCode(string inCode)
    {
        switch (inCode)
        {
            case "worker":
                return GameFlowManager.PlayerRole.worker;
            case "botcher":
                return GameFlowManager.PlayerRole.botcher;
        }

        return GameFlowManager.PlayerRole.invalid;
    }

    public static BotcherMode getBotcherModeFromCode(string inCode)
    {
        switch (inCode)
        {
            case "limited_drawing":
                return BotcherMode.limited_drawing;
            case "coverup":
                return BotcherMode.coverup;
            case "add_prompts":
                return BotcherMode.add_prompts;
            default:
                return BotcherMode.invalid;
        }
    }

    public void GameOver_ReturnToLobby()
    {
        SettingsManager.Instance.disconnected = false;
        if (PhotonNetwork.IsMasterClient)
        {
            LobbyNetwork.Instance.BroadcastQueue.Add("HostJoiningLobby");
            BroadcastQueue.Add("host_joining_lobby");
            SendEvents();
            LobbyNetwork.Instance.SendEvents();
        }

        SceneManager.LoadScene(0);
    }

    public void Disconnected_ReturnToLobby()
    {
        SettingsManager.Instance.disconnected = true;
        SceneManager.LoadScene(0);
    }

    

    public abstract class NetworkMessage
    {
        public enum SendType
        {
            broadcast, to_player, to_server
        }

        public string[] messageSegments;

        public NetworkMessage(string[] inMessageSegments)
        {
            messageSegments = inMessageSegments;
        }

        public void printMessageSegments()
        {
            string messageToPrint = "";
            foreach(string messageSegment in messageSegments)
            {
                messageToPrint += messageSegment + ",";
            }

            Debug.LogError(messageToPrint);
        }

        public bool validateSegmentCount(int expectedCount, string messageName)
        {
            if (messageSegments.Length != expectedCount)
            {
                Debug.LogError("Invalid number of segments in message[" + messageSegments.Length + "] for "+messageName+" message.");
                printMessageSegments();
                return false;
            }
            return true;
        }

        public abstract bool resolve();
    }

    #region Broadcast Messages

    public abstract class BroadcastNetworkMessage : NetworkMessage
    {
        public enum BroadcastMessageType
        {
            host_joining_lobby,
            server_is_ready,
            game_initialization,
            folder_colour_initialization,
            update_bird_folder_status,
            hide_cabinet_folder,
            access_drawer,
            cabinet_drawer_is_ready,
            evaluation_drawer_is_ready,
            update_drawing_round_state,
            drawing_phase_end_info,
            update_phase,
            slide_player_message,
            show_prompt_slide,
            show_drawing_slide,
            show_summary_slide,
            show_final_slide,
            show_evaluation_drawing_slide,
            show_evaluation_summary_slide,
            reset_evaluation_slide,
            reset_slide,
            slide_rating,
            evaluation_slide_rating,
            slide_speed,
            slide_round_end_info,
            slide_round_evaluation_end_info,
            drawing_phase_positions,
            accusation_seat,
            accuse_phase_positions,
            accusation_change_to_vote,
            accusation,
            update_timer,
            chain_details, 
            chain_prompt,
            start_presentation,
            start_evaluation_presentation,
            update_presentation_line,
            reset_accuse_round,
            resume_accusation,
            remove_player,
            endgame
        }

        public BroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }
    }

    public class HostJoiningLobbyBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public HostJoiningLobbyBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(1, "host joining lobby"))
            {
                return;
            }
        }

        public override bool resolve()
        {
            SettingsManager.Instance.isHostInLobby = true;
            return true;
        }
    }

    public class ServerIsReadyBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ServerIsReadyBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(1, "server is ready"))
            {
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.serverIsReady = true;
            return true;
        }
    }

    public class GameInitializationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        private int numberOfRounds = -1;
        private Dictionary<BirdName, string> playerNameMap = new Dictionary<BirdName, string>();

        public GameInitializationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "game initialization"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out numberOfRounds))
            {
                Debug.LogError("Invalid message segment for the number of rounds["+messageSegments[1]+"] in game initialization broadcast network message.");
                printMessageSegments();
                return;
            }

            string[] playerColours = messageSegments[2].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            string[] playerNames = messageSegments[3].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

            if(playerColours.Length != playerNames.Length)
            {
                Debug.LogError("Number of players[" + playerNames.Length + "] doesn't match the number of colours[" + playerColours.Length + "] in game initialization broadcast network message.");
                printMessageSegments();
                return;
            }

            BirdName currentBirdName;
            for(int i = 0; i < playerColours.Length; i++)
            {
                currentBirdName = getBirdNameFromCode(playerColours[i]);
                if(currentBirdName == BirdName.none)
                {
                    Debug.LogError("Invalid bird colour["+playerColours[i]+"] for player["+playerNames[i]+"] in game initialization broadcast network message.");
                    printMessageSegments();
                    return;
                }

                if (playerNameMap.ContainsKey(currentBirdName))
                {
                    Debug.LogError("Bird colour["+playerColours[i]+"] has been added multiple times to the player name map in game initialization broadcast network message.");
                    printMessageSegments();
                    return;
                }
                playerNameMap.Add(currentBirdName, playerNames[i]);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.numberOfCabinetRounds = numberOfRounds;
            GameManager.Instance.playerFlowManager.playerNameMap = playerNameMap;

            return true;
        }
    }

    public class FolderColourInitializationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetID = -1;
        BirdName guesserColour = BirdName.none;

        public FolderColourInitializationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(3, "folder colour initialization broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for folder colour initialization broadcast message.");
                printMessageSegments();
                return;
            }

            guesserColour = getBirdNameFromCode(messageSegments[2]);
            if(guesserColour == BirdName.none)
            {
                Debug.LogError("Invalid guesser["+messageSegments[2]+"] provided for folder colour initialization broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.initializeFolderColour(cabinetID, guesserColour);
            return true;
        }
    }

    public class UpdateBirdFolderStatusBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        BirdName birdName = BirdName.none;
        string folderStatusField = "";
        bool folderStatus = false;
        Color folderColour;

        public UpdateBirdFolderStatusBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "update bird folder status broadcast"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid bird name["+messageSegments[1]+"] provided to update bird folder status broadcast message.");
                printMessageSegments();
                return;
            }

            folderStatusField = messageSegments[2];
            switch (folderStatusField)
            {
                case "grabbed":
                    folderStatus = true;
                    break;
                case "released":
                    folderStatus = false;
                    break;
                default:
                    Debug.LogError("Invalid folder status value["+messageSegments[2]+"] provided to update bird folder status broadcast message.");
                    printMessageSegments();
                    return;
            }

            float r, g, b, a;
            if (!float.TryParse(messageSegments[3], out r) ||
                !float.TryParse(messageSegments[4], out g) ||
                !float.TryParse(messageSegments[5], out b) ||
                !float.TryParse(messageSegments[6], out a))
            {
                Debug.LogError("Invalid colour values provided to update bird folder status broadcast message.");
                printMessageSegments();
                return;
            }
            folderColour = new Color(r, g, b, a);
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.updateBirdFolderStatus(birdName, folderColour, folderStatus);
            return true;
        }
    }

    public class HideCabinetFolderBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetID = -1;

        public HideCabinetFolderBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(2, "hide cabinet folder broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided to hide cabinet folder broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].folderObject.SetActive(false);
            return true;
        }
    }

    public class AccessDrawerBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        private int cabinetIndex = -1;

        public AccessDrawerBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "access drawer"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out cabinetIndex))
            {
                Debug.LogError("Invalid message segment for the cabinetIndex[" + messageSegments[1] + "] in cabinet is started broadcast network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setDrawerAsUsed(cabinetIndex);
            return true;
        }
    }

    public class CabinetDrawerIsReadyBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        DrawingRound.CurrentState currentState = DrawingRound.CurrentState.invalid;
        int cabinetIndex = -1;
        int roundNumber = -1;
        BirdName birdName = BirdName.none;

        public CabinetDrawerIsReadyBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(5, "cabinet drawer is ready"))
            {
                return;
            }

            currentState = getDrawingRoundStateFromCode(messageSegments[1]);
            if(currentState == DrawingRound.CurrentState.invalid)
            {
                Debug.LogError("Invalid drawing round state["+messageSegments[1]+"] provided for cabinet drawer is ready broadcast message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[2], out cabinetIndex))
            {
                Debug.LogError("Invalid message segment for the cabinet index[" + messageSegments[2] + "] in cabinet drawer is ready broadcast network message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[3], out roundNumber))
            {
                Debug.LogError("Invalid message segments for round number["+messageSegments[3]+"] in cabinet drawer is ready broadcast network message.");
                printMessageSegments();
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[4]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid message segments for bird colour["+messageSegments[4]+"] in cabinet drawer is ready broadcast network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.currentState = currentState;
            GameManager.Instance.playerFlowManager.drawingRound.currentPlayerCabinetRoundIndex = roundNumber;
            GameManager.Instance.playerFlowManager.drawingRound.getCabinet(cabinetIndex).setAsReady(birdName);
            return true;
        }
    }

    public class EvaluationDrawerIsReadyBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetID = -1;
        int evaluationPhase = -1;
        BirdName bird = BirdName.none;

        public EvaluationDrawerIsReadyBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(4, "evaluation drawer is ready broadcast"))
            {
                return;
            }
            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for evaluation drawer is ready broadcast message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[2], out evaluationPhase))
            {
                Debug.LogError("Invalid evaluation round number["+messageSegments[2]+"] provided for evaluation drawer is ready broadcast message.");
                printMessageSegments();
                return;
            }
            bird = getBirdNameFromCode(messageSegments[3]);
            if(bird == BirdName.none)
            {
                Debug.LogError("Invalid player["+messageSegments[3]+"] provided for evaluation drawer is ready broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.currentState = DrawingRound.CurrentState.evaluation;
           GameManager.Instance.playerFlowManager.drawingRound.currentEvaluationPhase = evaluationPhase;
            GameManager.Instance.playerFlowManager.drawingRound.getCabinet(cabinetID).setAsReady(bird);
            return true;
        }
    }

    public class UpdateDrawingRoundStateBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        DrawingRound.CurrentState state = DrawingRound.CurrentState.invalid;

        public UpdateDrawingRoundStateBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(2,"update drawing round state broadcast"))
            {
                return;
            }

            state = getDrawingRoundStateFromCode(messageSegments[1]);
            if(state == DrawingRound.CurrentState.invalid)
            {
                Debug.LogError("Invalid drawing round state["+messageSegments[1]+"] provided to update drawing round state broadcast.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.currentState = state;
            return true;
        }
    }

    //Send just the results of each cabinet to each player
    public class ChainResultsInfoBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetIndex = -1;
        BirdName guesser = BirdName.none;
        string prefixGuess = "";
        string nounGuess = "";
        string correctPrefix = "";
        string correctNoun = "";

        public ChainResultsInfoBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "chain results info"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out cabinetIndex))
            {
                Debug.LogError("Invalid cabinet index[" + messageSegments[1] + "] provided to drawing chain results broadcast message.");
                printMessageSegments();
                return;
            }

            guesser = getBirdNameFromCode(messageSegments[2]);
            prefixGuess = messageSegments[3];
            nounGuess = messageSegments[4];
            correctPrefix = messageSegments[5];
            correctNoun = messageSegments[6];
        }

        public override bool resolve()
        {
            CabinetDrawer currentDrawer = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetIndex];
            currentDrawer.chainData.guesser = guesser;
            currentDrawer.chainData.prefixGuess = prefixGuess;
            currentDrawer.chainData.nounGuess = nounGuess;
            currentDrawer.chainData.correctPrefix = correctPrefix;
            currentDrawer.chainData.correctNoun = correctNoun;

            return true;
        }
    }

    public class SlideMessageBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        BirdName messagingBird = BirdName.none;
        string message = "";

        public SlideMessageBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "slide message"))
            {
                return;
            }

            messagingBird = getBirdNameFromCode(messageSegments[1]);
            if (messagingBird == BirdName.none)
            {
                Debug.LogError("Invalid bird colour provided[" + messageSegments[1] + "] for slide message broadcast message.");
                printMessageSegments();
                return;
            }

            message = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.setPeanutGalleryMessage(messagingBird, message);
            return true;
        }
    }

    public class ShowSummarySlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowSummarySlideBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowSummarySlide();
            return true;
        }
    }

    public class ShowPromptSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowPromptSlideBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowPromptSlide();
            return true;
        }
    }

    public class ShowDrawingSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowDrawingSlideBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowDrawingSlide();
            return true;
        }
    }

    public class ShowFinalSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowFinalSlideBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowFinalSlide();
            return true;
        }
    }

    public class ResetSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int slideIndex = -1;
        public ResetSlideBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "reset slide broadcast"))
            {
                return;
            }
            if(!int.TryParse(messageSegments[1], out slideIndex))
            {
                Debug.LogError("Invalid slide index["+messageSegments[1]+"] provided to reset slide broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ResetSlide(slideIndex);
            return true;
        }
    }

    public class ShowEvaluationDrawingSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowEvaluationDrawingSlideBroadcastNetworkMessage(string[] inMessages) : base(inMessages)
        {

        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowEvaluationDrawingSlide();
            return true;

        }
    }

    public class ShowEvaluationSummarySlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ShowEvaluationSummarySlideBroadcastNetworkMessage(string[] inMessages) : base(inMessages)
        {

        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ShowEvaluationSummarySlide();
            return true;
        }
    }

    public class ResetEvaluationSlideBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int slideRoundIndex = -1;
        public ResetEvaluationSlideBroadcastNetworkMessage(string[] inMessages): base(inMessages)
        {
            if (!validateSegmentCount(2, "reset evaluation slide broadcast"))
            {
                return;
            }
            if (!int.TryParse(messageSegments[1], out slideRoundIndex))
            {
                Debug.LogError("Invalid slide round index["+messageSegments[1]+"] provided to reset evaluation slide broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.ResetEvaluationSlide(slideRoundIndex);
            return true;
        }
    }

    public class SlideRatingBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetIndex = -1;
        int tabIndex = -1;
        string rating = "";

        public SlideRatingBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "slide rating"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out cabinetIndex))
            {
                Debug.LogError("Invalid message segment for the cabinet index[" + messageSegments[1] + "] in slide rating broadcast network message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[2], out tabIndex))
            {
                Debug.LogError("Invalid message segment for the tab index[" + messageSegments[1] + "] in slide rating broadcast network message.");
                printMessageSegments();
                return;
            }


            if (messageSegments[3] == "like" ||
                messageSegments[3] == "dislike")
            {
                rating = messageSegments[3];
            }
            else
            {
                Debug.LogError("Invalid rating value[" + messageSegments[3] + "] sent for slide rating broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            if (rating == "like")
            {
                //GameManager.Instance.playerFlowManager.slidesRound.addLike(cabinetIndex, tabIndex);
                GameManager.Instance.playerFlowManager.slidesRound.showLikedObject(cabinetIndex, tabIndex);
            }
            else if (rating == "dislike")
            {
                //GameManager.Instance.playerFlowManager.slidesRound.addDislike(cabinetIndex, tabIndex);
                GameManager.Instance.playerFlowManager.slidesRound.showDislikedObject(cabinetIndex, tabIndex);
            }
            else
            {
                return false;
            }
            return true;

        }
    }

    public class EvaluationSlideRatingBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetIndex = -1;
        int round = -1;
        string rating = "";
        public EvaluationSlideRatingBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "evaluation slide rating"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out cabinetIndex))
            {
                Debug.LogError("Invalid message segment for the cabinet index[" + messageSegments[1] + "] in evaluation slide rating broadcast network message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid message segment for the round[" + messageSegments[1] + "] in evaluation slide rating broadcast network message.");
                printMessageSegments();
                return;
            }


            if (messageSegments[3] == "like" ||
                messageSegments[3] == "dislike")
            {
                rating = messageSegments[3];
            }
            else
            {
                Debug.LogError("Invalid rating value[" + messageSegments[3] + "] sent for evaluation slide rating broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            if (rating == "like")
            {
                GameManager.Instance.playerFlowManager.slidesRound.showEvaluationLikedObject(cabinetIndex, round);
            }
            else if (rating == "dislike")
            {
                GameManager.Instance.playerFlowManager.slidesRound.showEvaluationDislikedObject(cabinetIndex, round);
            }
            else
            {
                return false;
            }
            return true;
        }
    }

    public class SlideSpeedBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        float slideSpeed = -1f;

        public SlideSpeedBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "slide speed"))
            {
                return;
            }

            if (!float.TryParse(messageSegments[1], out slideSpeed))
            {
                Debug.LogError("Invalid message segment for the slide speed[" + messageSegments[1] + "] in slide speed broadcast network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.setSlideSpeed(slideSpeed);
            return true;
        }
    }

    public class SlideRoundEndInfoBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        List<int> cabinetIDs = new List<int>();
        Dictionary<int,Dictionary<int, int>> roundLikes = new Dictionary<int,Dictionary<int, int>>();
        Dictionary<int,Dictionary<int, int>> roundDislikes = new Dictionary<int,Dictionary<int, int>>();
        Dictionary<int, Dictionary<int, BirdName>> targets = new Dictionary<int, Dictionary<int, BirdName>>();

        public SlideRoundEndInfoBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            string[] cabinetSegments;
            string[] tabSegments;
            string[] ratingSegments;
            int currentCabinetID;
            int currentRound;
            int currentLikeCount;
            int currentDislikeCount;
            BirdName currentBirdName;
            for (int i = 1; i < messageSegments.Length; i++)
            {
                cabinetSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

                if(cabinetSegments.Length != 2)
                {
                    Debug.LogError("Invalid length["+cabinetSegments.Length+"] for cabinet segments within slide round end info message.");
                    printMessageSegments();
                    continue;
                }

                if (!int.TryParse(cabinetSegments[0], out currentCabinetID))
                {
                    Debug.LogError("Invalid cabinet["+cabinetSegments[0]+"] for segment within slide round end info message.");
                    printMessageSegments();
                    return;
                }
                cabinetIDs.Add(currentCabinetID);
                if (!roundLikes.ContainsKey(currentCabinetID))
                {
                    roundLikes.Add(currentCabinetID, new Dictionary<int, int>());
                    
                }
                if (!roundDislikes.ContainsKey(currentCabinetID))
                {
                    roundDislikes.Add(currentCabinetID, new Dictionary<int, int>());
                }
                if (!targets.ContainsKey(currentCabinetID))
                {
                    targets.Add(currentCabinetID, new Dictionary<int, BirdName>());
                }

                tabSegments = cabinetSegments[1].Split(new string[] { GameDelim.ITEM }, StringSplitOptions.None);

                foreach (string tabSegment in tabSegments)
                {
                    ratingSegments = tabSegment.Split(new string[] { GameDelim.RATING }, StringSplitOptions.None);

                    if(ratingSegments.Length == 1)
                    {
                        continue;
                    }
                    if (ratingSegments.Length != 4)
                    {
                        Debug.LogError("Invalid length["+ratingSegments.Length.ToString()+"] for ratings segment within slide round end info message.");
                        printMessageSegments();
                        continue;
                    }
                    if(!int.TryParse(ratingSegments[0], out currentRound))
                    {
                        Debug.LogError("Invalid round["+ratingSegments[0]+"] for segment within slide round end info message.");
                        printMessageSegments();
                        return;
                    }
                    
                    if(!int.TryParse(ratingSegments[1], out currentLikeCount))
                    {
                        Debug.LogError("Invalid like count["+ratingSegments[1]+"] for segment within slide round end info message.");
                        printMessageSegments();
                        return;
                    }
                    if(!int.TryParse(ratingSegments[2], out currentDislikeCount))
                    {
                        Debug.LogError("Invalid dislike count["+ratingSegments[2]+"] for segment within slide round end info message.");
                        printMessageSegments();
                        return;
                    }
                    currentBirdName = getBirdNameFromCode(ratingSegments[3]);
                    if(currentBirdName == BirdName.none)
                    {
                        Debug.LogError("Invalid bird colour["+ratingSegments[3]+"] for segment within slide round end info message.");
                        printMessageSegments();
                        return;
                    }
                    if (!roundLikes[currentCabinetID].ContainsKey(currentRound))
                    {
                        roundLikes[currentCabinetID].Add(currentRound, currentLikeCount);
                    }
                    if (!roundDislikes[currentCabinetID].ContainsKey(currentRound))
                    {
                        roundDislikes[currentCabinetID].Add(currentRound, currentDislikeCount);
                    }
                    if (!targets[currentCabinetID].ContainsKey(currentRound))
                    {
                        targets[currentCabinetID].Add(currentRound, currentBirdName);
                    }
                }
            }

        }

        public override bool resolve()
        {
            foreach(int cabinetID in cabinetIDs)
            {
                GameManager.Instance.playerFlowManager.slidesRound.updateChainRatings(cabinetID, roundLikes[cabinetID], roundDislikes[cabinetID], targets[cabinetID]);
            }

            GameManager.Instance.playerFlowManager.slidesRound.hasReceivedRatings = true;

            if (GameManager.Instance.playerFlowManager.slidesRound.hasReceivedEvaluationRatings)
            {
                Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "ratings_loaded:" + SettingsManager.Instance.birdName);
            }

            return true;
        }
    }

    public class SlideRoundEvaluationInfoBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        List<int> rounds = new List<int>();
        Dictionary<int, Dictionary<int, int>> roundLikes = new Dictionary<int, Dictionary<int, int>>();
        Dictionary<int, Dictionary<int, int>> roundDislikes = new Dictionary<int, Dictionary<int, int>>();
        Dictionary<int, Dictionary<int, BirdName>> targets = new Dictionary<int, Dictionary<int, BirdName>>();

        public SlideRoundEvaluationInfoBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            string[] roundSegments;
            string[] cabinetSegments;
            string[] ratingSegments;
            int currentCabinetID;
            int currentRound;
            int currentLikeCount;
            int currentDislikeCount;
            BirdName currentBirdName;
            for (int i = 1; i < messageSegments.Length; i++)
            {
                roundSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

                if (roundSegments.Length != 2)
                {
                    Debug.LogError("Invalid length[" + roundSegments.Length + "] for cabinet segments within slide round evaluation end info message.");
                    printMessageSegments();
                    continue;
                }

                if (!int.TryParse(roundSegments[0], out currentRound))
                {
                    Debug.LogError("Invalid cabinet[" + roundSegments[0] + "] for segment within slide round evaluation end info message.");
                    printMessageSegments();
                    return;
                }
                rounds.Add(currentRound);
                if (!roundLikes.ContainsKey(currentRound))
                {
                    roundLikes.Add(currentRound, new Dictionary<int, int>());

                }
                if (!roundDislikes.ContainsKey(currentRound))
                {
                    roundDislikes.Add(currentRound, new Dictionary<int, int>());
                }
                if (!targets.ContainsKey(currentRound))
                {
                    targets.Add(currentRound, new Dictionary<int, BirdName>());
                }

                cabinetSegments = roundSegments[1].Split(new string[] { GameDelim.ITEM }, StringSplitOptions.None);

                foreach (string cabinetSegment in cabinetSegments)
                {
                    ratingSegments = cabinetSegment.Split(new string[] { GameDelim.RATING }, StringSplitOptions.None);

                    if (ratingSegments.Length == 1)
                    {
                        continue;
                    }
                    if (ratingSegments.Length != 4)
                    {
                        Debug.LogError("Invalid length[" + ratingSegments.Length.ToString() + "] for ratings segment within slide round evaluation end info message.");
                        printMessageSegments();
                        continue;
                    }
                    if (!int.TryParse(ratingSegments[0], out currentCabinetID))
                    {
                        Debug.LogError("Invalid cabinet[" + ratingSegments[0] + "] for segment within slide round evaluation end info message.");
                        printMessageSegments();
                        return;
                    }

                    if (!int.TryParse(ratingSegments[1], out currentLikeCount))
                    {
                        Debug.LogError("Invalid like count[" + ratingSegments[1] + "] for segment within slide round evaluation end info message.");
                        printMessageSegments();
                        return;
                    }
                    if (!int.TryParse(ratingSegments[2], out currentDislikeCount))
                    {
                        Debug.LogError("Invalid dislike count[" + ratingSegments[2] + "] for segment within slide round evaluation end info message.");
                        printMessageSegments();
                        return;
                    }
                    currentBirdName = getBirdNameFromCode(ratingSegments[3]);
                    if (currentBirdName == BirdName.none)
                    {
                        Debug.LogError("Invalid bird colour[" + ratingSegments[3] + "] for segment within slide round evaluation end info message.");
                        printMessageSegments();
                        return;
                    }
                    if (!roundLikes[currentRound].ContainsKey(currentCabinetID))
                    {
                        roundLikes[currentRound].Add(currentCabinetID, currentLikeCount);
                    }
                    if (!roundDislikes[currentRound].ContainsKey(currentCabinetID))
                    {
                        roundDislikes[currentRound].Add(currentCabinetID, currentDislikeCount);
                    }
                    if (!targets[currentRound].ContainsKey(currentCabinetID))
                    {
                        targets[currentRound].Add(currentCabinetID, currentBirdName);
                    }
                }
            }
        }

        public override bool resolve()
        {
            foreach (int round in rounds)
            {
                GameManager.Instance.playerFlowManager.slidesRound.updateEvaluationRatings(round, roundLikes[round], roundDislikes[round], targets[round]);
            }
            GameManager.Instance.playerFlowManager.slidesRound.hasReceivedEvaluationRatings = true;

            if (GameManager.Instance.playerFlowManager.slidesRound.hasReceivedRatings)
            {
                Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "ratings_loaded:" + SettingsManager.Instance.birdName);
            }
            
            return true;
        }
    }

    public class UpdatePhaseBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        GameFlowManager.GamePhase gamePhase = GameFlowManager.GamePhase.invalid;

        public UpdatePhaseBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "update phase"))
            {
                return;
            }

            gamePhase = getGamePhaseFromCode(messageSegments[1]);
            if(gamePhase == GameFlowManager.GamePhase.invalid)
            {
                Debug.LogError("Invalid game phase provided["+messageSegments[1]+"] for update phase broadcast network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.UpdatePhase(gamePhase);
            return true;
        }
    }

    public class DrawingPhasePositionsBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        Dictionary<BirdName, Vector3> birdArmPositionMap = new Dictionary<BirdName, Vector3>();

        public DrawingPhasePositionsBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            string[] armSegments;
            ColourManager.BirdName currentBirdName;
            Vector3 currentBirdArmPosition;

            float x = -1f;
            float y = -1f;
            float z = -1f;

            for (int i = 1; i < messageSegments.Length; i++)
            {
                armSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

                if(armSegments.Length != 4)
                {
                    Debug.LogError("Invalid number of arguments["+armSegments.Length+"] in an arm segment submessage for the drawing stage arm positions broadcast message.");
                    printMessageSegments();
                    return;
                }

                currentBirdName = getBirdNameFromCode(armSegments[0]);

                if(currentBirdName == BirdName.none)
                {
                    Debug.LogError("Invalid bird colour["+armSegments[0]+"] provided for the drawing stage arm positions broadcast message.");
                    printMessageSegments();
                    return;
                }

                if(!float.TryParse(armSegments[1], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                    !float.TryParse(armSegments[2], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                    !float.TryParse(armSegments[3], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
                {
                    Debug.LogError("Invalid position["+armSegments[1] + "," + armSegments[2] + "," + armSegments[3]+"] provided for the drawing stage arm positions broadcast message.");
                    printMessageSegments();
                    return;
                }

                currentBirdArmPosition = new Vector3(x, y, z);

                if (birdArmPositionMap.ContainsKey(currentBirdName))
                {
                    Debug.LogError("Bird name["+currentBirdName+"] exists more than once in the bird arm map for the drawing stage arm positions broadcast message.");
                    printMessageSegments();
                    return;
                }

                birdArmPositionMap.Add(currentBirdName, currentBirdArmPosition);
            }

        }

        public override bool resolve()
        {
            foreach(KeyValuePair<BirdName,Vector3> currentBirdArmPosition in birdArmPositionMap)
            {
                GameManager.Instance.playerFlowManager.drawingRound.SetBirdArmPosition(currentBirdArmPosition.Key, currentBirdArmPosition.Value);
            }
            return true;
        }
    }

    public class AccusationSeatBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int seatIndex = -1;
        BirdName birdName = BirdName.none;

        public AccusationSeatBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(3,"accusation seat broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out seatIndex))
            {
                Debug.LogError("Invalid seat index["+messageSegments[1]+"] provided to accusation seat broadcast message.");
                printMessageSegments();
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[2]);

            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid bird name["+messageSegments[2]+"] provided to accusation seat broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.initializePlayerSeat(seatIndex, birdName);
            GameManager.Instance.playerFlowManager.slidesRound.initializeGalleryBird(seatIndex, birdName);
            GameManager.Instance.playerFlowManager.accoladesRound.initializeAccoladeBirdRow(seatIndex, birdName);
            return true;
        }
    }

    public class AccusePhasePositionsBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        Dictionary<BirdName, Vector3> birdArmStretchMap = new Dictionary<BirdName, Vector3>();

        public AccusePhasePositionsBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            string[] armSegments;
            ColourManager.BirdName currentBirdName;
            Vector3 currentBirdArmPosition;

            float x = -1f;
            float y = -1f;
            float z = -1f;

            for (int i = 1; i < messageSegments.Length; i++)
            {
                armSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

                if (armSegments.Length != 4)
                {
                    Debug.LogError("Invalid number of arguments[" + armSegments.Length + "] in an arm segment submessage for the accuse stage arm stretch broadcast message.");
                    printMessageSegments();
                    return;
                }

                currentBirdName = getBirdNameFromCode(armSegments[0]);

                if (currentBirdName == BirdName.none)
                {
                    Debug.LogError("Invalid bird colour[" + armSegments[0] + "] provided for the accuse stage arm stretch broadcast message.");
                    printMessageSegments();
                    return;
                }

                if (!float.TryParse(armSegments[1], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                    !float.TryParse(armSegments[2], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                    !float.TryParse(armSegments[3], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
                {
                    Debug.LogError("Invalid position[" + armSegments[1] + "," + armSegments[2] + "," + armSegments[3] + "] provided for the accuse stage arm stretch broadcast message.");
                    printMessageSegments();
                    return;
                }

                currentBirdArmPosition = new Vector3(x, y, z);

                if (birdArmStretchMap.ContainsKey(currentBirdName))
                {
                    Debug.LogError("Bird name[" + currentBirdName + "] exists more than once in the bird arm map for the accuse stage arm stretch broadcast message.");
                    printMessageSegments();
                    return;
                }

                birdArmStretchMap.Add(currentBirdName, currentBirdArmPosition);
            }
        }

        public override bool resolve()
        {
            foreach (KeyValuePair<BirdName, Vector3> currentBirdArmPosition in birdArmStretchMap)
            {
                GameManager.Instance.playerFlowManager.accusationsRound.SetMeetingArmStretch(currentBirdArmPosition.Key, currentBirdArmPosition.Value);
            }

            return true;
        }
    }

    public class AccusationChangeToVoteBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public AccusationChangeToVoteBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.changeToVoteFromReview();
            return true;
        }
    }

    public class AccusationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        BirdName botcher = BirdName.none;
        Dictionary<BirdName, Dictionary<int, BirdName>> accusationMap = new Dictionary<BirdName, Dictionary<int, BirdName>>();
        Dictionary<int, BirdName> orderMap = new Dictionary<int, BirdName>();

        public AccusationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            

            botcher = getBirdNameFromCode(messageSegments[1]);
            if(botcher == BirdName.none)
            {
                Debug.LogError("Invalid botcher colour["+botcher.ToString()+"] provided for accusation broadcast message.");
                printMessageSegments();
                return;
            }
            BirdName accusingPlayerBird;
            BirdName accusedPlayerBird;
            int rowCounter = 1;
            for (int i = 2; i < messageSegments.Length; i++)
            {
                string[] accusationSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
                accusingPlayerBird = getBirdNameFromCode(accusationSegments[0]);
                accusedPlayerBird = getBirdNameFromCode(accusationSegments[1]);

                if (accusedPlayerBird == BirdName.none)
                {
                    continue;
                }

                if (!accusationMap.ContainsKey(accusedPlayerBird))
                {
                    accusationMap.Add(accusedPlayerBird, new Dictionary<int, BirdName>());
                    orderMap.Add(rowCounter, accusedPlayerBird);
                    rowCounter++;
                }
                accusationMap[accusedPlayerBird].Add(accusationMap[accusedPlayerBird].Count + 1, accusingPlayerBird);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.currentVoteRound++;
            GameManager.Instance.playerFlowManager.revealRound.setAccusations(botcher, accusationMap, orderMap);
            
            return true;
        }
    }

    public class UpdateTimerBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        float newTime = -1f;
        public UpdateTimerBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "update timer broadcast"))
            {
                return;
            }

            if(!float.TryParse(messageSegments[1], out newTime))
            {
                Debug.LogError("Invalid new time["+messageSegments[1]+"] for update timer broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.currentTimeInRound = newTime;
            return true;
        }
    }

    public class ChainDetailsBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetIndex = -1;
        string correctPrefix = "";
        string correctNoun = "";
        string prefixGuess = "";
        string nounGuess = "";
        int numberOfPrompts = -1;
        int numberOfDrawings = -1;
        
        BirdName guesser = BirdName.none;

        public ChainDetailsBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(9, "chain details broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetIndex))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for chain details broadcast message.");
                printMessageSegments();
                return;
            }

            correctPrefix = messageSegments[2];
            correctNoun = messageSegments[3];
            prefixGuess = messageSegments[4];
            nounGuess = messageSegments[5];

            if (!int.TryParse(messageSegments[6], out numberOfPrompts))
            {
                Debug.LogError("Invalid number of prompts value["+messageSegments[6]+"] provided for chain details broadcast message.");
                printMessageSegments();
                return;
            }
            if (!int.TryParse(messageSegments[7], out numberOfDrawings))
            {
                Debug.LogError("Invalid number of drawings value["+messageSegments[7]+"] provided for chain details broadcast message.");
                printMessageSegments();
                return;
            }
            guesser = getBirdNameFromCode(messageSegments[8]);
            if(guesser == BirdName.none)
            {
                Debug.LogError("Invalid guesser colour["+messageSegments[8]+"] provided for chain details broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.setChainDetails(cabinetIndex, correctPrefix, correctNoun, prefixGuess, nounGuess, numberOfPrompts, numberOfDrawings, guesser);
            return true;
        }
    }

    public class ChainPromptBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        BirdName birdName = BirdName.none;
        string prefix = "";
        string noun = "";
        float timeTaken = -1f;

        public ChainPromptBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "chain prompt broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for chain prompt broadcast message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid round["+messageSegments[2]+"] provided for chain prompt broadcast message.");
                printMessageSegments();
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[3]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid player colour["+messageSegments[3]+"] provided for chain prompt broadcast message.");
                printMessageSegments();
                return;
            }

            prefix = messageSegments[4];
            noun = messageSegments[5];

            if(!float.TryParse(messageSegments[6], out timeTaken))
            {
                Debug.LogError("Invalid time taken["+messageSegments[6]+"] provided for chain prompt broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.setChainPrompt(cabinetID, round, birdName, prefix, noun, timeTaken);
            return true;
        }
    }

    public class StartPresentationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        BirdName author = BirdName.none;

        public StartPresentationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "start presentation broadcast"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet value["+messageSegments[1]+"] provided for start presentation broadcast message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid round value["+messageSegments[2]+"] provided for start presentation broadcast message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(messageSegments[3]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+messageSegments[3]+"] provided for start presentation broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.initializePresentation(cabinetID, round, author);
            return true;
        }
    }

    public class StartEvaluationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        int evaluationRound = -1;
        int playerIndex = -1;
        BirdName author = BirdName.none;

        public StartEvaluationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "start evaluation presentation broadcast"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out evaluationRound))
            {
                Debug.LogError("Invalid evaluation round value[" + messageSegments[1] + "] provided for start evaluation presentation broadcast message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[2], out playerIndex))
            {
                Debug.LogError("Invalid player index value[" + messageSegments[2] + "] provided for start evaluation presentation broadcast message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(messageSegments[3]);
            if (author == BirdName.none)
            {
                Debug.LogError("Invalid author[" + messageSegments[3] + "] provided for start evaluation presentation broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.initializeEvaluationPresentation(evaluationRound, playerIndex, author);
            return true;
        }
    }

    public class UpdatePresentationLineBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        BirdName author = BirdName.none;
        string lineIdentifier = "";
        Dictionary<int,Vector3> points = new Dictionary<int,Vector3>();

        public UpdatePresentationLineBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            author = getBirdNameFromCode(messageSegments[1]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+messageSegments[1]+"] for update presentation line broadcast message.");
                printMessageSegments();
                return;
            }

            if(author == SettingsManager.Instance.birdName)
            {
                return;
            }

            lineIdentifier = messageSegments[2];

            string[] lineSegments = messageSegments[3].ToString().Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            Vector3 currentPoint;
            int pointIterator = 0;
            string[] pointSegments;
            float x, y, z;
            foreach(string lineSegment in lineSegments)
            {
                pointSegments = lineSegment.ToString().Split(new string[] { GameDelim.POINT }, StringSplitOptions.None);

                if(!float.TryParse(pointSegments[0], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                    !float.TryParse(pointSegments[1], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                    !float.TryParse(pointSegments[2], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
                {
                    Debug.LogError("Invalid point format["+lineSegment+"] for update presentation line broadcast message.");
                    printMessageSegments();
                    return;
                }
                currentPoint = new Vector3(x, y, z);
                points.Add(pointIterator, currentPoint);

                pointIterator++;
            }
        }

        public override bool resolve()
        {
            
            GameManager.Instance.playerFlowManager.accusationsRound.presentationSubround.viewerDrawings.updateLinePositions(author, lineIdentifier, points);
            return true;
        }
    }

    public class ResumeAccusationBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        float timeRemaining = -1f;
        public ResumeAccusationBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "resume accusation broadcast"))
            {
                return;
            }

            if(!float.TryParse(messageSegments[1],out timeRemaining))
            {
                Debug.LogError("Invalid time remaining["+messageSegments[1]+"] provided to resume accusation broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.resumeFromPresentation(timeRemaining);
            return true;

        }
    }

    public class RemovePlayerBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        BirdName birdName = BirdName.none;
        public RemovePlayerBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "remove player broadcast"))
            {
                return;
            }
            birdName = getBirdNameFromCode(messageSegments[1]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid player colour["+messageSegments[1]+"] for remove player broadcast message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.removeAccusedPlayer(birdName);
            return true;
        }
    }

    public class ResetAccuseRoundBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        public ResetAccuseRoundBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(1, "reset accuse round broadcast"))
            {
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.resetRound();
            return true;
        }
    }

    public class EndgameBroadcastNetworkMessage : BroadcastNetworkMessage
    {
        string winningTeam = "";
        Dictionary<BirdName, PlayerRole> alignmentMap = new Dictionary<BirdName, PlayerRole>();
        public EndgameBroadcastNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            string[] playerSegments;
            BirdName currentBirdName = BirdName.none;
            PlayerRole currentRole = PlayerRole.invalid;
            winningTeam = messageSegments[1];

            for(int i = 2; i < messageSegments.Length; i++)
            {
                playerSegments = messageSegments[i].Split(new string[] { GameDelim.NAME }, StringSplitOptions.None);
                if(playerSegments.Length != 2)
                {
                    Debug.LogError("Invalid length for player segment["+playerSegments.Length.ToString()+"] provided for endgame broadcast message.");
                    printMessageSegments();
                    return;
                }

                currentBirdName = getBirdNameFromCode(playerSegments[0]);
                if(currentBirdName == BirdName.none)
                {
                    Debug.LogError("Invalid player colour["+playerSegments[0]+"] provided in player segment for endgame broadcast message.");
                    printMessageSegments();
                    return;
                }
                currentRole = getPlayerRoleFromCode(playerSegments[1]);
                if(currentRole == PlayerRole.invalid)
                {
                    Debug.LogError("Invalid alignment["+playerSegments[1]+"] provided in player segment for endgame broadcast message.");
                    printMessageSegments();
                    return;
                }
                alignmentMap.Add(currentBirdName, currentRole);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.resultsRound.ShowResults(winningTeam, alignmentMap);
            return true;
        }
    }

    #endregion

    #region ToPlayer Messages

    public abstract class ToPlayerNetworkMessage : NetworkMessage
    {
        public enum ToPlayerMessageType
        {
            evaluation_details,
            add_botcher_round,
            add_botcher_prefixes,
            add_botcher_nouns,
            add_botcher_coverup,
            lines,
            expected_lines_notification,
            ready_for_lines,
            drawing_package_confirmation,
            empty_drawing,
            role_distribution,
            possible_prefixes,
            possible_nouns,
            initial_cabinet_prompt_contents,
            cabinet_prompt_contents,
            show_botcher_correct_option,
            force_submit,
            force_submit_response,
            response_round_correct_guess,

            review_player
        }

        public ToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
        }
        
    }

    public class EvaluationDetailsToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        string adjective = "";
        string noun = "";
        int cabinetID = -1;
        int evaluationRound = -1;

        public EvaluationDetailsToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(6, "evaluation details to player"))
            {
                return;
            }

            adjective = messageSegments[2];
            noun = messageSegments[3];

            if(!int.TryParse(messageSegments[4], out evaluationRound))
            {
                Debug.LogError("Invalid evaluation round["+messageSegments[4]+"] provided for evaluation details to player message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[5], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[5]+"] provided for evaluation details to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setEvaluationDetails(adjective, noun, cabinetID, evaluationRound);
            return true;
        }
    }

    public class AddBotcherRoundToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        BotcherMode botcherMode = BotcherMode.invalid;
        int botcherRound = -1;
        int cabinetID = -1;
        int drawingRound = -1;

        public AddBotcherRoundToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(6,"add botcher round to player"))
            {
                return;
            }

            botcherMode = getBotcherModeFromCode(messageSegments[2]);
            if(botcherMode == BotcherMode.invalid)
            {
                Debug.LogError("Invalid botcher mode["+messageSegments[2]+"] provided to add botcher round to player message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[3], out botcherRound))
            {
                Debug.LogError("Invalid botcher round["+messageSegments[3]+"] provided to add botcher round to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[4], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[4]+"] provided to add botcher round to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[5], out drawingRound))
            {
                Debug.LogError("Invalid drawing round["+messageSegments[5]+"] provided to add botcher round to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.botchingForm.addBotcherRound(botcherMode, botcherRound, cabinetID, drawingRound);
            return true;
        }
    }

    public class AddBotcherPrefixesToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int botcherRound = -1;
        List<string> prefixes = new List<string>();

        public AddBotcherPrefixesToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!int.TryParse(messageSegments[2], out botcherRound))
            {
                Debug.LogError("Invalid botcher round["+messageSegments[2]+"] provided for add botcher prefixes to player message.");
                printMessageSegments();
                return;
            }
            for(int i = 3; i < messageSegments.Length; i++)
            {
                prefixes.Add(messageSegments[i]);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.botchingForm.addBotcherPrefixes(botcherRound, prefixes);
            return true;
        }
    }

    public class AddBotcherNounsToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int botcherRound = -1;
        List<string> nouns = new List<string>();

        public AddBotcherNounsToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!int.TryParse(messageSegments[2], out botcherRound))
            {
                Debug.LogError("Invalid botcher round[" + messageSegments[2] + "] provided for add botcher nouns to player message.");
                printMessageSegments();
                return;
            }
            for (int i = 3; i < messageSegments.Length; i++)
            {
                nouns.Add(messageSegments[i]);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.botchingForm.addBotcherNouns(botcherRound, nouns);
            return true;
        }
    }

    public class AddBotcherCoverupToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        Coverup coverup = Coverup.invalid;

        public AddBotcherCoverupToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(5, "add botcher coverup to player"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet[" + messageSegments[2] + "] provided for add botcher coverup to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[3], out round))
            {
                Debug.LogError("Invalid round[" + messageSegments[3] + "] provided for add botcher coverup to player message.");
                printMessageSegments();
                return;
            }

            coverup = PlayerDrawingInputData.getCoverupFromCode(messageSegments[4]);
            if(coverup == Coverup.invalid)
            {
                Debug.LogError("Invalid coverup value[" + messageSegments[4] + "] provided for add botcher coverup to player message.");
                printMessageSegments();
                return;
            }

        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.addBotcherCoverup(cabinetID, round, coverup);
            return true;
        }
    }

    public class DrawingLinesToPlayerNetworkMessage : NetworkMessage
    {
        string packageID = "";
        int lineMessageID = -1;
        string message = "";

        public DrawingLinesToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(5, "drawing lines to player"))
            {
                return;
            }

            packageID = messageSegments[2];
            if (!GameManager.Instance.playerFlowManager.drawingLinePackageQueue.ContainsKey(packageID))
            {
                Debug.LogError("Failed to add the queued line message, couldn't find the corresponding packageID[" + packageID+"] for drawing lines to player message.");
                return;
            }
            DrawingLinesPackage currentPackage = GameManager.Instance.playerFlowManager.drawingLinePackageQueue[packageID];

            lineMessageID = int.Parse(messageSegments[3]);
            if (currentPackage.messages.ContainsKey(lineMessageID))
            {
                Debug.LogError("Failed to add the queued line message, the line messageID [" + messageSegments[3] + "] already exists for package ID[" + packageID + "] for drawing lines to player message.");
                printMessageSegments();
                return;
            }

            message = messageSegments[4];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.handleQueuedDrawingLineMessage(packageID, lineMessageID, message);
            return true;
        }
    }

    public class ExpectedDrawingLinesNotificationToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        BirdName sendingBirdName = BirdName.none;
        BirdName author = BirdName.none;
        int cabinetID = -1;
        int cabinetTab = -1;
        DrawingType drawingType = DrawingType.invalid;
        Coverup coverup = Coverup.invalid;
        string packageID = "";
        int expectedNumberOfMessages = -1;
        float timeTaken = -1f;
        public ExpectedDrawingLinesNotificationToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(8, "drawing lines package notification to player"))
            {
                return;
            }

            string[] idSegments = messageSegments[5].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);

            author = getBirdNameFromCode(idSegments[1]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+idSegments[1]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }
            
            drawingType = PlayerDrawingInputData.getDrawingTypeFromCode(idSegments[2]);
            if(drawingType == DrawingType.invalid)
            {
                Debug.LogError("Invalid drawing type["+idSegments[2]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }
            coverup = PlayerDrawingInputData.getCoverupFromCode(idSegments[5]);
            if(coverup == Coverup.invalid)
            {
                Debug.LogError("Invalid coverup["+idSegments[5]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }

            sendingBirdName = getBirdNameFromCode(messageSegments[2]);
            if(sendingBirdName == BirdName.none)
            {
                Debug.LogError("Invalid sending bird name["+messageSegments[2]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[3], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[3]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[4], out cabinetTab))
            {
                Debug.LogError("Invalid round["+messageSegments[4]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }

            packageID = messageSegments[5];
            
            if(!int.TryParse(messageSegments[6], out expectedNumberOfMessages))
            {
                Debug.LogError("Invalid expected number of messages value["+messageSegments[6]+"] for drawing lines package notification to player message.");
                printMessageSegments();
                return;
            }

            if(!float.TryParse(messageSegments[7], out timeTaken))
            {
                Debug.LogError("Invalid time taken value[] for drawing line package notification to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.initializeDrawingLinePackage(sendingBirdName, author, drawingType, cabinetID, cabinetTab, coverup, packageID, expectedNumberOfMessages, "ready_for_lines", timeTaken);
            return true;
        }
    }

    public class ReadyForDrawingLinesToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        string packageID = "";

        public ReadyForDrawingLinesToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "ready for drawing lines to player"))
            {
                return;
            }
            packageID = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.sendDrawingPackageToServer(packageID);
            return true;
        }
    }

    public class DrawingPackageConfirmationToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        string packageID = "";

        public DrawingPackageConfirmationToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "drawing package confirmation to player"))
            {
                return;
            }
            packageID = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.sendDrawingPackageToServer(packageID);
            return false;
        }
    }

    public class RoleDistributionToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        GameFlowManager.PlayerRole playerRole = GameFlowManager.PlayerRole.invalid;

        public RoleDistributionToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "role distribution to player"))
            {
                return;
            }
            playerRole = getPlayerRoleFromCode(messageSegments[2]);

            if(playerRole == GameFlowManager.PlayerRole.invalid)
            {
                Debug.LogError("Invalid format for player role[" + messageSegments[2] + "] in role distribution to player network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {

            GameManager.Instance.playerFlowManager.assignRole(playerRole);
            return true;
        }
    }

    public class PossiblePrefixesToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        List<string> possiblePrefixes = new List<string>();

        public PossiblePrefixesToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[2]+"] for possible prefixes to player message.");
                printMessageSegments();
                return;
            }

            for (int i = 3; i < messageSegments.Length; i++)
            {
                possiblePrefixes.Add(messageSegments[i]);
            }        
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.responsesRound.setPossiblePrefixes(cabinetID, possiblePrefixes);
            return true;
        }
    }

    public class PossibleNounsToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        List<string> possibleNouns = new List<string>();

        public PossibleNounsToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet[" + messageSegments[2] + "] for possible nouns to player message.");
                printMessageSegments();
                return;
            }

            for (int i = 3; i < messageSegments.Length; i++)
            {
                possibleNouns.Add(messageSegments[i]);
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.responsesRound.setPossibleNouns(cabinetID, possibleNouns);
            return true;
        }
    }

    public class InitialPromptContentsToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        string prefix = "";
        string noun = "";
        int cabinetIndex = -1;
        int tab = -1;
        BirdName author = BirdName.none;

        public InitialPromptContentsToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "cabinet prompt contents to player"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[2], out cabinetIndex))
            {
                Debug.LogError("Invalid cabinet index[" + messageSegments[2] + "] for cabinet prompt contents to player message.");
                printMessageSegments();
                return;
            }
            if (!int.TryParse(messageSegments[3], out tab))
            {
                Debug.LogError("Invalid tab[" + messageSegments[3] + "] for cabinet prompt contents to palyer message.");
            }
            prefix = messageSegments[4];
            noun = messageSegments[5];

            author = getBirdNameFromCode(messageSegments[6]);
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setPrompt(cabinetIndex, tab, author, prefix, noun, 0f, false);
            return true;
        }
    }

    public class CabinetPromptContentsToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        string prefix = "";
        string noun = "";
        int cabinetIndex = -1;
        int tab = -1;
        BirdName author = BirdName.none;

        public CabinetPromptContentsToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(8, "cabinet prompt contents to player"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[2], out cabinetIndex))
            {
                Debug.LogError("Invalid cabinet index["+messageSegments[2]+"] for cabinet prompt contents to player message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[3], out tab))
            {
                Debug.LogError("Invalid tab["+messageSegments[3]+"] for cabinet prompt contents to palyer message.");
            }
            author = getBirdNameFromCode(messageSegments[4]);

            prefix = messageSegments[5];
            noun = messageSegments[6];

            

        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setPrompt(cabinetIndex, tab, author, prefix, noun,0f,true);
            return true;
        }
    }

    public class ShowBotcherCorrectOptionToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        int tab = -1;
        string correctPrefix = "";
        string correctNoun = "";


        public ShowBotcherCorrectOptionToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(6, "show botcher correct option to player"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[2]+"] provided for show botcher correct option to player message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[3], out tab))
            {
                Debug.LogError("Invalid tab["+messageSegments[3]+"] provided for show botcher correct option to player message.");
                printMessageSegments();
                return;
            }

            correctPrefix = messageSegments[4];
            correctNoun = messageSegments[5];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setBotcherCorrectOption(cabinetID, tab, correctPrefix, correctNoun);
            return true;
        }
    }

    public class ForceSubmitToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        public ForceSubmitToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "force submit to player"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[2]+"] provided for force submit to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.currentActiveCabinetIndex = cabinetID;
            GameManager.Instance.playerFlowManager.drawingRound.Submit(true);

            return true;
        }
    }

    public class ForceSubmitResponseToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;

        public ForceSubmitResponseToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "force submit response to player"))
            {
                return;
            }
            if(!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[2]+"] provided for force submit response to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.responsesRound.forceSubmit();
            return true;
        }
    }

    public class EmptyDrawingToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        DrawingType drawingType = DrawingType.invalid;
        int cabinetID = -1;
        int round = -1;
        BirdName author = BirdName.none;
        public EmptyDrawingToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(6, "empty drawing to player"))
            {
                return;
            }

            drawingType = getDrawingTypeFromCode(messageSegments[2]);
            if(drawingType == DrawingType.invalid)
            {
                Debug.LogError("Invalid drawingtype["+messageSegments[2]+"] provided for empty drawing to player message.");
                printMessageSegments();
                return;
            }
            
            if(!int.TryParse(messageSegments[3], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[3]+"] provided for empty drawing to player message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[4], out round))
            {
                Debug.LogError("Invalid round["+messageSegments[4]+"] provided for empty drawing to player message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(messageSegments[5]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+messageSegments[5]+"] provided for empty drawing to player message.");
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.handleEmptyDrawingToPlayer(drawingType, cabinetID, round, author);
            return true;
        }
    }

    public class ResponseRoundCorrectGuessToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        int cabinetID = -1;
        string prefix = "";
        string noun = "";
        public ResponseRoundCorrectGuessToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(5, "response round correct guess to player"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[2], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[2]+"] provided for response round correct guess to player message.");
                printMessageSegments();
                return;
            }

            prefix = messageSegments[3];
            noun = messageSegments[4];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.responsesRound.setCorrectGuess(prefix, noun);
            return true;
        }
    }

    public class ReviewPlayerToPlayerNetworkMessage : ToPlayerNetworkMessage
    {
        BirdName reviewee = BirdName.none;
        PlayerRole role = PlayerRole.invalid;

        public ReviewPlayerToPlayerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "review player to player"))
            {
                return;
            }

            reviewee = getBirdNameFromCode(messageSegments[2]);
            if(reviewee == BirdName.none)
            {
                Debug.LogError("Invalid player colour["+messageSegments[2]+"] provided to review player to player message.");
                printMessageSegments();
                return;
            }

            role = getPlayerRoleFromCode(messageSegments[3]);
            if(role == PlayerRole.invalid)
            {
                Debug.LogError("Invalid player role["+messageSegments[3]+"] provided to review player to player message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.revealBirdAlignment(reviewee, role);
            return true;
        }
    }
    #endregion

    #region ToServer Messages
    public abstract class ToServerNetworkMessage : NetworkMessage
    {
        public enum ToServerMessageType
        {
            transition_condition,
            update_bird_folder_status,
            hide_cabinet_folder,
            ready_for_lines,
            expected_lines_notification,
            drawing_receipt_notification,
            lines,
            initial_cabinet_prompt_contents_receipt,
            cabinet_prompt_contents_receipt,
            prompt,
            access_drawer,
            player_responses_loaded,
            add_botcher_coverup,
            botcher_prefix_option,
            botcher_noun_option,
            prompt_guess,
            accuse_player,
            drawing_arm_position,
            accuse_arm_position,
            slide_round_message,
            rate_slide,
            rate_evaluation_slide,
            player_connected,
            player_cabinet_complete,
            ratings_confirmation,
            failed_response_confirmation,
            cabinet_time_taken,
            review_player,
            accusation_confirmation,
            speed_up_slides,
            drawer_released,
            empty_drawing,
            empty_drawing_confirmation,
            response_round_ready,
            all_chains_loaded,
            start_presentation,
            start_evaluation_presentation,
            update_presentation_line,
            resume_accusation,
            finished_reveal_round
        }

        public ToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {

        }
    }

    public class TransitionConditionToServerNetworkMessage : ToServerNetworkMessage
    {
        string transitionCondition = "";

        public TransitionConditionToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(2, "transition condition to server"))
            {
                return;
            }

            transitionCondition = messageSegments[1];
        }

        public override bool resolve()
        {
            GameManager.Instance.gameFlowManager.resolveTransitionCondition(transitionCondition);
            return true;
        }
    }

    public class UpdateHeldBirdFolderStatusToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        string folderStatusField = "";
        bool folderStatus = false;
        Color folderColour;
        public UpdateHeldBirdFolderStatusToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "update bird folder status to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if (birdName == BirdName.none)
            {
                Debug.LogError("Invalid bird name[" + messageSegments[1] + "] provided to update bird folder status to server message.");
                printMessageSegments();
                return;
            }

            folderStatusField = messageSegments[2];
            switch (folderStatusField)
            {
                case "grabbed":
                    folderStatus = true;
                    break;
                case "released":
                    folderStatus = false;
                    break;
                default:
                    Debug.LogError("Invalid folder status value[" + messageSegments[2] + "] provided to update bird folder status to server message.");
                    printMessageSegments();
                    return;
            }

            float r, g, b, a;
            if(!float.TryParse(messageSegments[3], out r) ||
                !float.TryParse(messageSegments[4], out g) ||
                !float.TryParse(messageSegments[5], out b) ||
                !float.TryParse(messageSegments[6], out a))
            {
                Debug.LogError("Invalid colour values provided to update bird folder status to server message.");
                printMessageSegments();
                return;
            }
            folderColour = new Color(r, g, b, a);
        }

        public override bool resolve()
        {
            Instance.BroadcastQueue.Add("update_bird_folder_status" + GameDelim.BASE + birdName + GameDelim.BASE + folderStatusField);
            GameManager.Instance.playerFlowManager.drawingRound.updateBirdFolderStatus(birdName, folderColour, folderStatus);
            return true;
        }
    }

    public class HideCabinetFolderToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;

        public HideCabinetFolderToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if(!validateSegmentCount(2, "hide cabinet folder to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided to hide cabinet folder to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].folderObject.SetActive(false);
            Instance.BroadcastQueue.Add("hide_cabinet_folder" + GameDelim.BASE + cabinetID.ToString());
            return true;
        }
    }

    public class ReadyForDrawingLinesToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        string packageID = "";

        public ReadyForDrawingLinesToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "drawing package confirmation to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid bird name[" + messageSegments[1] + "] in drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            packageID = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.sendDrawingPackageToClient(birdName, packageID);
            return true;
        }
    }

    public class ExpectedDrawingLinesNotificationToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName sendingBirdName = BirdName.none;
        BirdName author = BirdName.none;
        DrawingType drawingType = DrawingType.invalid;
        int cabinetID = -1;
        int cabinetTab = -1;
        Coverup drawingCoverup = Coverup.invalid;
        string packageID = "";
        int numberOfMessages = -1;
        float timeTaken = -1f;
        
        public ExpectedDrawingLinesNotificationToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "drawing package confirmation to server"))
            {
                return;
            }

            string[] idSegments = messageSegments[1].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            sendingBirdName = getBirdNameFromCode(idSegments[0]);

            if(sendingBirdName == BirdName.none)
            {
                Debug.LogError("Invalid bird name[" + idSegments[0] + "] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(idSegments[1]);

            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+idSegments[1]+"] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            drawingType = PlayerDrawingInputData.getDrawingTypeFromCode(idSegments[2]);

            if(drawingType == DrawingType.invalid)
            {
                Debug.LogError("Invalid drawing type["+idSegments[2]+"] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(idSegments[3], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet ID[" + idSegments[3] + "] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(idSegments[4], out cabinetTab))
            {
                Debug.LogError("Invalid value for cabinet tab[" + idSegments[4] + "] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            drawingCoverup = PlayerDrawingInputData.getCoverupFromCode(idSegments[5]);
            if(drawingCoverup == Coverup.invalid)
            {
                Debug.LogError("Invalid coverup value["+idSegments[5]+"] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            packageID = messageSegments[1];
            if(!int.TryParse(messageSegments[2], out numberOfMessages))
            {
                Debug.LogError("Invalid value for number of messages[" + messageSegments[2] + "] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }

            if(!float.TryParse(messageSegments[3], out timeTaken))
            {
                Debug.LogError("Invalid value for time taken["+messageSegments[3]+"] for drawing package confirmation to server network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.initializeDrawingLinePackage(sendingBirdName, author, drawingType, cabinetID, cabinetTab,drawingCoverup, packageID, numberOfMessages, "ready_for_lines", timeTaken);
            return true;
        }
    }

    public class DrawingLinesToServerNetworkMessage : ToServerNetworkMessage
    {
        string packageID = "";
        int lineMessageID = -1;
        string message = "";
        public DrawingLinesToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "drawing lines to server"))
            {
                return;
            }

            packageID = messageSegments[1];
            if (!GameManager.Instance.playerFlowManager.drawingLinePackageQueue.ContainsKey(packageID))
            {
                Debug.LogError("Failed to add the queued line message, couldn't find the corresponding packageID[" + packageID + "] for drawing lines to server message.");
                return;
            }
            DrawingLinesPackage currentPackage = GameManager.Instance.playerFlowManager.drawingLinePackageQueue[packageID];

            lineMessageID = int.Parse(messageSegments[2]);
            if (currentPackage.messages.ContainsKey(lineMessageID))
            {
                Debug.LogError("Failed to add the queued line message, the line messageID [" + messageSegments[2] + "] already exists for package ID[" + packageID + "] for drawing lines to server message.");
                printMessageSegments();
                return;
            }

            message = messageSegments[3];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.handleQueuedDrawingLineMessage(packageID, lineMessageID, message);
            return true;
        }
    }

    public class PromptToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int tab = -1;
        BirdName author = BirdName.none;
        string prefix = "";
        string noun = "";
        float timeTaken = -1f;

        public PromptToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(7, "prompt to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value provided for cabinet[" + messageSegments[1] + "] for prompt to server network message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[2], out tab))
            {
                Debug.LogError("Invalid value provided for tab[" + messageSegments[2] + "] for prompt to server network message.");
                printMessageSegments();
                return;
            }
            author = getBirdNameFromCode(messageSegments[3]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid value provided for author["+messageSegments[3]+"] for prompt to server network message.");
            }
            prefix = messageSegments[4];
            noun = messageSegments[5];
            if(!float.TryParse(messageSegments[6], NumberStyles.Number, CultureInfo.InvariantCulture, out timeTaken))
            {
                Debug.LogError("Invalid value provided for time taken[" + messageSegments[6] + "] for prompt to server network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setPrompt(cabinetID, tab, author, prefix, noun, timeTaken,true);
            GameManager.Instance.gameFlowManager.resolveTransitionCondition("prompt_submitted:" + author);
            return true;
        }
    }

    public class AccessDrawerToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;

        public AccessDrawerToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(2, "access drawer to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[1] + "] for access drawer to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.setDrawerAsUsed(cabinetID);
            GameNetwork.Instance.BroadcastQueue.Add("access_drawer" + GameDelim.BASE + cabinetID.ToString());
            return true;
        }
    }

    public class GuessToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        string prefix = "";
        string noun = "";
        int cabinetID = -1;

        public GuessToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(5, "guess to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if (birdName == BirdName.none)
            {
                Debug.LogError("Invalid value for bird colour[" + messageSegments[1] + "] for guess to server message.");
                printMessageSegments();
                return;
            }

            prefix = messageSegments[2];
            noun = messageSegments[3];

            if(!int.TryParse(messageSegments[4], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[4] + "] for guess to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.addGuessPrompt(birdName, prefix, noun, cabinetID);
            return true;
        }
    }

    public class AddBotcherCoverupToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        Coverup coverup = Coverup.invalid;

        public AddBotcherCoverupToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "add botcher coverup to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet[" + messageSegments[1] + "] provided for add botcher coverup to server message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid round[" + messageSegments[2] + "] provided for add botcher coverup to server message.");
                printMessageSegments();
                return;
            }

            coverup = PlayerDrawingInputData.getCoverupFromCode(messageSegments[3]);
            if(coverup == Coverup.invalid)
            {
                Debug.LogError("Invalid coverup value[" + messageSegments[3] + "] provided for add botcher coverup to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.addBotcherCoverup(cabinetID, round, coverup);
            return true;
        }
    }

    public class RequestReviewToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName reviewer = BirdName.none;
        BirdName reviewee = BirdName.none;
        public RequestReviewToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "request review to server"))
            {
                return;
            }

            reviewer = getBirdNameFromCode(messageSegments[1]);
            if(reviewer == BirdName.none)
            {
                Debug.LogError("Invalid reviewer["+messageSegments[1]+"] provided for request review to server message.");
                printMessageSegments();
                return;
            }

            reviewee = getBirdNameFromCode(messageSegments[2]);
            if(reviewee == BirdName.none)
            {
                Debug.LogError("Invalid reviewee["+messageSegments[2]+"] provided for request review to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;
            Instance.ToPlayerQueue.Add(reviewer.ToString() + GameDelim.BASE + "review_player" + GameDelim.BASE + reviewee.ToString() + GameDelim.BASE + GameManager.Instance.gameFlowManager.gamePlayers[reviewee].playerRole.ToString());
            return true;
        }
    }

    public class AccuseToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName accusingPlayer = BirdName.none;
        BirdName accusedPlayer = BirdName.none;

        public AccuseToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "accuse to server"))
            {
                return;
            }

            accusingPlayer = getBirdNameFromCode(messageSegments[1]);
            if(accusingPlayer == BirdName.none)
            {
                Debug.LogError("Invalid value for accusing bird colour[" + messageSegments[1] + "] for accuse to server network message.");
                printMessageSegments();
                return;
            }

            accusedPlayer = getBirdNameFromCode(messageSegments[2]);
            if(accusedPlayer == BirdName.none)
            {
                Debug.LogError("Invalid value for accused bird colour[" + messageSegments[2] + "] for accuse to server network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            AccusationsRound accuseRound = GameManager.Instance.playerFlowManager.accusationsRound;

            if (!accuseRound.accusationMap.ContainsKey(accuseRound.currentVoteRound))
            {
                accuseRound.accusationMap.Add(accuseRound.currentVoteRound, new Dictionary<BirdName, BirdName>());
            }
            if (accuseRound.accusationMap[accuseRound.currentVoteRound].ContainsKey(accusingPlayer))
            {
                Debug.LogError("Attempting to add vote from player["+accusingPlayer+"] for player["+accusedPlayer+"] twice for round["+accuseRound.currentVoteRound+"].");
                return false;
            }
            accuseRound.accusationMap[accuseRound.currentVoteRound].Add(accusingPlayer, accusedPlayer);
            if (GameManager.Instance.gameFlowManager.isCurrentAccusationReady())
            {
                GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;
            }
            return true;
        }
    }

    public class BotcherPrefixOptionToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        string prefix = "";

        public BotcherPrefixOptionToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "botcher prefix option to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for botcher prefix option to server message.");
                printMessageSegments();
                return;
            }
            prefix = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.addSelectedBotcherPrefix(cabinetID, prefix);
            return true;
        }
    }

    public class BotcherNounOptionToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        string noun = "";

        public BotcherNounOptionToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "botcher noun option to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[1]+"] provided for botcher noun option to server message.");
                printMessageSegments();
                return;
            }

            noun = messageSegments[2];
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.addSelectedBotcherNoun(cabinetID, noun);
            return true;
        }
    }

    public class DrawingArmPositionToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        Vector3 currentPosition = Vector3.zero;

        public DrawingArmPositionToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "drawing arm position to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid value for bird colour[" + messageSegments[1] + "] for drawing arm position to server message.");
                printMessageSegments();
                return;
            }

            string[] currentSplit = messageSegments[2].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            float x = -1f;
            float y = -1f;
            float z = -1f;

            if(currentSplit.Length != 3)
            {
                Debug.LogError("Invalid number of fields[" + currentSplit.Length + "] for position in drawing arm position to server message.");
                printMessageSegments();
                return;
            }

            if(!float.TryParse(currentSplit[0], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                !float.TryParse(currentSplit[1], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                !float.TryParse(currentSplit[2], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
            {
                Debug.LogError("Invalid value for one of the position coordinates[" + currentSplit[0] + "," + currentSplit[1] + "," + currentSplit[2] + "] for drawing arm position to server message.");
                printMessageSegments();
                return;
            }
            currentPosition = new Vector3(x,y,z);

        }

        public override bool resolve()
        {
            GameManager.Instance.gameFlowManager.SetBirdArmPosition(birdName, currentPosition);
            return true;
        }
    }

    public class AccuseArmPositionToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        Vector3 currentPosition = Vector3.zero;

        public AccuseArmPositionToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "accuse arm stretch to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if (birdName == BirdName.none)
            {
                Debug.LogError("Invalid value for bird colour[" + messageSegments[1] + "] for accuse arm stretch to server message.");
                printMessageSegments();
                return;
            }

            string[] currentSplit = messageSegments[2].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            float x = -1f;
            float y = -1f;
            float z = -1f;

            if (currentSplit.Length != 3)
            {
                Debug.LogError("Invalid number of fields[" + currentSplit.Length + "] for position in accuse arm stretch to server message.");
                printMessageSegments();
                return;
            }

            if (!float.TryParse(currentSplit[0], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                !float.TryParse(currentSplit[1], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                !float.TryParse(currentSplit[2], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
            {
                Debug.LogError("Invalid value for one of the position coordinates[" + currentSplit[0] + "," + currentSplit[1] + "," + currentSplit[2] + "] for accuse arm stretch to server message.");
                printMessageSegments();
                return;
            }
            currentPosition = new Vector3(x, y, z);
        }

        public override bool resolve()
        {
            GameManager.Instance.gameFlowManager.SetAccuseArmStretch(birdName, currentPosition);
            return true;
        }
    }

    public class SlidePlayerMessageToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName birdName = BirdName.none;
        string message = "";

        public SlidePlayerMessageToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(3, "slide player message to server"))
            {
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[1]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid value for bird colour[" + messageSegments[1] + "] for slide player message to server message.");
                printMessageSegments();
                return;
            }

            message = messageSegments[2];
        }

        public override bool resolve()
        {
            AudioManager.Instance.PlaySound("ChatPop");
            GameManager.Instance.playerFlowManager.slidesRound.setPeanutGalleryMessage(birdName, message);
            Instance.BroadcastQueue.Add("slide_player_message" + GameDelim.BASE + birdName.ToString() + GameDelim.BASE + message);

            return true;
        }
    }

    public class RateSlideToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int tab = -1;
        string rating = "";

        public RateSlideToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "rate slide to server"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[1] + "] for rate slide to server message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[2], out tab))
            {
                Debug.LogError("Invalid value for tab[" + messageSegments[2] + "] for rate slide to server message.");
                printMessageSegments();
                return;
            }
            rating = messageSegments[3];
            if(rating != "like" &&
                rating != "dislike")
            {
                Debug.LogError("Invalid value for rating[" + messageSegments[3] + "] for rate slide to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.addToRating(cabinetID, tab, rating);
            return true;
        }
    }

    public class RateEvaluationSlideToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        string rating = "";

        public RateEvaluationSlideToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "evaluation rate slide to server"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[1] + "] for rate evaluation slide to server message.");
                printMessageSegments();
                return;
            }
            if (!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid value for round[" + messageSegments[2] + "] for rate evaluation slide to server message.");
                printMessageSegments();
                return;
            }
            rating = messageSegments[3];
            if (rating != "like" &&
                rating != "dislike")
            {
                Debug.LogError("Invalid value for rating[" + messageSegments[3] + "] for rate evaluation slide to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.addToEvaluationRating(cabinetID, round, rating);
            return true;
        }
    }

    public class PlayerConnectedToServerNetworkMessage : ToServerNetworkMessage
    {
        public PlayerConnectedToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(1, "player connected to server"))
            {
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.gameFlowManager.totalPlayersConnected++;
            return true;
        }
    }

    public class CabinetTimeTakenToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int tab = -1;
        float timeTaken = -1f;
        public CabinetTimeTakenToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "cabinet time taken to server"))
            {
                return;
            }
            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[1] + "] for cabinet time taken to server message.");
                printMessageSegments();
                return;
            }
            if(!int.TryParse(messageSegments[2], out tab))
            {
                Debug.LogError("Invalid value for tab[" + messageSegments[2] + "] for cabinet time taken to server message.");
                printMessageSegments();
                return;
            }
            if(!float.TryParse(messageSegments[3], NumberStyles.Number, CultureInfo.InvariantCulture, out timeTaken))
            {
                Debug.LogError("Invalid value for time taken[" + messageSegments[3] + "] for cabinet time taken to server message.");
                printMessageSegments();
                return;
            }

        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.addCabinetTimeTaken(cabinetID, tab, timeTaken);
            return true;
        }
    }

    public class SpeedUpSlidesToServerNetworkMessage : ToServerNetworkMessage
    {
        public SpeedUpSlidesToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(1, "speed up slides to server"))
            {
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.slidesRound.increaseSlideSpeed();
            return true;
        }
    }

    public class DrawerReleasedToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        BirdName currentPlayer = BirdName.none;
        bool useDrawer = false;
        BirdName tab1Player = BirdName.none;
        BirdName tab2Player = BirdName.none;
        BirdName tab3Player = BirdName.none;
        BirdName tab4Player = BirdName.none;
        BirdName tab5Player = BirdName.none;
        int roundNumber = -1;

        public DrawerReleasedToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(10, "drawer released to server"))
            {
                return;
            }
            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid value for cabinet[" + messageSegments[1] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }

            currentPlayer = getBirdNameFromCode(messageSegments[2]);
            if(currentPlayer == BirdName.none)
            {
                Debug.LogError("Invalid value for current player colour[" + messageSegments[2] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }

            if (!bool.TryParse(messageSegments[3], out useDrawer))
            {
                Debug.LogError("Invalid value for use cabinet check[" + messageSegments[3] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }

            tab1Player = getBirdNameFromCode(messageSegments[4]);
            if (tab1Player == BirdName.none)
            {
                Debug.LogError("Invalid value for tab1 player colour[" + messageSegments[4] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }
            tab2Player = getBirdNameFromCode(messageSegments[5]);
            if (tab2Player == BirdName.none)
            {
                Debug.LogError("Invalid value for tab2 player colour[" + messageSegments[5] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }
            tab3Player = getBirdNameFromCode(messageSegments[6]);
            if (tab3Player == BirdName.none)
            {
                Debug.LogError("Invalid value for tab3 player colour[" + messageSegments[6] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }
            tab4Player = getBirdNameFromCode(messageSegments[7]);
            if (tab4Player == BirdName.none)
            {
                Debug.LogError("Invalid value for tab4 player colour[" + messageSegments[7] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }
            tab5Player = getBirdNameFromCode(messageSegments[8]);
            if (tab5Player == BirdName.none)
            {
                Debug.LogError("Invalid value for tab5 player colour[" + messageSegments[8] + "] for drawer released to server message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[9], out roundNumber))
            {
                Debug.LogError("Invalid value for round number["+messageSegments[9]+"] for drawer released to server message.");
                printMessageSegments();
                return;
            }

        }

        public override bool resolve()
        {
            CabinetDrawer currentCabinet = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID];
            currentCabinet.setDrawerVisuals(currentPlayer, useDrawer, tab1Player, tab2Player, tab3Player, tab4Player, tab5Player);
            return true;
        }
    }

    public class EmptyDrawingToServerNetworkMessage : ToServerNetworkMessage
    {
        DrawingType drawingType = DrawingType.invalid;
        BirdName birdName = BirdName.none;
        int cabinetID = -1;
        int round = -1;

        public EmptyDrawingToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(5, "empty drawing to server"))
            {
                return;
            }

            drawingType = getDrawingTypeFromCode(messageSegments[1]);
            if(drawingType == DrawingType.invalid)
            {
                Debug.LogError("Invalid drawingtype["+messageSegments[1]+"] provided to empty drawing to server message.");
                printMessageSegments();
                return;
            }

            birdName = getBirdNameFromCode(messageSegments[2]);
            if(birdName == BirdName.none)
            {
                Debug.LogError("Invalid player colour["+messageSegments[2]+"] provided to empty drawing to server message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[3], out cabinetID))
            {
                Debug.LogError("Invalid cabinet["+messageSegments[3]+"] provided to empty drawing to server message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[4], out round))
            {
                Debug.LogError("Invalid round["+messageSegments[4]+"] provided to empty drawing to server message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.drawingRound.handleEmptyDrawingToServer(drawingType, birdName, cabinetID, round);

            return true;
        }
    }

    public class StartPresentationToServerNetworkMessage : ToServerNetworkMessage
    {
        int cabinetID = -1;
        int round = -1;
        BirdName author = BirdName.none;

        public StartPresentationToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "start presentation to network"))
            {
                return;
            }

            if(!int.TryParse(messageSegments[1], out cabinetID))
            {
                Debug.LogError("Invalid cabinet value["+messageSegments[1]+"] provided for start presentation to network message.");
                printMessageSegments();
                return;
            }

            if(!int.TryParse(messageSegments[2], out round))
            {
                Debug.LogError("Invalid round value["+messageSegments[2]+"] provided for start presentation to network message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(messageSegments[3]);
            if(author == BirdName.none)
            {
                Debug.LogError("Invalid author["+messageSegments[3]+"] provided for start presentation to network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.initializePresentation(cabinetID, round, author);
            Instance.BroadcastQueue.Add("start_presentation" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + author.ToString());
            return true;
        }
    }

    public class StartEvaluationPresentationToServerNetworkMessage : ToServerNetworkMessage
    {
        int evaluationRound = -1;
        int playerIndex = -1;
        BirdName author = BirdName.none;

        public StartEvaluationPresentationToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            if (!validateSegmentCount(4, "start evaluation presentation to network"))
            {
                return;
            }

            if (!int.TryParse(messageSegments[1], out evaluationRound))
            {
                Debug.LogError("Invalid evaluation round value[" + messageSegments[1] + "] provided for start evaluation presentation to network message.");
                printMessageSegments();
                return;
            }

            if (!int.TryParse(messageSegments[2], out playerIndex))
            {
                Debug.LogError("Invalid player index value[" + messageSegments[2] + "] provided for start evaluation presentation to network message.");
                printMessageSegments();
                return;
            }

            author = getBirdNameFromCode(messageSegments[3]);
            if (author == BirdName.none)
            {
                Debug.LogError("Invalid author[" + messageSegments[3] + "] provided for start evaluation presentation to network message.");
                printMessageSegments();
                return;
            }
        }

        public override bool resolve()
        {
            GameManager.Instance.playerFlowManager.accusationsRound.initializeEvaluationPresentation(evaluationRound, playerIndex, author);
            Instance.BroadcastQueue.Add("start_evaluation_presentation" + GameDelim.BASE + evaluationRound.ToString() + GameDelim.BASE + playerIndex.ToString() + GameDelim.BASE + author.ToString());
            return true;
        }
    }

    public class UpdatePresentationLineToServerNetworkMessage : ToServerNetworkMessage
    {
        BirdName author = BirdName.none;
        string lineIdentifier = "";
        Dictionary<int, Vector3> points = new Dictionary<int, Vector3>();

        public UpdatePresentationLineToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {
            author = getBirdNameFromCode(messageSegments[1]);
            if (author == BirdName.none)
            {
                Debug.LogError("Invalid author[" + messageSegments[1] + "] for update presentation line broadcast message.");
                printMessageSegments();
                return;
            }

            if (author == SettingsManager.Instance.birdName)
            {
                return;
            }

            lineIdentifier = messageSegments[2];

            string[] lineSegments = messageSegments[3].ToString().Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            Vector3 currentPoint;
            int pointIterator = 0;
            string[] pointSegments;
            float x, y, z;
            foreach (string lineSegment in lineSegments)
            {
                pointSegments = lineSegment.ToString().Split(new string[] { GameDelim.POINT}, StringSplitOptions.None);

                if (!float.TryParse(pointSegments[0], NumberStyles.Number, CultureInfo.InvariantCulture, out x) ||
                    !float.TryParse(pointSegments[1], NumberStyles.Number, CultureInfo.InvariantCulture, out y) ||
                    !float.TryParse(pointSegments[2], NumberStyles.Number, CultureInfo.InvariantCulture, out z))
                {
                    Debug.LogError("Invalid point format[" + lineSegment + "] for update presentation line broadcast message.");
                    printMessageSegments();
                    return;
                }
                currentPoint = new Vector3(x, y, z);
                points.Add(pointIterator, currentPoint);

                pointIterator++;
            }
        }

        public override bool resolve()
        {
            string message = "update_presentation_line" + GameDelim.BASE + author + GameDelim.BASE + lineIdentifier + GameDelim.BASE;

            int iterator = 0;
            foreach(Vector3 point in points.Values)
            {
                if (iterator != 0)
                {
                    message += GameDelim.SUB;
                }
                message += Math.Round(point.x, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(point.y, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(point.z, 2).ToString(CultureInfo.InvariantCulture);
                iterator++;
            }
            Instance.BroadcastQueue.Add(message);
            GameManager.Instance.playerFlowManager.accusationsRound.presentationSubround.viewerDrawings.updateLinePositions(author, lineIdentifier, points);
            return true;
        }
    }

    public class ResumeAccusationToServerNetworkMessage : ToServerNetworkMessage
    {
        public ResumeAccusationToServerNetworkMessage(string[] inMessageSegments) : base(inMessageSegments)
        {

        }

        public override bool resolve()
        {
            float timeRemaining = GameManager.Instance.playerFlowManager.accusationsRound.presentationSubround.savedAccusationVoteRoundTime;
            GameManager.Instance.gameFlowManager.timeRemainingInPhase = timeRemaining;
            Instance.BroadcastQueue.Add("resume_accusation" + GameDelim.BASE + timeRemaining);
            GameManager.Instance.playerFlowManager.accusationsRound.resumeFromPresentation(timeRemaining);
            return true;
        }
    }

    #endregion





}
