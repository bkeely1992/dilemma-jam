﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public float timePerCycle = 90.0f;
    public delegate void ClockCompleteAction();
    public event ClockCompleteAction OnClockComplete;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void StartCycle()
    {
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }

        animator = GetComponent<Animator>();
        animator.speed = (float)(1.0f / timePerCycle);

        animator.SetTrigger("Start");
    }

    public void EndCycle()
    {
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }
        animator.SetTrigger("Stop");
    }
    
    public void SetComplete()
    {
        if(OnClockComplete != null)
        {
            OnClockComplete();
        }
        
    }
}
