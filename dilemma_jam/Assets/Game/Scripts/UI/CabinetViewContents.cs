﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class CabinetViewContents : MonoBehaviour
{
    public Text promptText, playerPrompt1, playerPrompt2;
    public GameObject arrow1, arrow2, arrow3, arrow4, arrow5;
    public GameObject drawing1, drawing2, drawing3;
    public GameObject guessLabel, failLabel;

    public Text prefixText, nounText, guess;
    public GameObject prefixLikeObject, prefixDislikeObject, nounLikeObject, nounDislikeObject;

    public Image author1, author2, author3, author4, author5, guesser, failer;
    public List<FolderRating> allFolderRatings;

    public Canvas drawingCanvas;

    private Dictionary<int, FolderRating> folderRatingMap;
    private bool isInitialized = false;
    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
        
    }

    private void initialize()
    {
        folderRatingMap = new Dictionary<int, FolderRating>();

        foreach (FolderRating folderRating in allFolderRatings)
        {
            folderRatingMap.Add(folderRating.index, folderRating);
        }

        isInitialized = true;
    }


    public void SetImage(Image imageToSet, string inBirdName)
    {
        if (inBirdName != "" && inBirdName != "none")
        {
            ColourManager.BirdName birdName = GameNetwork.getBirdNameFromCode(inBirdName);
            if (!ColourManager.Instance.birdMap.ContainsKey(birdName))
            {
                imageToSet.gameObject.SetActive(false);
                return;
            }
            imageToSet.gameObject.SetActive(true);
            imageToSet.sprite = ColourManager.Instance.birdMap[birdName].faceSprite;
        }
        else
        {
            imageToSet.gameObject.SetActive(false);
        }
    }

    public void SetAuthorColour(Text authorText, string inBirdName)
    {
        if (inBirdName != "")
        {
            authorText.gameObject.SetActive(true);
            ColourManager.BirdName birdName = GameNetwork.getBirdNameFromCode(inBirdName);
            authorText.color = ColourManager.Instance.birdMap[birdName].colour;
        }
    }

    public void SetAccuserColour(Text authorText, string inValue, string correctValue)
    {
        if (inValue != "")
        {
            authorText.color = inValue == correctValue ? Color.green : Color.red;
        }
    }

    public void SetText(Text authorText, string inValue)
    {
        if (inValue != "")
        {
            authorText.gameObject.SetActive(true);
            authorText.text = inValue;
        }
        else
        {
            authorText.gameObject.SetActive(false);
        }
    }

    public void SetRating(int index, int likes, int dislikes)
    {
        if (!isInitialized)
        {
            initialize();
        }

        if(index == GameManager.Instance.playerFlowManager.numberOfCabinetRounds + 1)
        {
            index = -1;
        }

        if (folderRatingMap.ContainsKey(index))
        {
            if(likes > 0)
            {
                folderRatingMap[index].likeCountText.gameObject.SetActive(true);
                folderRatingMap[index].likeCountText.text = "x" + likes.ToString();
            }
            if(dislikes > 0)
            {
                folderRatingMap[index].dislikeCountText.gameObject.SetActive(true);
                folderRatingMap[index].dislikeCountText.text = "x" + dislikes.ToString();
            }
        }
        else
        {
            Debug.LogError("Folder rating map does not contain tab["+index.ToString()+"].");
        }
    }

    public void setLikeReviewForPrefix()
    {
        AnalyticsEvent.Custom("Rating", new Dictionary<string, object>() { { "Like", prefixText.text } });

        disableButton(prefixDislikeObject);
        disableButton(prefixLikeObject);

        AudioManager.Instance.PlaySound("Like");
    }

    public void setDislikeReviewForPrefix()
    {
        AnalyticsEvent.Custom("Rating", new Dictionary<string, object>() { { "Dislike", prefixText.text } });

        disableButton(prefixDislikeObject);
        disableButton(prefixLikeObject);

        AudioManager.Instance.PlaySound("Sus" + UnityEngine.Random.Range(1, 5));
    }

    public void setLikeReviewForNoun()
    {
        AnalyticsEvent.Custom("Rating", new Dictionary<string, object>() { { "Like", nounText.text } });

        disableButton(nounDislikeObject);
        disableButton(nounLikeObject);

        AudioManager.Instance.PlaySound("Like");
    }

    public void setDislikeReviewForNoun()
    {
        AnalyticsEvent.Custom("Rating", new Dictionary<string, object>() { { "Dislike", nounText.text } });

        disableButton(nounDislikeObject);
        disableButton(nounLikeObject);

        AudioManager.Instance.PlaySound("Sus" + UnityEngine.Random.Range(1, 5));
    }

    private void disableButton(GameObject buttonObject)
    {
        buttonObject.GetComponent<Button>().enabled = false;
        buttonObject.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    }

}
