﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PossiblePrompt : MonoBehaviour
{
    public string identifier;
    public Text displayText;
    public Image backgroundImage;
    public bool isCorrect = false;
}
