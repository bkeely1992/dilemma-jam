﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccoladesBirdRow : MonoBehaviour
{
    public Text playerNameText, likesCounterText, dislikesCounterText, timeCounterText, failsCounterText;

    public ColourManager.BirdName birdName;

    public Image pinImage, birdHeadImage;

    public bool isInitialized = false;
}
