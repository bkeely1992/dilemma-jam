﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviewPrompt : MonoBehaviour
{
    public Text prompt;
    public ColourManager.BirdName selectedBirdName;

    public void Review()
    {
        GameManager.Instance.playerFlowManager.accusationsRound.Review();
        gameObject.SetActive(false);
    }

    public void Close()
    {
        GameManager.Instance.playerFlowManager.accusationsRound.HideReviewPrompt(selectedBirdName);
        gameObject.SetActive(false);
    }
}
