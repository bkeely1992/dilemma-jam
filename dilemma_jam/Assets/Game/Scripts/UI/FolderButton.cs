﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FolderButton : MonoBehaviour, IPointerDownHandler
{
    public int folderIndex;
    public Image image, statusImage;

    private PlayerFlowManager playerFlowManager;
    private GameFlowManager gameFlowManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (!gameFlowManager)
        {
            gameFlowManager = GameManager.Instance.gameFlowManager;
        }

        playerFlowManager.accusationsRound.OpenFolder();
        playerFlowManager.accusationsRound.getMeetingBirdArm(SettingsManager.Instance.birdName).activated = false;
    }
}
