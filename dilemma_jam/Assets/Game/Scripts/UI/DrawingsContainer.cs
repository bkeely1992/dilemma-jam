﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingsContainer : MonoBehaviour
{
    public GameObject drawingHolderPrefab = null;
    public Dictionary<int, Dictionary<int,DrawingHolderObject>> drawingHolderMap = new Dictionary<int, Dictionary<int,DrawingHolderObject>>();

    public bool addDrawingHolder(int cabinetID, int round)
    {
        if (!drawingHolderMap.ContainsKey(cabinetID))
        {
            drawingHolderMap.Add(cabinetID, new Dictionary<int, DrawingHolderObject>());
        }
        if (!drawingHolderPrefab)
        {
            return false;
        }
        GameObject newDrawingHolderObject;
        DrawingHolderObject drawingHolder;
        if (drawingHolderMap[cabinetID].ContainsKey(round))
        {
            foreach(Transform child in drawingHolderMap[cabinetID][round].transform)
            {
                Destroy(child.gameObject);
            }
        }
        else
        {
            newDrawingHolderObject = Instantiate(drawingHolderPrefab, transform);
            drawingHolder = newDrawingHolderObject.GetComponent<DrawingHolderObject>();
            if (!drawingHolder)
            {
                return false;
            }
            drawingHolder.Cabinet = cabinetID;
            drawingHolder.Round = round;
            drawingHolderMap[cabinetID].Add(round, drawingHolder);
        }

        return true;
    }
}
