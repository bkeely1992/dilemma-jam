﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using Photon.Realtime;
using System;
using static ColourManager;

public class MenuLobbyButtons : Singleton<MenuLobbyButtons>
{
    public GameObject SplashPageObject, RoomsPageObject, LobbyPageObject;
    public List<PlayerIdentification> PlayerIdentifiers = new List<PlayerIdentification>();

    public Text CreateRoomNameText, SetPlayerNameText, RoomListingsPlayerNameText;
    public GameObject LobbyStartGameBtn;

    public CameraDock baseTransitionCameraDock;
    public CameraDock splashTransitionCameraDock;
    public RectTransform splashRectTransform;
    public GameObject openDoorObject;

    public GameObject RoomListingPrefab;
    public GameObject WaitingForHostPrompt;
    public Transform RoomListingsParent;

    public PlayerLayoutGroup PlayerListings;
    public PlayerIdentification SelectedID;
    public float TimeForIssueLogging = 4.0f;
    public Dictionary<ColourManager.BirdName, PlayerIdentification> PlayerIdentificationMap = new Dictionary<ColourManager.BirdName, PlayerIdentification>();
    public Dictionary<string, PlayerListing> PlayerListingMap = new Dictionary<string, PlayerListing>();

    public PlayerMessagePrompt PlayerPrompt;
    public MusicButton musicButton;

    public TutorialButton stickiesButton, tutorialsButton;

    public GameObject lobbyBGObject, logoObject, loginObject, roomListingsObject, createGameObject, joinGameObject;
    public InputField loginInputField, createRoomInputField, joinGameInputField;

    public GameObject settingsObject;
    public Text drawingRoundSettingText, accuseRoundSettingText, correctCabinetThresholdSettingText;
    public Slider drawingRoundSettingSlider, accuseRoundSettingSlider, correctCabinetThresholdSettingSlider;

    public float timeToBootUp = 2.0f, timeToTurnOn = 1.0f;
    public float timeToFadeIn = 1.0f, timeToFadeOut = 1.0f;

    public Image lobbyScreenImage;
    public Image splashScreenFadeImage;

    public List<Animator> splashArmAnimators;
    private Dictionary<BirdName, Animator> splashArmAnimatorMap = new Dictionary<BirdName, Animator>();

    private List<RoomListing> activeRoomListings = new List<RoomListing>();
    private List<RoomInfo> activeRoomInfo = new List<RoomInfo>();

    private RoomListing selectedRoomListing;
    private float currentTimeLogging = 0.0f;
    private float timeTurningOn = 0.0f, timeBootingUp = 0.0f;
    private float timeFadingIn = 0.0f, timeFadingOut = 0.0f;

    public Color activeBirdColour, inactiveBirdColour;

    private bool isInitialized = false;
    private string sceneName = "";
    private bool hostHasAdjustedCorrectCabinetThreshold = false;

    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    private bool initialize()
    {
        sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "Game")
        {
            return false;
        }

        if (PhotonNetwork.IsMasterClient)
        {
            accuseRoundSettingSlider.interactable = true;
            drawingRoundSettingSlider.interactable = true;
            correctCabinetThresholdSettingSlider.interactable = true;
            //settingsObject.SetActive(true);
            correctCabinetThresholdSettingSlider.maxValue = PhotonNetwork.PlayerList.Count();
        }

        foreach (PlayerIdentification lobbyID in PlayerIdentifiers)
        {
            PlayerIdentificationMap.Add(lobbyID.birdName, lobbyID);
        }

        foreach(Animator splashArmAnimator in splashArmAnimators)
        {
            splashArmAnimatorMap.Add(splashArmAnimator.GetComponent<BirdTag>().birdName, splashArmAnimator);
        }
        if (PhotonNetwork.InRoom)
        {
            LobbyPageObject.SetActive(true);
            RoomsPageObject.SetActive(true);

            if (!PhotonNetwork.IsMasterClient)
            {
                if (!SettingsManager.Instance.isHostInLobby)
                {
                    WaitingForHostPrompt.SetActive(true);
                }
                else
                {
                    //Request bird avatars
                    LobbyNetwork.Instance.FromClientQueue.Add("RequestUsedBirdsFromClient" + GameDelim.BASE + PhotonNetwork.NickName);
                    LobbyNetwork.Instance.SendEvents();
                }
            }

            if (PhotonNetwork.IsMasterClient)
            {
                if (!SettingsManager.Instance.GetSetting("tutorials", true))
                {
                    tutorialsButton.tutorialStatusText.text = "Off";
                    tutorialsButton.tutorialButtonStatusObject.gameObject.SetActive(true);
                }
                else
                {
                    tutorialsButton.tutorialButtonStatusObject.gameObject.SetActive(false);

                }
                tutorialsButton.containerObject.SetActive(true);
            }
            else
            {
                tutorialsButton.containerObject.SetActive(false);
            }
            if (!SettingsManager.Instance.GetSetting("stickies", true))
            {
                stickiesButton.tutorialStatusText.text = "Off";
                stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(true);
            }
            else
            {
                stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(false);

            }

            LobbyNetwork.Instance.OnJoinedRoom();
            PhotonNetwork.CurrentRoom.IsVisible = true;
            PhotonNetwork.CurrentRoom.IsOpen = true;
        }

        if (!SettingsManager.Instance.GetSetting("music", true))
        {
            AudioManager.Instance.SetMusic(false);
            musicButton.musicButtonStatusImage.sprite = musicButton.offSprite;
        }
        if(!SettingsManager.Instance.GetSetting("stickies", true))
        {
            stickiesButton.tutorialStatusText.text = "Off";
            stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(true);
        }
        else
        {
            stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(false);
        }

        
        bool tutorialSetting = SettingsManager.Instance.GetSetting("stickies", true);
        
        Cursor.SetCursor(null, Vector2.zero,CursorMode.Auto);

        isInitialized = true;
        return true;
    }

    private void Update()
    {
        if (sceneName == "Game")
        {
            return;
        }
        if (currentTimeLogging > 0.0f)
        {
            currentTimeLogging += Time.deltaTime;
            if (currentTimeLogging > TimeForIssueLogging)
            {
                currentTimeLogging = 0.0f;
            }
        }
        if(timeTurningOn > 0.0f)
        {
            timeTurningOn += Time.deltaTime;
            if (timeTurningOn > timeToTurnOn)
            {
                AudioManager.Instance.PlaySound("pc-boot");
                timeTurningOn = 0.0f;
                timeBootingUp += Time.deltaTime;
                logoObject.SetActive(true);
            }
        }
        if(timeBootingUp > 0.0f)
        {
            timeBootingUp += Time.deltaTime;
            if(timeBootingUp > timeToBootUp)
            {
                timeBootingUp = 0.0f;
                loginObject.SetActive(true);
                loginInputField.Select();
            }
        }
        if (isTransitioning)
        {
            cameraUpdate();
        }
        else if (timeFadingOut > 0.0f)
        {
            timeFadingOut += Time.deltaTime;
            splashScreenFadeImage.color = new Color(splashScreenFadeImage.color.r, splashScreenFadeImage.color.g, splashScreenFadeImage.color.b, (timeFadingOut / timeToFadeOut));

            if (timeFadingOut > timeToFadeOut)
            {
                timeFadingIn += Time.deltaTime;
                timeFadingOut = 0.0f;
            }
        }
        else if (timeFadingIn > 0.0f)
        {
            timeFadingIn += Time.deltaTime;
            lobbyScreenImage.color = new Color(lobbyScreenImage.color.r, lobbyScreenImage.color.g, lobbyScreenImage.color.b, timeFadingIn / timeToFadeIn);
            if (timeFadingIn > timeToFadeIn)
            {
                SplashPageObject.SetActive(false);
                //Randomize the bird arm
                int birdIndex = UnityEngine.Random.Range(0, ColourManager.Instance.allBirds.Count);
                splashArmAnimatorMap[ColourManager.Instance.allBirds[birdIndex].name].SetTrigger("move");
                timeTurningOn += Time.deltaTime;
                timeFadingIn = 0.0f;
            }
        }
        ClearEmptyRooms();
    }



    private void ClearEmptyRooms()
    {
        RoomListing rListingBehaviour;
        for (int i = activeRoomInfo.Count - 1; i >= 0; i--)
        {
            if (activeRoomInfo[i] == null ||
                activeRoomInfo[i].RemovedFromList ||
                !activeRoomInfo[i].IsVisible ||
                !activeRoomInfo[i].IsOpen ||
                activeRoomInfo[i].PlayerCount == 0)
            {
                if (activeRoomListings.Where(rl => rl.name == activeRoomInfo[i].Name).Count() == 1)
                {
                    rListingBehaviour = activeRoomListings.Single(rl => rl.name == activeRoomInfo[i].Name);
                    Destroy(rListingBehaviour.gameObject);
                    activeRoomListings.Remove(rListingBehaviour);
                    activeRoomInfo.Remove(activeRoomInfo[i]);
                }
            }
        }
    }

    #region SplashPage
    private float cameraStateTime = 0f;
    private bool isTransitioning = false;

    private void cameraUpdate()
    {

        cameraStateTime += Time.deltaTime;

        //Transition
        splashRectTransform.anchoredPosition = Vector3.Lerp(baseTransitionCameraDock.position, splashTransitionCameraDock.position, cameraStateTime * baseTransitionCameraDock.transitionMoveSpeed);
        splashRectTransform.localScale = Vector3.Lerp(baseTransitionCameraDock.zoom, splashTransitionCameraDock.zoom, cameraStateTime * baseTransitionCameraDock.transitionZoomSpeed);

        if (cameraStateTime * baseTransitionCameraDock.transitionMoveSpeed > 1)
        {
            cameraStateTime = 0.0f;
            lobbyBGObject.SetActive(true);

            timeFadingOut += Time.deltaTime;
            splashScreenFadeImage.gameObject.SetActive(true);
            isTransitioning = false;
        }
        

    }

    public void SplashPage_MultiplayerOnClick()
    {
        AudioManager.Instance.PlaySound("DoorOpen");
        isTransitioning = true;
        openDoorObject.SetActive(true);


        //EventSystem.current.SetSelectedGameObject(CreateGameBtn);
    }

    public void SplashPage_QuitClick()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        //Exit the game
        Application.Quit();
    }
    #endregion

    #region Network Responses
    public void ConnectedToServer()
    {
        if(PhotonNetwork.NetworkingClient.State == ClientState.JoiningLobby)
        {
            return;
        }
        LobbyNetwork.Instance.JoinLobby();
    }

    public void JoinedLobby()
    {

    }

    public void JoinedRoom()
    {
        LobbyPageObject.SetActive(true);
        RoomsPageObject.SetActive(false);

        if (PhotonNetwork.IsMasterClient)
        {
            //settingsObject.SetActive(true);
            accuseRoundSettingSlider.interactable = true;
            drawingRoundSettingSlider.interactable = true;
            correctCabinetThresholdSettingSlider.interactable = true;
            if (!SettingsManager.Instance.GetSetting("tutorials", true))
            {
                tutorialsButton.tutorialStatusText.text = "Off";
                tutorialsButton.tutorialButtonStatusObject.gameObject.SetActive(true);
            }
            else
            {
                tutorialsButton.tutorialButtonStatusObject.gameObject.SetActive(false);

            }
            tutorialsButton.containerObject.SetActive(true);
        }
        else
        {
            tutorialsButton.containerObject.SetActive(false);
        }
        if (!SettingsManager.Instance.GetSetting("stickies", true))
        {
            stickiesButton.tutorialStatusText.text = "Off";
            stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(true);
        }
        else
        {
            stickiesButton.tutorialButtonStatusObject.gameObject.SetActive(false);

        }

        for (int i = activeRoomListings.Count - 1; i >= 0; i--)
        {
            Destroy(activeRoomListings[i].gameObject);
        }
        activeRoomListings.Clear();
        activeRoomInfo.Clear();
        PlayerListings.Refresh();
    }

    public void FailedToCreateRoom(string message)
    {
        currentTimeLogging += Time.deltaTime;
    }

    public void LeftLobby()
    {
        PlayerListingMap.Clear();
        SettingsManager.Instance.playerNameMap.Clear();
        SplashPageObject.SetActive(true);
    }

    public void LeftRoom()
    {
        
        //Clear room contents
        PlayerListingMap.Clear();
        SettingsManager.Instance.playerNameMap.Clear();

        foreach(KeyValuePair<ColourManager.BirdName, PlayerIdentification> playerID in PlayerIdentificationMap)
        {
            playerID.Value.playerName = "";
            playerID.Value.gameObject.SetActive(true);
        }


        if (RoomsPageObject)
        {
            RoomsPageObject.SetActive(true);
        }
        
    }

    public void UpdateRoomListings(Dictionary<RoomInfo, bool> rooms)
    {
        GameObject newRoomListing;
        RoomListing rListingBehaviour;

        foreach (KeyValuePair<RoomInfo, bool> room in rooms)
        {
            if (room.Value)
            {
                if (!activeRoomListings.Any(rl => rl.roomName == room.Key.Name))
                {
                    newRoomListing = Instantiate(RoomListingPrefab, RoomListingsParent);
                    rListingBehaviour = newRoomListing.GetComponent<RoomListing>();
                    if (rListingBehaviour)
                    {
                        rListingBehaviour.RoomNameText.text = room.Key.Name.Length < 25 ? room.Key.Name : room.Key.Name.Substring(0,25) + "..";
                        rListingBehaviour.AttendeeCountText.text = room.Key.PlayerCount.ToString() + "/" + room.Key.MaxPlayers.ToString();
                        rListingBehaviour.roomName = room.Key.Name;
                        activeRoomListings.Add(rListingBehaviour);
                        activeRoomInfo.Add(room.Key);
                    }
                }
                else
                {
                    rListingBehaviour = activeRoomListings.Single(rl => rl.roomName == room.Key.Name);
                    rListingBehaviour.AttendeeCountText.text = room.Key.PlayerCount.ToString() + "/" + room.Key.MaxPlayers.ToString();
                }
            }
            else
            {
                List<RoomListing> roomListingsToRemove = activeRoomListings.Where(rl => rl.RoomNameText.text == room.Key.Name).ToList();
                foreach (RoomListing roomListingToRemove in roomListingsToRemove)
                {
                    Destroy(roomListingToRemove.gameObject);
                    activeRoomListings.Remove(roomListingToRemove);
                }

            }
        }
    }

    #endregion

    #region LoginPage
    public void LoginPage_OKClick()
    {
        if(loginInputField.text == "")
        {
            return;
        }
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1,9));

        PhotonNetwork.NickName = loginInputField.text;
        roomListingsObject.SetActive(true);
        RoomListingsPlayerNameText.text = "User: \n" + PhotonNetwork.NickName;
    }

    public void LoginPage_CancelClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        loginObject.SetActive(false);
        logoObject.SetActive(false);
        lobbyBGObject.SetActive(false);
        SplashPageObject.SetActive(true);
    }
    #endregion

    #region RoomListingsPage
    public void RoomListingsPage_JoinRoomClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        joinGameObject.SetActive(true);
        joinGameInputField.Select();
    }

    public void RoomListingsPage_BookRoomClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        createGameObject.SetActive(true);
        createRoomInputField.Select();
    }
    #endregion

    #region CreateGamePrompt
    public void CreateGamePrompt_OKClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        if (createRoomInputField.text != "")
        {
            LobbyNetwork.Instance.CreateRoom(createRoomInputField.text);
        }
    }

    public void CreateGamePrompt_CancelClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        createGameObject.SetActive(false);
    }

    #endregion

    #region JoinGamePrompt
    public void JoinGamePrompt_OKClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        if (joinGameInputField.text != "")
        {
            LobbyNetwork.Instance.JoinRoom(joinGameInputField.text);
        }
    }

    public void JoinGamePrompt_CancelClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        joinGameObject.SetActive(false);
    }
    #endregion

    #region RoomsPage
    public void RoomsPage_CreateGameOnClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));

        if (SetPlayerNameText.text == "")
        {
            PlayerPrompt.Activate("Please choose a player name.");
            return;
        }
        else if(CreateRoomNameText.text == "")
        {
            PlayerPrompt.Activate("Please choose a room name.");
            return;
        }
        else
        {
            PhotonNetwork.NickName = GameDelim.stripGameDelims(SetPlayerNameText.text);
        }
        if (!LobbyNetwork.Instance.CreateRoom(GameDelim.stripGameDelims(CreateRoomNameText.text)))
        {

        }
    }

    public void RoomsPage_JoinGameOnClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));

        if (SetPlayerNameText.text == "")
        {
            PlayerPrompt.Activate("Please choose a player name.");
            return;
        }
        else
        {
            PhotonNetwork.NickName = GameDelim.stripGameDelims(SetPlayerNameText.text);
        }
        
        if (selectedRoomListing == null)
        {

            return;
        }

        LobbyNetwork.Instance.JoinRoom(selectedRoomListing.RoomNameText.text);
    }

    public void RoomsPage_BackClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));

        SplashPageObject.SetActive(true);
        RoomsPageObject.SetActive(false);
    }

    public void SelectRoomListing(RoomListing roomListing)
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));

        selectedRoomListing = roomListing;

        if (selectedRoomListing == null)
        {
            return;
        }

        LobbyNetwork.Instance.JoinRoom(selectedRoomListing.roomName);
    }
    #endregion

    #region LobbyPage
    public void LobbyPage_StartGameClick()
    {
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));

        string loadingLevel = "Game";
        if (PhotonNetwork.IsMasterClient)
        {
            if (loadingLevel != "" && SettingsManager.Instance.playerNameMap.Count > 2)
            {
                foreach (PlayerIdentification playerID in PlayerIdentifiers)
                {
                    if (playerID.playerName == PhotonNetwork.NickName)
                    {
                        SettingsManager.Instance.birdName = playerID.birdName;
                        PhotonNetwork.CurrentRoom.IsVisible = false;
                        PhotonNetwork.CurrentRoom.IsOpen = false;
                        LobbyNetwork.Instance.BroadcastQueue.Add("LoadLevel" + GameDelim.BASE + loadingLevel);
                        LobbyNetwork.Instance.LoadLevel(loadingLevel);
                    }
                }
            }
        }
    }


    public bool TrySelectingBird(string birdText, string playerName)
    {
        ColourManager.BirdName birdNameToSelect = GetBirdNameFromText(birdText);
        PlayerIdentification previouslySelectedID = null;
        if (PhotonNetwork.IsMasterClient)
        {
            if(PlayerIdentificationMap[birdNameToSelect].playerName != "")
            {
                return false;
            }

            //Check if player already has an ID selected
            foreach(PlayerIdentification playerID in PlayerIdentifiers)
            {
                if(playerID.playerName == playerName)
                {
                    previouslySelectedID = playerID;
                    break;
                }
            }
            if (previouslySelectedID)
            {
                previouslySelectedID.playerName = "";
                previouslySelectedID.gameObject.SetActive(true);

                if (SettingsManager.Instance.playerNameMap.ContainsKey(previouslySelectedID.birdName))
                {
                    SettingsManager.Instance.playerNameMap.Remove(previouslySelectedID.birdName);
                }
            }


            //Update the server's card & player listing
            PlayerIdentificationMap[birdNameToSelect].playerName = playerName;
            PlayerIdentificationMap[birdNameToSelect].gameObject.SetActive(false);

            PlayerListingMap[playerName].ChangePlayerBird(birdNameToSelect);

            //Store the name of the player in the settings manager
            if (!SettingsManager.Instance.playerNameMap.ContainsKey(birdNameToSelect))
            {
                SettingsManager.Instance.playerNameMap.Add(birdNameToSelect, playerName);
            }

            if(PhotonNetwork.NickName == playerName)
            {
                AudioManager.Instance.PlaySound("scan-beep");
            }
       
            //Broadcast any changes to all other clients
            foreach (PlayerIdentification playerID in PlayerIdentifiers)
            {
                LobbyNetwork.Instance.BroadcastQueue.Add("UpdatePlayerID" + GameDelim.BASE + playerID.birdName.ToString() + GameDelim.BASE + playerID.playerName);
            }
        }
        else
        {
            LobbyNetwork.Instance.FromClientQueue.Add("TrySelectBird" + GameDelim.BASE + birdNameToSelect);
        }
        return true;
    }


    public void UpdatePlayerID(string[] messageSegments)
    {
        if (!isInitialized && !initialize())
        {
            return;
        }

        string playerName = messageSegments[2];
        ColourManager.BirdName birdName = GetBirdNameFromText(messageSegments[1]);

        PlayerIdentificationMap[birdName].playerName = playerName;
        if (playerName == "")
        {
            PlayerIdentificationMap[birdName].gameObject.SetActive(true);
        }
        else
        {
            //Update the player listing
            PlayerListingMap[playerName].ChangePlayerBird(birdName);

            PlayerIdentificationMap[birdName].gameObject.SetActive(false);
        }


    }

    public void LobbyPage_DisconnectClick()
    {
        if (SelectedID)
        {

        }
        AudioManager.Instance.PlaySound("random-boop" + UnityEngine.Random.Range(1, 9));
        SelectedID = null;
        LobbyNetwork.Instance.SendEvents();
        LobbyPageObject.SetActive(false);
        LobbyNetwork.Instance.LeaveRoom();

    }

    public void SelectPlayerBird(ColourManager.BirdName birdName, string playerName)
    {
        if (!SettingsManager.Instance.playerNameMap.ContainsKey(birdName))
        {
            SettingsManager.Instance.playerNameMap.Add(birdName, playerName);
        }

        PlayerListingMap[playerName].ChangePlayerBird(birdName);
        PlayerIdentificationMap[birdName].gameObject.SetActive(false);
        PlayerIdentificationMap[birdName].playerName = playerName;
        PlayerIdentificationMap[birdName].birdImage.color = activeBirdColour;
    }

    public void DeselectPlayerBird(ColourManager.BirdName birdName)
    {
        if (SettingsManager.Instance.playerNameMap.ContainsKey(birdName))
        {
            SettingsManager.Instance.playerNameMap.Remove(birdName);
        }

        if (PlayerIdentificationMap.ContainsKey(birdName) && 
            PlayerListingMap.ContainsKey(PlayerIdentificationMap[birdName].playerName))
        {
            PlayerListingMap[PlayerIdentificationMap[birdName].playerName].ResetPlayerText();
        }

        
        PlayerIdentificationMap[birdName].gameObject.SetActive(true);
        PlayerIdentificationMap[birdName].playerName = "";
    }

    public void UpdatePlayerIdentifiers(string[] messageSegments, int startingIndex)
    {
        Dictionary<ColourManager.BirdName, string> playerBirdNames = new Dictionary<ColourManager.BirdName, string>();
        string[] playerSegments;
        for(int i = (1 + startingIndex); i < messageSegments.Length; i++)
        {
            playerSegments = messageSegments[i].Split(new string[] { GameDelim.NAME }, StringSplitOptions.None);
            playerBirdNames.Add(GameNetwork.getBirdNameFromCode(playerSegments[0]), playerSegments[1]);
        }


        foreach (KeyValuePair<ColourManager.BirdName,string> playerBirdName in playerBirdNames)
        {
            if(playerBirdName.Value == "")
            {
                DeselectPlayerBird(playerBirdName.Key);
            }
            else
            {
                SelectPlayerBird(playerBirdName.Key, playerBirdName.Value);
            }
        }
    }
    #endregion


    #region GameSettings
    public void UpdateDrawingTime(int inDrawingTime)
    {
        drawingRoundSettingText.text = "Drawing Time:\n" + ((int)drawingRoundSettingSlider.value).ToString() + "s";
        SettingsManager.Instance.drawingTime = (int)drawingRoundSettingSlider.value;
    }

    public void UpdateAccuseTime(int inAccuseTime)
    {
        accuseRoundSettingText.text = "Accuse Time:\n" + ((int)accuseRoundSettingSlider.value).ToString() + "s";
        SettingsManager.Instance.accuseTime = (int)accuseRoundSettingSlider.value;
    }

    public void UpdateCorrectCabinetThreshold(int inCorrectCabinetThreshold)
    {
        hostHasAdjustedCorrectCabinetThreshold = true;
        correctCabinetThresholdSettingText.text = "Correct Cabinet Threshold:\n" + ((int)correctCabinetThresholdSettingSlider.value).ToString();
        SettingsManager.Instance.correctCabinetThreshold = (int)correctCabinetThresholdSettingSlider.value;
    }

    public void UpdateCorrectCabinetThresholdMaximum(int newMax)
    {
        correctCabinetThresholdSettingSlider.maxValue = newMax;
        correctCabinetThresholdSettingSlider.value = newMax > 1 ? newMax - 1 : newMax;
    }

    #endregion

    public ColourManager.BirdName GetBirdNameFromText(string birdText)
    {
        switch (birdText)
        {
            case "red":
                return ColourManager.BirdName.red;
            case "blue":
                return ColourManager.BirdName.blue;
            case "green":
                return ColourManager.BirdName.green;
            case "purple":
                return ColourManager.BirdName.purple;
            case "brown":
                return ColourManager.BirdName.brown;
            case "grey":
                return ColourManager.BirdName.grey;
            case "orange":
                return ColourManager.BirdName.orange;
            case "black":
                return ColourManager.BirdName.black;
        }

        return ColourManager.BirdName.none;
    }

    
}
