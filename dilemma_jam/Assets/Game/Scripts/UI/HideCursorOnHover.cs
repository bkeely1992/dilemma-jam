﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideCursorOnHover : MonoBehaviour
{
    public bool activated = true;
    public LayerMask mouseDetectionLayer;

    private void Update()
    {
        if (activated)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100.0f, mouseDetectionLayer);
            if (hit)
            {
                Cursor.visible = false;
            }
            else
            {
                Cursor.visible = true;
            }
        }

    }
}
