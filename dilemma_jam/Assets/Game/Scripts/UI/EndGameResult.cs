﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameResult : MonoBehaviour
{
    public Text playerText, honkText;
    public Text accusationText, honkText2;
    public ColourManager.BirdName birdName;
    public Image image;
}
