﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideProgressIndicator : MonoBehaviour
{
    public ColourManager.BirdName birdName;
    public int cabinetID;
    public int tab;
    public Image image;

    public List<GameObject> objectsToHideOnRate;

    public void Like()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        foreach (GameObject objectToHideOnRate in objectsToHideOnRate)
        {
            objectToHideOnRate.SetActive(false);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;
            if (!selectedCabinetData.ratings.ContainsKey(tab + 1))
            {
                selectedCabinetData.ratings.Add(tab + 1, new PlayerRatingData());
            }
            selectedCabinetData.ratings[tab + 1].likeCount++;
            selectedCabinetData.ratings[tab + 1].target = selectedCabinetData.playerOrder[tab + 1];
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("LikeSlide" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString());
        }
    }

    public void Dislike()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        foreach (GameObject objectToHideOnRate in objectsToHideOnRate)
        {
            objectToHideOnRate.SetActive(false);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;
            if(!selectedCabinetData.ratings.ContainsKey(tab + 1))
            {
                selectedCabinetData.ratings.Add(tab + 1, new PlayerRatingData());
            }
            selectedCabinetData.ratings[tab + 1].dislikeCount++;
            selectedCabinetData.ratings[tab + 1].target = selectedCabinetData.playerOrder[tab + 1];
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("DislikeSlide" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString());
        }
    }
}
