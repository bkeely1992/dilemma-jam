﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileSummaryEmailContents : MonoBehaviour
{
    public List<BirdTag> allFileSummaryEmails;

    public Dictionary<ColourManager.BirdName, CabinetViewContents> fileSummaryEmailMap;

    public bool isInitialized = false;

    private Dictionary<int, GameObject> fileSummaryIndexMap;
    private int currentPageIndex = 1;
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    public void initialize()
    {
        fileSummaryEmailMap = new Dictionary<ColourManager.BirdName, CabinetViewContents>();
        fileSummaryIndexMap = new Dictionary<int, GameObject>();
        int counter = 1;
        foreach (BirdTag fileSummaryTag in allFileSummaryEmails)
        {
            fileSummaryEmailMap.Add(fileSummaryTag.birdName, fileSummaryTag.gameObject.GetComponent<CabinetViewContents>());
        }

        foreach(KeyValuePair<ColourManager.BirdName,string> player in GameManager.Instance.playerFlowManager.playerNameMap)
        {
            fileSummaryIndexMap.Add(counter, fileSummaryEmailMap[player.Key].gameObject);
            counter++;
        }
        fileSummaryIndexMap[currentPageIndex].SetActive(true);
        isInitialized = true;
    }

    public void shiftPageLeft()
    {
        fileSummaryIndexMap[currentPageIndex].SetActive(false);
        currentPageIndex--;
        if(currentPageIndex <= 0)
        {
            currentPageIndex = fileSummaryIndexMap.Count;
        }
        fileSummaryIndexMap[currentPageIndex].SetActive(true);

    }

    public void shiftPageRight()
    {
        fileSummaryIndexMap[currentPageIndex].SetActive(false);
        currentPageIndex++;
        if (currentPageIndex > fileSummaryIndexMap.Count)
        {
            currentPageIndex = 1;
        }
        fileSummaryIndexMap[currentPageIndex].SetActive(true);
    }
}
