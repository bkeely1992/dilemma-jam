﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class BirdButton : MonoBehaviour, IPointerDownHandler
{
    public enum ButtonType
    {
        lobby, accusation
    }
    public ButtonType buttonType;


    public ColourManager.BirdName birdName;
    public Image birdImage;
    public Text playerName;
    public GameObject playerLabelObject;
    public GameObject parent;
    public string birdSound;

    public void OnPointerDown(PointerEventData eventData)
    {

        switch (buttonType)
        {
            case ButtonType.lobby:
                LobbyPress();
                break;
            case ButtonType.accusation:
                AccusePress();
                break;
        }

        
    }

    private void LobbyPress()
    {
        // this object was clicked - do something
        if (playerName.text == "")
        {
            MenuLobbyButtons.Instance.TrySelectingBird(birdName.ToString(), PhotonNetwork.NickName);
        }
    }

    private void AccusePress()
    {
        if (birdName == ColourManager.BirdName.none ||
            birdName == SettingsManager.Instance.birdName ||
            !GameManager.Instance.playerFlowManager.accusationsRound.getMeetingBirdArm(SettingsManager.Instance.birdName).activated)
        {
            return;
        }
        else if (GameManager.Instance.playerFlowManager.accusationsRound.roundState == AccusationsRound.AccusationRoundState.employee_review)
        {
            if(GameManager.Instance.playerFlowManager.accoladesRound.employeeOfTheMonth == SettingsManager.Instance.birdName)
            {
                AudioManager.Instance.PlaySound(birdName.ToString().ToLower() +"_squawk");
                GameManager.Instance.playerFlowManager.accusationsRound.ShowPrompt(birdName);
            }
            else
            {
                //Show text to indicate it's not time to vote yet
            }
        }
        else if(GameManager.Instance.playerFlowManager.accusationsRound.roundState == AccusationsRound.AccusationRoundState.accusation)
        {
            AudioManager.Instance.PlaySound(birdName.ToString().ToLower() + "_squawk");
            GameManager.Instance.playerFlowManager.accusationsRound.ShowPrompt(birdName);
        }

        
    }
}
