﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class PresentationDrawing : MonoBehaviour
{
    public float timeForDotClick = 0.25f;
    public LayerMask drawingLayer;
    public Dictionary<int,GameObject> lineObjects;
    public Dictionary<int, bool> lineRequiresUpdateMap;
    public float timePerUpdate = 0.25f;
    public GameObject currentDrawingPrefab = null;
    public float currentDrawingSize = 0.05f;

    private GameObject currentLine;
    private List<Vector2> fingerPositions = new List<Vector2>();
    private LineRenderer lineRenderer;
    private float timeSinceMouseDown = 0.0f;
    private bool startNewLine = true;
    private PolygonCollider2D polygonCollider2D;
    private Color currentDrawingColour = Color.black;
    
    private Material currentDrawingMaterial = null;
    private float timeSinceLastUpdate = 0f;
    private int lineIterator = 0;

    private bool isInitialized = false;


    // Start is called before the first frame update
    void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }   
    }

    public void initialize()
    {
        if (!isInitialized)
        {
            polygonCollider2D = GetComponent<PolygonCollider2D>();
            lineObjects = new Dictionary<int,GameObject>();
            lineRequiresUpdateMap = new Dictionary<int, bool>();
            isInitialized = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 temp = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100.0f, drawingLayer);
        bool isInDrawingArea = hit;

        if (startNewLine &&
            Input.GetMouseButtonDown(0) &&
            isInDrawingArea)
        {
            startNewLine = false;
            timeSinceMouseDown = 0.0f;
            CreateForegroundLine();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            startNewLine = true;
            if (timeSinceMouseDown < timeForDotClick &&
                isInDrawingArea)
            {
                CreateDot();
            }
            lineIterator++;
        }
        else if (Input.GetMouseButton(0))
        {
            if (isInDrawingArea)
            {
                if (startNewLine)
                {
                    startNewLine = false;
                    timeSinceMouseDown = 0.0f;
                    CreateForegroundLine();
                }
                else
                {
                    Vector2 tempFingerPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (Vector2.Distance(tempFingerPos, fingerPositions[fingerPositions.Count - 1]) > .05f)
                    {
                        UpdateForegroundLine(tempFingerPos);
                    }
                }
            }
            else
            {
                lineIterator++;
                startNewLine = true;
            }
        }


        timeSinceLastUpdate += Time.deltaTime;
        if(timeSinceLastUpdate > timePerUpdate)
        {
            List<int> updatedIDs = new List<int>();
            timeSinceLastUpdate = 0.0f;
            foreach(KeyValuePair<int,bool> potentialUpdateLine in lineRequiresUpdateMap)
            {
                if (potentialUpdateLine.Value)
                {
                    if (PhotonNetwork.IsMasterClient)
                    {
                        broadcastLineToPlayers(potentialUpdateLine.Key);
                    }
                    else
                    {
                        sendLineToServer(potentialUpdateLine.Key);
                    }
                    updatedIDs.Add(potentialUpdateLine.Key);
                }
            }

            foreach(int updatedID in updatedIDs)
            {
                lineRequiresUpdateMap[updatedID] = false;
            }
        }

        timeSinceMouseDown += Time.deltaTime;
    }

    void broadcastLineToPlayers(int lineID)
    {
        LineRenderer line = lineObjects[lineID].GetComponent<LineRenderer>();
        string message = "update_presentation_line" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + lineID.ToString() + GameDelim.BASE;
        Vector3 currentPosition;
        for (int i = 0; i < line.positionCount; i++)
        {
            currentPosition = line.GetPosition(i);
            if (i != 0)
            {
                message += GameDelim.SUB;
            }
            message += Math.Round(currentPosition.x, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(currentPosition.y, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(currentPosition.z, 2).ToString(CultureInfo.InvariantCulture);

        }
        GameNetwork.Instance.BroadcastQueue.Add(message);
    }

    void sendLineToServer(int lineID)
    {
        LineRenderer line = lineObjects[lineID].GetComponent<LineRenderer>();
        string message = "update_presentation_line" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + lineID.ToString() + GameDelim.BASE;
        //Vector3[] positions = new List<Vector3>().ToArray();
        //line.GetPositions(positions);
        Vector3 currentPosition;
        for(int i = 0; i < line.positionCount; i++)
        {
            
            currentPosition = line.GetPosition(i);
            
            if (i != 0)
            {
                message += GameDelim.SUB;
            }
            message += Math.Round(currentPosition.x, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(currentPosition.y, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.POINT + Math.Round(currentPosition.z, 2).ToString(CultureInfo.InvariantCulture);
        }

        GameNetwork.Instance.ToServerQueue.Add(message);
    }


    void CreateForegroundLine()
    {
        currentLine = Instantiate(currentDrawingPrefab, Vector3.zero, Quaternion.identity, transform);
        DrawingLine lineData = currentLine.GetComponent<DrawingLine>();
        lineData.author = SettingsManager.Instance.birdName;

        lineObjects.Add(lineIterator,currentLine);
        lineRequiresUpdateMap.Add(lineIterator, true);
        currentLine.transform.position = Vector3.zero;
        lineRenderer = currentLine.GetComponent<LineRenderer>();
        lineRenderer.startWidth = currentDrawingSize;
        lineRenderer.endWidth = currentDrawingSize;
        lineRenderer.material = ColourManager.Instance.birdMap[lineData.author].material;
        lineRenderer.sortingOrder = 100;
        
        fingerPositions.Clear();
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        Vector3 temp = new Vector3(fingerPositions[0].x, fingerPositions[0].y, -1.6f);
        lineRenderer.SetPosition(0, temp);
        temp = new Vector3(fingerPositions[1].x, fingerPositions[1].y, -1.6f);
        lineRenderer.SetPosition(1, temp);
    }

    private void UpdateForegroundLine(Vector2 newFingerPos)
    {
        if (!lineRenderer)
        {
            CreateForegroundLine();
            return;
        }
        Vector3 temp = new Vector3(newFingerPos.x, newFingerPos.y, -1.6f);
        fingerPositions.Add(newFingerPos);

        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, temp);

        lineRequiresUpdateMap[lineIterator] = true;

        if(lineRenderer.positionCount > 300)
        {
            startNewLine = true;
            lineIterator++;
        }
    }

    private void CreateDot()
    {
        Vector3 lastPosition = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
        Vector3 temp = new Vector3(lastPosition.x + 0.0075f, lastPosition.y, -1.6f);
        fingerPositions.Add(temp);
        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, temp);
    }
}
