﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationFolderPage : MonoBehaviour
{
    public List<EvaluationFolderDrawing> allEvaluationFolderDrawings;
    public List<IndexMap> allEvaluationFolderButtons;
    public List<IndexMap> allExpandedDrawings;
    public List<FolderRating> allEvaluationRatings;
    public Text promptText;

    public GameObject closeExpandedDrawingButton;
    public List<GameObject> objectsToHideOnDrawingExpand = new List<GameObject>();
    public int round;
    public bool active = false;
    public Canvas evaluationDrawingCanvas;

    private bool isInitialized = false;
    private Dictionary<int, EvaluationFolderDrawing> evaluationFolderDrawingMap = new Dictionary<int, EvaluationFolderDrawing>();
    private Dictionary<int, GameObject> evaluationFolderButtonMap = new Dictionary<int, GameObject>();
    private Dictionary<int, GameObject> expandedDrawingMap = new Dictionary<int, GameObject>();
    private Dictionary<int, FolderRating> evaluationRatingMap = new Dictionary<int, FolderRating>();

    public void initialize()
    {
        if (!isInitialized)
        {
            evaluationFolderDrawingMap.Clear();
            foreach (EvaluationFolderDrawing evaluationDrawing in allEvaluationFolderDrawings)
            {
                evaluationFolderDrawingMap.Add(evaluationDrawing.index, evaluationDrawing);
            }

            evaluationFolderButtonMap.Clear();
            foreach (IndexMap evaluationDrawingButton in allEvaluationFolderButtons)
            {
                evaluationFolderButtonMap.Add(evaluationDrawingButton.index, evaluationDrawingButton.gameObject);
            }

            expandedDrawingMap.Clear();
            foreach (IndexMap expandedDrawing in allExpandedDrawings)
            {
                expandedDrawingMap.Add(expandedDrawing.index, expandedDrawing.gameObject);
            }

            evaluationRatingMap.Clear();
            foreach(FolderRating evaluationRating in allEvaluationRatings)
            {
                evaluationRatingMap.Add(evaluationRating.index, evaluationRating);
            }
            isInitialized = true;
        }

    }

    public void initializeEvaluationDrawings(EvaluationData evaluationData)
    {
        setPrompt(evaluationData.adjective, evaluationData.noun);

        EvaluationFolderDrawing evaluationFolderDrawing;
        foreach(KeyValuePair<int,PlayerDrawingInputData> drawing in evaluationData.drawings)
        {
            evaluationFolderDrawing = evaluationFolderDrawingMap[drawing.Key];
            Transform drawingParent = evaluationFolderDrawing.transform;
            GameManager.Instance.playerFlowManager.createDrawingLines(drawing.Value, drawingParent, drawingParent.position, new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, evaluationDrawingCanvas);
            evaluationFolderDrawing.authorSprite.sprite = ColourManager.Instance.birdMap[drawing.Value.author].faceSprite;
            evaluationFolderDrawing.gameObject.SetActive(true);

            drawingParent = expandedDrawingMap[drawing.Key].transform;
            GameManager.Instance.playerFlowManager.createDrawingLines(drawing.Value, drawingParent, drawingParent.position, new Vector3(0.65f, 0.65f, 0.65f), 0.3f, 0.15f, 0.075f, evaluationDrawingCanvas);
            evaluationFolderButtonMap[drawing.Key].SetActive(true);
        }

        FolderRating folderRating;
        foreach(KeyValuePair<int,PlayerRatingData> rating in evaluationData.ratings)
        {
            folderRating = evaluationRatingMap[rating.Key];
            if(rating.Value.likeCount > 0)
            {
                folderRating.likeCountText.gameObject.SetActive(true);
                folderRating.likeCountText.text = "x" + rating.Value.likeCount.ToString();
            }
            if(rating.Value.dislikeCount > 0)
            {
                folderRating.dislikeCountText.gameObject.SetActive(true);
                folderRating.dislikeCountText.text = "x" + rating.Value.dislikeCount.ToString();
            }
        }
        active = true;
    }

    public void setPrompt(string adjective, string noun)
    {
        promptText.text = "EVALUATION:\n" + adjective + "\n" + noun;
    }

    public void expandDrawing(int index)
    {
        foreach (GameObject objectToHideOnDrawingExpand in objectsToHideOnDrawingExpand)
        {
            objectToHideOnDrawingExpand.SetActive(false);
        }

        foreach (KeyValuePair<int, GameObject> expandedDrawing in expandedDrawingMap)
        {
            if (expandedDrawing.Key == index)
            {
                expandedDrawing.Value.gameObject.SetActive(true);
            }
            else
            {
                expandedDrawing.Value.gameObject.SetActive(false);
            }
        }

        closeExpandedDrawingButton.SetActive(true);
    }

    public void closeExpandedDrawing()
    {
        foreach (GameObject objectToHideOnDrawingExpand in objectsToHideOnDrawingExpand)
        {
            objectToHideOnDrawingExpand.gameObject.SetActive(true);
        }

        foreach (KeyValuePair<int, GameObject> expandedDrawing in expandedDrawingMap)
        {
            expandedDrawing.Value.gameObject.SetActive(false);
        }

        closeExpandedDrawingButton.SetActive(false);
    }
}
