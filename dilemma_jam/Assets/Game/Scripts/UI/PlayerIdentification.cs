﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerIdentification : MonoBehaviour, IPointerDownHandler
{
    public ColourManager.BirdName birdName;
    public Image birdImage;
    public string playerName;

    public void OnPointerDown(PointerEventData eventData)
    {
        // this object was clicked - do something
        if (playerName == "")
        {
            MenuLobbyButtons.Instance.TrySelectingBird(birdName.ToString(), PhotonNetwork.NickName);
        }
    }
}
