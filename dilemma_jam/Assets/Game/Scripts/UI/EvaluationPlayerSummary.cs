﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationPlayerSummary : MonoBehaviour
{
    public int evaluationRound;
    public int cabinetIndex;
    public GameObject likeButtonObject, dislikeButtonObject;
    public Image likeButtonImage, dislikeButtonImage;
    public GameObject wasLikedObject, wasDislikedObject;
    public Text playerNameText;
    public Image playerBirdImage;
    public GameObject drawingParentObject;
    public float totalTimeToShowRatingObject = 0.75f;

    private float timeShowingRatingObject = -1f;

    private void Update()
    {
        if (timeShowingRatingObject > 0.0f)
        {
            timeShowingRatingObject += Time.deltaTime;

            if (timeShowingRatingObject > totalTimeToShowRatingObject)
            {
                timeShowingRatingObject = 0.0f;
                wasLikedObject.SetActive(false);
                wasDislikedObject.SetActive(false);
            }
        }
    }

    public void Like(int likedPlayerIndex)
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        GameManager.Instance.playerFlowManager.slidesRound.hideEvaluationLikeButtons(evaluationRound);
        disableDislikeButton(likedPlayerIndex);


        if (PhotonNetwork.IsMasterClient)
        {
            EvaluationData selectedEvaluationData = GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[evaluationRound];

            if (!selectedEvaluationData.ratings.ContainsKey(likedPlayerIndex))
            {
                selectedEvaluationData.ratings.Add(likedPlayerIndex, new PlayerRatingData());
            }

            selectedEvaluationData.ratings[likedPlayerIndex].likeCount++;
            selectedEvaluationData.ratings[likedPlayerIndex].target = selectedEvaluationData.drawings[likedPlayerIndex].author;

            if (likedPlayerIndex == cabinetIndex)
            {
                showRatingObject(wasLikedObject, "Like");
                hideRatingObject(wasDislikedObject);
            }

            GameNetwork.Instance.BroadcastQueue.Add("evaluation_slide_rating" + GameDelim.BASE + likedPlayerIndex.ToString() + GameDelim.BASE + evaluationRound.ToString() + GameDelim.BASE + "like");
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("rate_evaluation_slide" + GameDelim.BASE + likedPlayerIndex.ToString() + GameDelim.BASE + evaluationRound.ToString() + GameDelim.BASE + "like");
        }

    }

    public void Dislike(int dislikedPlayerIndex)
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        GameManager.Instance.playerFlowManager.slidesRound.hideEvaluationDislikeButtons(evaluationRound);
        disableLikeButton(dislikedPlayerIndex);


        if (PhotonNetwork.IsMasterClient)
        {
            EvaluationData selectedEvaluationData = GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[evaluationRound];

            if (!selectedEvaluationData.ratings.ContainsKey(dislikedPlayerIndex))
            {
                selectedEvaluationData.ratings.Add(dislikedPlayerIndex, new PlayerRatingData());
            }

            selectedEvaluationData.ratings[dislikedPlayerIndex].dislikeCount++;
            selectedEvaluationData.ratings[dislikedPlayerIndex].target = selectedEvaluationData.drawings[dislikedPlayerIndex].author;


            if (dislikedPlayerIndex == cabinetIndex)
            {
                showRatingObject(wasDislikedObject, "Sus" + UnityEngine.Random.Range(1, 5));
                hideRatingObject(wasLikedObject);
            }

            GameNetwork.Instance.BroadcastQueue.Add("evaluation_slide_rating" + GameDelim.BASE + dislikedPlayerIndex.ToString() + GameDelim.BASE + evaluationRound.ToString() + GameDelim.BASE + "dislike");
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("rate_evaluation_slide" + GameDelim.BASE + dislikedPlayerIndex.ToString() + GameDelim.BASE + evaluationRound.ToString() + GameDelim.BASE + "dislike");
        }

    }

    public void disableButton(GameObject buttonObject, Image buttonImage)
    {
        buttonObject.GetComponent<Button>().interactable = false;
        buttonImage.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    }

    public void showRatingObject(GameObject ratingObject, string sound)
    {
        AudioManager.Instance.PlaySound(sound);
        ratingObject.SetActive(true);

        timeShowingRatingObject = Time.deltaTime;
    }

    public void hideRatingObject(GameObject ratingObject)
    {
        ratingObject.SetActive(false);
    }

    public void disableLikeButton(int dislikedPlayerIndex)
    {
        if (dislikedPlayerIndex == cabinetIndex)
        {
            disableButton(likeButtonObject, likeButtonImage);
        }
        else
        {
            return;
        }
    }

    public void disableDislikeButton(int likedPlayerIndex)
    {
        if (likedPlayerIndex == cabinetIndex)
        {
            disableButton(dislikeButtonObject, dislikeButtonImage);
        }
        else
        {
            return;
        }
    }

    public void disableAllLikeButtons()
    {
        disableLikeButton(cabinetIndex);
    }

    public void disableAllDislikeButtons()
    {
        disableDislikeButton(cabinetIndex);
    }
}
