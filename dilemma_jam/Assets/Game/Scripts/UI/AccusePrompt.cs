﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccusePrompt : MonoBehaviour
{
    public Text prompt;
    public ColourManager.BirdName selectedBirdName;

    public void Accuse()
    {
        GameManager.Instance.playerFlowManager.accusationsRound.Accuse();
        gameObject.SetActive(false);
    }

    public void Close()
    {
        GameManager.Instance.playerFlowManager.accusationsRound.HideAccusePrompt(selectedBirdName);
        gameObject.SetActive(false);
    }
}
