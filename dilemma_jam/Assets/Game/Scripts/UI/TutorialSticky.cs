﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TutorialSticky : MonoBehaviour, IPointerDownHandler
{
    public Text optionA, optionB;
    public bool hasBeenPlaced = false;
    public bool hasBeenClicked = false;

    public void Click()
    {
        AudioManager.Instance.PlaySound("StickyPeel");
        optionA.gameObject.SetActive(false);
        optionB.gameObject.SetActive(false);
        gameObject.SetActive(false);
        hasBeenClicked = true;
    }

    public void Place(bool useOptionA)
    {
        optionA.gameObject.SetActive(false);
        optionB.gameObject.SetActive(false);
        AudioManager.Instance.PlaySound("StickyPlace");
        if (useOptionA)
        {
            optionA.gameObject.SetActive(true);
        }
        else
        {
            optionB.gameObject.SetActive(true);
        }
        gameObject.SetActive(true);
        hasBeenPlaced = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Click();
    }


}
