﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideContents : MonoBehaviour
{
    public Text prompt1, prompt2, prompt3;
    public GameObject drawing1, drawing2, drawing3;
    public GameObject finalSlideObject, summarySlideObject;
    public Image author1, author2, author3, author4, author5;
    public Image guesser, failer;
    public Text guess;
    public Text failText;
    public Text originalPromptReminder;

    public Text playerName1, playerName2, playerName3, playerName4, playerName5, playerNameGuesser, playerNameFailer;
    public int currentPromptIndex = 0, currentDrawingIndex = 0;
    public bool active = false;

    public List<SlideSummaryBird> allSlideSummaryBirds;

    private Dictionary<int, Text> promptSlides;
    private Dictionary<int, GameObject> drawingSlides;
    public Dictionary<int, SlideSummaryBird> slideSummaryBirdMap;

    public Canvas drawingCanvas;

    private int totalPrompts, totalDrawings;
    public bool failedRound;
    public int totalSlidesShown = 0;
    public int totalPlayers = 0;

    private bool isInitialized = false;

    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }

    }

    private void initialize()
    {
        promptSlides = new Dictionary<int, Text>();
        drawingSlides = new Dictionary<int, GameObject>();
        slideSummaryBirdMap = new Dictionary<int, SlideSummaryBird>();

        promptSlides.Add(0, prompt2);
        promptSlides.Add(1, prompt3);

        drawingSlides.Add(0, drawing1);
        drawingSlides.Add(1, drawing2);
        drawingSlides.Add(2, drawing3);

        foreach (SlideSummaryBird slideSummaryBird in allSlideSummaryBirds)
        {
            slideSummaryBirdMap.Add(slideSummaryBird.tab, slideSummaryBird);
        }

        isInitialized = true;
    }

    public void SetImage(Image imageToSet, string inBirdName)
    {
        if (inBirdName != "" && inBirdName != "none")
        {
            ColourManager.BirdName birdName = GameNetwork.getBirdNameFromCode(inBirdName);
            if (!ColourManager.Instance.birdMap.ContainsKey(birdName))
            {
                Debug.LogError("Could not find corresponding bird["+inBirdName+"] in the birdmap.");
                return;
            }
            imageToSet.sprite = ColourManager.Instance.birdMap[birdName].faceSprite;
        }
    }

    public void SetProgressIndicatorImage(int index, string inBirdName)
    {
        if (!isInitialized)
        {
            initialize();
        }

        if(inBirdName != "")
        {
            ColourManager.BirdName birdName = GameNetwork.getBirdNameFromCode(inBirdName);
            if (slideSummaryBirdMap.ContainsKey(index + 1) && ColourManager.Instance.birdMap.ContainsKey(birdName))
            {
                slideSummaryBirdMap[index + 1].birdName = birdName;
                slideSummaryBirdMap[index + 1].image.sprite = ColourManager.Instance.birdMap[birdName].faceSprite;
                if (GameManager.Instance.playerFlowManager.playerNameMap.ContainsKey(birdName))
                {
                    slideSummaryBirdMap[index + 1].playerName.text = GameManager.Instance.playerFlowManager.playerNameMap[birdName];
                    slideSummaryBirdMap[index + 1].playerName.color = ColourManager.Instance.birdMap[birdName].colour;
                }

            }
        }
    }

    public void SetAuthorColour(Text authorText, string inBirdName)
    {
        if (inBirdName != "")
        {
            ColourManager.BirdName birdName = GameNetwork.getBirdNameFromCode(inBirdName);
            authorText.color = ColourManager.Instance.birdMap[birdName].colour;
        }
    }

    public void SetAccuserColour(Text authorText, string inValue, string correctValue)
    {
        if (inValue != "")
        {
            authorText.color = inValue == correctValue ? Color.green : Color.red;
        }
    }

    public void SetText(Text authorText, string inValue)
    {
        if (inValue != "")
        {
            authorText.text = inValue;
        }
    }

    public void reset()
    {
        currentDrawingIndex = -1;
        currentPromptIndex = -1;
    }

    public void showOriginalPrompt()
    {
        prompt1.gameObject.SetActive(true);
    }

    public void showNextPrompt()
    {
        if (drawingSlides.ContainsKey(currentDrawingIndex))
        {
            drawingSlides[currentDrawingIndex].gameObject.SetActive(false);
        }

        currentPromptIndex++;
        slideSummaryBirdMap[currentPromptIndex * 2 + 2].gameObject.SetActive(true);
        promptSlides[currentPromptIndex].gameObject.SetActive(true);
        totalSlidesShown++;
    }

    public void showNextDrawing()
    {
        if (!isInitialized)
        {
            initialize();
        }

        if (promptSlides.ContainsKey(currentPromptIndex))
        {
            promptSlides[currentPromptIndex].gameObject.SetActive(false);
        }

        prompt1.gameObject.SetActive(false);
        originalPromptReminder.gameObject.SetActive(true);
        currentDrawingIndex++;

        if (drawingSlides.ContainsKey(currentDrawingIndex))
        {
            slideSummaryBirdMap[currentDrawingIndex * 2 + 1].gameObject.SetActive(true);
            drawingSlides[currentDrawingIndex].gameObject.SetActive(true);
            totalSlidesShown++;
        }
        else
        {
            Debug.LogError("Could not show drawing, missing the following index: " + currentDrawingIndex.ToString());
        }
        
    }

    public bool isFinished()
    {
        return totalSlidesShown >= totalPlayers;
    }

    public void showFinalSlide()
    {
        if (promptSlides.ContainsKey(currentPromptIndex))
        {
            promptSlides[currentPromptIndex].gameObject.SetActive(false);
        }
        if (drawingSlides.ContainsKey(currentDrawingIndex))
        {
            drawingSlides[currentDrawingIndex].gameObject.SetActive(false);
        }

        if (failedRound)
        {
            prompt1.gameObject.SetActive(false);
            originalPromptReminder.gameObject.SetActive(true);
            failText.gameObject.SetActive(true);
        }
        else
        {
            slideSummaryBirdMap[-1].gameObject.SetActive(true);
            guess.gameObject.SetActive(true);
        }
        finalSlideObject.SetActive(true);
    }

    public void showSummarySlide()
    {
        originalPromptReminder.gameObject.SetActive(false);
        finalSlideObject.SetActive(false);
        failText.gameObject.SetActive(false);
        summarySlideObject.SetActive(true);
    }

    public void setPrompt(int index, string promptText, ColourManager.BirdName author)
    {
        if (!isInitialized)
        {
            initialize();
        }
        switch (index)
        {
            case 2:
                prompt2.text = promptText;
                if (!slideSummaryBirdMap.ContainsKey(2))
                {
                    Debug.LogError("Could not find slide summary bird[2] in the map. Printing contents of the map:");

                    foreach (KeyValuePair<int, SlideSummaryBird> slideSummaryBird in slideSummaryBirdMap)
                    {
                        Debug.LogError("[" + slideSummaryBird.Key.ToString() + "]");
                    }

                }
                slideSummaryBirdMap[2].displayText.text = promptText;
                slideSummaryBirdMap[2].displayText.color = ColourManager.Instance.birdMap[author].colour;
                SetImage(slideSummaryBirdMap[4].image, author.ToString());
                break;
            case 4:
                prompt3.text = promptText;
                slideSummaryBirdMap[4].displayText.text = promptText;
                slideSummaryBirdMap[4].displayText.color = ColourManager.Instance.birdMap[author].colour;
                SetImage(slideSummaryBirdMap[4].image, author.ToString());
                break;
        }
    }

    public void setGuessed(string inGuess, string inGuesser, bool correct)
    {
        guess.text = "Guess:\n" + inGuess;

        int playerCount = GameManager.Instance.playerFlowManager.playerNameMap.Count;
        SetImage(guesser, inGuesser);
        SetImage(slideSummaryBirdMap[-1].image, inGuesser);
        failedRound = false;
        SetProgressIndicatorImage(-2, inGuesser);

        slideSummaryBirdMap[-1].displayText.text = "Guessed:\n" + inGuess;
        slideSummaryBirdMap[-1].displayText.color = correct ? Color.green : Color.red;
    }

    public void setCorrectAnswer(string correctAnswer)
    {
        slideSummaryBirdMap[-1].displayText2.text = "Original Prompt:\n" + correctAnswer;
    }

    public void setFailed(string inFailer, string inGuesser)
    {
        guess.text = "";

        SetImage(failer, inFailer);
        SetImage(guesser, inGuesser);
        SetImage(slideSummaryBirdMap[6].image, inFailer);
        failedRound = true;

        slideSummaryBirdMap[6].displayText.text = "Failed";
        slideSummaryBirdMap[6].displayText.color = Color.red;
    }
}
