﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class PresentationPrompt : MonoBehaviour
{
    public List<DrawingSlideButton> allDrawingSlideButtons;
    public List<PromptSlideButton> allPromptSlideButtons;
    public List<EvaluationSlideButton> allEvaluationSlideButtons;
    public List<EvaluationAuthorVisual> allEvaluationAuthorVisuals;
    public PromptSlideButton guessSlideButton;
    public int playerIndex = 0;
    public BirdName currentPlayerBird = BirdName.none;

    public int evaluationRoundIndex = 1;
    public Text switchButtonText;
    public Text evaluationPromptText;

    public enum SelectionState
    {
        chains, evaluations
    }
    public SelectionState currentSelectionState = SelectionState.chains;
    public GameObject chainsSelectionObject;
    public GameObject evaluationsSelectionObject;

    public List<BirdName> birdList;

    public Dictionary<int, DrawingSlideButton> drawingSlideButtonMap = new Dictionary<int, DrawingSlideButton>();
    public Dictionary<int, PromptSlideButton> promptSlideButtonMap = new Dictionary<int, PromptSlideButton>();
    public Dictionary<int, EvaluationSlideButton> evaluationSlideButtonMap = new Dictionary<int, EvaluationSlideButton>();
    public Dictionary<int, Dictionary<int, EvaluationAuthorVisual>> evaluationAuthorVisualMap = new Dictionary<int, Dictionary<int, EvaluationAuthorVisual>>();

    public Image currentBirdImage;

    public GameObject presentationPromptObject;
    public List<GameObject> objectsToHideOnOpen;
    public int playerCount = 0;
    public int roundCount = 0;

    private bool isInitialized = false;

    public void initialize()
    {
        if (!isInitialized)
        {
            IndexMap currentIndex;
            foreach (DrawingSlideButton drawingSlideButton in allDrawingSlideButtons)
            {
                currentIndex = drawingSlideButton.gameObject.GetComponent<IndexMap>();
                drawingSlideButtonMap.Add(currentIndex.index, drawingSlideButton);
            }

            foreach (PromptSlideButton promptSlideButton in allPromptSlideButtons)
            {
                currentIndex = promptSlideButton.gameObject.GetComponent<IndexMap>();
                promptSlideButtonMap.Add(currentIndex.index, promptSlideButton);
            }

            foreach(EvaluationSlideButton evaluationSlideButton in allEvaluationSlideButtons)
            {
                currentIndex = evaluationSlideButton.gameObject.GetComponent<IndexMap>();
                evaluationSlideButtonMap.Add(currentIndex.index, evaluationSlideButton);
            }


            foreach(EvaluationAuthorVisual evaluationAuthorVisual in allEvaluationAuthorVisuals)
            {
                if (!evaluationAuthorVisualMap.ContainsKey(evaluationAuthorVisual.roundIndex))
                {
                    evaluationAuthorVisualMap.Add(evaluationAuthorVisual.roundIndex, new Dictionary<int, EvaluationAuthorVisual>());
                }
                evaluationAuthorVisualMap[evaluationAuthorVisual.roundIndex].Add(evaluationAuthorVisual.playerIndex, evaluationAuthorVisual);
            }

            playerCount = GameManager.Instance.playerFlowManager.playerNameMap.Count;
            roundCount = GameManager.Instance.playerFlowManager.numberOfCabinetRounds;

            foreach (EvaluationSlideButton evaluationSlideButton in allEvaluationSlideButtons)
            {
                foreach (IndexMap drawing in evaluationSlideButton.allDrawings)
                {
                    evaluationSlideButton.drawingObjectMap.Add(drawing.index, drawing.gameObject);
                }
            }

            int iterator = 0;
            foreach (BirdName birdName in GameManager.Instance.playerFlowManager.playerNameMap.Keys)
            {
                birdList.Add(birdName);
                foreach (DrawingSlideButton drawingSlideButton in allDrawingSlideButtons)
                {
                    drawingSlideButton.birdNameIndexMap.Add(iterator, birdName);
                    drawingSlideButton.allDrawings[iterator].birdName = birdName;
                    drawingSlideButton.drawingObjectMap.Add(birdName, drawingSlideButton.allDrawings[iterator].gameObject);
                }

                foreach (PromptSlideButton promptSlideButton in allPromptSlideButtons)
                {
                    promptSlideButton.birdNameIndexMap.Add(iterator, birdName);
                    promptSlideButton.allPrefixes[iterator].birdName = birdName;
                    promptSlideButton.prefixObjectMap.Add(birdName, promptSlideButton.allPrefixes[iterator].GetComponent<Text>());
                    promptSlideButton.allNouns[iterator].birdName = birdName;
                    promptSlideButton.nounObjectMap.Add(birdName, promptSlideButton.allNouns[iterator].GetComponent<Text>());
                }

                foreach(EvaluationSlideButton evaluationSlideButton in allEvaluationSlideButtons)
                {
                    evaluationSlideButton.drawingAuthorMap.Add(iterator, birdName);
                }

                guessSlideButton.birdNameIndexMap.Add(iterator, birdName);
                guessSlideButton.allPrefixes[iterator].birdName = birdName;
                guessSlideButton.prefixObjectMap.Add(birdName, guessSlideButton.allPrefixes[iterator].GetComponent<Text>());
                guessSlideButton.allNouns[iterator].birdName = birdName;
                guessSlideButton.nounObjectMap.Add(birdName, guessSlideButton.allNouns[iterator].GetComponent<Text>());

                iterator++;
            }
            Transform currentTransform;
            foreach (CabinetDrawer cabinet in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap.Values)
            {
                if (!cabinet.chainData.active)
                {
                    continue;
                }
                //Load all prompts
                promptSlideButtonMap[2].prefixObjectMap[cabinet.chainData.prompts[2].author].text = cabinet.chainData.prompts[2].prefix;
                promptSlideButtonMap[2].prefixObjectMap[cabinet.chainData.prompts[2].author].color = ColourManager.Instance.birdMap[cabinet.chainData.prompts[2].author].colour;
                promptSlideButtonMap[2].nounObjectMap[cabinet.chainData.prompts[2].author].text = cabinet.chainData.prompts[2].noun;
                promptSlideButtonMap[2].nounObjectMap[cabinet.chainData.prompts[2].author].color = ColourManager.Instance.birdMap[cabinet.chainData.prompts[2].author].colour;

                if (roundCount >= 5)
                {
                    promptSlideButtonMap[4].prefixObjectMap[cabinet.chainData.prompts[4].author].text = cabinet.chainData.prompts[4].prefix;
                    promptSlideButtonMap[4].prefixObjectMap[cabinet.chainData.prompts[4].author].color = ColourManager.Instance.birdMap[cabinet.chainData.prompts[4].author].colour;
                    promptSlideButtonMap[4].nounObjectMap[cabinet.chainData.prompts[4].author].text = cabinet.chainData.prompts[4].noun;
                    promptSlideButtonMap[4].nounObjectMap[cabinet.chainData.prompts[4].author].color = ColourManager.Instance.birdMap[cabinet.chainData.prompts[4].author].colour;
                }

                currentTransform = drawingSlideButtonMap[1].drawingObjectMap[cabinet.chainData.drawings[1].author].transform;
                GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.chainData.drawings[1], currentTransform, currentTransform.position, new Vector3(0.08f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, drawingSlideButtonMap[1].drawingCanvas);

                currentTransform = drawingSlideButtonMap[3].drawingObjectMap[cabinet.chainData.drawings[3].author].transform;
                GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.chainData.drawings[3], currentTransform, currentTransform.position, new Vector3(0.08f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, drawingSlideButtonMap[3].drawingCanvas);

                if (roundCount >= 5)
                {
                    currentTransform = drawingSlideButtonMap[5].drawingObjectMap[cabinet.chainData.drawings[5].author].transform;
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.chainData.drawings[5], currentTransform, currentTransform.position, new Vector3(0.08f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, drawingSlideButtonMap[5].drawingCanvas);
                }

                //Load all guesses
                guessSlideButton.prefixObjectMap[cabinet.chainData.guesser].text = cabinet.chainData.prefixGuess;
                guessSlideButton.prefixObjectMap[cabinet.chainData.guesser].color = ColourManager.Instance.birdMap[cabinet.chainData.guesser].colour;
                guessSlideButton.nounObjectMap[cabinet.chainData.guesser].text = cabinet.chainData.nounGuess;
                guessSlideButton.nounObjectMap[cabinet.chainData.guesser].color = ColourManager.Instance.birdMap[cabinet.chainData.guesser].colour;
            }

            iterator = 1;
            foreach(KeyValuePair<int,EvaluationData> evaluationData in GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap)
            {
                
                foreach(KeyValuePair<int,PlayerDrawingInputData> drawing in evaluationData.Value.drawings)
                {
                    if (!evaluationSlideButtonMap.ContainsKey(drawing.Key))
                    {
                        Debug.LogError("Evaluation slide button map is missing drawing["+drawing.Key+"].");
                    }
                    else if (!evaluationSlideButtonMap[drawing.Key].drawingObjectMap.ContainsKey(evaluationData.Key))
                    {
                        Debug.LogError("Evaluation slide button map is missing round["+evaluationData.Key+"] for drawing["+drawing.Key+"].");
                    }
                    evaluationSlideButtonMap[drawing.Key].gameObject.SetActive(true);
                    currentTransform = evaluationSlideButtonMap[drawing.Key].drawingObjectMap[evaluationData.Key].transform;
                    GameManager.Instance.playerFlowManager.createDrawingLines(drawing.Value, currentTransform, currentTransform.position, new Vector3(0.08f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, evaluationSlideButtonMap[drawing.Key].drawingCanvas);
                    evaluationAuthorVisualMap[evaluationData.Key][drawing.Key].birdImage.sprite = ColourManager.Instance.birdMap[drawing.Value.author].faceSprite;
                    evaluationSlideButtonMap[drawing.Key].drawingObjectMap[evaluationData.Key].gameObject.SetActive(true);
                }
            }

            isInitialized = true;
        }
        currentPlayerBird = birdList[0];
        currentBirdImage.sprite = ColourManager.Instance.birdMap[currentPlayerBird].faceSprite;

        setCurrentSlides();
    }

    public void open()
    {
        presentationPromptObject.SetActive(true);
        Cursor.visible = true;
        foreach (GameObject objectToHideOnOpen in objectsToHideOnOpen)
        {
            objectToHideOnOpen.SetActive(false);
        }
        gameObject.SetActive(true);

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentMenuSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.accusePresentMenuSticky.Place(true);
            }
        }
    }

    public void shiftLeft()
    {
        switch (currentSelectionState)
        {
            case SelectionState.chains:
                playerIndex--;
                if (playerIndex < 0)
                {
                    playerIndex = playerCount - 1;
                }
                currentPlayerBird = birdList[playerIndex];
                currentBirdImage.sprite = ColourManager.Instance.birdMap[currentPlayerBird].faceSprite;

                setCurrentSlides();
                break;
            case SelectionState.evaluations:
                evaluationRoundIndex--;
                if(evaluationRoundIndex == 0)
                {
                    if (playerCount < 6)
                    {
                        evaluationRoundIndex = 2;
                    }
                    else
                    {
                        evaluationRoundIndex = 3;
                    }
                }
                setCurrentEvaluationSlides();
                break;
        }


    }

    public void shiftRight()
    {
        switch (currentSelectionState)
        {
            case SelectionState.chains:
                playerIndex++;
                if (playerIndex == playerCount)
                {
                    playerIndex = 0;
                }
                currentPlayerBird = birdList[playerIndex];
                currentBirdImage.sprite = ColourManager.Instance.birdMap[currentPlayerBird].faceSprite;

                setCurrentSlides();
                break;
            case SelectionState.evaluations:
                evaluationRoundIndex++;
                if((evaluationRoundIndex == 3 && playerCount < 6) ||
                    evaluationRoundIndex == 4)
                {
                    evaluationRoundIndex = 1;
                }
                setCurrentEvaluationSlides();
                break;
        }

    }

    public void cancel()
    {
        Cursor.visible = false;
        foreach (GameObject objectToHideOnOpen in objectsToHideOnOpen)
        {
            objectToHideOnOpen.SetActive(true);
        }
        gameObject.SetActive(false);
        presentationPromptObject.SetActive(false);
    }

    public void switchSelectionState()
    {
        switch (currentSelectionState)
        {
            case SelectionState.chains:
                setCurrentEvaluationSlides();
                chainsSelectionObject.SetActive(false);
                evaluationsSelectionObject.SetActive(true);
                switchButtonText.text = "Chains";
                currentSelectionState = SelectionState.evaluations;
                break;
            case SelectionState.evaluations:
                chainsSelectionObject.SetActive(true);
                evaluationsSelectionObject.SetActive(false);
                switchButtonText.text = "Evaluations";
                currentSelectionState = SelectionState.chains;
                
                break;
        }
    }

    private void setCurrentSlides()
    {
        foreach(DrawingSlideButton drawingSlideButton in drawingSlideButtonMap.Values)
        {
            foreach(GameObject drawingObject in drawingSlideButton.drawingObjectMap.Values)
            {
                drawingObject.SetActive(false);
            }
        }
        foreach(PromptSlideButton promptSlideButton in promptSlideButtonMap.Values)
        {
            foreach(Text prefixText in promptSlideButton.prefixObjectMap.Values)
            {
                prefixText.gameObject.SetActive(false);
            }
            foreach(Text nounText in promptSlideButton.nounObjectMap.Values)
            {
                nounText.gameObject.SetActive(false);
            }
        }

        foreach(Text prefixText in guessSlideButton.prefixObjectMap.Values)
        {
            prefixText.gameObject.SetActive(false);
        }
        foreach(Text nounText in guessSlideButton.nounObjectMap.Values)
        {
            nounText.gameObject.SetActive(false);
        }

        drawingSlideButtonMap[1].drawingObjectMap[currentPlayerBird].SetActive(true);

        promptSlideButtonMap[2].prefixObjectMap[currentPlayerBird].gameObject.SetActive(true);
        promptSlideButtonMap[2].nounObjectMap[currentPlayerBird].gameObject.SetActive(true);

        drawingSlideButtonMap[3].drawingObjectMap[currentPlayerBird].SetActive(true);

        guessSlideButton.prefixObjectMap[currentPlayerBird].gameObject.SetActive(true);
        guessSlideButton.nounObjectMap[currentPlayerBird].gameObject.SetActive(true);

        if (playerCount > 5)
        {
            promptSlideButtonMap[4].prefixObjectMap[currentPlayerBird].gameObject.SetActive(true);
            promptSlideButtonMap[4].nounObjectMap[currentPlayerBird].gameObject.SetActive(true);

            drawingSlideButtonMap[5].drawingObjectMap[currentPlayerBird].SetActive(true);

            promptSlideButtonMap[4].gameObject.SetActive(true);
            drawingSlideButtonMap[5].gameObject.SetActive(true);
        }
        else
        {
            promptSlideButtonMap[4].prefixObjectMap[currentPlayerBird].gameObject.SetActive(false);
            promptSlideButtonMap[4].nounObjectMap[currentPlayerBird].gameObject.SetActive(false);

            drawingSlideButtonMap[5].drawingObjectMap[currentPlayerBird].SetActive(false);

            promptSlideButtonMap[4].gameObject.SetActive(false);
            drawingSlideButtonMap[5].gameObject.SetActive(false);
        }
    }

    private void setCurrentEvaluationSlides()
    {
        foreach(KeyValuePair<int,Dictionary<int,EvaluationAuthorVisual>> group in evaluationAuthorVisualMap)
        {
            if(group.Key == evaluationRoundIndex)
            {
                for(int i = 1; i <= playerCount; i++)
                {
                    group.Value[i].gameObject.SetActive(true);
                }
            }
            else
            {
                foreach (EvaluationAuthorVisual authorVisual in group.Value.Values)
                {
                    authorVisual.gameObject.SetActive(false);
                }
            }
        }

        foreach(KeyValuePair<int,EvaluationSlideButton> evaluationSlideButton in evaluationSlideButtonMap)
        {
            foreach(IndexMap evaluationDrawing in evaluationSlideButton.Value.allDrawings)
            {
                evaluationDrawing.gameObject.SetActive(false);
            }
        }

        for (int i = 1; i <= playerCount; i++)
        {
            evaluationSlideButtonMap[i].drawingObjectMap[evaluationRoundIndex].SetActive(true);
        }

        evaluationPromptText.text = "EVALUATION:\n" + GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[evaluationRoundIndex].adjective + " " + GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[evaluationRoundIndex].noun;

    }

    public void startPresentation(int round)
    {
        if(round == -1)
        {
            round = roundCount + 1;
        }

        int cabinetID = -1;
        //Determine the cabinet
        foreach(CabinetDrawer cabinet in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap.Values)
        {
            if(round == (roundCount + 1))
            {
                if(cabinet.chainData.guesser == currentPlayerBird)
                {
                    cabinetID = cabinet.id;
                    break;
                }
            }
            else if(round % 2 == 0)
            {
                if(cabinet.chainData.prompts[round].author == currentPlayerBird)
                {
                    cabinetID = cabinet.id;
                    break;
                }
            }
            else
            {
                if(cabinet.chainData.drawings[round].author == currentPlayerBird)
                {
                    cabinetID = cabinet.id;
                    break;
                }

            }
        }

        if(cabinetID == -1)
        {
            Debug.LogError("Could not start presentation, could not isolate corresponding author of slide to be presented.");
            return;
        }

        foreach (GameObject objectToHideOnOpen in objectsToHideOnOpen)
        {
            objectToHideOnOpen.SetActive(true);
        }
        GameManager.Instance.playerFlowManager.accusationsRound.presentationSubround.continueButtonObject.SetActive(true);
        GameManager.Instance.playerFlowManager.accusationsRound.presentationButtonObject.SetActive(false);
        GameManager.Instance.playerFlowManager.accusationsRound.presentationPrompt.gameObject.SetActive(false);

        if (PhotonNetwork.IsMasterClient)
        {
            //Start the presentation
            GameManager.Instance.playerFlowManager.accusationsRound.initializePresentation(cabinetID, round, SettingsManager.Instance.birdName);

            //Broadcast to start the presentation
            GameNetwork.Instance.BroadcastQueue.Add("start_presentation" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + SettingsManager.Instance.birdName.ToString());
        }
        else
        {
            //Send message to server asking to start presentation
            GameNetwork.Instance.ToServerQueue.Add("start_presentation" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + SettingsManager.Instance.birdName.ToString());
        }
    }

    public void startEvaluationPresentation(int playerIndex)
    {
        foreach (GameObject objectToHideOnOpen in objectsToHideOnOpen)
        {
            objectToHideOnOpen.SetActive(true);
        }
        GameManager.Instance.playerFlowManager.accusationsRound.presentationSubround.continueButtonObject.SetActive(true);
        GameManager.Instance.playerFlowManager.accusationsRound.presentationButtonObject.SetActive(false);
        GameManager.Instance.playerFlowManager.accusationsRound.presentationPrompt.gameObject.SetActive(false);

        if (PhotonNetwork.IsMasterClient)
        {
            //Start the presentation
            GameManager.Instance.playerFlowManager.accusationsRound.initializeEvaluationPresentation(evaluationRoundIndex, playerIndex, SettingsManager.Instance.birdName);

            //Broadcast to start the presentation
            GameNetwork.Instance.BroadcastQueue.Add("start_evaluation_presentation" + GameDelim.BASE + evaluationRoundIndex.ToString() + GameDelim.BASE + playerIndex.ToString() + GameDelim.BASE + SettingsManager.Instance.birdName.ToString());
        }
        else
        {
            //Send message to server asking to start presentation
            GameNetwork.Instance.ToServerQueue.Add("start_evaluation_presentation" + GameDelim.BASE + evaluationRoundIndex.ToString() + GameDelim.BASE + playerIndex.ToString() + GameDelim.BASE + SettingsManager.Instance.birdName.ToString());
        }
    }

}
