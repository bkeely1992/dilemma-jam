﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationSlideContents : MonoBehaviour
{
    public bool active = false;
    public int round;
    public GameObject introSlideObject;
    public List<EvaluationDrawingGroupSlide> allDrawingGroupSlides;
    public GameObject summarySlideObject;
    public Text originalEvaluationReminderText;
    public Text introOriginalEvaluationText;
    public Text summaryInstructionsText;
    public List<EvaluationPlayerSummary> allEvaluationPlayerSummaries;

    private int currentDrawingGroupSlide = -1;

    public void disableAllLikeButtons()
    {
        foreach(EvaluationPlayerSummary playerSummary in allEvaluationPlayerSummaries)
        {
            playerSummary.disableAllLikeButtons();
        }
    }

    public void disableAllDislikeButtons()
    {
        foreach (EvaluationPlayerSummary playerSummary in allEvaluationPlayerSummaries)
        {
            playerSummary.disableAllDislikeButtons();
        }
    }

    public void showSummarySlide()
    {
        allDrawingGroupSlides[currentDrawingGroupSlide].gameObject.SetActive(false);
        originalEvaluationReminderText.gameObject.SetActive(false);
        allDrawingGroupSlides[currentDrawingGroupSlide].gameObject.SetActive(false);
        summarySlideObject.SetActive(true);
    }

    public bool isFinished()
    {
        return currentDrawingGroupSlide+1 >= allDrawingGroupSlides.Where(s => s.active).Count();
    }

    public void showNextDrawing()
    {
        introSlideObject.SetActive(false);
        if(currentDrawingGroupSlide >= 0)
        {
            allDrawingGroupSlides[currentDrawingGroupSlide].gameObject.SetActive(false);
        }
        
        currentDrawingGroupSlide++;
        originalEvaluationReminderText.gameObject.SetActive(true);
        allDrawingGroupSlides[currentDrawingGroupSlide].gameObject.SetActive(true);

    }

    public void showOriginalPrompt()
    {
        introSlideObject.SetActive(true);
    }

    public void reset()
    {
        currentDrawingGroupSlide = -1;
    }

}
