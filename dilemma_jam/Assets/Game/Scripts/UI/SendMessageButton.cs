﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendMessageButton : MonoBehaviour
{
    public InputField sendMessageInput;

    public void Send()
    {
        string sendingMessage = GameDelim.stripGameDelims(sendMessageInput.text);
        if (PhotonNetwork.IsMasterClient)
        {
            GameNetwork.Instance.BroadcastQueue.Add("slide_player_message" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + sendingMessage);

            //Update the message locally
            GameManager.Instance.playerFlowManager.slidesRound.setPeanutGalleryMessage(SettingsManager.Instance.birdName,sendingMessage);
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("slide_round_message" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + sendingMessage);
        }

        sendMessageInput.text = "";
    }
}
