﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideSummaryBird : MonoBehaviour
{
    public ColourManager.BirdName birdName;
    public Text playerName;
    public int cabinetID;
    public int tab;
    public Image image;
    public Text displayText, displayText2;
    public GameObject drawingPosition;
    public GameObject wasLikedObject, wasDislikedObject;
    public GameObject likeButtonObject, dislikeButtonObject;
    public Image likeButtonImage, dislikeButtonImage;
    public float totalTimeToShowRatingObject = 0.75f;

    private float timeShowingRatingObject = 0.0f;

    private void Start()
    {
        if(birdName == SettingsManager.Instance.birdName)
        {
            disableButton(likeButtonObject, likeButtonImage);
            disableButton(dislikeButtonObject, dislikeButtonImage);
        }
        if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.boss_rush)
        {
            dislikeButtonObject.SetActive(false);
        }
    }

    public void disableLikeButton()
    {
        disableButton(likeButtonObject, likeButtonImage);
    }

    public void disableDislikeButton()
    {
        disableButton(dislikeButtonObject, dislikeButtonImage);
    }

    private void disableButton(GameObject buttonObject, Image buttonImage)
    {
        buttonObject.GetComponent<Button>().interactable = false;
        buttonImage.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    }

    private void Update()
    {
        if(timeShowingRatingObject > 0.0f)
        {
            timeShowingRatingObject += Time.deltaTime;

            if(timeShowingRatingObject > totalTimeToShowRatingObject)
            {
                timeShowingRatingObject = 0.0f;
                wasLikedObject.SetActive(false);
                wasDislikedObject.SetActive(false);
            }
        }
    }

    public void Like()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        GameManager.Instance.playerFlowManager.slidesRound.hideLikeButtons(cabinetID);
        disableButton(dislikeButtonObject, dislikeButtonImage);

        if (PhotonNetwork.IsMasterClient)
        {
            ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;

            if (tab == -1)
            {
                tab = GameManager.Instance.playerFlowManager.playerNameMap.Count;
            }

            if (!selectedCabinetData.ratings.ContainsKey(tab))
            {
                selectedCabinetData.ratings.Add(tab, new PlayerRatingData());
            }
            
            selectedCabinetData.ratings[tab].likeCount++;
            selectedCabinetData.ratings[tab].target = selectedCabinetData.playerOrder[tab];

            showLikedObject();
            GameNetwork.Instance.BroadcastQueue.Add("slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "like");
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("rate_slide" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "like");
        }
        
    }

    public void Dislike()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        GameManager.Instance.playerFlowManager.slidesRound.hideDislikeButtons(cabinetID);
        disableButton(likeButtonObject, likeButtonImage);

        if (PhotonNetwork.IsMasterClient)
        {
            ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;

            if (tab == -1)
            {
                tab = GameManager.Instance.playerFlowManager.playerNameMap.Count;
            }

            if (!selectedCabinetData.ratings.ContainsKey(tab))
            {
                selectedCabinetData.ratings.Add(tab, new PlayerRatingData());
            }
            selectedCabinetData.ratings[tab].dislikeCount++;
            selectedCabinetData.ratings[tab].target = selectedCabinetData.playerOrder[tab];
            showDislikedObject();
            GameNetwork.Instance.BroadcastQueue.Add("slide_rating" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "dislike");
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("rate_slide" + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + tab.ToString() + GameDelim.BASE + "dislike");
        }
        
    }

    public void showLikedObject()
    {
        AudioManager.Instance.PlaySound("Like");
        wasLikedObject.SetActive(true);
        wasDislikedObject.SetActive(false);
        timeShowingRatingObject = Time.deltaTime;
    }

    public void showDislikedObject()
    {
        AudioManager.Instance.PlaySound("Sus" + UnityEngine.Random.Range(1, 5));
        wasLikedObject.SetActive(false);
        wasDislikedObject.SetActive(true);
        timeShowingRatingObject = Time.deltaTime;
    }
}
