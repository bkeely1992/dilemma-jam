﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class PromptSlideButton : MonoBehaviour
{
    public List<BirdTag> allPrefixes;
    public List<BirdTag> allNouns;
    public Dictionary<BirdName, Text> prefixObjectMap = new Dictionary<BirdName, Text>();
    public Dictionary<BirdName, Text> nounObjectMap = new Dictionary<BirdName, Text>();

    public Dictionary<int, BirdName> birdNameIndexMap = new Dictionary<int, BirdName>();
}
