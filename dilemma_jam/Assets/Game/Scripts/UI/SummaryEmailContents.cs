﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryEmailContents : MonoBehaviour
{
    public Text outcomeText;
    public List<GameObject> voteRoundObjects = new List<GameObject>();

    private Dictionary<int, GameObject> voteRoundObjectMap = new Dictionary<int, GameObject>();
    private int currentRoundIndex = 1;

    public void Initialize()
    {
        IndexMap currentIndex;
        foreach(GameObject voteRoundObject in voteRoundObjects)
        {
            currentIndex = voteRoundObject.GetComponent<IndexMap>();
            voteRoundObjectMap.Add(currentIndex.index, voteRoundObject);
        }
    }

    public void setNumberOfVoteRounds(int numberOfVoteRounds)
    {
        if(numberOfVoteRounds == -1 || numberOfVoteRounds == 0)
        {
            return;
        }
        int iterator = 0;
        IndexMap currentIndex;

        voteRoundObjectMap.Clear();
        foreach (GameObject voteRoundObject in voteRoundObjects)
        {
            if(iterator == numberOfVoteRounds)
            {
                break;
            }
            iterator++;
            currentIndex = voteRoundObject.GetComponent<IndexMap>();
            voteRoundObjectMap.Add(currentIndex.index, voteRoundObject);
        }
    }

    public void shiftPageLeft()
    {
        if (voteRoundObjectMap.ContainsKey(currentRoundIndex))
        {
            voteRoundObjectMap[currentRoundIndex].SetActive(false);
            currentRoundIndex--;
            if (currentRoundIndex <= 0)
            {
                currentRoundIndex = voteRoundObjectMap.Count;
            }
            voteRoundObjectMap[currentRoundIndex].SetActive(true);
        }
    }

    public void shiftPageRight()
    {
        if (voteRoundObjectMap.ContainsKey(currentRoundIndex))
        {
            voteRoundObjectMap[currentRoundIndex].SetActive(false);
            currentRoundIndex++;
            if (currentRoundIndex > voteRoundObjectMap.Count)
            {
                currentRoundIndex = 1;
            }
            voteRoundObjectMap[currentRoundIndex].SetActive(true);
        }

    }
}
