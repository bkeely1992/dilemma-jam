﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialButton : MonoBehaviour
{
    public GameObject containerObject;
    public GameObject tutorialButtonStatusObject;
    public Text tutorialStatusText;
    public bool isOn = true;

    public void Click(string settingName)
    {
        isOn = !isOn;
        tutorialButtonStatusObject.gameObject.SetActive(!isOn);
        tutorialStatusText.text = isOn ? "On" : "Off";
        SettingsManager.Instance.UpdateSetting(settingName, isOn);
    }
}
