﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static BotchingForm.BotcherRoundData;
using static ColourManager;
using static PlayerDrawingInputData;

public class BotchingForm : MonoBehaviour
{
    public Dictionary<int, BotcherRoundData> botcherRoundDataMap = new Dictionary<int, BotcherRoundData>();
    public int currentBotcherRound = 0;

    public Dictionary<int, Dictionary<int, string>> prefixesToAdd = new Dictionary<int, Dictionary<int, string>>();
    public Dictionary<int, Dictionary<int, string>> nounsToAdd = new Dictionary<int, Dictionary<int, string>>();

    public Text correctAnswerText;
    public Text instructionsText;
    public GameObject drawingContainerObject;

    public GameObject coverupContainer;
    public GameObject addPromptsContainer;
    public GameObject limitedDrawingContainer;

    public List<BlockingGroup> allLimitedDrawingBlockerGroups;
    public List<IndexMap> allLimitedDrawingControllers;
    public List<PossiblePrompt> allAddPrefixTexts;
    public List<PossiblePrompt> allAddNounTexts;

    public GameObject blockerPrefab;
    public float blockerSize;
    public Canvas botcherDrawingCanvas;

    public Vector3 botchingFormLineOffset;
    public Vector3 botchingFormLineScaleAdjustment;

    private Dictionary<int, BlockingGroup> limitedDrawingBlockerGroupMap = new Dictionary<int, BlockingGroup>();
    private Dictionary<int, DrawingController> limitedDrawingControllerMap = new Dictionary<int, DrawingController>();
    private Dictionary<int, Text> addPrefixTextMap = new Dictionary<int, Text>();
    private Dictionary<int, Text> addNounTextMap = new Dictionary<int, Text>();
    private int activeBlockingGroupIndex = -1;
    private bool isInitialized = false;
    private Coverup selectedCoverup;
    private int selectedPrefixIndex = -1;
    private int selectedNounIndex = -1;

    public class BotcherRoundData
    {
        public enum BotcherMode
        {
            limited_drawing, coverup, add_prompts, invalid
        }
        public BotcherMode selectedBotcherMode = BotcherMode.invalid;
        public int evaluationRoundIndex = -1;
        public int cabinetIndex = -1;
        public int drawingRound = -1;
    }

    public void initialize()
    {
        if (!isInitialized)
        {
            limitedDrawingBlockerGroupMap.Clear();
            foreach(BlockingGroup limitedDrawingBlockerGroup in allLimitedDrawingBlockerGroups)
            {
                limitedDrawingBlockerGroupMap.Add(limitedDrawingBlockerGroup.index, limitedDrawingBlockerGroup);
            }

            limitedDrawingControllerMap.Clear();
            foreach(IndexMap limitedDrawingController in allLimitedDrawingControllers)
            {
                limitedDrawingControllerMap.Add(limitedDrawingController.index, limitedDrawingController.gameObject.GetComponent<DrawingController>());
            }

            isInitialized = true;
        }

        
    }

    public void addBotcherRound(BotcherMode inSelectedBotcherMode, int inEvaluationRoundIndex, int inCabinetIndex, int inDrawingRound)
    {
        BotcherRoundData botcherRoundData = new BotcherRoundData()
        {
            selectedBotcherMode = inSelectedBotcherMode,
            evaluationRoundIndex = inEvaluationRoundIndex,
            cabinetIndex = inCabinetIndex,
            drawingRound = inDrawingRound
        };
        botcherRoundDataMap.Add(inEvaluationRoundIndex, botcherRoundData);
    }

    public void addBotcherPrompts(int botcherRound, List<string> prefixes, List<string> nouns)
    {

        addBotcherPrefixes(botcherRound, prefixes);
        addBotcherNouns(botcherRound, nouns);
        
    }

    public void addBotcherPrefixes(int botcherRound, List<string> prefixes)
    {
        if (!prefixesToAdd.ContainsKey(botcherRound))
        {
            prefixesToAdd.Add(botcherRound, new Dictionary<int, string>());
        }

        for (int i = 0; i < prefixes.Count; i++)
        {
            prefixesToAdd[botcherRound].Add(i + 1, prefixes[i]);
        }
    }

    public void addBotcherNouns(int botcherRound, List<string> nouns)
    {
        if (!nounsToAdd.ContainsKey(botcherRound))
        {
            nounsToAdd.Add(botcherRound, new Dictionary<int, string>());
        }

        for (int i = 0; i < nouns.Count; i++)
        {
            nounsToAdd[botcherRound].Add(i + 1, nouns[i]);
        }
    }

    public void show(int botcherRound)
    {
        if (!isInitialized)
        {
            initialize();
        }

        gameObject.SetActive(true);
        addPromptsContainer.SetActive(false);
        coverupContainer.SetActive(false);
        limitedDrawingContainer.SetActive(false);
        
        currentBotcherRound = botcherRound;

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.Place(false);
            }

        }

        BotcherRoundData selectedBotcherRoundData = botcherRoundDataMap[botcherRound];
        switch (selectedBotcherRoundData.selectedBotcherMode)
        {
            case BotcherMode.add_prompts:
                showAddPromptsForm(botcherRound);
                break;
            case BotcherMode.coverup:
                showCoverupForm(botcherRound);
                break;
            case BotcherMode.limited_drawing:
                showLimitedDrawingForm(botcherRound);
                break;
            case BotcherMode.invalid:
                break;
        }
    }

    private void showAddPromptsForm(int botcherRound)
    {
        instructionsText.text = "ADD A PROMPT AND NOUN TO GUESS OPTIONS";
        correctAnswerText.gameObject.SetActive(false);
        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[botcherRound];
        ChainData currentChainData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentBotcherRoundData.cabinetIndex].chainData;

        foreach(KeyValuePair<int,string> prefixToAdd in prefixesToAdd[botcherRound])
        {
            allAddPrefixTexts[prefixToAdd.Key-1].displayText.text = prefixToAdd.Value;
        }
        foreach(KeyValuePair<int,string> nounToAdd in nounsToAdd[botcherRound])
        {
            allAddNounTexts[nounToAdd.Key-1].displayText.text = nounToAdd.Value;
        }
        if (!currentChainData.drawings.ContainsKey(currentBotcherRoundData.drawingRound))
        {
            Debug.LogError("Missing drawing from round[" + currentBotcherRoundData.drawingRound + "] in cabinet[" + currentBotcherRoundData.cabinetIndex + "]. Cannot show botcher form.");
        }
        showDrawing(currentChainData.drawings[currentBotcherRoundData.drawingRound]);
        addPromptsContainer.SetActive(true);

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.Place(false);
            }
        }
        
    }

    private void showCoverupForm(int botcherRound)
    {
        instructionsText.text = "COVER UP\nA QUADRANT";
        correctAnswerText.gameObject.SetActive(false);
        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[botcherRound];
        ChainData currentChainData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentBotcherRoundData.cabinetIndex].chainData;

        if (!currentChainData.drawings.ContainsKey(currentBotcherRoundData.drawingRound))
        {
            Debug.LogError("Missing drawing from round["+ currentBotcherRoundData.drawingRound + "] in cabinet["+ currentBotcherRoundData.cabinetIndex + "]. Cannot show botcher form.");
        }
        showDrawing(currentChainData.drawings[currentBotcherRoundData.drawingRound]);
        coverupContainer.SetActive(true);
    }

    private void showLimitedDrawingForm(int botcherRound)
    {
        instructionsText.text = "DRAW WITHIN\nPROVIDED BOX";
        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[botcherRound];
        ChainData currentChainData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentBotcherRoundData.cabinetIndex].chainData;

        //correctAnswerText.text = "CORRECT ANSWER:\n" + currentChainData.correctPrefix + " " + currentChainData.correctNoun;
        correctAnswerText.gameObject.SetActive(false);

        foreach(BlockingGroup blockerGroup in allLimitedDrawingBlockerGroups)
        {
            blockerGroup.gameObject.SetActive(false);
        }
        activeBlockingGroupIndex = UnityEngine.Random.Range(1, allLimitedDrawingBlockerGroups.Count+1);
        BlockingGroup currentBlockingGroup = limitedDrawingBlockerGroupMap[activeBlockingGroupIndex];
        limitedDrawingControllerMap[activeBlockingGroupIndex].gameObject.SetActive(true);
        DrawingRound drawingRound = GameManager.Instance.playerFlowManager.drawingRound;
        if (!currentChainData.drawings.ContainsKey(currentBotcherRoundData.drawingRound))
        {
            Debug.LogError("Missing drawing from round[" + currentBotcherRoundData.drawingRound + "] in cabinet[" + currentBotcherRoundData.cabinetIndex + "]. Cannot show botcher form.");
        }
        PlayerDrawingInputData drawing = currentChainData.drawings[currentBotcherRoundData.drawingRound];

        //get the highest sort order number for the existing drawing
        int highestSortOrder = -1;
        foreach(DrawingLine line in drawing.lines)
        {
            if(line.sortingOrder > highestSortOrder)
            {
                highestSortOrder = line.sortingOrder;
            }
        }

        currentBlockingGroup.drawingController.initialize();
        currentBlockingGroup.drawingController.currentSortingOrder = highestSortOrder + 1;
        currentBlockingGroup.drawingController.setButtonDrawingSize("Big", drawingRound.bigBrushSize, drawingRound.bigBrushUIDotSize, "Big");
        currentBlockingGroup.drawingController.setButtonDrawingSize("Medium", drawingRound.mediumBrushSize, drawingRound.mediumBrushUIDotSize, "Medium");
        currentBlockingGroup.drawingController.setButtonDrawingSize("Small", drawingRound.smallBrushSize, drawingRound.smallBrushUIDotSize, "Small");
        Bird playerBird = ColourManager.Instance.birdMap[drawing.author];
        currentBlockingGroup.drawingController.setButtonDrawingColour("Colour", playerBird.colour, drawingRound.colourLinePrefab, playerBird.material, "Colour");
        currentBlockingGroup.drawingController.setButtonDrawingColour("Light", playerBird.bgColour, drawingRound.colourLinePrefab, playerBird.bgLineMaterial, "Light");
        currentBlockingGroup.drawingController.setButtonDrawingColour("Base", Color.black, drawingRound.baseLinePrefab, drawingRound.baseLineMaterial, "Base");
        currentBlockingGroup.drawingController.setButtonDrawingColour("Erase", Color.white, drawingRound.eraseLinePrefab, drawingRound.eraseLineMaterial, "Erase");
        currentBlockingGroup.drawingController.changeCurrentDrawingColour("Base");
        currentBlockingGroup.drawingController.changeCurrentDrawingSize("Medium");
        currentBlockingGroup.drawingController.toolbarContainer.SetActive(true);
        currentBlockingGroup.gameObject.SetActive(true);

        showDrawing(drawing);
        limitedDrawingContainer.SetActive(true);

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.Place(false);
            }
        }

    }

    private void showDrawing(PlayerDrawingInputData drawing)
    {
        foreach (Transform child in drawingContainerObject.transform)
        {
            Destroy(child.gameObject);
        }
        GameManager.Instance.playerFlowManager.createDrawingLines(drawing, drawingContainerObject.transform, drawingContainerObject.transform.position, new Vector3(0.85f, 0.8f, 0.65f), 0.2f, 0.1f, 0.05f, botcherDrawingCanvas);
    }

    public BotcherMode getRandomBotcherMode()
    {
        List<BotcherMode> allBotcherModes = new List<BotcherMode>() { BotcherMode.limited_drawing};
        int randomIndex = UnityEngine.Random.Range(0, allBotcherModes.Count);
        return allBotcherModes[randomIndex];

    }

    public void submit()
    {
        BotcherRoundData selectedBotcherRoundData = botcherRoundDataMap[currentBotcherRound];


        if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.hasBeenClicked)
        {
            GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.Click();
        }

        switch (selectedBotcherRoundData.selectedBotcherMode)
        {
            case BotcherMode.add_prompts:
                submitAddedPrompts();
                break;
            case BotcherMode.coverup:
                submitCoverup();
                break;
            case BotcherMode.limited_drawing:
                submitLimitedDrawing();
                break;
            case BotcherMode.invalid:
                break;
        }
    }

    public void selectPrefixToAdd(int index)
    {
        foreach (PossiblePrompt prefix in allAddPrefixTexts)
        {
            if (prefix.identifier == index.ToString())
            {
                prefix.backgroundImage.color = Color.green;
                selectedPrefixIndex = index;
            }
            else
            {
                prefix.backgroundImage.color = Color.white;
            }
        }
    }

    public void selectNounToAdd(int index)
    {
        foreach (PossiblePrompt noun in allAddNounTexts)
        {
            if (noun.identifier == index.ToString())
            {
                noun.backgroundImage.color = Color.green;
                selectedNounIndex = index;
            }
            else
            {
                noun.backgroundImage.color = Color.white;
            }
        }
    }

    public void selectCoverup(string coverupValue)
    {
        switch (coverupValue)
        {
            case "top_left":
                selectedCoverup = Coverup.top_left;
                break;
            case "top_right":
                selectedCoverup = Coverup.top_right;
                break;
            case "bottom_left":
                selectedCoverup = Coverup.bottom_left;
                break;
            case "bottom_right":
                selectedCoverup = Coverup.bottom_right;
                break;
            default:
                Debug.LogError("Invalid coverup value["+coverupValue+"] provided to select coverup method.");
                return;
        }
    }

    private void submitAddedPrompts()
    {
        if(selectedPrefixIndex == -1 && selectedNounIndex == -1)
        {
            return;
        }
        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[currentBotcherRound];
        ChainData currentChainData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentBotcherRoundData.cabinetIndex].chainData;
        if (PhotonNetwork.IsMasterClient)
        {
            if(selectedPrefixIndex != -1)
            {
                currentChainData.selectedBotcherPrefix = prefixesToAdd[currentBotcherRound][selectedPrefixIndex+1];
            }
            if(selectedNounIndex != -1)
            {
                currentChainData.selectedBotcherNoun = nounsToAdd[currentBotcherRound][selectedNounIndex+1];
            }
        }
        else
        {
            if (selectedPrefixIndex != -1)
            {
                GameNetwork.Instance.ToServerQueue.Add("botcher_prefix_option" + GameDelim.BASE + currentBotcherRoundData.cabinetIndex.ToString() + GameDelim.BASE + prefixesToAdd[currentBotcherRound][selectedPrefixIndex+1]);
            }
            if (selectedNounIndex != -1)
            {
                GameNetwork.Instance.ToServerQueue.Add("botcher_noun_option" + GameDelim.BASE + currentBotcherRoundData.cabinetIndex.ToString() + GameDelim.BASE + nounsToAdd[currentBotcherRound][selectedNounIndex+1]);

            }
        }

        foreach (PossiblePrompt noun in allAddNounTexts)
        {
            noun.backgroundImage.color = Color.white;
        }
        foreach (PossiblePrompt prefix in allAddPrefixTexts)
        {
            prefix.backgroundImage.color = Color.white;
        }
        transitionToEvaluation();

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.Click();
            }
        }

    }

    private void submitCoverup()
    {
        if(selectedCoverup == Coverup.none)
        {
            return;
        }
        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[currentBotcherRound];
        if (PhotonNetwork.IsMasterClient)
        {
            GameManager.Instance.playerFlowManager.drawingRound.addBotcherCoverup(currentBotcherRoundData.cabinetIndex, currentBotcherRoundData.drawingRound, selectedCoverup);
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("add_botcher_coverup" + GameDelim.BASE + currentBotcherRoundData.cabinetIndex + GameDelim.BASE + currentBotcherRoundData.drawingRound + GameDelim.BASE + selectedCoverup);
        }

        transitionToEvaluation();
    }

    private void submitLimitedDrawing()
    {
        DrawingController drawingController = limitedDrawingBlockerGroupMap[activeBlockingGroupIndex].drawingController;
        if (!drawingController.hasDrawingLines())
        {
            Debug.LogError("Botcher limited drawing doesn't have lines.");
            return;
        }

        BotcherRoundData currentBotcherRoundData = botcherRoundDataMap[currentBotcherRound];
        ChainData currentChainData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentBotcherRoundData.cabinetIndex].chainData;
        PlayerDrawingInputData existingDrawing = currentChainData.drawings[currentBotcherRoundData.drawingRound];
        BirdName nextInQueue;

        List<DrawingLine> newLines = drawingController.getDrawingLines();
        foreach(DrawingLine newLine in newLines)
        {
            newLine.author = existingDrawing.author;
            for(int i = 0; i < newLine.positions.Count; i++)
            {
                newLine.positions[i] = new Vector3(newLine.positions[i].x*botchingFormLineScaleAdjustment.x, newLine.positions[i].y*botchingFormLineScaleAdjustment.y, newLine.positions[i].z) + botchingFormLineOffset;
            }
        }

        existingDrawing.lines.AddRange(newLines);
        drawingController.clearDrawingLines();
        drawingController.toolbarContainer.SetActive(false);
        drawingController.gameObject.SetActive(false);
        if (PhotonNetwork.IsMasterClient)
        {
            nextInQueue = currentChainData.playerOrder[currentBotcherRoundData.drawingRound + 1];
            GameManager.Instance.playerFlowManager.drawingRound.setCabinetDrawingContents(currentBotcherRoundData.cabinetIndex, currentBotcherRoundData.drawingRound, existingDrawing);
            if (nextInQueue != SettingsManager.Instance.birdName)
            {
                GameManager.Instance.gameFlowManager.addTransitionCondition("botcher_update_receipt:" + nextInQueue);
                GameManager.Instance.playerFlowManager.drawingRound.prepareLinesForClient(existingDrawing, currentBotcherRoundData.cabinetIndex, currentBotcherRoundData.drawingRound, nextInQueue, DrawingType.botcher_update);
            }
        }
        else
        {
            GameManager.Instance.playerFlowManager.drawingRound.prepareLinesForServer(existingDrawing, existingDrawing.author, currentBotcherRoundData.cabinetIndex, currentBotcherRoundData.drawingRound, DrawingType.botcher_update);
        }
        transitionToEvaluation();

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.Click();
            }
        }

    }

    public void skip()
    {
        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.addPromptBotcherSticky.Click();
            }
            if (!GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.limitedDrawingBotcherSticky.Click();
            }
            if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.evaluationBotcherSticky.Click();
            }
        }


        transitionToEvaluation();
    }

    private void transitionToEvaluation()
    {
        gameObject.SetActive(false);
        GameManager.Instance.playerFlowManager.drawingRound.evaluationDrawingBoard.gameObject.SetActive(true);
        GameManager.Instance.playerFlowManager.drawingRound.evaluationFormParent.gameObject.SetActive(true);
        GameManager.Instance.submitBtn.SetActive(true);
        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.Place(true);
            }
        }

    }
}
