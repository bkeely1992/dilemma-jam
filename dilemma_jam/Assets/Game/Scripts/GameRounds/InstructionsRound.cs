﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsRound : PlayerRound
{
    public Image deskPlayerImage;
    public Text alignmentText;

    public List<TutorialSticky> allCabinetStickies;
    public TutorialSticky roleDeskSticky, traitorPromptsDeskSticky, removeDeskSticky, responsesDeskSticky, slidesRatingSticky, slidesChatSticky, accuseFolderSticky, accusePlayerSticky,
                            accuseRevealSticky, accusePresentButtonSticky, accusePresentMenuSticky, accuseFolderZoomSticky, accusePresentationViewerSticky, accusePresentationPresenterSticky,
                            evaluationsSticky, limitedDrawingBotcherSticky, addPromptBotcherSticky, evaluationBotcherSticky;
    public bool hasSeenFirstCabinet = false, hasGottenFirstCabinet = false, hasClickedFirstCabinet = false;
    public GameObject lanyardObject, botcherScribbles, botcherNotes;
    public Animator bossNoteWorkerAnimator, bossNoteBotcherAnimator, vultureNoteAnimator;

    public float timeBeforeShowingBotcherScribbles, timeBeforeShowingBotcherNotes;

    private Dictionary<int, TutorialSticky> cabinetStickyMap;
    private float timeInRoundSoFar = 0.0f;
    private bool hasShownBotcherScribbles = false, hasShownBotcherNotes = false;
    public bool active = false;

    public override void StartRound()
    {
        base.StartRound();
        hasShownBotcherScribbles = false;
        hasShownBotcherNotes = false;
        hasGottenFirstCabinet = false;
        hasSeenFirstCabinet = false;

        cabinetStickyMap = new Dictionary<int, TutorialSticky>();
        foreach(TutorialSticky cabinetSticky in allCabinetStickies)
        {
            cabinetStickyMap.Add(cabinetSticky.gameObject.GetComponent<IndexMap>().index, cabinetSticky);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            initializeInstructionsRound();
        }

        playerFlowManager.drawingRound.deskRenderer.color = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].bgColour;
    }

    private void initializeInstructionsRound()
    {
        Dictionary<int, CabinetDrawer> cabinetDrawerMap = playerFlowManager.drawingRound.cabinetDrawerMap;
        bool playerIsBotcher;
        

        //Distribute the alignments
        foreach (KeyValuePair<ColourManager.BirdName, PlayerData> player in gameFlowManager.gamePlayers)
        {
            if (SettingsManager.Instance.birdName == player.Key)
            {
                playerIsBotcher = player.Value.playerRole == GameFlowManager.PlayerRole.botcher;
                alignmentText.text = (playerIsBotcher ? "BOTCHER" : "WORKER");
                playerFlowManager.playerRole = player.Value.playerRole;
                alignmentText.color = playerIsBotcher ? GameManager.Instance.traitorColour : GameManager.Instance.workerColour;
                lanyardObject.SetActive(true);
                
                if (SettingsManager.Instance.GetSetting("stickies"))
                {
                    removeDeskSticky.Place(true);

                    if (playerIsBotcher)
                    {
                        roleDeskSticky.Place(false);
                    }
                    else
                    {
                        roleDeskSticky.Place(true);
                    }
                }

                if (playerIsBotcher)
                {
                    vultureNoteAnimator.SetBool("Slide", true);
                    bossNoteBotcherAnimator.SetBool("Slide", true);
                }
                else
                {
                    bossNoteWorkerAnimator.SetBool("Slide", true);
                }
            }
            else
            {
                GameNetwork.Instance.ToPlayerQueue.Add(player.Key.ToString() + GameDelim.BASE + "role_distribution" + GameDelim.BASE + player.Value.playerRole.ToString());
            }
        }

        active = true;
    }

    private void Update()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (active
            && playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher)
        {
            timeInRoundSoFar += Time.deltaTime;

            if (timeInRoundSoFar > timeBeforeShowingBotcherScribbles
                && !hasShownBotcherScribbles)
            {
                hasShownBotcherScribbles = true;
                botcherScribbles.SetActive(true);
                AudioManager.Instance.PlaySound("Scribble");
            }
            else if (timeInRoundSoFar > timeBeforeShowingBotcherNotes
                && !hasShownBotcherNotes)
            {
                hasShownBotcherNotes = true;
                botcherNotes.SetActive(true);
                AudioManager.Instance.PlaySound("Scribble");
            }

        }

    }

    public void showCabinetSticky(int index, bool messageToShow)
    {
        cabinetStickyMap[index].Place(messageToShow);
        if (messageToShow)
        {
            hasSeenFirstCabinet = true;
        }
        else
        {
            hasGottenFirstCabinet = true;
        }
    }

    public void hideCabinetStickies()
    {
        hasClickedFirstCabinet = true;
        hasGottenFirstCabinet = true;
        hasSeenFirstCabinet = true;
        removeDeskSticky.Click();

        foreach (KeyValuePair<int,TutorialSticky> cabinetSticky in cabinetStickyMap)
        {
            cabinetSticky.Value.Click();
        }
    }

    public void handleCabinetOpening(int id, bool isCurrentPlayer)
    {
        if (!hasGottenFirstCabinet)
        {
            if (isCurrentPlayer)
            {
                showCabinetSticky(id, false);
            }
            else if (!hasSeenFirstCabinet)
            {
                showCabinetSticky(id, true);
            }
        }
        else if (!hasSeenFirstCabinet)
        {
            if (!isCurrentPlayer)
            {
                showCabinetSticky(id, true);
            }
        }
    }
}
