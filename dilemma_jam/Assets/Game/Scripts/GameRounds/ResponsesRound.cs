﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ResponsesRound : PlayerRound
{
    public List<PossiblePrompt> allPossiblePrefixes;
    public List<PossiblePrompt> allPossibleNouns;
    public Text failedCompletionText;
    public GameObject containerObject;
    public Transform lastDrawingObject;
    public Canvas drawingCanvas;
    public Vector3 responsesDrawingOffset;

    public List<SpriteRenderer> responseFolderRenderers;

    private Dictionary<string, PossiblePrompt> possiblePrefixMap;
    private Dictionary<string, PossiblePrompt> possibleNounMap;
    private int responseCabinetIndex = -1;

    private string correctPrefix = "";
    private string correctNoun = "";
    private string prefixGuess = "";
    private string nounGuess = "";
    private bool prefixesAreLoaded = false, nounsAreLoaded = false;
    

    void Start()
    {
        possiblePrefixMap = new Dictionary<string, PossiblePrompt>();
        possibleNounMap = new Dictionary<string, PossiblePrompt>();

        foreach (PossiblePrompt possiblePrefix in allPossiblePrefixes)
        {
            possiblePrefixMap.Add(possiblePrefix.identifier, possiblePrefix);
        }

        foreach (PossiblePrompt possibleNoun in allPossibleNouns)
        {
            possibleNounMap.Add(possibleNoun.identifier, possibleNoun);
        }
    }

    public override void StartRound()
    {
        base.StartRound();

        CabinetDrawer selectedCabinet = null;
        ChainData selectedCabinetData = null;
        
        if(responseCabinetIndex == -1)
        {
            Debug.LogError("Player has not been assigned a cabinet for the response round.");
            return;
        }

        selectedCabinet = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[responseCabinetIndex];
        foreach(int drawerID in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap.Keys)
        {
            GameManager.Instance.playerFlowManager.drawingRound.setDrawerAsUsed(drawerID);
        }
        
        selectedCabinetData = selectedCabinet.chainData;

        //Set possible prefixes and nouns
        for (int i = 1; i <= selectedCabinetData.possiblePrefixes.Count; i++)
        {
            try
            {
                if (playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher &&
                    selectedCabinetData.possiblePrefixes[i - 1] == selectedCabinetData.correctPrefix)
                {
                    possiblePrefixMap[i.ToString()].isCorrect = true;
                    possiblePrefixMap[i.ToString()].backgroundImage.color = Color.green;
                }

                possiblePrefixMap[i.ToString()].displayText.text = selectedCabinetData.possiblePrefixes[i - 1];
                possiblePrefixMap[i.ToString()].gameObject.SetActive(true);
            }
            catch
            {
                Debug.LogError("Possible prefix map could not access cabinet[" + i.ToString() + "] at index[" + i.ToString() + "]");
                foreach (KeyValuePair<string, PossiblePrompt> possiblePrefix in possiblePrefixMap)
                {
                    Debug.LogError("PossiblePrompt[" + possiblePrefix.Key + "]: " + possiblePrefix.Value.displayText.text);
                }
            }
        }
        for (int i = 1; i <= selectedCabinetData.possibleNouns.Count; i++)
        {
            try
            {
                if (playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher &&
                    selectedCabinetData.possibleNouns[i - 1] == selectedCabinetData.correctNoun)
                {
                    possibleNounMap[i.ToString()].isCorrect = true;
                    possibleNounMap[i.ToString()].backgroundImage.color = Color.green;
                }

                possibleNounMap[i.ToString()].displayText.text = selectedCabinetData.possibleNouns[i - 1];
                possibleNounMap[i.ToString()].gameObject.SetActive(true);
            }
            catch
            {
                Debug.LogError("Possible noun map could not access cabinet[" + i.ToString() + "] at index[" + i.ToString() + "]");
                foreach (KeyValuePair<string, PossiblePrompt> possibleNoun in possibleNounMap)
                {
                    Debug.LogError("PossiblePrompt[" + possibleNoun.Key + "]: " + possibleNoun.Value.displayText.text);
                }
            }
        }

        foreach (SpriteRenderer folderRenderer in responseFolderRenderers)
        {
            //folderRenderer.color = ColourManager.Instance.birdMap[SettingsManager.Instance.birdName].colour;
        }

        playerFlowManager.drawingRound.responsesFormAnimator.SetBool("Slide", true);

        if (selectedCabinetData.drawings.ContainsKey(playerFlowManager.numberOfCabinetRounds))
        {
            PlayerDrawingInputData selectedDrawingData = selectedCabinetData.drawings[playerFlowManager.numberOfCabinetRounds];
            playerFlowManager.createDrawingLines(selectedDrawingData, lastDrawingObject, responsesDrawingOffset, new Vector3(0.75f, 0.75f, 1), 0.2f, 0.1f, 0.05f, drawingCanvas);
        }

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            //playerFlowManager.instructionRound.responsesDeskSticky.Place(true);
        }
    }

    public void setCorrectGuess(string inPrefix, string inNoun)
    {
        correctPrefix = inPrefix;
        correctNoun = inNoun;

        foreach(PossiblePrompt prefix in allPossiblePrefixes)
        {
            if(prefix.displayText.text == correctPrefix)
            {
                prefix.backgroundImage.color = Color.green;
                prefix.isCorrect = true;
            }
        }

        foreach (PossiblePrompt noun in allPossibleNouns)
        {
            if (noun.displayText.text == correctNoun)
            {
                noun.backgroundImage.color = Color.green;
                noun.isCorrect = true;
            }
        }
    }

    public PossiblePrompt getPossiblePrefix(string inString)
    {
        return possiblePrefixMap[inString];
    }

    public PossiblePrompt getPossibleNoun(string inString)
    {
        return possibleNounMap[inString];
    }

    public void setPossiblePrefixGuess(string prefixGuessIndex)
    {
        prefixGuess = possiblePrefixMap[prefixGuessIndex].displayText.text;

        //Unhighlight other prefix buttons
        foreach(KeyValuePair<string,PossiblePrompt> prefix in possiblePrefixMap)
        {
            if (prefix.Value.isCorrect)
            {
                prefix.Value.backgroundImage.color = Color.green;
            }
            else
            {
                prefix.Value.backgroundImage.color = Color.white;
            }
        }


        //Highlight the button
        possiblePrefixMap[prefixGuessIndex].backgroundImage.color = Color.yellow;
    }

    public void setPossibleNounGuess(string nounGuessIndex)
    {
        nounGuess = possibleNounMap[nounGuessIndex].displayText.text;

        //Unhighlight other noun buttons
        foreach (KeyValuePair<string, PossiblePrompt> noun in possibleNounMap)
        {
            if (noun.Value.isCorrect)
            {
                noun.Value.backgroundImage.color = Color.green;
            }
            else
            {
                noun.Value.backgroundImage.color = Color.white;
            }
            
        }

        //Highlight the button
        possibleNounMap[nounGuessIndex].backgroundImage.color = Color.yellow;
    }

    public void GuessPrompt()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (!gameFlowManager)
        {
            gameFlowManager = GameManager.Instance.gameFlowManager;
        }
        
        AudioManager.Instance.PlaySound("ButtonPress");

        if(nounGuess == "" || prefixGuess == "")
        {
            return;
        }

        foreach (PossiblePrompt possiblePrefixes in allPossiblePrefixes)
        {
            possiblePrefixes.gameObject.SetActive(false);
        }
        foreach (PossiblePrompt possibleNouns in allPossibleNouns)
        {
            possibleNouns.gameObject.SetActive(false);
        }

        //Clear the drawings
        List<LineRenderer> drawingLines = playerFlowManager.drawingRound.drawingBoard.GetComponentsInChildren<LineRenderer>().ToList();

        for (int i = drawingLines.Count - 1; i >= 0; i--)
        {
            Destroy(drawingLines[i].gameObject);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            playerFlowManager.addGuessPrompt(SettingsManager.Instance.birdName, prefixGuess, nounGuess, responseCabinetIndex);

            if (GameManager.Instance.gameFlowManager.isRoundOver())
            {
                Debug.LogError("Ready to transition from responses round.");
                GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;
            }
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("prompt_guess" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + prefixGuess + GameDelim.BASE + nounGuess + GameDelim.BASE + responseCabinetIndex.ToString());
        }
        playerFlowManager.drawingRound.responsesFormAnimator.SetBool("Slide", false);
    }

    public void forceSubmit()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (!gameFlowManager)
        {
            gameFlowManager = GameManager.Instance.gameFlowManager;
        }

        AudioManager.Instance.PlaySound("ButtonPress");

        foreach (PossiblePrompt possiblePrefixes in allPossiblePrefixes)
        {
            possiblePrefixes.gameObject.SetActive(false);
        }
        foreach (PossiblePrompt possibleNouns in allPossibleNouns)
        {
            possibleNouns.gameObject.SetActive(false);
        }

        //Clear the drawings
        List<LineRenderer> drawingLines = playerFlowManager.drawingRound.drawingBoard.GetComponentsInChildren<LineRenderer>().ToList();

        for (int i = drawingLines.Count - 1; i >= 0; i--)
        {
            Destroy(drawingLines[i].gameObject);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            playerFlowManager.addGuessPrompt(SettingsManager.Instance.birdName, prefixGuess, nounGuess, responseCabinetIndex);
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("prompt_guess" + GameDelim.BASE + SettingsManager.Instance.birdName + GameDelim.BASE + prefixGuess + GameDelim.BASE + nounGuess + GameDelim.BASE + responseCabinetIndex.ToString());
        }
        playerFlowManager.drawingRound.responsesFormAnimator.SetBool("Slide", false);
    }

    public void setPossiblePrefixes(int cabinetID, List<string> inPossiblePrefixes)
    {
        responseCabinetIndex = cabinetID;

        PossiblePrompt currentPossiblePrefix;
        int iterator = 1;
        foreach (string inPossiblePrefix in inPossiblePrefixes)
        {
            currentPossiblePrefix = getPossiblePrefix(iterator.ToString());
            currentPossiblePrefix.displayText.text = inPossiblePrefix;
            currentPossiblePrefix.gameObject.SetActive(true);
            iterator++;
        }

        prefixesAreLoaded = true;

        foreach (PossiblePrompt prefix in allPossiblePrefixes)
        {
            if (prefix.displayText.text == correctPrefix)
            {
                prefix.backgroundImage.color = Color.green;
            }
        }

        if (!PhotonNetwork.IsMasterClient)
        {
            if (nounsAreLoaded)
            {
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "response_round_ready:" +SettingsManager.Instance.birdName);
            }
        }
        
    }

    public void setPossibleNouns(int cabinetID, List<string> inPossibleNouns)
    {
        responseCabinetIndex = cabinetID;

        PossiblePrompt currentPossibleNoun;
        int iterator = 1;
        foreach (string inPossibleNoun in inPossibleNouns)
        {
            currentPossibleNoun = getPossibleNoun(iterator.ToString());
            currentPossibleNoun.displayText.text = inPossibleNoun;
            currentPossibleNoun.gameObject.SetActive(true);
            iterator++;
        }

        nounsAreLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
        {
            if (prefixesAreLoaded)
            {
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "response_round_ready:" + SettingsManager.Instance.birdName);
            }
        }

        foreach (PossiblePrompt noun in allPossibleNouns)
        {
            if (noun.displayText.text == correctNoun)
            {
                noun.backgroundImage.color = Color.green;
            }
        }

    }
}
