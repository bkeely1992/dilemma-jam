﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class PresentationSubround : MonoBehaviour
{
    public GameObject presentationSpriteLayer;
    public float presentationSubroundTime;
    public float savedAccusationVoteRoundTime;

    public GameObject continueButtonObject;

    public List<PresentationDrawingContainer> allMainPresentationDrawingContainers;
    public List<PresentationPromptContainer> allMainPresentationPromptContainers;
    public Dictionary<int, PresentationDrawingContainer> mainPresentationDrawingContainerMap = new Dictionary<int, PresentationDrawingContainer>();

    public List<PresentationDrawingContainer> allNextPresentationDrawingContainers;
    public List<PresentationPromptContainer> allNextPresentationPromptContainers;
    public Dictionary<int, PresentationDrawingContainer> nextPresentationDrawingContainerMap = new Dictionary<int, PresentationDrawingContainer>();

    public List<PresentationDrawingContainer> allPreviousPresentationDrawingContainers;
    public List<PresentationPromptContainer> allPreviousPresentationPromptContainers;
    public Dictionary<int, PresentationDrawingContainer> previousPresentationDrawingContainerMap = new Dictionary<int, PresentationDrawingContainer>();

    public List<PresentationDrawingContainer> allEvaluationDrawingContainers;
    public List<IndexMap> allEvaluationPromptContainers;

    public Dictionary<int, Dictionary<int, Text>> mainPromptMap = new Dictionary<int, Dictionary<int, Text>>();
    public Dictionary<int, Dictionary<int, GameObject>> mainDrawingMap = new Dictionary<int, Dictionary<int, GameObject>>();

    public Dictionary<int, Dictionary<int, Text>> previousPromptMap = new Dictionary<int, Dictionary<int, Text>>();
    public Dictionary<int, Dictionary<int, GameObject>> previousDrawingMap = new Dictionary<int, Dictionary<int, GameObject>>();

    public Dictionary<int, Dictionary<int, Text>> nextPromptMap = new Dictionary<int, Dictionary<int, Text>>();
    public Dictionary<int, Dictionary<int, GameObject>> nextDrawingMap = new Dictionary<int, Dictionary<int, GameObject>>();

    public Dictionary<int, Dictionary<int, SpriteRenderer>> mainPromptFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();
    public Dictionary<int, Dictionary<int, SpriteRenderer>> mainDrawingFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();

    public Dictionary<int, Dictionary<int, SpriteRenderer>> previousPromptFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();
    public Dictionary<int, Dictionary<int, SpriteRenderer>> previousDrawingFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();

    public Dictionary<int, Dictionary<int, SpriteRenderer>> nextPromptFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();
    public Dictionary<int, Dictionary<int, SpriteRenderer>> nextDrawingFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();

    public Dictionary<int, Dictionary<int, GameObject>> evaluationDrawingMap = new Dictionary<int, Dictionary<int, GameObject>>();
    public Dictionary<int, Dictionary<int, SpriteRenderer>> evaluationFaceMap = new Dictionary<int, Dictionary<int, SpriteRenderer>>();
    public Dictionary<int, Text> evaluationPromptMap = new Dictionary<int, Text>();

    public List<IndexMap> allCorrectPrompts;
    public Dictionary<int, Text> correctPromptMap = new Dictionary<int, Text>();

    public List<GameObject> objectsToShowOnStart;
    public List<GameObject> objectsToHideOnStart;
    public List<GameObject> objectsToHideOnEnd;

    public Image presenterImage;
    public bool isActive = false;

    public Vector3 previousDrawingOffset;
    public Vector3 mainDrawingOffset;
    public Vector3 nextDrawingOffset;

    public ViewerDrawing viewerDrawings;
    public PresentationDrawing presentorDrawings;

    bool isInitialized = false;

    private BirdName currentPresenter = BirdName.none;

    public void initialize()
    {
        if (!isInitialized)
        {
            int cabinetID;

            #region main presentation
            foreach (PresentationDrawingContainer presentationDrawingContainer in allMainPresentationDrawingContainers)
            {
                presentationDrawingContainer.initialize();
                mainPresentationDrawingContainerMap.Add(presentationDrawingContainer.cabinetID, presentationDrawingContainer);
                cabinetID = presentationDrawingContainer.cabinetID;
                if (!mainDrawingMap.ContainsKey(cabinetID))
                {
                    mainDrawingMap.Add(cabinetID, new Dictionary<int, GameObject>());
                }
                
                foreach (KeyValuePair<int, GameObject> drawingObject in presentationDrawingContainer.drawingMap)
                {
                    if (!mainDrawingMap[cabinetID].ContainsKey(drawingObject.Key))
                    {
                        mainDrawingMap[cabinetID].Add(drawingObject.Key, drawingObject.Value);
                    }
                }
                if (!mainDrawingFaceMap.ContainsKey(cabinetID))
                {
                    mainDrawingFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int,SpriteRenderer> drawingFace in presentationDrawingContainer.faceMap)
                {
                    if (!mainDrawingFaceMap[cabinetID].ContainsKey(drawingFace.Key))
                    {
                        mainDrawingFaceMap[cabinetID].Add(drawingFace.Key, drawingFace.Value);
                    }
                }
            }

            foreach (PresentationPromptContainer presentationPromptContainer in allMainPresentationPromptContainers)
            {
                presentationPromptContainer.initialize();
                cabinetID = presentationPromptContainer.cabinetID;
                if (!mainPromptMap.ContainsKey(cabinetID))
                {
                    mainPromptMap.Add(cabinetID, new Dictionary<int, Text>());
                }

                foreach (KeyValuePair<int, Text> promptText in presentationPromptContainer.promptMap)
                {
                    if (!mainPromptMap[cabinetID].ContainsKey(promptText.Key))
                    {
                        mainPromptMap[cabinetID].Add(promptText.Key, promptText.Value);
                    }

                }

                if (!mainPromptFaceMap.ContainsKey(cabinetID))
                {
                    mainPromptFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> promptFace in presentationPromptContainer.faceMap)
                {
                    if (!mainPromptFaceMap[cabinetID].ContainsKey(promptFace.Key))
                    {
                        mainPromptFaceMap[cabinetID].Add(promptFace.Key, promptFace.Value);
                    }
                }
            }
            #endregion

            #region next presentation

            foreach (PresentationDrawingContainer presentationDrawingContainer in allNextPresentationDrawingContainers)
            {
                presentationDrawingContainer.initialize();
                nextPresentationDrawingContainerMap.Add(presentationDrawingContainer.cabinetID, presentationDrawingContainer);
                cabinetID = presentationDrawingContainer.cabinetID;
                if (!nextDrawingMap.ContainsKey(cabinetID))
                {
                    nextDrawingMap.Add(cabinetID, new Dictionary<int, GameObject>());
                }
                foreach (KeyValuePair<int, GameObject> drawingObject in presentationDrawingContainer.drawingMap)
                {
                    if (!nextDrawingMap[cabinetID].ContainsKey(drawingObject.Key))
                    {
                        nextDrawingMap[cabinetID].Add(drawingObject.Key, drawingObject.Value);
                    }
                }
                if (!nextDrawingFaceMap.ContainsKey(cabinetID))
                {
                    nextDrawingFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> drawingFace in presentationDrawingContainer.faceMap)
                {
                    if (!nextDrawingFaceMap[cabinetID].ContainsKey(drawingFace.Key))
                    {
                        nextDrawingFaceMap[cabinetID].Add(drawingFace.Key, drawingFace.Value);
                    }
                }
            }

            foreach (PresentationPromptContainer presentationPromptContainer in allNextPresentationPromptContainers)
            {
                presentationPromptContainer.initialize();
                cabinetID = presentationPromptContainer.cabinetID;
                if (!nextPromptMap.ContainsKey(cabinetID))
                {
                    nextPromptMap.Add(cabinetID, new Dictionary<int, Text>());
                }

                foreach (KeyValuePair<int, Text> promptText in presentationPromptContainer.promptMap)
                {
                    if (!nextPromptMap[cabinetID].ContainsKey(promptText.Key))
                    {
                        nextPromptMap[cabinetID].Add(promptText.Key, promptText.Value);
                    }

                }

                if (!nextPromptFaceMap.ContainsKey(cabinetID))
                {
                    nextPromptFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> promptFace in presentationPromptContainer.faceMap)
                {
                    if (!nextPromptFaceMap[cabinetID].ContainsKey(promptFace.Key))
                    {
                        nextPromptFaceMap[cabinetID].Add(promptFace.Key, promptFace.Value);
                    }
                }
            }

            #endregion

            #region previous presentation
            foreach (PresentationDrawingContainer presentationDrawingContainer in allPreviousPresentationDrawingContainers)
            {
                presentationDrawingContainer.initialize();
                previousPresentationDrawingContainerMap.Add(presentationDrawingContainer.cabinetID, presentationDrawingContainer);
                cabinetID = presentationDrawingContainer.cabinetID;
                if (!previousDrawingMap.ContainsKey(cabinetID))
                {
                    previousDrawingMap.Add(cabinetID, new Dictionary<int, GameObject>());
                }
                foreach (KeyValuePair<int, GameObject> drawingObject in presentationDrawingContainer.drawingMap)
                {
                    if (!previousDrawingMap[cabinetID].ContainsKey(drawingObject.Key))
                    {
                        previousDrawingMap[cabinetID].Add(drawingObject.Key, drawingObject.Value);
                    }
                }

                if (!previousDrawingFaceMap.ContainsKey(cabinetID))
                {
                    previousDrawingFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> drawingFace in presentationDrawingContainer.faceMap)
                {
                    if (!previousDrawingFaceMap[cabinetID].ContainsKey(drawingFace.Key))
                    {
                        previousDrawingFaceMap[cabinetID].Add(drawingFace.Key, drawingFace.Value);
                    }
                }
            }

            foreach (PresentationPromptContainer presentationPromptContainer in allPreviousPresentationPromptContainers)
            {
                presentationPromptContainer.initialize();
                cabinetID = presentationPromptContainer.cabinetID;
                if (!previousPromptMap.ContainsKey(cabinetID))
                {
                    previousPromptMap.Add(cabinetID, new Dictionary<int, Text>());
                }

                foreach (KeyValuePair<int, Text> promptText in presentationPromptContainer.promptMap)
                {
                    if (!previousPromptMap[cabinetID].ContainsKey(promptText.Key))
                    {
                        previousPromptMap[cabinetID].Add(promptText.Key, promptText.Value);
                    }

                }

                if (!previousPromptFaceMap.ContainsKey(cabinetID))
                {
                    previousPromptFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> promptFace in presentationPromptContainer.faceMap)
                {
                    if (!previousPromptFaceMap[cabinetID].ContainsKey(promptFace.Key))
                    {
                        previousPromptFaceMap[cabinetID].Add(promptFace.Key, promptFace.Value);
                    }
                }
            }
            #endregion

            #region evaluations
            foreach (PresentationDrawingContainer evaluationDrawingContainer in allEvaluationDrawingContainers)
            {
                cabinetID = evaluationDrawingContainer.cabinetID;
                evaluationDrawingContainer.initialize();
                if (!evaluationDrawingMap.ContainsKey(cabinetID))
                {
                    evaluationDrawingMap.Add(cabinetID, new Dictionary<int, GameObject>());
                }
                foreach (KeyValuePair<int, GameObject> drawingObject in evaluationDrawingContainer.drawingMap)
                {
                    if (!evaluationDrawingMap[cabinetID].ContainsKey(drawingObject.Key))
                    {
                        evaluationDrawingMap[cabinetID].Add(drawingObject.Key, drawingObject.Value);
                    }
                }
                if (!evaluationFaceMap.ContainsKey(cabinetID))
                {
                    evaluationFaceMap.Add(cabinetID, new Dictionary<int, SpriteRenderer>());
                }
                foreach (KeyValuePair<int, SpriteRenderer> evaluationFace in evaluationDrawingContainer.faceMap)
                {
                    evaluationFaceMap[cabinetID].Add(evaluationFace.Key, evaluationFace.Value);
                }
            }
            foreach (IndexMap evaluationPrompt in allEvaluationPromptContainers)
            {
                if (!evaluationPromptMap.ContainsKey(evaluationPrompt.index))
                {
                    evaluationPromptMap.Add(evaluationPrompt.index, evaluationPrompt.gameObject.GetComponent<Text>());
                }
            }
            #endregion

            foreach (IndexMap correctPrompt in allCorrectPrompts)
            {
                if (!correctPromptMap.ContainsKey(correctPrompt.index))
                {
                    correctPromptMap.Add(correctPrompt.index, correctPrompt.gameObject.GetComponent<Text>());
                }
            }

            //Iterate through the cabinets and populate the texts and the drawings
            foreach (KeyValuePair<int, CabinetDrawer> cabinet in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap)
            {
                if (!cabinet.Value.chainData.active)
                {
                    continue;
                }

                #region previous prompts
                if (previousPromptMap[cabinet.Key].ContainsKey(2) &&
                    cabinet.Value.chainData.prompts.ContainsKey(2))
                {
                    previousPromptMap[cabinet.Key][2].text = cabinet.Value.chainData.prompts[2].prefix + " " + cabinet.Value.chainData.prompts[2].noun;
                    previousPromptMap[cabinet.Key][2].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].colour;
                    previousPromptFaceMap[cabinet.Key][2].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].faceSprite;
                    
                }
                if (previousPromptMap[cabinet.Key].ContainsKey(4) &&
                    cabinet.Value.chainData.prompts.ContainsKey(4))
                {
                    previousPromptMap[cabinet.Key][4].text = cabinet.Value.chainData.prompts[4].prefix + " " + cabinet.Value.chainData.prompts[4].noun;
                    previousPromptMap[cabinet.Key][4].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].colour;
                    previousPromptFaceMap[cabinet.Key][4].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].faceSprite;
                }
                if (previousPromptMap[cabinet.Key].ContainsKey(-1))
                {
                    previousPromptMap[cabinet.Key][-1].text = cabinet.Value.chainData.prefixGuess + " " + cabinet.Value.chainData.nounGuess;
                    previousPromptMap[cabinet.Key][-1].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].colour;
                    previousPromptFaceMap[cabinet.Key][-1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].faceSprite;
                }
                #endregion

                #region main prompts
                if (mainPromptMap[cabinet.Key].ContainsKey(2) &&
                    cabinet.Value.chainData.prompts.ContainsKey(2))
                {
                    mainPromptMap[cabinet.Key][2].text = cabinet.Value.chainData.prompts[2].prefix + " " + cabinet.Value.chainData.prompts[2].noun;
                    mainPromptMap[cabinet.Key][2].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].colour;
                    mainPromptFaceMap[cabinet.Key][2].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].faceSprite;
                }
                if (mainPromptMap[cabinet.Key].ContainsKey(4) &&
                    cabinet.Value.chainData.prompts.ContainsKey(4))
                {
                    mainPromptMap[cabinet.Key][4].text = cabinet.Value.chainData.prompts[4].prefix + " " + cabinet.Value.chainData.prompts[4].noun;
                    mainPromptMap[cabinet.Key][4].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].colour;
                    mainPromptFaceMap[cabinet.Key][4].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].faceSprite;
                }
                if (mainPromptMap[cabinet.Key].ContainsKey(-1))
                {
                    mainPromptMap[cabinet.Key][-1].text = cabinet.Value.chainData.prefixGuess + " " + cabinet.Value.chainData.nounGuess;
                    mainPromptMap[cabinet.Key][-1].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].colour;
                    mainPromptFaceMap[cabinet.Key][-1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].faceSprite;
                }
                #endregion

                #region next prompts
                if (nextPromptMap[cabinet.Key].ContainsKey(2) &&
                    cabinet.Value.chainData.prompts.ContainsKey(2))
                {
                    nextPromptMap[cabinet.Key][2].text = cabinet.Value.chainData.prompts[2].prefix + " " + cabinet.Value.chainData.prompts[2].noun;
                    nextPromptMap[cabinet.Key][2].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].colour;
                    nextPromptFaceMap[cabinet.Key][2].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[2].author].faceSprite;
                }
                if (nextPromptMap[cabinet.Key].ContainsKey(4) &&
                    cabinet.Value.chainData.prompts.ContainsKey(4))
                {
                    nextPromptMap[cabinet.Key][4].text = cabinet.Value.chainData.prompts[4].prefix + " " + cabinet.Value.chainData.prompts[4].noun;
                    nextPromptMap[cabinet.Key][4].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].colour;
                    nextPromptFaceMap[cabinet.Key][4].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.prompts[4].author].faceSprite;
                }
                if (nextPromptMap[cabinet.Key].ContainsKey(-1))
                {
                    nextPromptMap[cabinet.Key][-1].text = cabinet.Value.chainData.prefixGuess + " " + cabinet.Value.chainData.nounGuess;
                    nextPromptMap[cabinet.Key][-1].color = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].colour;
                    nextPromptFaceMap[cabinet.Key][-1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.guesser].faceSprite;
                }
                #endregion

                #region previous drawings
                if (previousDrawingMap[cabinet.Key].ContainsKey(1) &&
                    cabinet.Value.chainData.drawings.ContainsKey(1))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[1], previousDrawingMap[cabinet.Key][1].transform, previousDrawingMap[cabinet.Key][1].transform.position+previousDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, previousPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    previousDrawingFaceMap[cabinet.Key][1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[1].author].faceSprite;
                }
                if (previousDrawingMap[cabinet.Key].ContainsKey(3) &&
                    cabinet.Value.chainData.drawings.ContainsKey(3))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[3], previousDrawingMap[cabinet.Key][3].transform, previousDrawingMap[cabinet.Key][3].transform.position + previousDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, previousPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    previousDrawingFaceMap[cabinet.Key][3].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[3].author].faceSprite;
                }
                if (previousDrawingMap[cabinet.Key].ContainsKey(5) &&
                    cabinet.Value.chainData.drawings.ContainsKey(5))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[5], previousDrawingMap[cabinet.Key][5].transform, previousDrawingMap[cabinet.Key][5].transform.position + previousDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, previousPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    previousDrawingFaceMap[cabinet.Key][5].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[5].author].faceSprite;
                }

                #endregion

                #region main drawings
                if (mainDrawingMap[cabinet.Key].ContainsKey(1) &&
                    cabinet.Value.chainData.drawings.ContainsKey(1))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[1], mainDrawingMap[cabinet.Key][1].transform, mainDrawingMap[cabinet.Key][1].transform.position + mainDrawingOffset, new Vector3(0.8f, 0.8f, 0.8f), 0.15f, 0.075f, 0.0375f, mainPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    mainDrawingFaceMap[cabinet.Key][1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[1].author].faceSprite;
                }
                if (mainDrawingMap[cabinet.Key].ContainsKey(3) &&
                    cabinet.Value.chainData.drawings.ContainsKey(3))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[3], mainDrawingMap[cabinet.Key][3].transform, mainDrawingMap[cabinet.Key][3].transform.position + mainDrawingOffset, new Vector3(0.8f, 0.8f, 0.8f), 0.15f, 0.075f, 0.0375f, mainPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    mainDrawingFaceMap[cabinet.Key][3].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[3].author].faceSprite;
                }
                if (mainDrawingMap[cabinet.Key].ContainsKey(5) &&
                    cabinet.Value.chainData.drawings.ContainsKey(5))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[5], mainDrawingMap[cabinet.Key][5].transform, mainDrawingMap[cabinet.Key][5].transform.position + mainDrawingOffset, new Vector3(0.8f, 0.8f, 0.8f), 0.15f, 0.075f, 0.0375f, mainPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    mainDrawingFaceMap[cabinet.Key][5].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[5].author].faceSprite;
                }

                #endregion

                #region next drawings
                if (nextDrawingMap[cabinet.Key].ContainsKey(1) &&
                    cabinet.Value.chainData.drawings.ContainsKey(1))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[1], nextDrawingMap[cabinet.Key][1].transform, nextDrawingMap[cabinet.Key][1].transform.position + nextDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, nextPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    nextDrawingFaceMap[cabinet.Key][1].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[1].author].faceSprite;
                }
                if (nextDrawingMap[cabinet.Key].ContainsKey(3) &&
                    cabinet.Value.chainData.drawings.ContainsKey(3))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[3], nextDrawingMap[cabinet.Key][3].transform, nextDrawingMap[cabinet.Key][3].transform.position + nextDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, nextPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    nextDrawingFaceMap[cabinet.Key][3].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[3].author].faceSprite;
                }
                if (nextDrawingMap[cabinet.Key].ContainsKey(5) &&
                    cabinet.Value.chainData.drawings.ContainsKey(5))
                {
                    GameManager.Instance.playerFlowManager.createDrawingLines(cabinet.Value.chainData.drawings[5], nextDrawingMap[cabinet.Key][5].transform, nextDrawingMap[cabinet.Key][5].transform.position + nextDrawingOffset, new Vector3(0.7f, 0.7f, 0.7f), 0.1f, 0.05f, 0.025f, nextPresentationDrawingContainerMap[cabinet.Key].drawingCanvas);
                    nextDrawingFaceMap[cabinet.Key][5].sprite = ColourManager.Instance.birdMap[cabinet.Value.chainData.drawings[5].author].faceSprite;
                }

                #endregion



                if (correctPromptMap.ContainsKey(cabinet.Key))
                {
                    correctPromptMap[cabinet.Key].text = cabinet.Value.chainData.correctPrefix + " " + cabinet.Value.chainData.correctNoun;
                }

            }

            foreach(KeyValuePair<int,EvaluationData> evaluationData in GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap)
            {
                if(evaluationData.Key == 3 && GameManager.Instance.playerFlowManager.playerNameMap.Count < 6)
                {
                    continue;
                }

                foreach(KeyValuePair<int,PlayerDrawingInputData> drawing in evaluationData.Value.drawings)
                {
                    if(evaluationDrawingMap.ContainsKey(evaluationData.Key) && evaluationDrawingMap[evaluationData.Key].ContainsKey(drawing.Key))
                    {
                        GameManager.Instance.playerFlowManager.createDrawingLines(drawing.Value, evaluationDrawingMap[evaluationData.Key][drawing.Key].transform, evaluationDrawingMap[evaluationData.Key][drawing.Key].transform.position + mainDrawingOffset, new Vector3(0.8f, 0.8f, 0.8f), 0.15f, 0.075f, 0.0375f, null);
                        evaluationFaceMap[evaluationData.Key][drawing.Key].sprite = ColourManager.Instance.birdMap[drawing.Value.author].faceSprite;
                    }
                    else
                    {
                        Debug.LogError("Missing evaluation drawing in map for evaluation round["+evaluationData.Key+"] drawing["+drawing.Key+"].");
                    }
                }

                evaluationPromptMap[evaluationData.Key].text = "EVALUATION:\n" + evaluationData.Value.adjective + " " + evaluationData.Value.noun;
            }

            isInitialized = true;
        }

    }

    public void prepareSlides(int cabinet, int round, BirdName presenter)
    {
        if(presenter == SettingsManager.Instance.birdName)
        {
            presentorDrawings.gameObject.SetActive(true);
            viewerDrawings.gameObject.SetActive(false);

            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.hasBeenPlaced)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.Place(true);
                }
            }
        }
        else
        {
            presentorDrawings.gameObject.SetActive(false);
            viewerDrawings.gameObject.SetActive(true);

            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.hasBeenPlaced)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.Place(true);
                }
            }
        }

        //Reset all main prompts and drawings
        foreach(Dictionary<int,Text> mainPrompt in mainPromptMap.Values)
        {
            foreach(Text mainText in mainPrompt.Values)
            {
                mainText.gameObject.SetActive(false);
            }
        }
        foreach(Dictionary<int,GameObject> mainDrawing in mainDrawingMap.Values)
        {
            foreach(GameObject mainObject in mainDrawing.Values)
            {
                mainObject.SetActive(false);
            }
        }
        //Reset all next prompts and drawings
        foreach (Dictionary<int, Text> nextPrompt in nextPromptMap.Values)
        {
            foreach (Text nextText in nextPrompt.Values)
            {
                nextText.gameObject.SetActive(false);
            }
        }
        foreach (Dictionary<int, GameObject> nextDrawing in nextDrawingMap.Values)
        {
            foreach (GameObject nextObject in nextDrawing.Values)
            {
                nextObject.SetActive(false);
            }
        }

        //Reset all previous prompts and drawings
        foreach (Dictionary<int, Text> previousPrompt in previousPromptMap.Values)
        {
            foreach (Text previousText in previousPrompt.Values)
            {
                previousText.gameObject.SetActive(false);
            }
        }
        foreach (Dictionary<int, GameObject> previousDrawing in previousDrawingMap.Values)
        {
            foreach (GameObject previousObject in previousDrawing.Values)
            {
                previousObject.SetActive(false);
            }
        }

        //Reset all correct prompts
        foreach(Text correctText in correctPromptMap.Values)
        {
            correctText.gameObject.SetActive(false);
        }

        correctPromptMap[cabinet].gameObject.SetActive(true);

        //Determine what kind of round it is
        if (round == (GameManager.Instance.playerFlowManager.numberOfCabinetRounds + 1))
        {
            //This is the guessing round
            if (mainPromptMap[cabinet].ContainsKey(-1))
            {
                mainPromptMap[cabinet][-1].gameObject.SetActive(true);
            }
            if (previousDrawingMap[cabinet].ContainsKey(round - 1))
            {
                previousDrawingMap[cabinet][round - 1].SetActive(true);
            }
        }
        else if (round % 2 == 0)
        {
            //This is a prompt round
            if(nextDrawingMap[cabinet].ContainsKey(round + 1))
            {
                nextDrawingMap[cabinet][round + 1].SetActive(true);
            }
            if (mainPromptMap[cabinet].ContainsKey(round))
            {
                mainPromptMap[cabinet][round].gameObject.SetActive(true);
            }
            if (previousDrawingMap[cabinet].ContainsKey(round - 1))
            {
                previousDrawingMap[cabinet][round - 1].SetActive(true);
            }
        }
        else
        {
            //This is a drawing round
            if(round == (GameManager.Instance.playerFlowManager.numberOfCabinetRounds))
            {
                if (nextPromptMap[cabinet].ContainsKey(-1))
                {
                    nextPromptMap[cabinet][-1].gameObject.SetActive(true);
                }
            }
            else if (nextPromptMap[cabinet].ContainsKey(round + 1))
            {
                nextPromptMap[cabinet][round + 1].gameObject.SetActive(true);
            }
            if (mainDrawingMap[cabinet].ContainsKey(round))
            {
                mainDrawingMap[cabinet][round].gameObject.SetActive(true);
            }
            if (previousPromptMap[cabinet].ContainsKey(round - 1))
            {
                previousPromptMap[cabinet][round - 1].gameObject.SetActive(true);
            }
        }

        presenterImage.sprite = ColourManager.Instance.birdMap[presenter].faceSprite;
    }

    public void prepareEvaluationSlide(int evaluationRound, int playerIndex, BirdName presenter)
    {
        if (presenter == SettingsManager.Instance.birdName)
        {
            presentorDrawings.gameObject.SetActive(true);
            viewerDrawings.gameObject.SetActive(false);

            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.hasBeenPlaced)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.Place(true);
                }
            }
        }
        else
        {
            presentorDrawings.gameObject.SetActive(false);
            viewerDrawings.gameObject.SetActive(true);

            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.hasBeenPlaced)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.Place(true);
                }
            }
        }

        //Reset all main prompts and drawings
        foreach (Dictionary<int, Text> mainPrompt in mainPromptMap.Values)
        {
            foreach (Text mainText in mainPrompt.Values)
            {
                mainText.gameObject.SetActive(false);
            }
        }
        foreach (Dictionary<int, GameObject> mainDrawing in mainDrawingMap.Values)
        {
            foreach (GameObject mainObject in mainDrawing.Values)
            {
                mainObject.SetActive(false);
            }
        }
        //Reset all next prompts and drawings
        foreach (Dictionary<int, Text> nextPrompt in nextPromptMap.Values)
        {
            foreach (Text nextText in nextPrompt.Values)
            {
                nextText.gameObject.SetActive(false);
            }
        }
        foreach (Dictionary<int, GameObject> nextDrawing in nextDrawingMap.Values)
        {
            foreach (GameObject nextObject in nextDrawing.Values)
            {
                nextObject.SetActive(false);
            }
        }

        //Reset all previous prompts and drawings
        foreach (Dictionary<int, Text> previousPrompt in previousPromptMap.Values)
        {
            foreach (Text previousText in previousPrompt.Values)
            {
                previousText.gameObject.SetActive(false);
            }
        }
        foreach (Dictionary<int, GameObject> previousDrawing in previousDrawingMap.Values)
        {
            foreach (GameObject previousObject in previousDrawing.Values)
            {
                previousObject.SetActive(false);
            }
        }

        //Reset all evaluation prompts and drawings
        foreach(Text evaluationPrompt in evaluationPromptMap.Values)
        {
            evaluationPrompt.gameObject.SetActive(false);
        }

        foreach(Dictionary<int,GameObject> evaluationDrawingGroup in evaluationDrawingMap.Values)
        {
            foreach(GameObject evaluationDrawing in evaluationDrawingGroup.Values)
            {
                evaluationDrawing.SetActive(false);
            }
        }

        //Reset all correct prompts
        foreach (Text correctText in correctPromptMap.Values)
        {
            correctText.gameObject.SetActive(false);
        }

        evaluationPromptMap[evaluationRound].gameObject.SetActive(true);

        if (evaluationDrawingMap[evaluationRound].ContainsKey(playerIndex))
        {
            evaluationDrawingMap[evaluationRound][playerIndex].gameObject.SetActive(true);
        }

        presenterImage.sprite = ColourManager.Instance.birdMap[presenter].faceSprite;
    }

    public void showSlides()
    {
        presentationSpriteLayer.SetActive(true);

        foreach (GameObject objectToShowOnStart in objectsToShowOnStart)
        {
            objectToShowOnStart.SetActive(true);
        }

        foreach (GameObject objectToHideOnStart in objectsToHideOnStart)
        {
            objectToHideOnStart.SetActive(false);
        }
    }

    public void finish()
    {
        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if(currentPresenter == SettingsManager.Instance.birdName)
            {
                if (GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.hasBeenPlaced &&
                    !GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.hasBeenClicked)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationPresenterSticky.Click();
                }
            }
            else
            {
                if (GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.hasBeenPlaced &&
                    !GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.hasBeenClicked)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accusePresentationViewerSticky.Click();
                }
            }
            
        }


        //Hide subround and re-open accusation round with saved time
        if (PhotonNetwork.IsMasterClient)
        {
            GameManager.Instance.gameFlowManager.timeRemainingInPhase = savedAccusationVoteRoundTime;
            GameNetwork.Instance.BroadcastQueue.Add("resume_accusation" + GameDelim.BASE + savedAccusationVoteRoundTime.ToString());
            GameManager.Instance.playerFlowManager.accusationsRound.resumeFromPresentation(savedAccusationVoteRoundTime);
            
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("resume_accusation");
        }
        reset();
    }

    public void reset()
    {
        savedAccusationVoteRoundTime = 0.0f;
        continueButtonObject.SetActive(false);
        presentationSpriteLayer.SetActive(false);
        viewerDrawings.reset();
        foreach (GameObject objectToShowOnStart in objectsToShowOnStart)
        {
            objectToShowOnStart.SetActive(false);
        }

        foreach (GameObject objectToHideOnStart in objectsToHideOnStart)
        {
            objectToHideOnStart.SetActive(true);
        }

        foreach(GameObject objectToHideOnEnd in objectsToHideOnEnd)
        {
            objectToHideOnEnd.SetActive(false);
        }
    }
}
