﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRound : MonoBehaviour
{
    public List<GameObject> objectsToHideOnStart = new List<GameObject>(), objectsToShowOnStart = new List<GameObject>();
    public List<string> soundsToPlayOnStart, soundsToStopOnStart;
    public float timeInRound;
    public GameFlowManager.GamePhase gamePhaseName;

    protected PlayerFlowManager playerFlowManager;
    protected GameFlowManager gameFlowManager;

    public virtual void StartRound()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        foreach(GameObject objectToHideOnStart in objectsToHideOnStart)
        {
            if (objectToHideOnStart)
            {
                objectToHideOnStart.SetActive(false);
            }
        }

        foreach(GameObject objectToShowOnStart in objectsToShowOnStart)
        {
            if (objectToShowOnStart)
            {
                objectToShowOnStart.SetActive(true);
            }
        }

        foreach(string soundToStopOnStart in soundsToStopOnStart)
        {
            AudioManager.Instance.StopSound(soundToStopOnStart);
        }

        foreach(string soundToPlayOnStart in soundsToPlayOnStart)
        {
            AudioManager.Instance.PlaySound(soundToPlayOnStart);
        }

        playerFlowManager.currentTimeInRound = timeInRound;

        if (PhotonNetwork.IsMasterClient)
        {
            if (!gameFlowManager)
            {
                gameFlowManager = GameManager.Instance.gameFlowManager;
            }

            gameFlowManager.currentGamePhase = gamePhaseName;
        }

    }
}
