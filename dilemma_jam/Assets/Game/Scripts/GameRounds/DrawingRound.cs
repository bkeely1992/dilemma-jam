﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;
using static PlayerDrawingInputData;

public class DrawingRound : PlayerRound
{
    public enum CurrentState
    {
        prompting, drawing, evaluation, waiting, invalid
    }
    public CurrentState currentState = CurrentState.drawing;

    public Text promptText, evaluationText, playerNameText;
    public List<CabinetDrawer> allCabinets = new List<CabinetDrawer>();
    public List<BirdArm> allBirdArms = new List<BirdArm>();
    public PlayerBirdArm playerBirdArm;
    public InputField prefixInputField, nounInputField;
    public DrawingController drawingBoard, evaluationDrawingBoard;
    public GameObject evaluationFormParent;
    public bool canGetCabinet;
    public Clock deskClock;

    public SpriteRenderer deskRenderer;

    public Animator drawingFormAnimator, promptFormAnimator, evaluationFormAnimator, responsesFormAnimator;

    public List<Text> allQueuedPrompts;
    public List<GameObject> allQueuedDrawingContainers;

    public DrawingsContainer drawingContainer;
    public Canvas drawingCanvas;
    public Dictionary<int, CabinetDrawer> cabinetDrawerMap;
    public Dictionary<int, EvaluationData> evaluationsMap = new Dictionary<int, EvaluationData>();
    public int maxLineMessageLength = 1000;

    public Color correctTPColour, incorrectTPColour;
    public Vector3 promptDrawingOffset;

    public bool isInitialized = false;
    public bool roundIsInProgress = false;
    public bool hasSubmitted = false;
    public float timeInDrawingRound = 30.0f;
    public float timeInPromptingRound = 15.0f;
    public float timeInEvaluationRound = 60.0f;

    public int currentEvaluationPhase = 0;
    public int currentPlayerCabinetRoundIndex = 0;
    public int currentActiveCabinetIndex = -1;

    public float smallBrushSize = 0.05f;
    public float mediumBrushSize = 0.1f;
    public float bigBrushSize = 0.2f;
    public float smallBrushUIDotSize = 0.05f;
    public float mediumBrushUIDotSize = 0.1f;
    public float bigBrushUIDotSize = 0.2f;

    public GameObject eraseLinePrefab;
    public GameObject baseLinePrefab;
    public Material eraseLineMaterial;
    public GameObject colourLinePrefab;
    public Material baseLineMaterial;

    public DrawingTraitorContainer drawingBotcherContainer;
    public BotchingForm botchingForm;

    public List<SpriteRenderer> drawingFolderRenderers;
    public List<SpriteRenderer> promptingFolderRenderers;
    public List<SpriteRenderer> evaluationFolderRenderers;

    private Dictionary<BirdName, BirdArm> birdArmMap;
    private Dictionary<string, Dictionary<int,string>> queuedLineMessageMap;

    private void Awake()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    private void Update()
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

    }

    public void initialize()
    {
        timeInRound = SettingsManager.Instance.drawingTime;

        cabinetDrawerMap = new Dictionary<int, CabinetDrawer>();
        foreach (CabinetDrawer cabinet in allCabinets)
        {
            cabinetDrawerMap.Add(cabinet.id, cabinet);
        }

        birdArmMap = new Dictionary<ColourManager.BirdName, BirdArm>();
        foreach (BirdArm birdArm in allBirdArms)
        {
            birdArmMap.Add(birdArm.birdName, birdArm);
        }

        queuedLineMessageMap = new Dictionary<string, Dictionary<int,string>>();

        roundIsInProgress = true;
        isInitialized = true;
    }

    public override void StartRound()
    {
        base.StartRound();

        if (!isInitialized)
        {
            initialize();
        }

        //Set all of the drawing values for the buttons
        initializeDrawingController(drawingBoard, ColourManager.Instance.birdMap[SettingsManager.Instance.birdName]);
        initializeDrawingController(evaluationDrawingBoard, ColourManager.Instance.birdMap[SettingsManager.Instance.birdName]);

        //Hide the instructions
        if (playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher)
        {
            playerFlowManager.instructionRound.bossNoteBotcherAnimator.SetBool("Slide", false);
            playerFlowManager.instructionRound.vultureNoteAnimator.SetBool("Slide", false);
        }
        else
        {
            playerFlowManager.instructionRound.bossNoteWorkerAnimator.SetBool("Slide", false);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            BirdName currentBird;
            foreach (KeyValuePair<int, CabinetDrawer> cabinetDrawer in cabinetDrawerMap)
            {
                currentBird = cabinetDrawer.Value.chainData != null && cabinetDrawer.Value.chainData.active ? cabinetDrawer.Value.chainData.playerOrder[1] : BirdName.none;

                if(currentBird != BirdName.none)
                {
                    GameManager.Instance.gameFlowManager.addTransitionCondition("drawing_submitted:" + currentBird);

                    //Open the cabinet drawer
                    cabinetDrawer.Value.setAsReady(currentBird);

                    //Broadcast to other players to open the drawers
                    GameNetwork.Instance.BroadcastQueue.Add("cabinet_drawer_is_ready" + GameDelim.BASE + "drawing" + GameDelim.BASE + cabinetDrawer.Key.ToString() + GameDelim.BASE + "1" + GameDelim.BASE + currentBird.ToString());
                }
            }
        }

        //Start the clock
        playerFlowManager.currentTimeInRound = timeInDrawingRound;
        currentPlayerCabinetRoundIndex = 1;
    }

    public void initializeDrawingController(DrawingController drawingSurface, Bird drawingBird)
    {
        drawingSurface.initialize();
        drawingSurface.setButtonDrawingSize("Big", bigBrushSize, bigBrushUIDotSize, "Big");
        drawingSurface.setButtonDrawingSize("Medium", mediumBrushSize, mediumBrushUIDotSize, "Medium");
        drawingSurface.setButtonDrawingSize("Small", smallBrushSize, smallBrushUIDotSize, "Small");

        drawingSurface.setButtonDrawingColour("Colour", drawingBird.colour, colourLinePrefab, drawingBird.material, "Colour");
        drawingSurface.setButtonDrawingColour("Light", drawingBird.bgColour, colourLinePrefab, drawingBird.bgLineMaterial, "Light");
        drawingSurface.setButtonDrawingColour("Base", Color.black, baseLinePrefab, baseLineMaterial, "Base");
        drawingSurface.setButtonDrawingColour("Erase", Color.white, eraseLinePrefab, eraseLineMaterial, "Erase");
        drawingSurface.changeCurrentDrawingColour("Base");
        drawingSurface.changeCurrentDrawingSize("Medium");
    }

    public void initializeFolderColour(int cabinetID, BirdName guesser)
    {
        cabinetDrawerMap[cabinetID].chainData.guesser = guesser;
    }

    public void setDrawerAsUsed(int cabinetIndex)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        CabinetDrawer selectedCabinet = cabinetDrawerMap[cabinetIndex];
        if (selectedCabinet.ready)
        {
            selectedCabinet.ready = false;
            selectedCabinet.setDrawerColours();
            selectedCabinet.gameObject.SetActive(true);
            selectedCabinet.folderObject.SetActive(true);
            selectedCabinet.close();
        }
    }

    public void loadHeldFolder()
    {
        ChainData chainData = cabinetDrawerMap[currentActiveCabinetIndex].chainData;
        GameManager.Instance.playerFlowManager.drawingRound.playerBirdArm.folderState = PlayerBirdArm.FolderState.inactive;
        if (playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher)
        {
            if (chainData.correctNoun != "" &&
                chainData.correctPrefix != ""
                )
            {
                if (currentPlayerCabinetRoundIndex != 1)
                {
                    //drawingBotcherContainer.loadCorrectOption(currentActiveCabinetIndex, currentPlayerCabinetRoundIndex);
                }

            }
            else
            {
                drawingBotcherContainer.readyToShowCorrectOption = true;
            }

            if (chainData.botcherPrefixOptions.Count != 0 &&
                chainData.botcherNounOptions.Count != 0)
            {
                //botcherContainer.loadNewBotcherOptions(id);
            }
            else
            {
                drawingBotcherContainer.readyToShowBotcherOptions = true;
            }
        }

        if (SettingsManager.Instance.GetSetting("stickies") &&
            !playerFlowManager.instructionRound.hasClickedFirstCabinet)
        {
            playerFlowManager.instructionRound.hideCabinetStickies();
        }

        int lastRoundIndex = currentPlayerCabinetRoundIndex - 1;

        switch (currentState)
        {
            case CurrentState.prompting:

                //If this is a drawing then show the queued drawing
                if (drawingContainer.drawingHolderMap.ContainsKey(currentActiveCabinetIndex))
                {
                    if (drawingContainer.drawingHolderMap[currentActiveCabinetIndex].ContainsKey(lastRoundIndex))
                    {
                        //Previous drawing was not empty for the player, show the last drawing
                        drawingContainer.drawingHolderMap[currentActiveCabinetIndex][lastRoundIndex].gameObject.SetActive(true);
                    }
                }

                foreach(SpriteRenderer folderRenderer in drawingFolderRenderers)
                {
                    //folderRenderer.color = ColourManager.Instance.birdMap[cabinetDrawerMap[currentActiveCabinetIndex].chainData.guesser].colour;
                }

                promptFormAnimator.SetBool("Slide", true);
                promptText.gameObject.SetActive(true);
                prefixInputField.Select();
                GameManager.Instance.submitBtn.SetActive(true);
                

                break;
            case CurrentState.drawing:
                if (lastRoundIndex != 0)
                {
                    promptText.text = chainData.prompts[lastRoundIndex].prefix + " " + chainData.prompts[lastRoundIndex].noun;

                }
                else
                {
                    promptText.text = chainData.correctPrefix + " " + chainData.correctNoun;
                }

                foreach (SpriteRenderer folderRenderer in promptingFolderRenderers)
                {
                    //folderRenderer.color = ColourManager.Instance.birdMap[cabinetDrawerMap[currentActiveCabinetIndex].chainData.guesser].colour;
                }

                drawingBoard.gameObject.SetActive(true);
                drawingFormAnimator.SetBool("Slide", true);
                GameManager.Instance.submitBtn.SetActive(true);
                break;
            case CurrentState.evaluation:
                int evaluationRound = currentEvaluationPhase;
                if (!evaluationsMap.ContainsKey(evaluationRound))
                {
                    Debug.LogError("Evaluation map does not contain round[" + evaluationRound.ToString() + "].");
                }
                string adjective = evaluationsMap[evaluationRound].adjective;
                string noun = evaluationsMap[evaluationRound].noun; ;
                evaluationText.text = adjective + " " + noun;

                foreach (SpriteRenderer folderRenderer in evaluationFolderRenderers)
                {
                    //folderRenderer.color = ColourManager.Instance.evaluationFolderColour;
                }

                evaluationFormAnimator.SetBool("Slide", true);

                if (GameManager.Instance.playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher)
                {
                    botchingForm.show(evaluationRound);
                    evaluationFormParent.gameObject.SetActive(false);
                }
                else
                {
                    if (SettingsManager.Instance.GetSetting("stickies"))
                    {
                        if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.hasBeenPlaced)
                        {
                            GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.Place(true);
                        }
                    }
                    evaluationDrawingBoard.gameObject.SetActive(true);
                    evaluationFormParent.gameObject.SetActive(true);
                    GameManager.Instance.submitBtn.SetActive(true);
                }

                break;
        }

        AudioManager.Instance.PlaySound("FolderOpen");
    }

    public void disableCabinet(int cabinetID)
    {
        cabinetDrawerMap[cabinetID].gameObject.SetActive(false);
    }

    public CabinetDrawer getCabinet(int inIndex)
    {
        return cabinetDrawerMap[inIndex];
    }

    public BirdArm getBirdArm(BirdName inBirdName)
    {
        return birdArmMap[inBirdName];
    }

    public void releaseDeskFolder()
    {
        CabinetDrawer selectedCabinet = cabinetDrawerMap[currentActiveCabinetIndex];
        ChainData selectedCabinetData = selectedCabinet.chainData;
        AudioManager.Instance.PlaySound("FolderOpen");

        switch (currentState)
        {
            case CurrentState.drawing:
                PlayerDrawingInputData newDrawing;
                if (!cabinetDrawerMap[currentActiveCabinetIndex].chainData.drawings.ContainsKey(currentPlayerCabinetRoundIndex))
                {
                    newDrawing = new PlayerDrawingInputData() { author = SettingsManager.Instance.birdName };
                    selectedCabinetData.drawings.Add(currentPlayerCabinetRoundIndex, newDrawing);
                }

                if (!releaseDeskDrawingFolder(selectedCabinetData, false))
                {
                    return;
                }
                break;
            case CurrentState.prompting:
                if (!releaseDeskPromptFolder(selectedCabinet, false))
                {
                    return;
                }
                break;
            case CurrentState.evaluation:
                if (!releaseDeskEvaluationDrawingFolder(selectedCabinet.id, false))
                {
                    return;
                }
                break;
        }

        Submit(false);
        selectedCabinet.close();

        if (PhotonNetwork.IsMasterClient)
        {
            GameNetwork.Instance.BroadcastQueue.Add("access_drawer" + GameDelim.BASE + selectedCabinet.id);
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("access_drawer" + GameDelim.BASE + selectedCabinet.id);
        }
    }

    public void updateBirdFolderStatus(BirdName birdName, Color folderColour, bool isOn)
    {
        birdArmMap[birdName].heldFolderObject.SetActive(isOn);
        birdArmMap[birdName].heldFolderObject.GetComponent<SpriteRenderer>().color = folderColour;
    }

    private bool releaseDeskDrawingFolder(ChainData currentChain, bool force)
    {
        if (!drawingBoard.hasDrawingLines() && !force)
        {
            return false;
        }
        float timeUsed = timeInDrawingRound - GameManager.Instance.playerFlowManager.currentTimeInRound;

        PlayerDrawingInputData newDrawing = currentChain.drawings[currentPlayerCabinetRoundIndex];
        
        newDrawing.author = SettingsManager.Instance.birdName;
        newDrawing.timeTaken = timeUsed;
        newDrawing.type = DrawingType.cabinet;
        newDrawing.lines = drawingBoard.getDrawingLines();
        drawingBoard.clearDrawingLines();

        drawingFormAnimator.SetBool("Slide", false);
        drawingBoard.gameObject.SetActive(false);

        return true;
    }

    private bool releaseDeskPromptFolder(CabinetDrawer selectedCabinet, bool force)
    {
        if ((prefixInputField.text == "" || nounInputField.text == "") && !force)
        {
            return false;
        }

        float timeUsed = timeInPromptingRound - GameManager.Instance.playerFlowManager.currentTimeInRound;

        PlayerTextInputData prompt;
        if (!selectedCabinet.chainData.prompts.ContainsKey(currentPlayerCabinetRoundIndex))
        {
            prompt = new PlayerTextInputData();
            selectedCabinet.chainData.prompts.Add(currentPlayerCabinetRoundIndex, prompt);
        }
        else
        {
            prompt = selectedCabinet.chainData.prompts[currentPlayerCabinetRoundIndex];
        }
        
        prompt.prefix = GameDelim.stripGameDelims(prefixInputField.text);
        prompt.noun = GameDelim.stripGameDelims(nounInputField.text);
        
        prompt.timeTaken = timeUsed;
        prefixInputField.text = "";
        nounInputField.text = "";

        promptFormAnimator.SetBool("Slide", false);
        if(drawingContainer.drawingHolderMap.ContainsKey(selectedCabinet.id) &&
            drawingContainer.drawingHolderMap[selectedCabinet.id].ContainsKey(currentPlayerCabinetRoundIndex - 1))
        {
            drawingContainer.drawingHolderMap[selectedCabinet.id][currentPlayerCabinetRoundIndex - 1].gameObject.SetActive(false);
        }
        
        GameManager.Instance.submitBtn.SetActive(false);

        return true;
    }

    private bool releaseDeskEvaluationDrawingFolder(int selectedCabinetID, bool force)
    {
        PlayerDrawingInputData newDrawing;
        float timeUsed = timeInEvaluationRound - GameManager.Instance.playerFlowManager.currentTimeInRound;

        if (!evaluationDrawingBoard.hasDrawingLines() && !force)
        {
            return false;
        }

        if (evaluationsMap.ContainsKey(currentEvaluationPhase) &&
                evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(selectedCabinetID))
        {
            newDrawing = evaluationsMap[currentEvaluationPhase].drawings[selectedCabinetID];
        }
        else
        {
            Debug.LogError("Evaluations map is missing cabinet[" + selectedCabinetID + "] for evaluation round[" + currentEvaluationPhase + "]. Cannot add drawing.");
            return false;
        }

        newDrawing.type = PlayerDrawingInputData.DrawingType.evaluation;
        newDrawing.lines = evaluationDrawingBoard.getDrawingLines();
        evaluationDrawingBoard.clearDrawingLines();

        evaluationFormAnimator.SetBool("Slide", false);
        evaluationDrawingBoard.gameObject.SetActive(false);

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.hasBeenClicked)
            {
                GameManager.Instance.playerFlowManager.instructionRound.evaluationsSticky.Click();
            }
        }

        return true;
    }

    public void Submit(bool force)
    {
        CabinetDrawer selectedCabinet = null;

        switch (currentState)
        {
            case CurrentState.drawing:
                if (currentActiveCabinetIndex == -1 ||
                    !cabinetDrawerMap.ContainsKey(currentActiveCabinetIndex))
                {
                    currentActiveCabinetIndex = -1;
                    foreach (CabinetDrawer cabinet in cabinetDrawerMap.Values)
                    {
                        if(cabinet.chainData.active &&
                            cabinet.chainData.playerOrder[currentPlayerCabinetRoundIndex] == SettingsManager.Instance.birdName)
                        {
                            currentActiveCabinetIndex = cabinet.id;
                        }
                    }
                    if(currentActiveCabinetIndex == -1)
                    {
                        Debug.LogError("Could not isolate cabinet for player on drawing round["+currentPlayerCabinetRoundIndex+"].");
                    }
                }

                selectedCabinet = cabinetDrawerMap[currentActiveCabinetIndex];
                selectedCabinet.submit();

                submitDrawing(currentActiveCabinetIndex, force);

                break;
            case CurrentState.prompting:
                if (currentActiveCabinetIndex == -1 ||
    !               cabinetDrawerMap.ContainsKey(currentActiveCabinetIndex))
                {
                    currentActiveCabinetIndex = -1;
                    foreach (CabinetDrawer cabinet in cabinetDrawerMap.Values)
                    {
                        if (cabinet.chainData.active &&
                            cabinet.chainData.playerOrder[currentPlayerCabinetRoundIndex] == SettingsManager.Instance.birdName)
                        {
                            currentActiveCabinetIndex = cabinet.id;
                        }
                    }
                    if (currentActiveCabinetIndex == -1)
                    {
                        Debug.LogError("Could not isolate cabinet for player on prompting round[" + currentPlayerCabinetRoundIndex + "].");
                    }
                }

                selectedCabinet = cabinetDrawerMap[currentActiveCabinetIndex];

                selectedCabinet.submit();
                submitPrompt(selectedCabinet, force);
                break;
            case CurrentState.evaluation:

                if (currentActiveCabinetIndex == -1 ||
                    evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(currentActiveCabinetIndex))
                {
                    currentActiveCabinetIndex = -1;
                    if (evaluationsMap.ContainsKey(currentEvaluationPhase))
                    {
                        foreach (KeyValuePair<int, PlayerDrawingInputData> drawing in evaluationsMap[currentEvaluationPhase].drawings)
                        {
                            if (drawing.Value.author == SettingsManager.Instance.birdName)
                            {
                                currentActiveCabinetIndex = drawing.Key;
                            }
                        }
                    }
                    if (currentActiveCabinetIndex == -1)
                    {
                        Debug.LogError("Could not isolate cabinet for player on evaluation round[" + currentEvaluationPhase + "].");
                    }
                }

                selectedCabinet = cabinetDrawerMap[currentActiveCabinetIndex];
                selectedCabinet.submit();
                submitEvaluationDrawing(selectedCabinet, force);
                break;
        }

        hasSubmitted = true;
        currentActiveCabinetIndex = -1;

        if (force)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                GameManager.Instance.gameFlowManager.resolveTransitionCondition("force_submit:" + SettingsManager.Instance.birdName);
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "force_submit:" + SettingsManager.Instance.birdName);
            }
        }
        
    }

    private void submitDrawing(int cabinetID, bool force)
    {

        ChainData selectedCabinetData = cabinetDrawerMap[cabinetID].chainData;
        BirdName nextPlayer;
        PlayerDrawingInputData newDrawing;
        if (!cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(currentPlayerCabinetRoundIndex))
        {
            newDrawing = new PlayerDrawingInputData() { author = SettingsManager.Instance.birdName };
            selectedCabinetData.addDrawing(currentPlayerCabinetRoundIndex, newDrawing);
        }
        else
        {
            newDrawing = selectedCabinetData.drawings[currentPlayerCabinetRoundIndex];
        }

        if (force)
        {
            releaseDeskDrawingFolder(selectedCabinetData, force);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if(selectedCabinetData == null)
            {
                Debug.LogError("Missing chain data for server. Cannot submit drawing.");
            }
            nextPlayer = selectedCabinetData.playerOrder[currentPlayerCabinetRoundIndex + 1];
            
            if (selectedCabinetData.drawings[currentPlayerCabinetRoundIndex].lines.Count == 0)
            {
                if (force)
                {
                    Debug.LogError("Submitting empty drawing to clients.");
                    sendEmptyCabinetDrawingToClients(nextPlayer, SettingsManager.Instance.birdName, cabinetID, currentPlayerCabinetRoundIndex);
                }
                else
                {
                    return;
                }
            }
            else
            {
                sendDrawingToClients(newDrawing, nextPlayer, SettingsManager.Instance.birdName, cabinetID, currentPlayerCabinetRoundIndex);
            }
        }
        else
        {
            
            if(force &&
                newDrawing.lines.Count == 0)
            {
                sendEmptyDrawingToServer(SettingsManager.Instance.birdName, cabinetID, currentPlayerCabinetRoundIndex, DrawingType.cabinet);
            }
            else
            {
                prepareLinesForServer(newDrawing, SettingsManager.Instance.birdName, cabinetID, currentPlayerCabinetRoundIndex, DrawingType.cabinet);
            }
        }

        

        canGetCabinet = true;
    }

    private void sendEmptyCabinetDrawingToClients(BirdName nextPlayer, BirdName author, int cabinetID, int round)
    {
        

        if (nextPlayer != SettingsManager.Instance.birdName)
        {
            GameManager.Instance.gameFlowManager.addTransitionCondition("empty_drawing_receipt:" + nextPlayer);
            sendEmptyDrawingToClient(cabinetID, round, nextPlayer, DrawingType.cabinet, author);
        }

        GameManager.Instance.gameFlowManager.resolveTransitionCondition("drawing_submitted:" + author);

        //Send empty drawing to botcher
        foreach (PlayerData player in GameManager.Instance.gameFlowManager.gamePlayers.Values)
        {
            if (player.playerRole == GameFlowManager.PlayerRole.botcher)
            {
                if (SettingsManager.Instance.birdName == player.birdName)
                {
                    continue;
                }

                //Add condition for the receipt of the drawing we're sending to the botcher, we cannot proceed until they have a copy of the drawing 
                GameManager.Instance.gameFlowManager.addTransitionCondition("empty_botcher_drawing_receipt:" + player.birdName + "-" + author);
                sendEmptyDrawingToClient(cabinetID, round, player.birdName, DrawingType.to_botcher, author);
            }
        }
    }

    private void sendDrawingToClients(PlayerDrawingInputData drawing, BirdName nextPlayer, BirdName author, int cabinetID, int round)
    {
        GameManager.Instance.gameFlowManager.addTransitionCondition("drawing_receipt:" + nextPlayer);
        GameManager.Instance.gameFlowManager.resolveTransitionCondition("drawing_submitted:" + SettingsManager.Instance.birdName);
        prepareLinesForClient(drawing, cabinetID, round, nextPlayer, DrawingType.cabinet);

        //Send drawing lines to botcher
        foreach (PlayerData player in GameManager.Instance.gameFlowManager.gamePlayers.Values)
        {
            if (player.playerRole == GameFlowManager.PlayerRole.botcher)
            {
                if (SettingsManager.Instance.birdName == player.birdName)
                {
                    continue;
                }

                //Add condition for the receipt of the drawing we're sending to the botcher, we cannot proceed until they have a copy of the drawing 
                GameManager.Instance.gameFlowManager.addTransitionCondition("botcher_drawing_receipt:" + player.birdName + "-" + author);
                prepareLinesForClient(drawing, cabinetID, round, player.birdName, DrawingType.to_botcher);
            }
        }
    }

    private void sendEmptyDrawingToServer(BirdName author, int cabinetID, int round, DrawingType drawingType)
    {
        Debug.LogError("Sending empty drawing to server.");
        GameNetwork.Instance.ToServerQueue.Add("empty_drawing" + GameDelim.BASE + drawingType.ToString() + GameDelim.BASE + author.ToString() + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round);
    }

    private void submitPrompt(CabinetDrawer selectedCabinet, bool force)
    {
        PlayerTextInputData prompt;
        if (force)
        {
            releaseDeskPromptFolder(selectedCabinet, force);
            prompt = selectedCabinet.chainData.prompts.ContainsKey(currentPlayerCabinetRoundIndex) ? selectedCabinet.chainData.prompts[currentPlayerCabinetRoundIndex] : new PlayerTextInputData();
            prompt.timeTaken = timeInPromptingRound;
            
        }
        else
        {
            prompt = selectedCabinet.chainData.prompts.ContainsKey(currentPlayerCabinetRoundIndex) ? selectedCabinet.chainData.prompts[currentPlayerCabinetRoundIndex] : new PlayerTextInputData();
        }

        if (PhotonNetwork.IsMasterClient)
        {
            setPrompt(selectedCabinet.id, currentPlayerCabinetRoundIndex, SettingsManager.Instance.birdName, prompt.prefix,
                prompt.noun, prompt.timeTaken, true);
            GameManager.Instance.gameFlowManager.resolveTransitionCondition("prompt_submitted:" + SettingsManager.Instance.birdName);
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("prompt" + GameDelim.BASE + selectedCabinet.id + GameDelim.BASE + currentPlayerCabinetRoundIndex + GameDelim.BASE + 
                SettingsManager.Instance.birdName.ToString() + GameDelim.BASE + prompt.prefix + GameDelim.BASE +
                prompt.noun + GameDelim.BASE + prompt.timeTaken.ToString(CultureInfo.InvariantCulture));
        }
        canGetCabinet = true;

    }

    private void submitEvaluationDrawing(CabinetDrawer selectedCabinet, bool force)
    {
        if (!evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(selectedCabinet.id))
        {
            evaluationsMap[currentEvaluationPhase].drawings.Add(selectedCabinet.id, new PlayerDrawingInputData());
        }

        if (force)
        {
            releaseDeskEvaluationDrawingFolder(selectedCabinet.id, force);
        }

        PlayerDrawingInputData newDrawing = evaluationsMap[currentEvaluationPhase].drawings[selectedCabinet.id];
        if(newDrawing.lines.Count == 0)
        {
            if (!force)
            {
                return;
            }
            if (PhotonNetwork.IsMasterClient)
            {
                sendEmptyEvaluationDrawingToClients(selectedCabinet.id, newDrawing.author);
            }
            else
            {
                sendEmptyDrawingToServer(SettingsManager.Instance.birdName, selectedCabinet.id, currentEvaluationPhase, DrawingType.evaluation);
            }
        }
        else
        {
            if (PhotonNetwork.IsMasterClient)
            {

                foreach (BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
                {
                    if (player == SettingsManager.Instance.birdName)
                    {
                        continue;
                    }

                    GameManager.Instance.gameFlowManager.addTransitionCondition("evaluation_drawing_receipt:" + player + "-" + SettingsManager.Instance.birdName);
                    prepareLinesForClient(newDrawing, selectedCabinet.id, currentEvaluationPhase, player, DrawingType.evaluation);
                }
                GameManager.Instance.gameFlowManager.resolveTransitionCondition("evaluation_drawing_submitted:" + SettingsManager.Instance.birdName);
            }
            else
            {
                prepareLinesForServer(newDrawing, SettingsManager.Instance.birdName, selectedCabinet.id, currentEvaluationPhase, DrawingType.evaluation);
            }
        }

        
        canGetCabinet = true;
    }

    private void sendEmptyEvaluationDrawingToClients(int cabinetID, BirdName author)
    {
        foreach (BirdName player in GameManager.Instance.gameFlowManager.gamePlayers.Keys)
        {
            if (player == SettingsManager.Instance.birdName)
            {
                continue;
            }

            GameManager.Instance.gameFlowManager.addTransitionCondition("empty_evaluation_drawing_receipt:" + player + "-" + author);
            sendEmptyDrawingToClient(cabinetID, currentEvaluationPhase, player, DrawingType.evaluation, author);
        }
        GameManager.Instance.gameFlowManager.resolveTransitionCondition("evaluation_drawing_submitted:" + author);
    }

    public bool isPlayerReady(BirdName player)
    {
        int cabinetID = -1;
        switch (currentState)
        {
            case CurrentState.drawing:
                foreach (KeyValuePair<int, CabinetDrawer> drawer in cabinetDrawerMap)
                {
                    if (drawer.Value.chainData.playerOrder.ContainsKey(currentPlayerCabinetRoundIndex) &&
                        drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex] == player)
                    {
                        cabinetID = drawer.Key;
                    }
                }
                if (cabinetID == -1)
                {
                    Debug.LogError("Could not find a corresponding cabinet for the player[" + player.ToString() + "] when attempting to force submit for a drawing.");
                    return false;
                }

                if (cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(currentPlayerCabinetRoundIndex) &&
                    cabinetDrawerMap[cabinetID].chainData.drawings[currentPlayerCabinetRoundIndex].lines.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            case CurrentState.prompting:
                foreach (KeyValuePair<int, CabinetDrawer> drawer in cabinetDrawerMap)
                {
                    if (drawer.Value.chainData.playerOrder.ContainsKey(currentPlayerCabinetRoundIndex) &&
                        drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex] == player)
                    {
                        cabinetID = drawer.Key;
                    }
                }
                if (cabinetID == -1)
                {
                    Debug.LogError("Could not find a corresponding cabinet for the player[" + player.ToString() + "] when attempting to force submit for a prompt.");
                    return false;
                }

                if(cabinetDrawerMap[cabinetID].chainData.prompts.ContainsKey(currentPlayerCabinetRoundIndex) &&
                    cabinetDrawerMap[cabinetID].chainData.prompts[currentPlayerCabinetRoundIndex].prefix != "" &&
                    cabinetDrawerMap[cabinetID].chainData.prompts[currentPlayerCabinetRoundIndex].noun != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            case CurrentState.evaluation:
                foreach (KeyValuePair<int, PlayerDrawingInputData> evaluation in evaluationsMap[currentEvaluationPhase].drawings)
                {
                    if (evaluation.Value.author == player)
                    {
                        cabinetID = evaluation.Key;
                    }
                }
                if (cabinetID == -1)
                {
                    Debug.LogError("Could not find a corresponding cabinet for the player[" + player.ToString() + "] when attempting to force submit for an evaluation.");
                    return false;
                }

                if(evaluationsMap[currentEvaluationPhase].drawings[cabinetID].lines.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            default:
                return false;
        }
    }

    public void forcePlayerToSubmit(BirdName player)
    {
        GameManager.Instance.gameFlowManager.addTransitionCondition("force_submit:" + player.ToString());
        if (SettingsManager.Instance.birdName == player)
        {
            Submit(true);
        }
        else
        {
            int cabinetID = -1;
            switch (currentState)
            {
                case CurrentState.drawing:
                case CurrentState.prompting:
                    foreach (CabinetDrawer drawer in cabinetDrawerMap.Values)
                    {

                        if (drawer.chainData.active &&
                            drawer.chainData.playerOrder[currentPlayerCabinetRoundIndex] == player)
                        {
                            cabinetID = drawer.id;
                        }
                    }
                    break;
                case CurrentState.evaluation:
                    foreach(KeyValuePair<int,PlayerDrawingInputData> drawing in evaluationsMap[currentEvaluationPhase].drawings)
                    {
                        if(drawing.Value.author == player)
                        {
                            cabinetID = drawing.Key;
                        }
                    }
                    break;
                default:
                    return;
            }
            

            if(cabinetID == -1)
            {
                Debug.LogError("Could not match cabinet ID to player["+player.ToString()+"] to request a forced submission.");
            }
            GameNetwork.Instance.ToPlayerQueue.Add(player.ToString() + GameDelim.BASE + "force_submit" + GameDelim.BASE + cabinetID);
        }
    }

    public void SetBirdArmPosition(BirdName inBirdName, Vector3 inPosition)
    {
        if (inBirdName == SettingsManager.Instance.birdName)
        {
            return;
        }

        getBirdArm(inBirdName).transform.position = inPosition;
    }

    public void setCabinetDrawingContents(int cabinetID, int tab, PlayerDrawingInputData drawingData)
    {
        if(drawingContainer.addDrawingHolder(cabinetID, tab))
        {
            if (!drawingContainer.drawingHolderMap.ContainsKey(cabinetID))
            {
                Debug.LogError("Drawing holder does not contain cabinet[" + cabinetID.ToString() + "].");
                return;
            }
            if (!drawingContainer.drawingHolderMap[cabinetID].ContainsKey(tab))
            {
                Debug.LogError("Drawing holder does not contain round[" + tab.ToString() + "] for cabinet[" + cabinetID.ToString() + "].");
                return;
            }
            playerFlowManager.createDrawingLines(drawingData, drawingContainer.drawingHolderMap[cabinetID][tab].gameObject.transform, promptDrawingOffset, new Vector3(0.75f, 0.85f, 1), 0.2f, 0.1f, 0.05f, drawingCanvas);
        }
        else
        {
            Debug.LogError("Failed to add drawing holder.");
            return;
        }

    }

    public void prepareLinesForServer(PlayerDrawingInputData drawingData, BirdName birdName, int cabinetID, int currentTab, DrawingType drawingType)
    {
        //Send the lines to the server
        string currentMessage;

        Vector3 currentPoint;
        string messageID = SettingsManager.Instance.birdName + GameDelim.SUB + drawingData.author.ToString() + GameDelim.SUB + drawingType + GameDelim.SUB + cabinetID.ToString() + GameDelim.SUB + currentTab.ToString() + GameDelim.SUB + drawingData.coverup;

        if (queuedLineMessageMap.ContainsKey(messageID))
        {
            Debug.LogError("Tried to queue messages for server but the messageID already existed in the map: " + messageID);
            return;
        }

        int numberOfLines = drawingData.lines.Count;
        int numberOfPoints;
        int messageIterator = 0;
        List<string> drawingLineMessages = new List<string>();
        bool isNewMessage = true;
        currentMessage = "lines" + GameDelim.BASE + messageID.ToString() + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE  + drawingData.lines[0].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[0].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[0].sortingOrder.ToString() + GameDelim.SUBLINE;

        for (int i = 0; i < numberOfLines; i++)
        {
            if (!isNewMessage)
            {
                currentMessage += GameDelim.LINE;
                currentMessage += drawingData.lines[i].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[i].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[i].sortingOrder.ToString() + GameDelim.SUBLINE;
            }

            numberOfPoints = drawingData.lines[i].positions.Count;
            isNewMessage = true;

            for (int j = 0; j < numberOfPoints; j++)
            {
                if (!isNewMessage)
                {
                    currentMessage += GameDelim.POINT;
                }
                else
                {
                    isNewMessage = false;
                }
                currentPoint = drawingData.lines[i].positions[j];
                currentMessage += Math.Round(currentPoint.x, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.SUB + Math.Round(currentPoint.y, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.SUB + Math.Round(currentPoint.z, 2).ToString(CultureInfo.InvariantCulture);

                //Handling for if the message goes over the size limit so that we're not throttling the server with huge messages.
                //Also an edge case for if this is the last iteration of all the loops as it will already be added to the queue when the method exits.
                if(currentMessage.Length > maxLineMessageLength && 
                    (j + 1) != numberOfPoints &&
                    (i + 1) != numberOfLines)
                {
                    if (!queuedLineMessageMap.ContainsKey(messageID))
                    {
                        queuedLineMessageMap.Add(messageID, new Dictionary<int,string>());
                    }
                    isNewMessage = true;
                    queuedLineMessageMap[messageID].Add(messageIterator, currentMessage);
                    messageIterator++;
                    currentMessage = "lines" + GameDelim.BASE + messageID.ToString() + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE +drawingData.lines[i].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[i].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[i].sortingOrder.ToString() + GameDelim.SUBLINE;

                    //If the line is continued in the next message need to let the game manager know.
                    if ((j + 1) != numberOfPoints)
                    {
                        currentMessage += GameDelim.CONT;
                    }
                }
            }
        }

        if (!queuedLineMessageMap.ContainsKey(messageID))
        {
            queuedLineMessageMap.Add(messageID, new Dictionary<int,string>());
        }
        queuedLineMessageMap[messageID].Add(messageIterator, currentMessage);
        messageIterator++;

        //This message lets the server know how many messages it should be expecting before it releases the cabinet.
        currentMessage = "expected_lines_notification" + GameDelim.BASE + messageID.ToString() + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE + drawingData.timeTaken;
        GameNetwork.Instance.ToServerQueue.Add(currentMessage);
    }

    public void sendDrawingPackageToServer(string packageID)
    {
        if (!queuedLineMessageMap.ContainsKey(packageID))
        {
            Debug.LogError("Queued drawing lines did not contain the requested packageID: " + packageID);
            return;
        }

        foreach(KeyValuePair<int,string> queuedLineMessage in queuedLineMessageMap[packageID])
        {
            GameNetwork.Instance.ToServerQueue.Add(queuedLineMessage.Value);
        }
    }

    public void prepareLinesForClient(PlayerDrawingInputData drawingData, int cabinetID, int currentTab, BirdName birdName, DrawingType drawingType)
    {
        //Send the lines to the player
        string currentMessage;

        Vector3 currentPoint;
        string messageID = birdName + GameDelim.SUB + drawingData.author + GameDelim.SUB + drawingType + GameDelim.SUB + cabinetID.ToString() + GameDelim.SUB + currentTab.ToString() + GameDelim.SUB + drawingData.coverup;

        if (queuedLineMessageMap.ContainsKey(messageID))
        {
            Debug.LogError("Tried to queue messages for clients but the messageID already existed in the map: " + messageID);
            return;
        }

        int numberOfLines = drawingData.lines.Count;
        int numberOfPoints;
        int messageIterator = 0;
        bool isNewMessage = true;
        currentMessage = "lines" + GameDelim.BASE + messageID + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE + drawingData.lines[0].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[0].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[0].sortingOrder.ToString() + GameDelim.SUBLINE;


        for (int i = 0; i < numberOfLines; i++)
        {
            if (!isNewMessage)
            {
                currentMessage += GameDelim.LINE;
                currentMessage += drawingData.lines[i].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[i].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[i].sortingOrder.ToString()  + GameDelim.SUBLINE;
            }

            isNewMessage = true;
            numberOfPoints = drawingData.lines[i].positions.Count;

            for (int j = 0; j < numberOfPoints; j++)
            {
                if (!isNewMessage)
                {
                    currentMessage += GameDelim.POINT;
                }
                else
                {
                    isNewMessage = false;
                }
                currentPoint = drawingData.lines[i].positions[j];
                currentMessage += Math.Round(currentPoint.x, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.SUB + Math.Round(currentPoint.y, 2).ToString(CultureInfo.InvariantCulture) + GameDelim.SUB + Math.Round(currentPoint.z, 2).ToString(CultureInfo.InvariantCulture);

                //Handling for if the message goes over the size limit so that we're not throttling the server with huge messages.
                //Also an edge case for if this is the last iteration of all the loops as it will already be added to the queue when the method exits.
                if (currentMessage.Length > maxLineMessageLength &&
                    (j + 1) != numberOfPoints &&
                    (i + 1) != numberOfLines)
                {
                    if (!queuedLineMessageMap.ContainsKey(messageID))
                    {
                        queuedLineMessageMap.Add(messageID, new Dictionary<int,string>());
                    }

                    queuedLineMessageMap[messageID].Add(messageIterator,currentMessage);
                    messageIterator++;
                    currentMessage = "lines" + GameDelim.BASE + messageID + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE + drawingData.lines[i].lineType.ToString() + GameDelim.SUBLINE + drawingData.lines[i].lineSize.ToString() + GameDelim.SUBLINE + drawingData.lines[i].sortingOrder.ToString() + GameDelim.SUBLINE;
                    isNewMessage = true;

                    //If the line is continued in the next message need to let the game manager know.
                    if ((j + 1) != numberOfPoints)
                    {
                        currentMessage += GameDelim.CONT;
                    }
                }
            }
        }

        if (!queuedLineMessageMap.ContainsKey(messageID))
        {
            queuedLineMessageMap.Add(messageID, new Dictionary<int,string>());
        }
        queuedLineMessageMap[messageID].Add(messageIterator,currentMessage);
        messageIterator++;

        //This message lets the clients know how many messages it should be expecting before it releases the cabinet. 
        //Needs to add the drawing player, the cabinet ID and the round
        currentMessage = birdName + GameDelim.BASE + "expected_lines_notification"+ GameDelim.BASE + drawingData.author.ToString() + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + currentTab.ToString() + GameDelim.BASE + messageID.ToString() + GameDelim.BASE + messageIterator.ToString() + GameDelim.BASE + drawingData.timeTaken;
        GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);
    }

    public void sendDrawingPackageToClient(BirdName birdName, string packageID)
    {
        if (!queuedLineMessageMap.ContainsKey(packageID))
        {
            Debug.LogError("Queued drawing lines did not contain the requested packageID: " + packageID);
            return;
        }

        foreach (KeyValuePair<int,string> queuedLineMessage in queuedLineMessageMap[packageID])
        {
            GameNetwork.Instance.ToPlayerQueue.Add(birdName + GameDelim.BASE + queuedLineMessage.Value);
        }
    }

    public void sendEmptyDrawingToClient(int cabinetID, int round, BirdName nextPlayer, DrawingType drawingType, BirdName author)
    {
        GameNetwork.Instance.ToPlayerQueue.Add(nextPlayer + GameDelim.BASE + "empty_drawing" + GameDelim.BASE + drawingType.ToString() + GameDelim.BASE + cabinetID.ToString() + GameDelim.BASE + round.ToString() + GameDelim.BASE + author);
    }

    public void setPrompt(int cabinetID, int tab, BirdName author, string prefix, string noun, float timeTaken, bool notFirstPrompt)
    {
        ChainData selectedCabinetData;
        CabinetDrawer selectedCabinet = cabinetDrawerMap[cabinetID];
        
        BirdName queuedPlayer;

        if(cabinetDrawerMap[cabinetID].chainData == null)
        {
            cabinetDrawerMap[cabinetID].chainData = new ChainData();
        }

        selectedCabinetData = cabinetDrawerMap[cabinetID].chainData;
        PlayerTextInputData newPromptData = new PlayerTextInputData();

        if (notFirstPrompt)
        {
            
            if (!selectedCabinetData.prompts.ContainsKey(currentPlayerCabinetRoundIndex))
            {
                selectedCabinetData.prompts.Add(currentPlayerCabinetRoundIndex, newPromptData);
            }
            else
            {
                newPromptData = selectedCabinetData.prompts[currentPlayerCabinetRoundIndex];
            }
            newPromptData.prefix = prefix;
            newPromptData.noun = noun;
            newPromptData.timeTaken = timeTaken;
            newPromptData.author = author;


            if (PhotonNetwork.IsMasterClient)
            {
                queuedPlayer = selectedCabinetData.playerOrder[tab + 1];
                if (queuedPlayer != SettingsManager.Instance.birdName)
                {
                    GameManager.Instance.gameFlowManager.addTransitionCondition("cabinet_prompt_receipt:" + queuedPlayer);
                    GameNetwork.Instance.ToPlayerQueue.Add(queuedPlayer + GameDelim.BASE + "cabinet_prompt_contents" + GameDelim.BASE + selectedCabinet.id + GameDelim.BASE + tab.ToString() + GameDelim.BASE + newPromptData.author.ToString() + GameDelim.BASE + newPromptData.prefix + GameDelim.BASE + newPromptData.noun + GameDelim.BASE + timeTaken.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "cabinet_prompt_receipt:" +SettingsManager.Instance.birdName);
            }
        }
        else
        {
            selectedCabinetData.correctPrefix = prefix;
            selectedCabinetData.correctNoun = noun;
            if (PhotonNetwork.IsMasterClient)
            {
                queuedPlayer = selectedCabinetData.playerOrder[tab + 1];
                if (queuedPlayer != SettingsManager.Instance.birdName)
                {
                    GameManager.Instance.gameFlowManager.addTransitionCondition("initial_cabinet_prompt_receipt:" + queuedPlayer);
                    GameNetwork.Instance.ToPlayerQueue.Add(queuedPlayer + GameDelim.BASE + "initial_cabinet_prompt_contents" + GameDelim.BASE + selectedCabinet.id + GameDelim.BASE + tab.ToString() + GameDelim.BASE + prefix + GameDelim.BASE + noun + GameDelim.BASE + timeTaken.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "initial_cabinet_prompt_receipt:" + SettingsManager.Instance.birdName);
            }
        }

        promptText.gameObject.SetActive(true);
    }

    public void addSelectedBotcherPrefix(int cabinetID, string prefix)
    {
        cabinetDrawerMap[cabinetID].chainData.selectedBotcherPrefix = prefix;
        cabinetDrawerMap[cabinetID].chainData.possiblePrefixes = cabinetDrawerMap[cabinetID].chainData.possiblePrefixes.OrderBy(a => Guid.NewGuid()).ToList();

        if(cabinetDrawerMap[cabinetID].chainData.possiblePrefixes[0] == cabinetDrawerMap[cabinetID].chainData.correctPrefix)
        {
            cabinetDrawerMap[cabinetID].chainData.possiblePrefixes.RemoveAt(1);
        }
        else
        {
            cabinetDrawerMap[cabinetID].chainData.possiblePrefixes.RemoveAt(0);
        }

        cabinetDrawerMap[cabinetID].chainData.possiblePrefixes.Add(prefix);
        cabinetDrawerMap[cabinetID].chainData.possiblePrefixes = cabinetDrawerMap[cabinetID].chainData.possiblePrefixes.OrderBy(a => Guid.NewGuid()).ToList();
    }

    public void addSelectedBotcherNoun(int cabinetID, string noun)
    {
        cabinetDrawerMap[cabinetID].chainData.selectedBotcherNoun = noun;
        cabinetDrawerMap[cabinetID].chainData.possibleNouns = cabinetDrawerMap[cabinetID].chainData.possibleNouns.OrderBy(a => Guid.NewGuid()).ToList();

        if (cabinetDrawerMap[cabinetID].chainData.possibleNouns[0] == cabinetDrawerMap[cabinetID].chainData.correctNoun)
        {
            cabinetDrawerMap[cabinetID].chainData.possibleNouns.RemoveAt(1);
        }
        else
        {
            cabinetDrawerMap[cabinetID].chainData.possibleNouns.RemoveAt(0);
        }

        cabinetDrawerMap[cabinetID].chainData.possibleNouns.Add(noun);
        cabinetDrawerMap[cabinetID].chainData.possibleNouns = cabinetDrawerMap[cabinetID].chainData.possibleNouns.OrderBy(a => Guid.NewGuid()).ToList();
    }

    public void transitionRound()
    {
        switch (currentState)
        {
            case CurrentState.drawing:
                transitionFromDrawingState();
                break;
            case CurrentState.prompting:
                transitionFromPromptingState();
                break;
            case CurrentState.evaluation:
                transitionFromEvaluationState();
                break;
            case CurrentState.waiting:
                break;
            case CurrentState.invalid:
                Debug.LogError("Invalid drawing round state, cannot transition.");
                return;
        }
    }

    public void transitionFromDrawingState()
    {
        if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.botching)
        {
            transitionToEvaluationRound();
        }
        else
        {
            transitionToPromptingRound();
        }

    }

    private void transitionToEvaluationRound()
    {
        currentEvaluationPhase++;
        List<BirdName> allBirdNames = new List<BirdName>();

        foreach (KeyValuePair<int, CabinetDrawer> drawer in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap)
        {
            if (!drawer.Value.chainData.active)
            {
                continue;
            }
            if (!evaluationsMap.ContainsKey(currentEvaluationPhase))
            {
                Debug.LogError("Evaluations map does not contain round[" + currentEvaluationPhase + "].");
            }
            if (!evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(drawer.Key))
            {
                Debug.LogError("Evaluations map does not contain cabinet[" + drawer.Key.ToString() + "] in round[" + currentEvaluationPhase.ToString() + "].");
            }
            BirdName currentBird = evaluationsMap[currentEvaluationPhase].drawings[drawer.Key].author;
            if (currentBird != BirdName.none)
            {
                GameManager.Instance.gameFlowManager.addTransitionCondition("evaluation_drawing_submitted:" + currentBird);

                //Open the cabinet drawer
                drawer.Value.setAsReady(currentBird);

                GameManager.Instance.gameFlowManager.timeRemainingInPhase = timeInEvaluationRound;
                GameManager.Instance.playerFlowManager.currentTimeInRound = timeInEvaluationRound;
                GameNetwork.Instance.BroadcastQueue.Add("update_timer" + GameDelim.BASE + timeInEvaluationRound.ToString());

                if (gameFlowManager.gamePlayers[currentBird].playerRole == GameFlowManager.PlayerRole.botcher)
                {

                }

                //Broadcast to other players to open the drawers
                GameNetwork.Instance.BroadcastQueue.Add("evaluation_drawer_is_ready" + GameDelim.BASE + drawer.Key.ToString() + GameDelim.BASE + currentEvaluationPhase.ToString() + GameDelim.BASE + currentBird.ToString());
            }
        }

        currentState = CurrentState.evaluation;
    }
    private void transitionToPromptingRound()
    {
        currentPlayerCabinetRoundIndex++;
        string currentMessage = "";
        List<BirdName> allBirdNames = new List<BirdName>();

        if (currentPlayerCabinetRoundIndex <= GameManager.Instance.playerFlowManager.numberOfCabinetRounds)
        {
            foreach (KeyValuePair<int, CabinetDrawer> drawer in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap)
            {
                BirdName currentBird = drawer.Value.chainData != null && drawer.Value.chainData.active ? drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex] : BirdName.none;
                if (currentBird != BirdName.none)
                {
                    GameManager.Instance.gameFlowManager.addTransitionCondition("prompt_submitted:" + currentBird);
                    //Open the cabinet drawer
                    drawer.Value.setAsReady(currentBird);
                    GameManager.Instance.gameFlowManager.timeRemainingInPhase = timeInPromptingRound;
                    GameManager.Instance.playerFlowManager.currentTimeInRound = timeInPromptingRound;
                    GameNetwork.Instance.BroadcastQueue.Add("update_timer" + GameDelim.BASE + GameManager.Instance.playerFlowManager.drawingRound.timeInPromptingRound.ToString());

                    if (gameFlowManager.gamePlayers[currentBird].playerRole == GameFlowManager.PlayerRole.botcher)
                    {
                        if (currentBird != SettingsManager.Instance.birdName)
                        {
                            //Send correct botcher option
                            currentMessage = currentBird.ToString() + GameDelim.BASE + "show_botcher_correct_option" + GameDelim.BASE + drawer.Key.ToString() + GameDelim.BASE + currentPlayerCabinetRoundIndex.ToString() + GameDelim.BASE + drawer.Value.chainData.correctPrefix + GameDelim.BASE + drawer.Value.chainData.correctNoun;
                            GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);

                        }
                    }

                    //Broadcast to other players to open the drawers
                    GameNetwork.Instance.BroadcastQueue.Add("cabinet_drawer_is_ready" + GameDelim.BASE + "prompting" + GameDelim.BASE + drawer.Key.ToString() + GameDelim.BASE + currentPlayerCabinetRoundIndex.ToString() + GameDelim.BASE + drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex].ToString());
                }
            }

            currentState = CurrentState.prompting;
            return;
        }



        foreach (CabinetDrawer cabinet in cabinetDrawerMap.Values)
        {
            if (cabinet.chainData.active)
            {
                if (cabinet.chainData.guesser == SettingsManager.Instance.birdName)
                {
                    GameManager.Instance.playerFlowManager.responsesRound.setPossiblePrefixes(cabinet.id, cabinet.chainData.possiblePrefixes);
                    GameManager.Instance.playerFlowManager.responsesRound.setPossibleNouns(cabinet.id, cabinet.chainData.possibleNouns);
                }
                else
                {
                    GameManager.Instance.gameFlowManager.addTransitionCondition("response_round_ready:" + cabinet.chainData.guesser);

                    //Send possible prefixes
                    currentMessage = cabinet.chainData.guesser.ToString() + GameDelim.BASE + "possible_prefixes" + GameDelim.BASE + cabinet.id;
                    foreach (string possiblePrefix in cabinet.chainData.possiblePrefixes)
                    {
                        currentMessage += GameDelim.BASE + possiblePrefix;
                    }
                    GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);

                    //Send possible nouns
                    currentMessage = cabinet.chainData.guesser.ToString() + GameDelim.BASE + "possible_nouns" + GameDelim.BASE + cabinet.id;
                    foreach (string possibleNoun in cabinet.chainData.possibleNouns)
                    {
                        currentMessage += GameDelim.BASE + possibleNoun;
                    }
                    GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);

                    if (GameManager.Instance.gameFlowManager.gamePlayers[cabinet.chainData.guesser].playerRole == GameFlowManager.PlayerRole.botcher)
                    {
                        GameNetwork.Instance.ToPlayerQueue.Add(cabinet.chainData.guesser + GameDelim.BASE + "response_round_correct_guess" + GameDelim.BASE + cabinet.id + GameDelim.BASE + cabinet.chainData.correctPrefix + GameDelim.BASE + cabinet.chainData.correctNoun);
                    }
                }

            }
        }

        currentState = CurrentState.waiting;
        roundIsInProgress = false;
    }

    public void transitionFromPromptingState()
    {
        currentPlayerCabinetRoundIndex++;
        string currentMessage = "";
        List<BirdName> allBirdNames = new List<BirdName>();


        foreach (KeyValuePair<int, CabinetDrawer> drawer in GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap)
        {
            BirdName currentBird = drawer.Value.chainData != null && drawer.Value.chainData.active ? drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex] : BirdName.none;
            if (currentBird != BirdName.none)
            {
                GameManager.Instance.gameFlowManager.addTransitionCondition("drawing_submitted:" + currentBird);

                //Open the cabinet drawer
                drawer.Value.setAsReady(currentBird);

                GameManager.Instance.gameFlowManager.timeRemainingInPhase = timeInDrawingRound;
                GameManager.Instance.playerFlowManager.currentTimeInRound = timeInDrawingRound;
                GameNetwork.Instance.BroadcastQueue.Add("update_timer" + GameDelim.BASE + GameManager.Instance.playerFlowManager.drawingRound.timeInDrawingRound.ToString());

                if (gameFlowManager.gamePlayers[currentBird].playerRole == GameFlowManager.PlayerRole.botcher)
                {
                    if (currentBird != SettingsManager.Instance.birdName)
                    {
                        //Send correct botcher option
                        currentMessage = currentBird.ToString() + GameDelim.BASE + "show_botcher_correct_option" + GameDelim.BASE + drawer.Key.ToString() + GameDelim.BASE + currentPlayerCabinetRoundIndex.ToString() + GameDelim.BASE + drawer.Value.chainData.correctPrefix + GameDelim.BASE + drawer.Value.chainData.correctNoun;
                        GameNetwork.Instance.ToPlayerQueue.Add(currentMessage);
                    }
                }

                //Broadcast to other players to open the drawers
                GameNetwork.Instance.BroadcastQueue.Add("cabinet_drawer_is_ready" + GameDelim.BASE + "drawing" + GameDelim.BASE + drawer.Key.ToString() + GameDelim.BASE + currentPlayerCabinetRoundIndex.ToString() + GameDelim.BASE + drawer.Value.chainData.playerOrder[currentPlayerCabinetRoundIndex].ToString());
            }
        }

        currentState = CurrentState.drawing;
        return;
        
    }

    public void transitionFromEvaluationState()
    {
        transitionToPromptingRound();
    }

    public void setBotcherCorrectOption(int cabinetID, int tab, string prefix, string noun)
    {
        ChainData chain = cabinetDrawerMap[cabinetID].chainData;
        chain.correctPrefix = prefix;
        chain.correctNoun = noun;

        if(drawingBotcherContainer.readyToShowCorrectOption &&
            playerFlowManager.playerRole == GameFlowManager.PlayerRole.botcher)
        {
            //drawingBotcherContainer.loadCorrectOption(cabinetID, tab);
        }
    }

    public void handleEmptyDrawingToServer(DrawingType drawingType, BirdName author, int cabinetID, int round)
    {

        switch (drawingType)
        {
            case DrawingType.cabinet:
                BirdName nextInQueue = cabinetDrawerMap[cabinetID].chainData.playerOrder[round + 1];
                if (!cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(round))
                {
                    cabinetDrawerMap[cabinetID].chainData.addDrawing(round, new PlayerDrawingInputData() { author = author});
                }


                sendEmptyCabinetDrawingToClients(nextInQueue, author, cabinetID, round);
                break;
            case DrawingType.evaluation:
                if (!evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(cabinetID))
                {
                    evaluationsMap[currentEvaluationPhase].drawings.Add(cabinetID, new PlayerDrawingInputData() { author = author });
                }

                sendEmptyEvaluationDrawingToClients(cabinetID, author);

                break;
        }
    }

    public void handleEmptyDrawingToPlayer(DrawingType drawingType, int cabinetID, int round, BirdName author)
    {
        switch (drawingType)
        {
            case DrawingType.cabinet:
                Debug.LogError("Adding drawing from player["+author+"] for cabinet["+cabinetID+"] on round["+round+"].");
                if (!cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(round))
                {
                    cabinetDrawerMap[cabinetID].chainData.addDrawing(round, new PlayerDrawingInputData() { author = author });
                }
                drawingContainer.addDrawingHolder(cabinetID, round);
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "empty_drawing_receipt:" + SettingsManager.Instance.birdName);
                break;
            case DrawingType.evaluation:
                if (!evaluationsMap[currentEvaluationPhase].drawings.ContainsKey(cabinetID))
                {
                    evaluationsMap[currentEvaluationPhase].drawings.Add(cabinetID, new PlayerDrawingInputData() { author = author });
                }
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "empty_evaluation_drawing_receipt:" + SettingsManager.Instance.birdName + "-" + author.ToString());
                break;
            case DrawingType.to_botcher:
                if (!cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(round))
                {
                    cabinetDrawerMap[cabinetID].chainData.addDrawing(round, new PlayerDrawingInputData() { author = author });
                }
                drawingContainer.addDrawingHolder(cabinetID, round);
                GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "empty_botcher_drawing_receipt:" + SettingsManager.Instance.birdName + "-" + author.ToString());
                break;
            case DrawingType.chain:
                Debug.LogError("Adding drawing from player[" + author + "] for cabinet[" + cabinetID + "] on round[" + round + "].");
                if (!cabinetDrawerMap[cabinetID].chainData.drawings.ContainsKey(round))
                {
                    cabinetDrawerMap[cabinetID].chainData.addDrawing(round, new PlayerDrawingInputData() { author = author });
                }
                if (GameManager.Instance.playerFlowManager.chainsAreAllLoaded())
                {
                    GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "all_chains_loaded:" + SettingsManager.Instance.birdName);
                }
                break;
        }
    }

    public void toUpperPrefixText()
    {
        prefixInputField.text = prefixInputField.text.ToUpper();
    }

    public void toUpperNounText()
    {
        nounInputField.text = nounInputField.text.ToUpper();
    }

    public void addBotcherCoverup(int cabinetID, int round, Coverup coverup)
    {
        cabinetDrawerMap[cabinetID].chainData.drawings[round].coverup = coverup;

        if (PhotonNetwork.IsMasterClient)
        {
            ChainData currentChain = cabinetDrawerMap[cabinetID].chainData;
            BirdName nextInQueue;
            if((round) == GameManager.Instance.playerFlowManager.numberOfCabinetRounds)
            {
                nextInQueue = currentChain.guesser;
            }
            else
            {
                nextInQueue = currentChain.playerOrder[round + 1];
            }

            if(nextInQueue == SettingsManager.Instance.birdName)
            {
                return;
            }
            GameNetwork.Instance.ToPlayerQueue.Add(nextInQueue + GameDelim.BASE +"add_botcher_coverup" + GameDelim.BASE + cabinetID + GameDelim.BASE + round + GameDelim.BASE + coverup);
        }
    }

    public void setEvaluationDetails(string adjective, string noun, int cabinet, int evaluationRound)
    {
        EvaluationData evaluationData;
        if (!evaluationsMap.ContainsKey(evaluationRound))
        {
            evaluationData = new EvaluationData();
            evaluationsMap.Add(evaluationRound, evaluationData);
        }
        else
        {
            evaluationData = evaluationsMap[evaluationRound];
        }

        evaluationData.adjective = adjective;
        evaluationData.noun = noun;
        evaluationData.round = evaluationRound;
        evaluationData.drawings.Add(cabinet, new PlayerDrawingInputData() { author = SettingsManager.Instance.birdName });
    }

    public void debugTransitionInfo()
    {
        foreach(string transitionCondition in GameManager.Instance.gameFlowManager.activeTransitionConditions)
        {
            Debug.LogError(transitionCondition);
        }
    }
}
