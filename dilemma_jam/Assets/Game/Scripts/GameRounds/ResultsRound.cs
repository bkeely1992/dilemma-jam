﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;
using static GameFlowManager;

public class ResultsRound : PlayerRound
{
    public enum EndGameState
    {
        worker_early_win, worker_win, traitor_win, incomplete, worker_early_loss
    }

    public List<ResultVoteRow> allResultVoteRows;
    public Transform emailButtonContainer;
    public GameObject currentOpenEmail;
    public TraitorEmailContents traitorWinWindow;
    public WorkersOutcomeContents workerWinWindow, workerWinWindow2;
    public SummaryEmailContents gameSummaryWindow;
    public FileSummaryEmailContents fileSummaryWindow;
    public List<BirdTag> emailWindows;
    public GameObject traitorWinButtonPrefab, workerWinButtonPrefab, gameSummaryButtonPrefab, folderSummaryButtonPrefab;
    public GameObject containerObject;
    public EndGameState endgameState = EndGameState.incomplete;
    public Image lastSelectedButtonImage;
    public HonkManager honkManager;

    public Color selectedEmailButtonColour, unselectedEmailButtonColour;
    public Dictionary<int,Dictionary<int, ResultVoteRow>> resultVoteRowMap = new Dictionary<int, Dictionary<int, ResultVoteRow>>();

    
    public bool isInitialized = false;


    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    public void initialize()
    {

        resultVoteRowMap = new Dictionary<int,Dictionary<int, ResultVoteRow>>();
        foreach (ResultVoteRow resultVoteRow in allResultVoteRows)
        {
            if (!resultVoteRowMap.ContainsKey(resultVoteRow.round))
            {
                resultVoteRowMap.Add(resultVoteRow.round, new Dictionary<int, ResultVoteRow>());
            }
            if (resultVoteRowMap[resultVoteRow.round].ContainsKey(resultVoteRow.index))
            {
                Debug.LogError("Multiple vote rows exist for round["+resultVoteRow.round+"] with index["+resultVoteRow.index+"] in results round.");
            }
            resultVoteRowMap[resultVoteRow.round].Add(resultVoteRow.index, resultVoteRow);
        }
    }

    public override void StartRound()
    {
        base.StartRound();
        if (PhotonNetwork.IsMasterClient)
        {
            initializeResultsRound();
        }

        foreach (CabinetDrawer cabinet in playerFlowManager.drawingRound.allCabinets)
        {
            if (cabinet.currentPlayer != ColourManager.BirdName.none)
            {
                initializeCabinetEmailContents(cabinet, cabinet.chainData);
            }
        }
    }

    private void initializeResultsRound()
    {
        string message;
        string winType = "";
        Dictionary<BirdName, PlayerRole> alignmentMap = new Dictionary<BirdName, PlayerRole>();

        

        if (endgameState == EndGameState.worker_early_win)
        {
            message = "endgame"+ GameDelim.BASE + "Workers";
            winType = "Workers";
        }
        else if(endgameState == EndGameState.worker_early_loss)
        {
            message = "endgame" + GameDelim.BASE + "Boss";
            winType = "Boss";
        }
        else
        {
            
            if (endgameState == EndGameState.traitor_win)
            {
                winType = "Botcher";
            }
            else
            {
                winType = "Workers";
               
            }
            message = "endgame" + GameDelim.BASE + winType;
        }

        //Add all of the player names and their alignment
        foreach (KeyValuePair<BirdName, PlayerData> playerData in (gameFlowManager.gamePlayers))
        {
            alignmentMap.Add(playerData.Key, playerData.Value.playerRole);
            message += GameDelim.BASE + playerData.Key.ToString() + GameDelim.NAME + playerData.Value.playerRole;
        }

        SettingsManager.Instance.playerNameMap.Clear();
        //Broadcast
        GameNetwork.Instance.BroadcastQueue.Add(message);

        //Update endgame sheet
        ShowResults(winType, alignmentMap);
    }

    public void ShowResults(string winningTeam, Dictionary<BirdName,PlayerRole> alignmentMap)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        playerFlowManager.instructionRound.deskPlayerImage.gameObject.SetActive(false);

        GameObject temp;
        EmailButton tempButton;

        temp = Instantiate(gameSummaryButtonPrefab, emailButtonContainer);
        tempButton = temp.GetComponent<EmailButton>();
        

        if (tempButton)
        {
            tempButton.window = gameSummaryWindow.gameObject;
            currentOpenEmail = gameSummaryWindow.gameObject;
            gameSummaryWindow.gameObject.SetActive(true);
            tempButton.unreadImage.color = selectedEmailButtonColour;
            lastSelectedButtonImage = tempButton.unreadImage;
        }

        temp = Instantiate(folderSummaryButtonPrefab, emailButtonContainer);
        tempButton = temp.GetComponent<EmailButton>();
        if (tempButton)
        {
            tempButton.window = fileSummaryWindow.gameObject;
        }

        if (winningTeam == "Workers")
        {
            temp = Instantiate(workerWinButtonPrefab, emailButtonContainer);
            tempButton = temp.GetComponent<EmailButton>();

            if (tempButton)
            {
                foreach (BirdTag traitorImage in workerWinWindow.traitorCrossoutImages)
                {
                    if (!workerWinWindow.traitorCrossoutImageMap.ContainsKey(traitorImage.birdName))
                    {
                        workerWinWindow.traitorCrossoutImageMap.Add(traitorImage.birdName, traitorImage.gameObject);
                    }
                }
                tempButton.window = workerWinWindow.gameObject;
            }

            temp = Instantiate(workerWinButtonPrefab, emailButtonContainer);
            tempButton = temp.GetComponent<EmailButton>();

            if (tempButton)
            {
                foreach (BirdTag traitorImage in workerWinWindow2.traitorCrossoutImages)
                {
                    if (!workerWinWindow2.traitorCrossoutImageMap.ContainsKey(traitorImage.birdName))
                    {
                        workerWinWindow2.traitorCrossoutImageMap.Add(traitorImage.birdName, traitorImage.gameObject);
                    }
                }
                tempButton.window = workerWinWindow2.gameObject;
                tempButton.text.text = "Former coworker sentenced to life in cage.";
            }

            gameSummaryWindow.outcomeText.text = "Workers win!";
            gameSummaryWindow.outcomeText.color = GameManager.Instance.workerColour;

            foreach (KeyValuePair<BirdName, PlayerRole> alignment in alignmentMap)
            {
                if (alignment.Value == PlayerRole.botcher)
                {
                    workerWinWindow.traitorCrossoutImageMap[alignment.Key].SetActive(true);
                    workerWinWindow2.traitorCrossoutImageMap[alignment.Key].SetActive(true);
                }
            }
        }
        else if (winningTeam == "Botcher")
        {
            temp = Instantiate(traitorWinButtonPrefab, emailButtonContainer);
            tempButton = temp.GetComponent<EmailButton>();

            if (tempButton)
            {
                foreach (BirdTag traitorImage in traitorWinWindow.traitorImages)
                {
                    if (!traitorWinWindow.traitorImageMap.ContainsKey(traitorImage.birdName))
                    {
                        traitorWinWindow.traitorImageMap.Add(traitorImage.birdName, traitorImage.gameObject);
                    }
                }

                tempButton.window = traitorWinWindow.gameObject;
            }

            gameSummaryWindow.outcomeText.text = "Botcher wins!";
            gameSummaryWindow.outcomeText.color = GameManager.Instance.traitorColour;

            foreach (KeyValuePair<BirdName, PlayerRole> alignment in alignmentMap)
            {
                if (alignment.Value == PlayerRole.botcher)
                {
                    tempButton.text.text = alignment.Key.ToString() + " gets a paid vacation!";
                    traitorWinWindow.traitorImageMap[alignment.Key].SetActive(true);
                }
            }
        }
        else if(winningTeam == "Boss")
        {
            gameSummaryWindow.outcomeText.text = "Workers lose..";
            gameSummaryWindow.outcomeText.color = GameManager.Instance.traitorColour;

            temp = Instantiate(workerWinButtonPrefab, emailButtonContainer);
            tempButton = temp.GetComponent<EmailButton>();

            if (tempButton)
            {
                foreach (BirdTag traitorImage in workerWinWindow.traitorCrossoutImages)
                {
                    if (!workerWinWindow.traitorCrossoutImageMap.ContainsKey(traitorImage.birdName))
                    {
                        workerWinWindow.traitorCrossoutImageMap.Add(traitorImage.birdName, traitorImage.gameObject);
                    }
                }
                tempButton.window = workerWinWindow.gameObject;
            }

            foreach (KeyValuePair<BirdName, PlayerRole> alignment in alignmentMap)
            {
                workerWinWindow.traitorCrossoutImageMap[alignment.Key].SetActive(true);
            }
        }

        if(endgameState == EndGameState.worker_early_win ||
            endgameState == EndGameState.worker_early_loss)
        {
            ResultVoteRow currentResultVoteRow;
            int counter = 1;
            
            foreach(KeyValuePair<BirdName,string> player in playerFlowManager.playerNameMap)
            {
                currentResultVoteRow = resultVoteRowMap[1][counter];
                counter++;

                currentResultVoteRow.birdName = player.Key;
                currentResultVoteRow.birdImage.sprite = ColourManager.Instance.birdMap[player.Key].faceSprite;
                currentResultVoteRow.playerNameText.text = player.Value;
                currentResultVoteRow.playerNameText.color = ColourManager.Instance.birdMap[player.Key].colour;
                currentResultVoteRow.roleText.text = alignmentMap[player.Key] == PlayerRole.botcher ? "Botcher" : "Worker";
                currentResultVoteRow.roleText.color = alignmentMap[player.Key] == PlayerRole.botcher ? GameManager.Instance.traitorColour : GameManager.Instance.workerColour;
                currentResultVoteRow.gameObject.SetActive(true);
            }
        }

        gameSummaryWindow.setNumberOfVoteRounds(GameManager.Instance.playerFlowManager.accusationsRound.currentVoteRound);
    }

    public void initializeCabinetEmailContents(CabinetDrawer cabinet, ChainData cabinetData)
    {
        if (!fileSummaryWindow.isInitialized)
        {
            fileSummaryWindow.initialize();
        }

        ColourManager.BirdName birdName = cabinetData.drawings.ContainsKey(1) ? cabinetData.drawings[1].author : ColourManager.BirdName.none;
        CabinetViewContents emailContents = fileSummaryWindow.fileSummaryEmailMap[birdName];

        emailContents.promptText.text = cabinetData.correctPrefix + " " + cabinetData.correctNoun;

        emailContents.playerPrompt1.text = cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].prefix + " " + cabinetData.prompts[2].noun : "";
        emailContents.playerPrompt2.text = cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].prefix + " " + cabinetData.prompts[4].noun : "";
        emailContents.playerPrompt2.gameObject.SetActive(cabinetData.prompts.ContainsKey(4));

        if (cabinetData.prefixGuess == cabinetData.correctPrefix &&
            cabinetData.nounGuess == cabinetData.correctNoun)
        {
            emailContents.guess.color = Color.green;
        }
        else
        {
            emailContents.guess.color = Color.red;
        }


        emailContents.prefixText.text = cabinetData.correctPrefix;
        emailContents.nounText.text = cabinetData.correctNoun;

        emailContents.guess.text = cabinetData.prefixGuess + " " + cabinetData.nounGuess;
        emailContents.SetImage(emailContents.guesser, cabinetData.guesser.ToString());
                
        emailContents.guessLabel.SetActive(true);
        emailContents.guess.gameObject.SetActive(true);
        emailContents.guesser.gameObject.SetActive(true);

        emailContents.SetImage(emailContents.author1, cabinetData.drawings.ContainsKey(1) ? cabinetData.drawings[1].author.ToString() : "");
        emailContents.SetImage(emailContents.author2, cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].author.ToString() : "");
        emailContents.SetImage(emailContents.author3, cabinetData.drawings.ContainsKey(3) ? cabinetData.drawings[3].author.ToString() : "");
        emailContents.SetImage(emailContents.author4, cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].author.ToString() : "");
        emailContents.SetImage(emailContents.author5, cabinetData.drawings.ContainsKey(5) ? cabinetData.drawings[5].author.ToString() : "");

        emailContents.SetAuthorColour(emailContents.playerPrompt1, cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].author.ToString() : "");
        emailContents.SetAuthorColour(emailContents.playerPrompt2, cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].author.ToString() : "");

        emailContents.arrow1.SetActive(emailContents.author1.gameObject.activeSelf);
        emailContents.arrow2.SetActive(emailContents.author2.gameObject.activeSelf);
        emailContents.arrow3.SetActive(emailContents.author3.gameObject.activeSelf);
        emailContents.arrow4.SetActive(emailContents.author4.gameObject.activeSelf);
        emailContents.arrow5.SetActive(emailContents.author5.gameObject.activeSelf);
        emailContents.drawing1.SetActive(emailContents.author1.gameObject.activeSelf);
        emailContents.drawing2.SetActive(emailContents.author3.gameObject.activeSelf);
        emailContents.drawing3.SetActive(emailContents.author5.gameObject.activeSelf);

        Transform drawingParent;
        if (cabinetData.drawings.ContainsKey(1))
        {
            drawingParent = emailContents.drawing1.transform;
            playerFlowManager.createDrawingLines(cabinetData.drawings[1], drawingParent, drawingParent.position + new Vector3(0, 0, -1), new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, emailContents.drawingCanvas);
        }
        if (cabinetData.drawings.ContainsKey(3))
        {
            drawingParent = emailContents.drawing2.transform;
            playerFlowManager.createDrawingLines(cabinetData.drawings[3], drawingParent, drawingParent.position + new Vector3(0, 0, -1), new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, emailContents.drawingCanvas);
        }
        if (cabinetData.drawings.ContainsKey(5))
        {
            drawingParent = emailContents.drawing3.transform;
            playerFlowManager.createDrawingLines(cabinetData.drawings[5], drawingParent, drawingParent.position + new Vector3(0, 0, -1), new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, emailContents.drawingCanvas);
        }

        CabinetEmailContents contents = emailContents.gameObject.GetComponent<CabinetEmailContents>();

        if (!contents.isInitialized)
        {
            contents.initialize();
        }
        contents.roundObjectMap[2].SetActive(cabinetData.prompts.ContainsKey(2));
        contents.roundObjectMap[3].SetActive(cabinetData.drawings.ContainsKey(3));
        contents.roundObjectMap[4].SetActive(cabinetData.prompts.ContainsKey(4));
        contents.roundObjectMap[5].SetActive(cabinetData.drawings.ContainsKey(5));

        //Set the ratings
        foreach (KeyValuePair<int, PlayerRatingData> ratingData in cabinetData.ratings)
        {
            emailContents.SetRating(ratingData.Key, ratingData.Value.likeCount, ratingData.Value.dislikeCount);
        }
        
    }
}
