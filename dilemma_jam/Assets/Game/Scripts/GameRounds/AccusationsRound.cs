﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;
using static GameFlowManager;

public class AccusationsRound : PlayerRound
{
    public List<MeetingBirdArm> allMeetingArms;
    public List<CabinetViewContents> allCabinetViewContents;
    public List<AccusationFolder> allAccuseFolders;

    public AccusePrompt accusePrompt;
    public ReviewPrompt reviewPrompt;
    public PresentationPrompt presentationPrompt;

    public CabinetViewContents folderContents;
    public BirdName currentAccusation = BirdName.none;
    public BirdName currentReviewee = BirdName.none;
    public GameObject containerObject;
    public GameObject presentationButtonObject;
    public Clock meetingClock;

    public List<GameObject> objectsToHideOnFolderOpen;
    public List<GameObject> objectsToShowOnFolderOpen;

    public int numberOfAccuseRoundsLeft = 1;
    public Text accuseRoundsLeftText;
    public BirdName mostAccusedPlayer = BirdName.none;
    public bool isBeingReset = false;
    public List<BirdName> livingPlayers = new List<BirdName>();
    public bool isRemoved = false;

    public int currentVoteRound = 0;
    public Dictionary<int, Dictionary<BirdName, BirdName>> accusationMap = new Dictionary<int, Dictionary<BirdName, BirdName>>();

    public Text reviewStatusText, voteStatusText;
    public float timeForVoting;
    public SpriteRenderer folderRenderer;

    public Vector3 expandedDrawingOffset;

    public enum AccusationRoundState
    {
        employee_review, accusation
    }
    public AccusationRoundState roundState = AccusationRoundState.employee_review;

    public enum FolderState
    {
        chain, evaluation
    }
    public FolderState currentFolderState = FolderState.chain;



    public GameObject announcementBGObject;
    public GameObject presentationAnnouncementObject;
    public Image presentorImage;
    public GameObject reviewInstructionAnnouncementObject;
    public GameObject accuseInstructionAnnouncementObject;
    public PresentationSubround presentationSubround;
    public float timeForShowingReviewInstruction;
    public float timeForShowingAccuseInstruction;
    public float timeForTransitioningToPresentation;

    public List<EvaluationFolderPage> allEvaluationFolders;
    public List<GameObject> objectsToShowOnOpenChain;
    public List<GameObject> objectsToHideOnOpenChain;
    public List<GameObject> objectsToShowOnOpenEval;
    public List<GameObject> objectsToHideOnOpenEval;
    public Text folderSwitchButtonText;
    private int currentEvaluationFolder = 1;

    private Dictionary<int, EvaluationFolderPage> evaluationFolderMap = new Dictionary<int, EvaluationFolderPage>();

    private float timeShowingReviewInstruction = 0f;
    private float timeShowingAccuseInstruction = 0f;
    private float timeTransitioningToPresentation = 0f;

    private Dictionary<BirdName, BirdButton> accuseButtonMap;
    private Dictionary<int, CabinetViewContents> folderContentsMap;
    private Dictionary<int, AccusationFolder> accuseFolderMap;
    private int currentlySelectedFolder = 1;
    private int numberOfPlayers;
    private bool isInitialized = false;

    public MeetingBirdArm getMeetingBirdArm(BirdName inBirdName)
    {
        return allMeetingArms.Where(ma => ma.isInitialized).Single(ma => ma.birdName == inBirdName);
    }

    public void initializePlayerSeat(int seatIndex, BirdName bird)
    {
        allMeetingArms[seatIndex].initialize(bird);
    }

    public CabinetViewContents getFolderContents(int inIndex)
    {
        return folderContentsMap[inIndex];
    }

    void initialize()
    {
        if (!isInitialized)
        {
            timeInRound = SettingsManager.Instance.accuseTime;
            currentAccusation = BirdName.none;

            folderContentsMap = new Dictionary<int, CabinetViewContents>();
            foreach (CabinetViewContents folder in allCabinetViewContents)
            {
                folderContentsMap.Add(folder.gameObject.GetComponent<IndexMap>().index, folder);
            }

            evaluationFolderMap = new Dictionary<int, EvaluationFolderPage>();
            foreach (EvaluationFolderPage evaluationFolder in allEvaluationFolders)
            {
                evaluationFolderMap.Add(evaluationFolder.round, evaluationFolder);
            }

            accuseFolderMap = new Dictionary<int, AccusationFolder>();
            foreach (AccusationFolder accuseFolder in allAccuseFolders)
            {
                accuseFolderMap.Add(accuseFolder.gameObject.GetComponent<IndexMap>().index, accuseFolder);
            }

            currentlySelectedFolder = 1;
            isInitialized = true;
        }

    }

    public override void StartRound()
    {
        initialize();
        base.StartRound();
        currentAccusation = BirdName.none;
        //folderRenderer.color = ColourManager.Instance.birdMap[(GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[1].chainData.guesser)].colour;
        if (isRemoved)
        {
            Cursor.visible = true;
            GameManager.Instance.cursorHider.activated = true;

        }
        else
        {
            getMeetingBirdArm(SettingsManager.Instance.birdName).activated = true;
        }
        
        if (!isBeingReset)
        {
            timeShowingReviewInstruction += Time.deltaTime;
            reviewStatusText.text = "REVIEW ROUND:\n WAITING ON " + GameManager.Instance.playerFlowManager.accoladesRound.employeeOfTheMonth.ToString().ToUpper();

            announcementBGObject.SetActive(true);
            reviewInstructionAnnouncementObject.SetActive(true);

            if (SettingsManager.Instance.GetSetting("stickies"))
            {
                if (!GameManager.Instance.playerFlowManager.instructionRound.accuseRevealSticky.hasBeenPlaced)
                {
                    //GameManager.Instance.playerFlowManager.instructionRound.accuseRevealSticky.Place(true);
                }
                if (!GameManager.Instance.playerFlowManager.instructionRound.accuseFolderSticky.hasBeenPlaced)
                {
                    GameManager.Instance.playerFlowManager.instructionRound.accuseFolderSticky.Place(true);
                }
            }

            foreach (KeyValuePair<BirdName, string> gamePlayer in GameManager.Instance.playerFlowManager.playerNameMap)
            {
                playerFlowManager.revealRound.addPlayerName(gamePlayer.Key, gamePlayer.Value);
                livingPlayers.Add(gamePlayer.Key);
            }

            if (PhotonNetwork.IsMasterClient)
            {
                numberOfPlayers = gameFlowManager.numberOfPlayers;
            }

            List<LineRenderer> lines = playerFlowManager.drawingsContainer.GetComponentsInChildren<LineRenderer>().ToList();

            for (int i = lines.Count - 1; i >= 0; i--)
            {
                Destroy(lines[i].gameObject);
            }

            ChainData cabinetData;
            int numberOfSuccessfulChains = 0;
            foreach (CabinetDrawer cabinet in playerFlowManager.drawingRound.cabinetDrawerMap.Values)
            {
                if (!cabinet.chainData.active)
                {
                    folderContentsMap[cabinet.id].gameObject.SetActive(false);
                    continue;
                }
                if (cabinet.chainData.correctPrefix == cabinet.chainData.prefixGuess &&
                    cabinet.chainData.correctNoun == cabinet.chainData.nounGuess)
                {
                    numberOfSuccessfulChains++;
                }

                cabinetData = cabinet.chainData;
                InitializeFolderContents(cabinet);
            }

            foreach(KeyValuePair<int, EvaluationFolderPage> evaluationPage in evaluationFolderMap)
            {
                evaluationPage.Value.initialize();
            }
            foreach(KeyValuePair<int,EvaluationData> evaluationData in playerFlowManager.drawingRound.evaluationsMap)
            {
                if(evaluationData.Key == 3 &&
                    playerFlowManager.playerNameMap.Count <= 6)
                {
                    continue;
                }
                evaluationFolderMap[evaluationData.Key].initializeEvaluationDrawings(evaluationData.Value);
            }

            numberOfAccuseRoundsLeft = 1 + (numberOfSuccessfulChains / 3);

            if (numberOfAccuseRoundsLeft > 1)
            {
                accuseRoundsLeftText.text = numberOfAccuseRoundsLeft.ToString() + " VOTES LEFT";
            }
            else
            {
                accuseRoundsLeftText.text = "1 VOTE LEFT";
            }

            getMeetingBirdArm(playerFlowManager.accoladesRound.employeeOfTheMonth).eotmToken.SetActive(true);
            getMeetingBirdArm(playerFlowManager.accoladesRound.upForReview).upForReviewToken.SetActive(true);
            isBeingReset = true;
        }
        else
        {
            reviewInstructionAnnouncementObject.SetActive(false);
            accuseInstructionAnnouncementObject.SetActive(true);
            announcementBGObject.SetActive(true);
            timeShowingAccuseInstruction += Time.deltaTime;
            timeShowingReviewInstruction = 0.0f;
            GameManager.Instance.playerFlowManager.currentTimeInRound = timeForVoting;
            
        }

    }

    public void Update()
    {
        if(timeTransitioningToPresentation > 0)
        {
            timeTransitioningToPresentation += Time.deltaTime;
            if(timeTransitioningToPresentation > timeForTransitioningToPresentation)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    GameManager.Instance.gameFlowManager.timeRemainingInPhase = presentationSubround.presentationSubroundTime;
                }

                timeTransitioningToPresentation = 0f;
                presentationAnnouncementObject.SetActive(false);
                announcementBGObject.SetActive(false);
                presentationSubround.showSlides();
                GameManager.Instance.playerFlowManager.currentTimeInRound = presentationSubround.presentationSubroundTime;
            }
        }
        else if(timeShowingAccuseInstruction > 0)
        {
            timeShowingAccuseInstruction += Time.deltaTime;
            if(timeShowingAccuseInstruction > timeForShowingAccuseInstruction)
            {
                timeShowingAccuseInstruction = 0.0f;
                announcementBGObject.SetActive(false);
                accuseInstructionAnnouncementObject.SetActive(false);
            }
        }
        else if(timeShowingReviewInstruction > 0)
        {
            timeShowingReviewInstruction += Time.deltaTime;
            if(timeShowingReviewInstruction > timeForShowingReviewInstruction)
            {
                timeShowingReviewInstruction = 0.0f;
                announcementBGObject.SetActive(false);
                reviewInstructionAnnouncementObject.SetActive(false);
            }
        }

    }

    public void ShowPrompt(BirdName inBirdName)
    {
        
        switch (roundState)
        {
            case AccusationRoundState.employee_review:
                if(GameManager.Instance.playerFlowManager.accoladesRound.employeeOfTheMonth == SettingsManager.Instance.birdName)
                {
                    if (!isRemoved)
                    {
                        MeetingBirdArm currentMeetingBirdArm = getMeetingBirdArm(SettingsManager.Instance.birdName);
                        if (inBirdName == SettingsManager.Instance.birdName ||
                            !currentMeetingBirdArm.activated
                            )
                        {
                            return;
                        }
                        currentMeetingBirdArm.activated = false;

                        Cursor.visible = true;
                    }

                    currentReviewee = inBirdName;
                    reviewPrompt.prompt.text = "Review " + inBirdName.ToString() + "?";
                    reviewPrompt.selectedBirdName = inBirdName;
                    reviewPrompt.gameObject.SetActive(true);
                }

                break;
            case AccusationRoundState.accusation:
                if (!isRemoved)
                {
                    MeetingBirdArm currentMeetingBirdArm = getMeetingBirdArm(SettingsManager.Instance.birdName);
                    if (inBirdName == SettingsManager.Instance.birdName ||
                        !currentMeetingBirdArm.activated
                        )
                    {
                        return;
                    }
                    currentMeetingBirdArm.activated = false;

                    Cursor.visible = true;
                }

                currentAccusation = inBirdName;
                accusePrompt.prompt.text = "Accuse " + inBirdName.ToString() + "?";
                accusePrompt.selectedBirdName = inBirdName;
                accusePrompt.gameObject.SetActive(true);
                break;
        }


    }

    public void ShowPresentationPrompt()
    {
        AudioManager.Instance.PlaySound("ButtonPress");
        presentationPrompt.initialize();
        presentationPrompt.open();
    }

    public void HideAccusePrompt(BirdName inBirdName)
    {
        getMeetingBirdArm(SettingsManager.Instance.birdName).activated = true;
        Cursor.visible = false;
        currentAccusation = BirdName.none;
    }

    public void HideReviewPrompt(BirdName inBirdName)
    {
        getMeetingBirdArm(SettingsManager.Instance.birdName).activated = true;
        Cursor.visible = false;
        currentReviewee = BirdName.none;
    }

    public void Review()
    {
        AudioManager.Instance.PlaySound("ButtonPress");

        if (PhotonNetwork.IsMasterClient)
        {
            revealBirdAlignment(currentReviewee, GameManager.Instance.gameFlowManager.gamePlayers[currentReviewee].playerRole);
            GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;
        }
        else
        {
            GameNetwork.Instance.ToServerQueue.Add("review_player" + GameDelim.BASE + SettingsManager.Instance.birdName.ToString() + GameDelim.BASE + currentReviewee);
        }
    }

    public void revealBirdAlignment(BirdName birdName, PlayerRole playerRole)
    {
        Color roleColour = playerRole == PlayerRole.botcher ? GameManager.Instance.traitorColour : GameManager.Instance.workerColour;

        //Get bird button
        getMeetingBirdArm(birdName).accusationButton.birdImage.color = roleColour;

        getMeetingBirdArm(birdName).birdImage.color = roleColour;

        getMeetingBirdArm(birdName).birdHandImage.color = roleColour;

        //Play sound
        AudioManager.Instance.PlaySound(playerRole == PlayerRole.botcher ? "BotcherReveal" : "WorkerReveal");

        getMeetingBirdArm(SettingsManager.Instance.birdName).activated = true;
    }

    public void Accuse()
    {
        if (!isRemoved)
        {
            AudioManager.Instance.PlaySound("ButtonPress");

            if (PhotonNetwork.IsMasterClient)
            {
                if (!accusationMap.ContainsKey(currentVoteRound))
                {
                    accusationMap.Add(currentVoteRound, new Dictionary<BirdName, BirdName>());
                }
                if (accusationMap[currentVoteRound].ContainsKey(SettingsManager.Instance.birdName))
                {
                    Debug.LogError("Attempting to set player's["+ SettingsManager.Instance.birdName + "] accusation multiple times for round["+ currentVoteRound + "].");
                }
                else
                {
                    accusationMap[currentVoteRound].Add(SettingsManager.Instance.birdName,currentAccusation);

                }
                

                if (GameManager.Instance.gameFlowManager.isCurrentAccusationReady())
                {
                    GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;
                }
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("accuse_player" + GameDelim.BASE + SettingsManager.Instance.birdName.ToString() + GameDelim.BASE + currentAccusation);
            }
        }
    }

    public void InitializeFolderContents(CabinetDrawer cabinet)
    {
        Transform drawingParent;
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        CabinetViewContents currentFolderContents = folderContentsMap[cabinet.id];

        currentFolderContents.promptText.text = cabinet.chainData.correctPrefix + " " + cabinet.chainData.correctNoun; 

        currentFolderContents.playerPrompt1.text = cabinet.chainData.prompts.ContainsKey(2) ? cabinet.chainData.prompts[2].prefix + " " + cabinet.chainData.prompts[2].noun : "";
        currentFolderContents.playerPrompt2.text = cabinet.chainData.prompts.ContainsKey(4) ? cabinet.chainData.prompts[4].prefix + " " + cabinet.chainData.prompts[4].noun : "";
        currentFolderContents.playerPrompt2.gameObject.SetActive(cabinet.chainData.prompts.ContainsKey(4));

        if (cabinet.chainData.correctPrefix == cabinet.chainData.prefixGuess &&
            cabinet.chainData.correctNoun == cabinet.chainData.nounGuess)
        {
            currentFolderContents.guess.color = Color.green;
        }
        else
        {
            currentFolderContents.guess.color = Color.red;
        }


        currentFolderContents.SetImage(currentFolderContents.guesser, cabinet.chainData.guesser.ToString());
        currentFolderContents.guess.text = cabinet.chainData.prefixGuess + " " + cabinet.chainData.nounGuess;



        currentFolderContents.SetImage(currentFolderContents.author1, cabinet.chainData.drawings.ContainsKey(1) ? cabinet.chainData.drawings[1].author.ToString() : "");
        currentFolderContents.SetImage(currentFolderContents.author2, cabinet.chainData.prompts.ContainsKey(2) ? cabinet.chainData.prompts[2].author.ToString() : "");
        currentFolderContents.SetImage(currentFolderContents.author3, cabinet.chainData.drawings.ContainsKey(3) ? cabinet.chainData.drawings[3].author.ToString() : "");
        currentFolderContents.SetImage(currentFolderContents.author4, cabinet.chainData.prompts.ContainsKey(4) ? cabinet.chainData.prompts[4].author.ToString() : "");
        currentFolderContents.SetImage(currentFolderContents.author5, cabinet.chainData.drawings.ContainsKey(5) ? cabinet.chainData.drawings[5].author.ToString() : "");
        
        currentFolderContents.guessLabel.SetActive(true);
        currentFolderContents.guess.gameObject.SetActive(true);
        currentFolderContents.guesser.gameObject.SetActive(true);

        currentFolderContents.SetAuthorColour(currentFolderContents.playerPrompt1, cabinet.chainData.prompts.ContainsKey(2) ? cabinet.chainData.prompts[2].author.ToString() : "");
        currentFolderContents.SetAuthorColour(currentFolderContents.playerPrompt2, cabinet.chainData.prompts.ContainsKey(4) ? cabinet.chainData.prompts[4].author.ToString() : "");
        
        numberOfPlayers = playerFlowManager.drawingRound.allCabinets.Where(c => c.currentPlayer != ColourManager.BirdName.none).Count();

        currentFolderContents.arrow2.SetActive(currentFolderContents.playerPrompt1.text != "");
        currentFolderContents.arrow4.SetActive(currentFolderContents.playerPrompt2.text != "");
        accuseFolderMap[cabinet.id].initialize();

        if (cabinet.chainData.drawings.ContainsKey(1))
        {
            folderContentsMap[cabinet.id].drawing1.gameObject.SetActive(true);
            drawingParent = folderContentsMap[cabinet.id].drawing1.transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[1], drawingParent, drawingParent.position, new Vector3(0.1f, 0.1f, 0.1f),0.1f,0.05f, 0.025f, folderContentsMap[cabinet.id].drawingCanvas);

            accuseFolderMap[cabinet.id].expandedDrawingButtonMap[1].gameObject.SetActive(true);
            drawingParent = accuseFolderMap[cabinet.id].expandedDrawingMap[1].transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[1], drawingParent, drawingParent.position + expandedDrawingOffset, new Vector3(0.65f, 0.65f, 0.65f), 0.2f, 0.1f, 0.05f, accuseFolderMap[cabinet.id].drawingCanvas);

            folderContentsMap[cabinet.id].arrow1.SetActive(true);
            folderContentsMap[cabinet.id].drawing1.SetActive(true);
        }
        if (cabinet.chainData.drawings.ContainsKey(3))
        {
            folderContentsMap[cabinet.id].drawing2.gameObject.SetActive(true);
            drawingParent = folderContentsMap[cabinet.id].drawing2.transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[3], drawingParent, drawingParent.position, new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, folderContentsMap[cabinet.id].drawingCanvas);

            accuseFolderMap[cabinet.id].expandedDrawingButtonMap[3].gameObject.SetActive(true);
            drawingParent = accuseFolderMap[cabinet.id].expandedDrawingMap[3].transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[3], drawingParent, drawingParent.position, new Vector3(0.65f, 0.65f, 0.65f), 0.3f, 0.15f, 0.075f, accuseFolderMap[cabinet.id].drawingCanvas);

            folderContentsMap[cabinet.id].arrow3.SetActive(true);
            folderContentsMap[cabinet.id].drawing2.SetActive(true);
        }
        if (cabinet.chainData.drawings.ContainsKey(5))
        {
            folderContentsMap[cabinet.id].drawing3.gameObject.SetActive(true);
            drawingParent = folderContentsMap[cabinet.id].drawing3.transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[5], drawingParent, drawingParent.position, new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0.05f, 0.025f, folderContentsMap[cabinet.id].drawingCanvas);

            accuseFolderMap[cabinet.id].expandedDrawingButtonMap[5].gameObject.SetActive(true);
            drawingParent = accuseFolderMap[cabinet.id].expandedDrawingMap[5].transform;
            playerFlowManager.createDrawingLines(cabinet.chainData.drawings[5], drawingParent, drawingParent.position, new Vector3(0.65f, 0.65f, 0.65f), 0.2f, 0.1f, 0.05f, accuseFolderMap[cabinet.id].drawingCanvas);

            folderContentsMap[cabinet.id].arrow5.SetActive(true);
            folderContentsMap[cabinet.id].drawing3.SetActive(true);
        }

        foreach(KeyValuePair<int,PlayerRatingData> ratingData in cabinet.chainData.ratings)
        {
            folderContentsMap[cabinet.id].SetRating(ratingData.Key, ratingData.Value.likeCount, ratingData.Value.dislikeCount);
        }
    }

    public void OpenFolder()
    {
        foreach (GameObject objectToHideOnFolderOpen in objectsToHideOnFolderOpen)
        {
            if (objectToHideOnFolderOpen)
            {
                objectToHideOnFolderOpen.SetActive(false);
            }
            
        }
        foreach (GameObject objectToShowOnFolderOpen in objectsToShowOnFolderOpen)
        {
            objectToShowOnFolderOpen.SetActive(true);
        }

        CabinetViewContents currentFolder = folderContentsMap[currentlySelectedFolder];
        currentFolder.gameObject.SetActive(true);
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        Cursor.visible = true;

        AudioManager.Instance.PlaySound("FolderOpen");

        foreach(GameObject objectToShowOnFolderOpen in objectsToShowOnFolderOpen)
        {
            objectToShowOnFolderOpen.SetActive(true);
        }

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.accuseFolderZoomSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.accuseFolderZoomSticky.Place(true);
            }
        }

        getMeetingBirdArm(SettingsManager.Instance.birdName).activated = false;

    }

    public void CloseFolder()
    {
        foreach (GameObject objectToHideOnFolderOpen in objectsToHideOnFolderOpen)
        {
            if (objectToHideOnFolderOpen)
            {
                objectToHideOnFolderOpen.SetActive(true);
            }
            
        }
        foreach (GameObject objectToShowOnFolderOpen in objectsToShowOnFolderOpen)
        {
            objectToShowOnFolderOpen.SetActive(false);
        }

        CabinetViewContents currentFolder = folderContentsMap[currentlySelectedFolder];
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        

        playerFlowManager.timeRemainingText.gameObject.SetActive(true);
        AudioManager.Instance.PlaySound("ButtonPress");

        if(currentAccusation == ColourManager.BirdName.none)
        {
            Cursor.visible = false;
            getMeetingBirdArm(SettingsManager.Instance.birdName).activated = true;
        }
        
    }

    public void SwitchFolderLeft()
    {
        switch (currentFolderState)
        {
            case FolderState.chain:
                CabinetViewContents currentFolder = folderContentsMap[currentlySelectedFolder];
                currentFolder.gameObject.SetActive(false);

                currentlySelectedFolder--;

                if (currentlySelectedFolder == 0)
                {
                    currentlySelectedFolder = numberOfPlayers;
                }

                currentFolder = folderContentsMap[currentlySelectedFolder];
                //folderRenderer.color = ColourManager.Instance.birdMap[(GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentlySelectedFolder].chainData.guesser)].colour;
                currentFolder.gameObject.SetActive(true);
                
                break;
            case FolderState.evaluation:
                EvaluationFolderPage currentPage = evaluationFolderMap[currentEvaluationFolder];
                currentPage.gameObject.SetActive(false);
                currentEvaluationFolder--;
                if(currentlySelectedFolder == 0)
                {
                    if(playerFlowManager.playerNameMap.Count <= 6)
                    {
                        currentlySelectedFolder = 3;
                    }
                    else
                    {
                        currentlySelectedFolder = 2;
                    }
                }
                if (!evaluationFolderMap.ContainsKey(currentEvaluationFolder))
                {
                    Debug.LogError("Evaluation folder index[" + currentEvaluationFolder + "] is out of range.");
                }
                currentPage = evaluationFolderMap[currentEvaluationFolder];
                currentPage.gameObject.SetActive(true);
                break;
        }
        

        AudioManager.Instance.PlaySound("ButtonPress");
    }

    public void SwitchFolderRight()
    {
        switch (currentFolderState)
        {
            case FolderState.chain:
                CabinetViewContents currentFolder = folderContentsMap[currentlySelectedFolder];
                currentFolder.gameObject.SetActive(false);

                currentlySelectedFolder++;

                if (currentlySelectedFolder == numberOfPlayers + 1)
                {
                    currentlySelectedFolder = 1;
                }

                currentFolder = folderContentsMap[currentlySelectedFolder];
                //folderRenderer.color = ColourManager.Instance.birdMap[(GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentlySelectedFolder].chainData.guesser)].colour;
                currentFolder.gameObject.SetActive(true);
                break;
            case FolderState.evaluation:
                EvaluationFolderPage currentPage = evaluationFolderMap[currentEvaluationFolder];
                currentPage.gameObject.SetActive(false);
                currentEvaluationFolder++;

                if((currentEvaluationFolder == 3 && playerFlowManager.playerNameMap.Count < 6) ||
                    currentEvaluationFolder == 4)
                {
                    currentEvaluationFolder = 1;
                }
                if (!evaluationFolderMap.ContainsKey(currentEvaluationFolder))
                {
                    Debug.LogError("Evaluation folder index["+currentEvaluationFolder+"] is out of range.");
                }
                currentPage = evaluationFolderMap[currentEvaluationFolder];
                currentPage.gameObject.SetActive(true);
                break;
        }


        AudioManager.Instance.PlaySound("ButtonPress");
    }

    public void switchFolderMode()
    {
        switch (GameManager.Instance.playerFlowManager.accusationsRound.currentFolderState)
        {
            case FolderState.chain:
                foreach (GameObject objectToShow in objectsToShowOnOpenEval)
                {
                    objectToShow.SetActive(true);
                }
                foreach (GameObject objectToHide in objectsToHideOnOpenEval)
                {
                    objectToHide.SetActive(false);
                }
                folderSwitchButtonText.text = "Chains";
                GameManager.Instance.playerFlowManager.accusationsRound.currentFolderState = FolderState.evaluation;
                //folderRenderer.color = ColourManager.Instance.birdMap[(GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[currentlySelectedFolder].chainData.guesser)].colour;
                break;
            case FolderState.evaluation:
                foreach (GameObject objectToShow in objectsToShowOnOpenChain)
                {
                    objectToShow.SetActive(true);
                }
                foreach (GameObject objectToHide in objectsToHideOnOpenChain)
                {
                    objectToHide.SetActive(false);
                }
                GameManager.Instance.playerFlowManager.accusationsRound.currentFolderState = FolderState.chain;
                folderSwitchButtonText.text = "Evaluations";
                //folderRenderer.color = ColourManager.Instance.evaluationFolderColour;
                break;
        }
        AudioManager.Instance.PlaySound("ButtonPress");
    }

    public void SetMeetingArmStretch(BirdName inBirdName, Vector3 stretchPosition)
    {
        if (inBirdName == SettingsManager.Instance.birdName)
        {
            return;
        }

        getMeetingBirdArm(inBirdName).Stretch(inBirdName, stretchPosition);
    }

    public void setMeetingArmStretches(string message)
    {
        string[] messageSegments = message.Split(new string[] { GameDelim.BASE }, StringSplitOptions.None);
        string[] armSegments;
        BirdName currentBirdName;
        Vector3 currentBirdArmStretch;

        for (int i = 1; i < messageSegments.Length; i++)
        {
            armSegments = messageSegments[i].Split(new string[] { GameDelim.SUB }, StringSplitOptions.None);
            currentBirdName = GameNetwork.getBirdNameFromCode(armSegments[0]);

            if (livingPlayers.Contains(currentBirdName))
            {
                currentBirdArmStretch = new Vector3(float.Parse(armSegments[1], CultureInfo.InvariantCulture), float.Parse(armSegments[2], CultureInfo.InvariantCulture), float.Parse(armSegments[3], CultureInfo.InvariantCulture));
                SetMeetingArmStretch(currentBirdName, currentBirdArmStretch);
            }
            
        }
    }

    public void sendAccusationsToClients()
    {
        BirdName botcherBirdName = BirdName.none;
        Dictionary<BirdName, Dictionary<int, BirdName>> orderedAccuseMap = new Dictionary<BirdName, Dictionary<int, BirdName>>();
        Dictionary<int, BirdName> orderMap = new Dictionary<int, BirdName>();
        foreach(KeyValuePair<BirdName,PlayerData> playerData in gameFlowManager.gamePlayers)
        {
            if (playerData.Value.playerRole == GameFlowManager.PlayerRole.botcher)
            {
                botcherBirdName = playerData.Value.birdName;
            }
        }
        string message = "accusation" + GameDelim.BASE + botcherBirdName.ToString();
        int rowCounter = 1;

        if (!accusationMap.ContainsKey(currentVoteRound-1))
        {
            Debug.LogError("Could not send accusations to client, missing vote round["+(currentVoteRound-1).ToString()+"] in accusation map.");
            return;
        }
        foreach(KeyValuePair<BirdName,BirdName> accusation in accusationMap[currentVoteRound-1])
        {
            if(accusation.Key == BirdName.none ||
                accusation.Value == BirdName.none ||
                !livingPlayers.Contains(accusation.Key))
            {
                continue;
            }
            message += GameDelim.BASE + accusation.Key + GameDelim.SUB + accusation.Value;
            if (!orderedAccuseMap.ContainsKey(accusation.Value))
            {
                orderedAccuseMap.Add(accusation.Value, new Dictionary<int, BirdName>());
                orderMap.Add(rowCounter, accusation.Value);
                rowCounter++;
            }
            orderedAccuseMap[accusation.Value].Add(orderedAccuseMap[accusation.Value].Count + 1, accusation.Key);
        }

        GameNetwork.Instance.BroadcastQueue.Add(message);
        playerFlowManager.revealRound.setAccusations(botcherBirdName, orderedAccuseMap, orderMap);
    }

    public void removeAccusedPlayer(BirdName inMostAccusedPlayer)
    {
        numberOfPlayers--;
        
        livingPlayers.Remove(inMostAccusedPlayer);

        MeetingBirdArm currentMeetingBirdArm = getMeetingBirdArm(inMostAccusedPlayer);
        currentMeetingBirdArm.activated = false;
        currentMeetingBirdArm.birdParent.gameObject.SetActive(false);
        currentMeetingBirdArm.bodyImage.gameObject.SetActive(false);
        currentMeetingBirdArm.gameObject.SetActive(false);

        if (PhotonNetwork.IsMasterClient)
        {
            GameNetwork.Instance.BroadcastQueue.Add("remove_player" + GameDelim.BASE + inMostAccusedPlayer.ToString());
        }

        if(SettingsManager.Instance.birdName == inMostAccusedPlayer)
        {
            isRemoved = true;
        }
        
        mostAccusedPlayer = BirdName.none;
        
    }

    public void resetRound()
    {
        numberOfAccuseRoundsLeft--;
        accuseRoundsLeftText.text = numberOfAccuseRoundsLeft > 1 ? numberOfAccuseRoundsLeft.ToString() + " VOTES LEFT" : "1 VOTE LEFT";
    }

    public void changeToVoteFromReview()
    {
        roundState = AccusationRoundState.accusation;
        GameManager.Instance.playerFlowManager.currentTimeInRound = timeForVoting;
        presentationAnnouncementObject.SetActive(false);
        reviewInstructionAnnouncementObject.SetActive(false);
        accuseInstructionAnnouncementObject.SetActive(true);
        announcementBGObject.SetActive(true);
        timeShowingAccuseInstruction += Time.deltaTime;
        timeShowingReviewInstruction = 0.0f;
        reviewStatusText.gameObject.SetActive(false);
        presentationButtonObject.SetActive(true);
        voteStatusText.gameObject.SetActive(true);

        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            if (!GameManager.Instance.playerFlowManager.instructionRound.accusePlayerSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.accusePlayerSticky.Place(true);
            }
            if (!GameManager.Instance.playerFlowManager.instructionRound.accusePresentButtonSticky.hasBeenPlaced)
            {
                GameManager.Instance.playerFlowManager.instructionRound.accusePresentButtonSticky.Place(true);
            }
        }
    }

    public void initializePresentation(int cabinetID, int round, BirdName author)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            presentationSubround.savedAccusationVoteRoundTime = GameManager.Instance.gameFlowManager.timeRemainingInPhase;
        }

        accuseInstructionAnnouncementObject.SetActive(false);
        timeTransitioningToPresentation += Time.deltaTime;
        announcementBGObject.SetActive(true);
        presentorImage.sprite = ColourManager.Instance.birdMap[author].faceSprite;
        presentationAnnouncementObject.SetActive(true);
        presentationSubround.initialize();
        presentationSubround.prepareSlides(cabinetID, round, author);
        presentationSubround.isActive = true;
    }

    public void initializeEvaluationPresentation(int evaluationRound, int playerIndex, BirdName author)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            presentationSubround.savedAccusationVoteRoundTime = GameManager.Instance.gameFlowManager.timeRemainingInPhase;
        }

        accuseInstructionAnnouncementObject.SetActive(false);
        timeTransitioningToPresentation += Time.deltaTime;
        announcementBGObject.SetActive(true);
        presentorImage.sprite = ColourManager.Instance.birdMap[author].faceSprite;
        presentationAnnouncementObject.SetActive(true);
        presentationSubround.initialize();
        presentationSubround.prepareEvaluationSlide(evaluationRound, playerIndex, author);
        presentationSubround.isActive = true;
    }

    public void resumeFromPresentation(float time)
    {
        presentationSubround.isActive = false;
        presentationSubround.reset();
        voteStatusText.gameObject.SetActive(true);

        foreach (GameObject showObject in objectsToShowOnStart)
        {
            showObject.SetActive(true);
        }

        //Reset the accusation round timer
        GameManager.Instance.playerFlowManager.currentTimeInRound = time;
    }
}
