﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class SlidesRound : PlayerRound
{
    public enum SlideStage
    {
        setup, first_slide, slides, final_slide, summary_slide, evaluation_setup, evaluation_first_slide, evaluation_slides, evaluation_summary_slide
    }
    public SlideStage currentSlideStage = SlideStage.setup;
    public bool inProgress = false;
    public LayerMask slideRecipientLayerMask;

    public List<SlideContents> allSlideContents = new List<SlideContents>();
    public List<EvaluationSlideContents> allEvaluationSlideContents = new List<EvaluationSlideContents>();
    public List<PeanutBird> allPeanutBirds = new List<PeanutBird>();
    public float timeToSetupSlides, timePerPromptSlide, timePerDrawingSlide, timePerResultSlide, timePerSummarySlide;
    public SendMessageButton sendMessageButton;
    public Vector3 slideSummaryOffset1, slideSummaryOffset2, slideSummaryOffset3;

    private Dictionary<int, SlideContents> slideContentMap;
    private Dictionary<int, EvaluationSlideContents> evaluationSlideContentMap;
    private float currentTimeOnSlide;

    public int currentSlideContentIndex = 0;
    public int currentEvaluationSlideContentIndex = -1;
    public float slideSpeed = 1.0f;
    public float baseSlideSpeed = 1.0f;
    public Vector3 drawingOffset = new Vector3(1.0f, 0.0f, 0.0f);
    public Vector3 evaluationOffset = new Vector3(1.0f, 0.0f, 0.0f);

    public float timeToShowSlidePressIndicator = 1.0f;
    public GameObject slidePressIndicator;

    public bool hasReceivedRatings = false;
    public bool hasReceivedEvaluationRatings = false;

    private float timeShowingSlidePressIndicator = 0.0f;
    private SlideContents activeSlideContents;
    private EvaluationSlideContents activeEvaluationSlideContents;

    private bool hasSentRatings = false;
    private bool hasSpedUp = false;
    private int numPlayersSpedUp = 0;

    private void Awake()
    {
        slideContentMap = new Dictionary<int, SlideContents>();
        foreach(SlideContents slideContents in allSlideContents)
        {
            slideContentMap.Add(slideContents.gameObject.GetComponent<IndexMap>().index, slideContents);
        }

        evaluationSlideContentMap = new Dictionary<int, EvaluationSlideContents>();
        foreach (EvaluationSlideContents drawingGroup in allEvaluationSlideContents)
        {
            evaluationSlideContentMap.Add(drawingGroup.round, drawingGroup);
        }
    }

    public override void StartRound()
    {
        inProgress = true;
        base.StartRound();
       
        int numberOfPlayers = playerFlowManager.playerNameMap.Count;
        slideSpeed = numPlayersSpedUp == numberOfPlayers ? 4.0f : 4.0f / (4.0f - (numberOfPlayers - numPlayersSpedUp) / numberOfPlayers);

        foreach (CabinetDrawer cabinet in playerFlowManager.drawingRound.cabinetDrawerMap.Values)
        {
            if (!cabinet.chainData.active)
            {
                continue;
            }

            ChainData cabinetData = cabinet.chainData;

            slideContentMap[cabinet.id].totalPlayers = cabinetData.drawings.Where(cd => cd.Value.author != BirdName.none).Count()
                                                        + cabinetData.prompts.Where(cd => cd.Value.author != BirdName.none).Count();
            slideContentMap[cabinet.id].active = true;
            InitializeSlideContents(cabinet);

            Transform currentParent = slideContentMap[cabinet.id].drawing1.transform;
            if (cabinetData.drawings.ContainsKey(1))
            {
                playerFlowManager.createDrawingLines(cabinetData.drawings[1], currentParent, currentParent.position + drawingOffset, new Vector3(1f, 1f, 1f), 0.2f, 0.1f, 0.05f, slideContentMap[cabinet.id].drawingCanvas);
                currentParent = slideContentMap[cabinet.id].slideSummaryBirdMap[1].drawingPosition.transform;
                playerFlowManager.createDrawingLines(cabinetData.drawings[1], currentParent, currentParent.position + slideSummaryOffset1, new Vector3(0.25f, 0.25f, 0.25f), 0.1f, 0.05f, 0.025f, slideContentMap[cabinet.id].drawingCanvas);
                slideContentMap[cabinet.id].playerName1.text = playerFlowManager.playerNameMap[cabinetData.drawings[1].author];
                slideContentMap[cabinet.id].playerName1.color = ColourManager.Instance.birdMap[cabinetData.drawings[1].author].colour;
            }
            if (cabinetData.drawings.ContainsKey(3))
            {
                currentParent = slideContentMap[cabinet.id].drawing2.transform;
                playerFlowManager.createDrawingLines(cabinetData.drawings[3], currentParent, currentParent.position + drawingOffset, new Vector3(1f, 1f, 1f), 0.2f, 0.1f, 0.05f, slideContentMap[cabinet.id].drawingCanvas);
                currentParent = slideContentMap[cabinet.id].slideSummaryBirdMap[3].drawingPosition.transform;
                playerFlowManager.createDrawingLines(cabinetData.drawings[3], currentParent, currentParent.position + slideSummaryOffset2, new Vector3(0.25f, 0.25f, 0.25f), 0.1f, 0.05f, 0.025f, slideContentMap[cabinet.id].drawingCanvas);
                slideContentMap[cabinet.id].playerName3.text = playerFlowManager.playerNameMap[cabinetData.drawings[3].author];
                slideContentMap[cabinet.id].playerName3.color = ColourManager.Instance.birdMap[cabinetData.drawings[3].author].colour;
            }
            if (cabinetData.drawings.ContainsKey(5))
            {
                currentParent = slideContentMap[cabinet.id].drawing3.transform;
                playerFlowManager.createDrawingLines(cabinetData.drawings[5], currentParent, currentParent.position + drawingOffset, new Vector3(1f, 1f, 1f), 0.2f, 0.1f, 0.05f, slideContentMap[cabinet.id].drawingCanvas);
                currentParent = slideContentMap[cabinet.id].slideSummaryBirdMap[5].drawingPosition.transform;
                playerFlowManager.createDrawingLines(cabinetData.drawings[5], currentParent, currentParent.position + slideSummaryOffset3, new Vector3(0.25f, 0.25f, 0.25f), 0.1f, 0.05f, 0.025f, slideContentMap[cabinet.id].drawingCanvas);
                slideContentMap[cabinet.id].playerName5.text = playerFlowManager.playerNameMap[cabinetData.drawings[5].author];
                slideContentMap[cabinet.id].playerName5.color = ColourManager.Instance.birdMap[cabinetData.drawings[5].author].colour;
            }
        }

        if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.botching)
        {
            int roundIterator = 0;
            EvaluationSlideContents evaluationContents;
            EvaluationDrawingGroupSlide evaluationSlide;
            EvaluationPlayerSummary evaluationPlayerSummary;
            BirdName drawer;
            Transform currentDrawingParent;

            foreach (EvaluationData evaluationData in GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap.Values)
            {
                if (!evaluationSlideContentMap.ContainsKey(roundIterator + 1))
                {
                    Debug.LogError("Evaluation slide content map is missing evaluation round[" + (roundIterator + 1).ToString() + "].");
                }
                evaluationContents = evaluationSlideContentMap[roundIterator + 1];
                evaluationContents.active = true;
                evaluationContents.originalEvaluationReminderText.text = "Original Prompt:\n" + evaluationData.adjective + " " + evaluationData.noun;
                evaluationContents.introOriginalEvaluationText.text = evaluationData.adjective + " " + evaluationData.noun;

                evaluationContents.summaryInstructionsText.text = "Who drew the " + evaluationData.adjective + " " + evaluationData.noun + "?";

                for (int i = 1; i < evaluationData.drawings.Count; i += 2)
                {
                    evaluationSlide = evaluationContents.allDrawingGroupSlides[(i - 1) / 2];

                    evaluationSlide.active = true;
                    if (evaluationData.drawings.ContainsKey(i))
                    {
                        evaluationPlayerSummary = evaluationContents.allEvaluationPlayerSummaries[i - 1];

                        drawer = evaluationData.drawings[i].author;
                        evaluationSlide.playerBirdImage1.sprite = ColourManager.Instance.birdMap[drawer].faceSprite;
                        evaluationSlide.playerNameText1.text = GameManager.Instance.playerFlowManager.playerNameMap[drawer];
                        evaluationSlide.playerNameText1.color = ColourManager.Instance.birdMap[drawer].colour;
                        evaluationSlide.containerObject1.SetActive(true);
                        currentDrawingParent = evaluationSlide.drawingPositionObject1.transform;
                        playerFlowManager.createDrawingLines(evaluationData.drawings[i], currentDrawingParent, currentDrawingParent.position + evaluationOffset, new Vector3(2.25f, 2.25f, 2.25f), 0.2f, 0.1f, 0.05f, null);

                        evaluationPlayerSummary.gameObject.SetActive(true);
                        evaluationPlayerSummary.playerBirdImage.sprite = ColourManager.Instance.birdMap[drawer].faceSprite;
                        evaluationPlayerSummary.playerNameText.text = GameManager.Instance.playerFlowManager.playerNameMap[drawer];
                        evaluationPlayerSummary.playerNameText.color = ColourManager.Instance.birdMap[drawer].colour;

                        if (drawer == SettingsManager.Instance.birdName)
                        {
                            evaluationPlayerSummary.disableDislikeButton(i);
                            evaluationPlayerSummary.disableLikeButton(i);
                        }

                        currentDrawingParent = evaluationPlayerSummary.drawingParentObject.transform;
                        playerFlowManager.createDrawingLines(evaluationData.drawings[i], currentDrawingParent, currentDrawingParent.position + slideSummaryOffset3, new Vector3(1.5f, 1.5f, 1.5f), 0.1f, 0.05f, 0.025f, null);

                    }
                    if (evaluationData.drawings.ContainsKey(i + 1))
                    {
                        evaluationPlayerSummary = evaluationContents.allEvaluationPlayerSummaries[i];

                        drawer = evaluationData.drawings[i + 1].author;
                        evaluationSlide.playerBirdImage2.sprite = ColourManager.Instance.birdMap[drawer].faceSprite;
                        evaluationSlide.playerNameText2.text = GameManager.Instance.playerFlowManager.playerNameMap[drawer];
                        evaluationSlide.playerNameText2.color = ColourManager.Instance.birdMap[drawer].colour;
                        evaluationSlide.containerObject2.SetActive(true);
                        currentDrawingParent = evaluationSlide.drawingPositionObject2.transform;
                        playerFlowManager.createDrawingLines(evaluationData.drawings[i + 1], currentDrawingParent, currentDrawingParent.position + evaluationOffset, new Vector3(2.25f, 2.25f, 2.25f), 0.2f, 0.1f, 0.05f, null);

                        evaluationPlayerSummary.gameObject.SetActive(true);
                        evaluationPlayerSummary.playerBirdImage.sprite = ColourManager.Instance.birdMap[drawer].faceSprite;
                        evaluationPlayerSummary.playerNameText.text = GameManager.Instance.playerFlowManager.playerNameMap[drawer];
                        evaluationPlayerSummary.playerNameText.color = ColourManager.Instance.birdMap[drawer].colour;

                        if (drawer == SettingsManager.Instance.birdName)
                        {
                            evaluationPlayerSummary.disableDislikeButton(i + 1);
                            evaluationPlayerSummary.disableLikeButton(i + 1);
                        }

                        currentDrawingParent = evaluationPlayerSummary.drawingParentObject.transform;
                        playerFlowManager.createDrawingLines(evaluationData.drawings[i + 1], currentDrawingParent, currentDrawingParent.position + slideSummaryOffset3, new Vector3(1.5f, 1.5f, 1.5f), 0.1f, 0.05f, 0.025f, null);
                    }
                }

                roundIterator++;
            }
        }


        if (SettingsManager.Instance.GetSetting("stickies"))
        {
            playerFlowManager.instructionRound.slidesChatSticky.Place(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(timeShowingSlidePressIndicator > 0.0f)
        {
            timeShowingSlidePressIndicator += Time.deltaTime;
            if(timeShowingSlidePressIndicator > timeToShowSlidePressIndicator)
            {
                timeShowingSlidePressIndicator = 0.0f;
                slidePressIndicator.SetActive(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Return) &&
            sendMessageButton.sendMessageInput.text != "")
        {
            sendMessageButton.Send();
            sendMessageButton.sendMessageInput.ActivateInputField();
        }

        if (PhotonNetwork.IsMasterClient)
        {
            updateSlideFlow();
        }
    }

    private void updateSlideFlow()
    {
        if (inProgress)
        {
            switch (currentSlideStage)
            {
                case SlideStage.setup:
                    if (activeSlideContents)
                    {
                        activeSlideContents.gameObject.SetActive(false);
                    }

                    activeSlideContents = null;

                    if (activeEvaluationSlideContents)
                    {
                        activeEvaluationSlideContents.gameObject.SetActive(false);
                    }

                    activeEvaluationSlideContents = null;

                    while (!activeSlideContents ||
                            !activeSlideContents.active)
                    {
                        currentSlideContentIndex++;
                        if (!slideContentMap.ContainsKey(currentSlideContentIndex))
                        {
                            if (!hasSentRatings)
                            {
                                GameManager.Instance.gameFlowManager.timeRemainingInPhase = 0.0f;

                                //Send broadcast to players for ratings
                                sendRatingsToClients();
                                foreach (KeyValuePair<int, CabinetDrawer> cabinetData in playerFlowManager.drawingRound.cabinetDrawerMap)
                                {
                                    Dictionary<int, int> likeMap = new Dictionary<int, int>();
                                    Dictionary<int, int> dislikeMap = new Dictionary<int, int>();
                                    Dictionary<int, BirdName> targetsMap = new Dictionary<int, BirdName>();
                                    foreach (KeyValuePair<int, PlayerRatingData> ratingData in cabinetData.Value.chainData.ratings)
                                    {
                                        if (!likeMap.ContainsKey(ratingData.Key))
                                        {
                                            likeMap.Add(ratingData.Key, ratingData.Value.likeCount);
                                        }
                                        if (!dislikeMap.ContainsKey(ratingData.Key))
                                        {
                                            dislikeMap.Add(ratingData.Key, ratingData.Value.dislikeCount);
                                        }
                                        if (!targetsMap.ContainsKey(ratingData.Key))
                                        {
                                            targetsMap.Add(ratingData.Key, ratingData.Value.target);
                                        }
                                    }
                                    updateChainRatings(cabinetData.Key, likeMap, dislikeMap, targetsMap);
                                }
                            }
                            return;
                        }
                        activeSlideContents = slideContentMap[currentSlideContentIndex];
                    }

                    activeSlideContents.gameObject.SetActive(true);
                    activeSlideContents.reset();
                    activeSlideContents.showOriginalPrompt();

                    GameNetwork.Instance.BroadcastQueue.Add("reset_slide" + GameDelim.BASE + currentSlideContentIndex);
                    if (SettingsManager.Instance.GetSetting("stickies") &&
                            !playerFlowManager.instructionRound.slidesRatingSticky.hasBeenClicked &&
                            playerFlowManager.instructionRound.slidesRatingSticky.hasBeenPlaced)
                    {
                        playerFlowManager.instructionRound.slidesRatingSticky.Click();
                    }
                    currentSlideStage = SlideStage.first_slide;
                    currentTimeOnSlide = 0f;

                    break;
                case SlideStage.first_slide:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerPromptSlide / slideSpeed))
                    {
                        if (activeSlideContents.isFinished())
                        {
                            currentSlideStage = SlideStage.final_slide;
                            GameNetwork.Instance.BroadcastQueue.Add("show_final_slide");
                            activeSlideContents.showFinalSlide();
                        }
                        else
                        {
                            currentSlideStage = SlideStage.slides;
                            activeSlideContents.showNextDrawing();
                            GameNetwork.Instance.BroadcastQueue.Add("show_drawing_slide");
                        }
                        currentTimeOnSlide = 0f;
                    }
                    break;
                case SlideStage.slides:
                    currentTimeOnSlide += Time.deltaTime;

                    if (activeSlideContents.totalSlidesShown % 2 == 0 &&
                        currentTimeOnSlide > (timePerPromptSlide / slideSpeed))
                    {
                        currentTimeOnSlide = 0.0f;

                        if (activeSlideContents.isFinished())
                        {
                            currentSlideStage = SlideStage.final_slide;
                            GameNetwork.Instance.BroadcastQueue.Add("show_final_slide");
                            activeSlideContents.showFinalSlide();
                        }
                        else
                        {
                            GameNetwork.Instance.BroadcastQueue.Add("show_drawing_slide");
                            activeSlideContents.showNextDrawing();
                        }
                    }
                    else if (activeSlideContents.totalSlidesShown % 2 != 0 &&
                        currentTimeOnSlide > (timePerDrawingSlide / slideSpeed))
                    {
                        currentTimeOnSlide = 0.0f;

                        if (activeSlideContents.isFinished())
                        {
                            currentSlideStage = SlideStage.final_slide;
                            GameNetwork.Instance.BroadcastQueue.Add("show_final_slide");
                            activeSlideContents.showFinalSlide();
                        }
                        else
                        {
                            GameNetwork.Instance.BroadcastQueue.Add("show_prompt_slide");
                            activeSlideContents.showNextPrompt();
                        }
                    }
                    break;
                case SlideStage.final_slide:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerResultSlide / slideSpeed))
                    {
                        if (SettingsManager.Instance.GetSetting("stickies") &&
                            !playerFlowManager.instructionRound.slidesRatingSticky.hasBeenPlaced)
                        {
                            playerFlowManager.instructionRound.slidesRatingSticky.Place(true);
                        }
                        GameNetwork.Instance.BroadcastQueue.Add("show_summary_slide");
                        activeSlideContents.showSummarySlide();
                        currentTimeOnSlide = 0.0f;
                        currentSlideStage = SlideStage.summary_slide;
                    }
                    break;
                case SlideStage.summary_slide:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerSummarySlide / slideSpeed))
                    {
                        currentTimeOnSlide = 0.0f;

                        if(SettingsManager.Instance.gameSettings.gameMode == SettingsManager.GameSettings.GameMode.botching &&
                            currentSlideContentIndex % 2 != 0 &&
                            (evaluationSlideContentMap.ContainsKey(currentEvaluationSlideContentIndex+1) && evaluationSlideContentMap[currentEvaluationSlideContentIndex+1].active))
                        {
                            currentSlideStage = SlideStage.evaluation_setup;
                        }
                        else
                        {
                            currentSlideStage = SlideStage.setup;
                        }
                        
                    }
                    break;
                case SlideStage.evaluation_setup:
                    if (activeSlideContents)
                    {
                        activeSlideContents.gameObject.SetActive(false);
                    }
                    if (activeEvaluationSlideContents)
                    {
                        activeEvaluationSlideContents.gameObject.SetActive(false);
                    }

                    activeSlideContents = null;
                    currentEvaluationSlideContentIndex++;

                    activeEvaluationSlideContents = evaluationSlideContentMap[currentEvaluationSlideContentIndex];
                    activeEvaluationSlideContents.gameObject.SetActive(true);
                    activeEvaluationSlideContents.reset();
                    activeEvaluationSlideContents.showOriginalPrompt();
                    GameNetwork.Instance.BroadcastQueue.Add("reset_evaluation_slide" + GameDelim.BASE + currentEvaluationSlideContentIndex);
                    currentSlideStage = SlideStage.evaluation_first_slide;
                    currentTimeOnSlide = 0f;

                    break;
                case SlideStage.evaluation_first_slide:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerPromptSlide / slideSpeed))
                    {
                        currentSlideStage = SlideStage.evaluation_slides;
                        activeEvaluationSlideContents.showNextDrawing();
                        GameNetwork.Instance.BroadcastQueue.Add("show_evaluation_drawing_slide");

                        currentTimeOnSlide = 0f;
                    }
                    break;
                case SlideStage.evaluation_slides:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerDrawingSlide / slideSpeed))
                    {
                        currentTimeOnSlide = 0.0f;

                        if (activeEvaluationSlideContents.isFinished())
                        {
                            currentSlideStage = SlideStage.evaluation_summary_slide;
                            GameNetwork.Instance.BroadcastQueue.Add("show_evaluation_summary_slide");
                            activeEvaluationSlideContents.showSummarySlide();
                        }
                        else
                        {
                            GameNetwork.Instance.BroadcastQueue.Add("show_evaluation_drawing_slide");
                            activeEvaluationSlideContents.showNextDrawing();
                        }
                    }
                    break;
                case SlideStage.evaluation_summary_slide:
                    currentTimeOnSlide += Time.deltaTime;

                    if (currentTimeOnSlide > (timePerSummarySlide / slideSpeed))
                    {
                        currentTimeOnSlide = 0.0f;

                        currentSlideStage = SlideStage.setup;

                    }
                    break;
            }
        }
    }

    public void InitializeSlideContents(CabinetDrawer cabinet)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }

        ChainData cabinetData = cabinet.chainData;
        SlideContents currentSlideContents = slideContentMap[cabinet.id];

        currentSlideContents.prompt1.text = cabinetData.correctPrefix + " " + cabinetData.correctNoun;
        currentSlideContents.originalPromptReminder.text = cabinetData.correctPrefix + " " + cabinetData.correctNoun;

        if (cabinetData.prompts.ContainsKey(2))
        {
            currentSlideContents.setPrompt(2, cabinetData.prompts[2].prefix + " " + cabinetData.prompts[2].noun, cabinetData.prompts[2].author);
            currentSlideContents.playerName2.text = playerFlowManager.playerNameMap[cabinetData.prompts[2].author];
            currentSlideContents.playerName2.color = ColourManager.Instance.birdMap[cabinetData.prompts[2].author].colour;
        }
        if (cabinetData.prompts.ContainsKey(4))
        {
            currentSlideContents.setPrompt(4, cabinetData.prompts[4].prefix + " " + cabinetData.prompts[4].noun, cabinetData.prompts[4].author);
            currentSlideContents.playerName4.text = playerFlowManager.playerNameMap[cabinetData.prompts[4].author];
            currentSlideContents.playerName4.color = ColourManager.Instance.birdMap[cabinetData.prompts[4].author].colour;
        }

        if (cabinetData.correctPrefix == cabinetData.prefixGuess &&
            cabinetData.correctNoun == cabinetData.nounGuess)
        {
            currentSlideContents.guess.color = Color.green;
        }
        else
        {
            currentSlideContents.guess.color = Color.red;
        }

        currentSlideContents.SetImage(currentSlideContents.author1, cabinetData.drawings.ContainsKey(1) ? cabinetData.drawings[1].author.ToString() : "");
        currentSlideContents.SetImage(currentSlideContents.author2, cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].author.ToString() : "");
        currentSlideContents.SetImage(currentSlideContents.author3, cabinetData.drawings.ContainsKey(3) ? cabinetData.drawings[3].author.ToString() : "");
        currentSlideContents.SetImage(currentSlideContents.author4, cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].author.ToString() : "");
        currentSlideContents.SetImage(currentSlideContents.author5, cabinetData.drawings.ContainsKey(5) ? cabinetData.drawings[5].author.ToString() : "");

        currentSlideContents.setGuessed(cabinetData.prefixGuess + " " + cabinetData.nounGuess, cabinetData.guesser.ToString(), cabinetData.correctPrefix == cabinetData.prefixGuess && cabinetData.correctNoun == cabinetData.nounGuess);
        currentSlideContents.setCorrectAnswer(cabinetData.correctPrefix + " " + cabinetData.correctNoun);
        currentSlideContents.playerNameGuesser.text = playerFlowManager.playerNameMap[cabinetData.guesser];
        currentSlideContents.playerNameGuesser.color = ColourManager.Instance.birdMap[cabinetData.guesser].colour;

        currentSlideContents.SetProgressIndicatorImage(0, cabinetData.drawings.ContainsKey(1) ? cabinetData.drawings[1].author.ToString() : "");
        currentSlideContents.SetProgressIndicatorImage(1, cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].author.ToString() : "");
        currentSlideContents.SetProgressIndicatorImage(2, cabinetData.drawings.ContainsKey(3) ? cabinetData.drawings[3].author.ToString() : "");
        currentSlideContents.SetProgressIndicatorImage(3, cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].author.ToString() : "");
        currentSlideContents.SetProgressIndicatorImage(4, cabinetData.drawings.ContainsKey(5) ? cabinetData.drawings[5].author.ToString() : "");

        currentSlideContents.SetAuthorColour(currentSlideContents.prompt2, cabinetData.prompts.ContainsKey(2) ? cabinetData.prompts[2].author.ToString() : "");
        currentSlideContents.SetAuthorColour(currentSlideContents.prompt3, cabinetData.prompts.ContainsKey(4) ? cabinetData.prompts[4].author.ToString() : "");
    }

    public void ResetSlide(int slideIndex)
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        if (activeSlideContents)
        {
            activeSlideContents.gameObject.SetActive(false);
        }
        if (activeEvaluationSlideContents)
        {
            activeEvaluationSlideContents.gameObject.SetActive(false);
        }

        
        currentSlideContentIndex = slideIndex;
        activeSlideContents = slideContentMap[currentSlideContentIndex];
        activeSlideContents.reset();

        if (SettingsManager.Instance.GetSetting("stickies") &&
                !playerFlowManager.instructionRound.slidesRatingSticky.hasBeenClicked &&
                playerFlowManager.instructionRound.slidesRatingSticky.hasBeenPlaced)
        {
            playerFlowManager.instructionRound.slidesRatingSticky.Click();
        }
        activeSlideContents.gameObject.SetActive(true);
        activeSlideContents.showOriginalPrompt();
    }

    public void ShowPromptSlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        activeSlideContents.showNextPrompt();
    }

    public void ShowDrawingSlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        if (activeSlideContents)
        {
            activeSlideContents.showNextDrawing();
        }
        else
        {
            Debug.LogError("Failed to show drawing slide: Active Slide Contents has not been loaded.");
        }

    }

    public void ShowFinalSlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        activeSlideContents.showFinalSlide();
    }

    public void ShowSummarySlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        activeSlideContents.showSummarySlide();
        if (SettingsManager.Instance.GetSetting("stickies") &&
        !playerFlowManager.instructionRound.slidesRatingSticky.hasBeenPlaced)
        {
            playerFlowManager.instructionRound.slidesRatingSticky.Place(true);
        }
    }

    public void ResetEvaluationSlide(int slideRoundIndex)
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        if (activeSlideContents)
        {
            activeSlideContents.gameObject.SetActive(false);
        }
        if (activeEvaluationSlideContents)
        {
            activeEvaluationSlideContents.gameObject.SetActive(false);
        }

        currentEvaluationSlideContentIndex = slideRoundIndex;
        activeEvaluationSlideContents = evaluationSlideContentMap[currentEvaluationSlideContentIndex];
        activeEvaluationSlideContents.reset();

        activeEvaluationSlideContents.gameObject.SetActive(true);
        activeEvaluationSlideContents.showOriginalPrompt();
    }

    public void ShowEvaluationDrawingSlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        if (activeEvaluationSlideContents)
        {
            activeEvaluationSlideContents.showNextDrawing();
        }
        else
        {
            Debug.LogError("Failed to show evaluation drawing slide: Active Evaluation Slide Contents has not been loaded.");
        }
    }

    public void ShowEvaluationSummarySlide()
    {
        AudioManager.Instance.PlaySound("SlidesClick");
        activeEvaluationSlideContents.showSummarySlide();
    }

    public void setPeanutGalleryMessage(ColourManager.BirdName birdNameToUpdate, string message)
    {
        allPeanutBirds.Where(pb => pb.isInitialized).Single(pb => pb.birdName == birdNameToUpdate).chatBubble.resetTime();
        allPeanutBirds.Where(pb => pb.isInitialized).Single(pb => pb.birdName == birdNameToUpdate).chatBubble.bubbleText.text = message;
    }

    public void initializeGalleryBird(int index, ColourManager.BirdName inBirdName)
    {
        PeanutBird peanutBird = allPeanutBirds[index];
        peanutBird.gameObject.SetActive(true);
        peanutBird.GetComponent<Image>().sprite = ColourManager.Instance.birdMap[inBirdName].peanutGallerySprite;
        peanutBird.birdName = inBirdName;
        peanutBird.chatBubble.bubbleText.color = ColourManager.Instance.birdMap[inBirdName].colour;
        peanutBird.isInitialized = true;
    }

    private void sendRatingsToClients()
    {
        string message = "slide_round_end_info";
        bool isFirstRatingData = true;
        foreach(KeyValuePair<int,CabinetDrawer> cabinetData in playerFlowManager.drawingRound.cabinetDrawerMap)
        {
            if (!cabinetData.Value.chainData.active)
            {
                continue;
            }
            message += GameDelim.BASE + cabinetData.Key + GameDelim.SUB;

            isFirstRatingData = true;
            foreach (KeyValuePair<int,PlayerRatingData> ratingData in cabinetData.Value.chainData.ratings)
            {
                if (!cabinetData.Value.chainData.playerOrder.ContainsKey(ratingData.Key))
                {
                    continue;
                }
                if (isFirstRatingData)
                {
                    isFirstRatingData = false;
                }
                else
                {
                    message += GameDelim.ITEM;
                }
                message += ratingData.Key.ToString() + GameDelim.RATING + ratingData.Value.likeCount + GameDelim.RATING + ratingData.Value.dislikeCount + GameDelim.RATING + cabinetData.Value.chainData.playerOrder[ratingData.Key];
            }
        }
        GameNetwork.Instance.BroadcastQueue.Add(message);

        message = "slide_round_evaluation_end_info";
        isFirstRatingData = true;
        foreach (KeyValuePair<int, EvaluationData> evaluationData in playerFlowManager.drawingRound.evaluationsMap)
        {
            message += GameDelim.BASE + evaluationData.Key + GameDelim.SUB;

            isFirstRatingData = true;
            foreach (KeyValuePair<int, PlayerRatingData> ratingData in evaluationData.Value.ratings)
            {
                if (isFirstRatingData)
                {
                    isFirstRatingData = false;
                }
                else
                {
                    message += GameDelim.ITEM;
                }
                message += ratingData.Key.ToString() + GameDelim.RATING + ratingData.Value.likeCount + GameDelim.RATING + ratingData.Value.dislikeCount + GameDelim.RATING + evaluationData.Value.drawings[ratingData.Key].author;
            }
        }
        GameNetwork.Instance.BroadcastQueue.Add(message);
        hasSentRatings = true;
    }

    public void updateChainRatings(int cabinetID, Dictionary<int,int> roundLikes, Dictionary<int,int> roundDislikes, Dictionary<int,BirdName> targets)
    {
        ChainData selectedCabinetData = playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;
        PlayerRatingData selectedRatingData;

        foreach(KeyValuePair<int,int> roundLike in roundLikes)
        {
            if (!selectedCabinetData.ratings.ContainsKey(roundLike.Key))
            {
                selectedCabinetData.ratings.Add(roundLike.Key, new PlayerRatingData());
            }
            selectedRatingData = selectedCabinetData.ratings[roundLike.Key];
            selectedRatingData.likeCount = roundLike.Value;
            selectedRatingData.target = targets[roundLike.Key];
        }
        foreach (KeyValuePair<int, int> roundDislike in roundDislikes)
        {
            if (!selectedCabinetData.ratings.ContainsKey(roundDislike.Key))
            {
                selectedCabinetData.ratings.Add(roundDislike.Key, new PlayerRatingData());
            }
            selectedRatingData = selectedCabinetData.ratings[roundDislike.Key];
            selectedRatingData.dislikeCount = roundDislike.Value;
            selectedRatingData.target = targets[roundDislike.Key];
        }
    }

    public void updateEvaluationRatings(int round, Dictionary<int, int> roundLikes, Dictionary<int, int> roundDislikes, Dictionary<int, BirdName> targets)
    {
        EvaluationData selectedEvaluationData = playerFlowManager.drawingRound.evaluationsMap[round];
        PlayerRatingData selectedRatingData;

        foreach (KeyValuePair<int, int> roundLike in roundLikes)
        {
            if (!selectedEvaluationData.ratings.ContainsKey(roundLike.Key))
            {
                selectedEvaluationData.ratings.Add(roundLike.Key, new PlayerRatingData());
            }
            selectedRatingData = selectedEvaluationData.ratings[roundLike.Key];
            selectedRatingData.likeCount = roundLike.Value;
            selectedRatingData.target = targets[roundLike.Key];
        }
        foreach (KeyValuePair<int, int> roundDislike in roundDislikes)
        {
            if (!selectedEvaluationData.ratings.ContainsKey(roundDislike.Key))
            {
                selectedEvaluationData.ratings.Add(roundDislike.Key, new PlayerRatingData());
            }
            selectedRatingData = selectedEvaluationData.ratings[roundDislike.Key];
            selectedRatingData.dislikeCount = roundDislike.Value;
            selectedRatingData.target = targets[roundDislike.Key];
        }
    }

    public void SpeedUpSlidesClick()
    {
        if (!hasSpedUp)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                increaseSlideSpeed();
            }
            else
            {
                GameNetwork.Instance.ToServerQueue.Add("speed_up_slides");
            }
            hasSpedUp = true;
        }
    }

    public void increaseSlideSpeed()
    {
        numPlayersSpedUp++;
        int numberOfPlayers = playerFlowManager.playerNameMap.Count;
        setSlideSpeed(numPlayersSpedUp == numberOfPlayers ? 4.0f : 4.0f - (4.0f * (numberOfPlayers - numPlayersSpedUp) / numberOfPlayers));
        GameNetwork.Instance.BroadcastQueue.Add("slide_speed" + GameDelim.BASE + slideSpeed.ToString(CultureInfo.InvariantCulture));
    }

    public void setSlideSpeed(float inSlideSpeed)
    {
        AudioManager.Instance.PlaySound("SpeedUp");
        slideSpeed = inSlideSpeed;
        slidePressIndicator.SetActive(true);
        timeShowingSlidePressIndicator = Time.deltaTime;
    }

    public void hideLikeButtons(int cabinetID)
    {
        foreach(KeyValuePair<int,SlideSummaryBird> slideSummaryBird in slideContentMap[cabinetID].slideSummaryBirdMap)
        {
            slideSummaryBird.Value.disableLikeButton();
        }
    }

    public void hideDislikeButtons(int cabinetID)
    {
        foreach (KeyValuePair<int, SlideSummaryBird> slideSummaryBird in slideContentMap[cabinetID].slideSummaryBirdMap)
        {
            slideSummaryBird.Value.disableDislikeButton();
        }
    }

    public void hideEvaluationLikeButtons(int round)
    {
        evaluationSlideContentMap[round].disableAllLikeButtons();
    }

    public void hideEvaluationDislikeButtons(int round)
    {
        evaluationSlideContentMap[round].disableAllDislikeButtons();
    }

    public void addLike(int cabinetID, int tab, BirdName birdName)
    {
        ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;

        if (!selectedCabinetData.ratings.ContainsKey(tab))
        {
            selectedCabinetData.ratings.Add(tab, new PlayerRatingData());
        }
        selectedCabinetData.ratings[tab].likeCount++;
        selectedCabinetData.ratings[tab].target = birdName;

    }

    public void addDislike(int cabinetID, int tab, BirdName birdName)
    {
        ChainData selectedCabinetData = GameManager.Instance.playerFlowManager.drawingRound.cabinetDrawerMap[cabinetID].chainData;

        if (!selectedCabinetData.ratings.ContainsKey(tab))
        {
            selectedCabinetData.ratings.Add(tab, new PlayerRatingData());
        }
        selectedCabinetData.ratings[tab].dislikeCount++;
        selectedCabinetData.ratings[tab].target = birdName;


    }

    public void addEvaluationLike(int cabinetID, int round, BirdName birdName)
    {
        EvaluationData selectedEvaluationData = GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[round];

        if (!selectedEvaluationData.ratings.ContainsKey(cabinetID))
        {
            selectedEvaluationData.ratings.Add(cabinetID, new PlayerRatingData());
        }
        selectedEvaluationData.ratings[cabinetID].likeCount++;
        selectedEvaluationData.ratings[cabinetID].target = birdName;
    }

    public void addEvaluationDislike(int cabinetID, int round, BirdName birdName)
    {
        EvaluationData selectedEvaluationData = GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap[round];

        if (!selectedEvaluationData.ratings.ContainsKey(cabinetID))
        {
            selectedEvaluationData.ratings.Add(cabinetID, new PlayerRatingData());
        }
        selectedEvaluationData.ratings[cabinetID].dislikeCount++;
        selectedEvaluationData.ratings[cabinetID].target = birdName;
    }

    public void showEvaluationLikedObject(int cabinetID, int round)
    {
        EvaluationPlayerSummary playerSummary = evaluationSlideContentMap[round].allEvaluationPlayerSummaries[cabinetID-1];

        playerSummary.showRatingObject(playerSummary.wasLikedObject,"Like");
    }

    public void showEvaluationDislikedObject(int cabinetID, int round)
    {
        EvaluationPlayerSummary playerSummary = evaluationSlideContentMap[round].allEvaluationPlayerSummaries[cabinetID-1];

        playerSummary.showRatingObject(playerSummary.wasDislikedObject, "Sus" + UnityEngine.Random.Range(1, 5));
    }

    public void showLikedObject(int cabinetID, int tab)
    {
        if (tab == GameManager.Instance.playerFlowManager.numberOfCabinetRounds + 1)
        {
            tab = -1;
        }
        slideContentMap[cabinetID].slideSummaryBirdMap[tab].showLikedObject();
    }

    public void showDislikedObject(int cabinetID, int tab)
    {
        if (tab == GameManager.Instance.playerFlowManager.numberOfCabinetRounds + 1)
        {
            tab = -1;
        }
        slideContentMap[cabinetID].slideSummaryBirdMap[tab].showDislikedObject();
    }
}
