﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class AccoladesRound : PlayerRound
{
    public TMPro.TextMeshProUGUI employeeOfTheMonthPlayerText, upForReviewPlayerText;
    public Image employeeOfTheMonthPlayerImage, upForReviewPlayerImage;
    public GameObject winMarkerObject, loseMarkerObject, resultsContainerObject, cabinetResultsObject;

    public RectTransform accoladesObjectRect;
    public ColourManager.BirdName employeeOfTheMonth, upForReview;
    public List<AccoladesBirdRow> allAccoladeBirdRows;
    public List<IndexMap> allSuccessTokens, allFailTokens;
    public List<GameObject> leftTrashObjects, rightTrashObjects;
    public bool isActive = false;

    public List<CameraDock> cameraDocks;
    public CameraDock.CameraState currentCameraState;
    public CameraDock currentCameraDock, nextCameraDock;

    public GameObject accoladesSpriteContainer;
    public Image accoladesBackground;
    public WorkingGoalsManager workingGoalsManager;
    public Text workerWinText;

    private Dictionary<CameraDock.CameraState, CameraDock> cameraDockMap;
    private Dictionary<int, GameObject> successTokenMap, failTokenMap;
    private float timeSoFar = 0.0f, cameraStateTime = 0.0f;
    public float timeToShowCabinetResults = 2.0f, timeToShowResults = 4.0f, timeShowingResults = 8.0f;
    private bool hasShownResults = false, hasShownCabinetResults = false;
    private int numberOfRoundResultsShown = 0, totalFails = 0, totalSuccesses = 0;

    public override void StartRound()
    {
        base.StartRound();

        initializeAccoladesRound();
    }

    private void Update()
    {
        if (isActive)
        {
            if(currentCameraState != CameraDock.CameraState.reset &&
                currentCameraState != CameraDock.CameraState.result)
            {
                cameraUpdate();
            }
            else if (workingGoalsManager.active)
            {
                return;
            }
            else
            {
                timeSoFar += Time.deltaTime;

                if(timeSoFar > timeToShowCabinetResults &&
                    !hasShownCabinetResults)
                {
                    accoladesBackground.color = new Color(0, 0, 0, 0);
                    accoladesSpriteContainer.SetActive(true);
                    workingGoalsManager.startShowingWorkingGoals();
                    hasShownCabinetResults = true;
                    timeSoFar = 0.0f;
                }
                else if (timeSoFar > timeToShowResults &&
                    !hasShownResults)
                {
                    resultsContainerObject.SetActive(true);
                    hasShownResults = true;
                    timeSoFar = 0.0f;
                }
                else if(timeSoFar > timeShowingResults &&
                    hasShownResults)
                {
                    workingGoalsManager.gameObject.SetActive(false);
                    accoladesSpriteContainer.SetActive(false);
                    isActive = false;
                    
                }
            }

        }
        
    }

    private void cameraUpdate()
    {
        cameraStateTime += Time.deltaTime;

        if (!currentCameraDock.restingFinished)
        {
            if(currentCameraDock.restingTime < cameraStateTime)
            {
                currentCameraDock.restingFinished = true;
                cameraStateTime = 0.0f;
            }
        }
        else
        {
            //Transition
            accoladesObjectRect.anchoredPosition = Vector3.Lerp(currentCameraDock.position, nextCameraDock.position, cameraStateTime * currentCameraDock.transitionMoveSpeed);
            accoladesObjectRect.localScale = Vector3.Lerp(currentCameraDock.zoom, nextCameraDock.zoom, cameraStateTime * currentCameraDock.transitionZoomSpeed);

            if (cameraStateTime * currentCameraDock.transitionMoveSpeed > 1)
            {
                cameraStateTime = 0.0f;
                currentCameraState = currentCameraDock.nextState;
                currentCameraDock = cameraDockMap[currentCameraState];
                nextCameraDock = cameraDockMap[currentCameraDock.nextState];
            }
        }
    }

    public void initializeAccoladeBirdRow(int index, BirdName birdName)
    {
        allAccoladeBirdRows[index].birdName = birdName;
        allAccoladeBirdRows[index].pinImage.color = ColourManager.Instance.birdMap[birdName].colour;
        allAccoladeBirdRows[index].birdHeadImage.sprite = ColourManager.Instance.birdMap[birdName].faceSprite;
        allAccoladeBirdRows[index].gameObject.SetActive(true);
        allAccoladeBirdRows[index].isInitialized = true;
    }

    private void initializeAccoladesRound()
    {
        cameraDockMap = new Dictionary<CameraDock.CameraState, CameraDock>();
        foreach(CameraDock cameraDock in cameraDocks)
        {
            cameraDockMap.Add(cameraDock.state, cameraDock);
        }
        currentCameraDock = cameraDockMap[currentCameraState];
        nextCameraDock = cameraDockMap[currentCameraDock.nextState];

        successTokenMap = new Dictionary<int, GameObject>();
        failTokenMap = new Dictionary<int, GameObject>();

        Dictionary<ColourManager.BirdName, PlayerReviewStatus> playerReviewStatusMap = new Dictionary<ColourManager.BirdName, PlayerReviewStatus>();
        PlayerReviewStatus bestEmployeeCandidate = null;
        PlayerReviewStatus worstEmployeeCandidate = null;

        foreach(IndexMap successToken in allSuccessTokens)
        {
            successTokenMap.Add(successToken.index, successToken.gameObject);
        }
        foreach (IndexMap failToken in allFailTokens)
        {
            failTokenMap.Add(failToken.index, failToken.gameObject);
        }

        foreach (KeyValuePair<ColourManager.BirdName, string> player in playerFlowManager.playerNameMap)
        {
            playerReviewStatusMap.Add(player.Key, new PlayerReviewStatus());
            playerReviewStatusMap[player.Key].birdName = player.Key;
            playerReviewStatusMap[player.Key].playerName = player.Value;
        }

        AccoladesBirdRow currentRow;
        int iterator = 1;
        totalSuccesses = 0;
        int totalPoints = 0;
        //Determine the review status for each player
        foreach (KeyValuePair<int,CabinetDrawer> cabinet in playerFlowManager.drawingRound.cabinetDrawerMap)
        {
            ChainData chainData = cabinet.Value.chainData;
            if(!chainData.active)
            {
                continue;
            }
            foreach(KeyValuePair<int,PlayerTextInputData> prompt in chainData.prompts)
            {
                if (playerReviewStatusMap.ContainsKey(prompt.Value.author))
                {
                    playerReviewStatusMap[prompt.Value.author].totalTimeTaken += prompt.Value.timeTaken;
                }
                
            }
            foreach (KeyValuePair<int, PlayerDrawingInputData> drawing in chainData.drawings)
            {
                if (playerReviewStatusMap.ContainsKey(drawing.Value.author))
                {
                    playerReviewStatusMap[drawing.Value.author].totalTimeTaken += drawing.Value.timeTaken;
                }
               
            }

            
            foreach(KeyValuePair<int, PlayerRatingData> rating in chainData.ratings)
            {

                if (playerReviewStatusMap.ContainsKey(rating.Value.target))
                {
                    playerReviewStatusMap[rating.Value.target].netRating += rating.Value.likeCount;
                    playerReviewStatusMap[rating.Value.target].likeCount += rating.Value.likeCount;
                    playerReviewStatusMap[rating.Value.target].netRating -= rating.Value.dislikeCount;
                    playerReviewStatusMap[rating.Value.target].dislikeCount += rating.Value.dislikeCount;
                }

            }
            foreach(KeyValuePair<int, EvaluationData> evaluationData in GameManager.Instance.playerFlowManager.drawingRound.evaluationsMap)
            {
                if (evaluationData.Value.ratings.ContainsKey(cabinet.Key))
                {
                    PlayerRatingData rating = evaluationData.Value.ratings[cabinet.Key];
                    playerReviewStatusMap[rating.target].netRating += rating.likeCount;
                    playerReviewStatusMap[rating.target].likeCount += rating.likeCount;
                    playerReviewStatusMap[rating.target].netRating -= rating.dislikeCount;
                    playerReviewStatusMap[rating.target].dislikeCount += rating.dislikeCount;
                }
            }

            if(chainData.prefixGuess == chainData.correctPrefix)
            {
                totalPoints++;
            }
            if (chainData.nounGuess == chainData.prefixGuess)
            {
                totalPoints++;
            }
            if (chainData.prefixGuess != chainData.correctPrefix ||
                chainData.nounGuess != chainData.correctNoun)
            {
                totalFails++;
                failTokenMap[totalFails].SetActive(true);
                
            }
            else
            {
                totalPoints++;
                totalSuccesses++;
                successTokenMap[totalSuccesses].SetActive(true);
                
            }
            iterator++;
        }

        workerWinText.text = "\n" + (playerFlowManager.playerNameMap.Count * 2) + " points";

        if ((totalSuccesses) == playerFlowManager.playerNameMap.Count)
        {
            winMarkerObject.SetActive(true);
        }
        else
        {
            loseMarkerObject.SetActive(true);
        }

        Bird currentBird;
        //Iterate over each player to determine candidates for eotm and ufr
        foreach (KeyValuePair<ColourManager.BirdName, PlayerReviewStatus> playerReviewStatus in playerReviewStatusMap)
        {
            if (worstEmployeeCandidate == null)
            {
                worstEmployeeCandidate = playerReviewStatus.Value;
            }
            else
            {
                if(worstEmployeeCandidate.totalTimesFailed > playerReviewStatus.Value.totalTimesFailed)
                {
                    worstEmployeeCandidate = playerReviewStatus.Value;
                }
                else if(worstEmployeeCandidate.totalTimesFailed == playerReviewStatus.Value.totalTimesFailed)
                {
                    if(worstEmployeeCandidate.netRating > playerReviewStatus.Value.netRating)
                    {
                        worstEmployeeCandidate = playerReviewStatus.Value;
                    }
                    else if(worstEmployeeCandidate.netRating == playerReviewStatus.Value.netRating)
                    {
                        if(worstEmployeeCandidate.totalTimeTaken < playerReviewStatus.Value.totalTimeTaken)
                        {
                            worstEmployeeCandidate = playerReviewStatus.Value;
                        }
                    }
                }
            }

            if(bestEmployeeCandidate == null)
            {
                bestEmployeeCandidate = playerReviewStatus.Value;
            }
            else
            {
                if (bestEmployeeCandidate.totalTimesFailed < playerReviewStatus.Value.totalTimesFailed)
                {
                    bestEmployeeCandidate = playerReviewStatus.Value;
                }
                else if (bestEmployeeCandidate.totalTimesFailed == playerReviewStatus.Value.totalTimesFailed)
                {
                    if (bestEmployeeCandidate.netRating < playerReviewStatus.Value.netRating)
                    {
                        bestEmployeeCandidate = playerReviewStatus.Value;
                    }
                    else if (bestEmployeeCandidate.netRating == playerReviewStatus.Value.netRating)
                    {
                        if (bestEmployeeCandidate.totalTimeTaken > playerReviewStatus.Value.totalTimeTaken)
                        {
                            bestEmployeeCandidate = playerReviewStatus.Value;
                        }
                    }
                }
            }

            if(allAccoladeBirdRows.Where(abr => abr.isInitialized).Any(abr => abr.birdName == playerReviewStatus.Key))
            {
                currentRow = allAccoladeBirdRows.Where(abr => abr.isInitialized).Single(abr => abr.birdName == playerReviewStatus.Key);

                //Set the stats for the corkboard
                currentRow.gameObject.SetActive(true);
                currentBird = ColourManager.Instance.birdMap[playerReviewStatus.Key];
                currentRow.playerNameText.color = currentBird.colour;
                currentRow.playerNameText.text = playerReviewStatus.Value.playerName;
                currentRow.likesCounterText.text = "x" + playerReviewStatus.Value.likeCount.ToString();
                currentRow.dislikesCounterText.text = "x" + playerReviewStatus.Value.dislikeCount.ToString();
                currentRow.timeCounterText.text = ((int)playerReviewStatus.Value.totalTimeTaken).ToString() + "s";
                currentRow.failsCounterText.text = playerReviewStatus.Value.totalTimesFailed.ToString();
            }

        }

        //Set the accolades
        setAccolades(bestEmployeeCandidate.birdName, bestEmployeeCandidate.playerName, worstEmployeeCandidate.birdName, worstEmployeeCandidate.playerName);
        isActive = true;

        List<WorkingGoalsManager.Goal> goals = new List<WorkingGoalsManager.Goal>() 
        { new WorkingGoalsManager.Goal(WorkingGoalsManager.GoalType.worker_win, playerFlowManager.playerNameMap.Count * 2) };
        for(int i = 3; i < playerFlowManager.playerNameMap.Count; i += 2)
        {
            goals.Add(new WorkingGoalsManager.Goal(WorkingGoalsManager.GoalType.extra_vote, i*2));
        }
        List<WorkingGoalsManager.PlayerPoints> playerPoints = new List<WorkingGoalsManager.PlayerPoints>();
        foreach(CabinetDrawer cabinet in playerFlowManager.drawingRound.cabinetDrawerMap.Values)
        {
            if (!cabinet.chainData.active)
            {
                continue;
            }
            int points = 0;
            if(cabinet.chainData.correctNoun == cabinet.chainData.nounGuess)
            {
                points++;
            }
            if(cabinet.chainData.correctPrefix == cabinet.chainData.prefixGuess)
            {
                points++;
            }
            if(cabinet.chainData.correctNoun == cabinet.chainData.nounGuess &&
                cabinet.chainData.correctPrefix == cabinet.chainData.prefixGuess)
            {
                points++;
            }

            playerPoints.Add(new WorkingGoalsManager.PlayerPoints(cabinet.chainData.guesser, points, cabinet.chainData.prefixGuess, cabinet.chainData.correctPrefix, cabinet.chainData.nounGuess, cabinet.chainData.correctNoun));
        }

        workingGoalsManager.initializeWorkingGoals(goals, playerPoints);

        //leftTrashObjects[Random.Range(0, leftTrashObjects.Count)].SetActive(true);
        rightTrashObjects[Random.Range(0, rightTrashObjects.Count)].SetActive(true);
    }


    public void setAccolades(ColourManager.BirdName bestBird, string bestPlayerName, ColourManager.BirdName worstBird, string worstPlayerName)
    {
        employeeOfTheMonth = bestBird;
        upForReview = worstBird;

        employeeOfTheMonthPlayerText.text = bestPlayerName;
        employeeOfTheMonthPlayerText.color = ColourManager.Instance.birdMap[bestBird].colour;
        employeeOfTheMonthPlayerImage.sprite = ColourManager.Instance.birdMap[bestBird].faceSprite;

        upForReviewPlayerText.text = worstPlayerName;
        upForReviewPlayerText.color = ColourManager.Instance.birdMap[worstBird].colour;
        upForReviewPlayerImage.sprite = ColourManager.Instance.birdMap[worstBird].faceSprite;
    }
}

class PlayerReviewStatus
{
    public int netRating = 0;
    public int likeCount = 0, dislikeCount = 0;
    public float totalTimeTaken = 0.0f;
    public int totalTimesFailed = 0;
    public ColourManager.BirdName birdName = ColourManager.BirdName.none;
    public string playerName = "";
}

[System.Serializable]
public class CameraDock
{
    public enum CameraState
    {
        stats_rest, stats_to_accs, accs_rest, accs_to_result, result, reset
    }
    public CameraState state;
    public CameraState nextState;

    public Vector3 position;
    public Vector3 zoom;

    public float transitionMoveSpeed;
    public float transitionZoomSpeed;

    public float restingTime;
    public bool restingFinished = false;

    public void setRelativePositions()
    {
        position = Camera.main.ViewportToWorldPoint(position);
        zoom = Camera.main.ViewportToWorldPoint(position);
    }
}
