﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ColourManager;

public class RevealRound : PlayerRound
{
    public GameObject revealPlayerObject;
    public Text alignmentText, prefixText, playerNameText, resultText;

    public List<RevealVotesBird> revealVotesBirds;

    public int numberOfPlayers = 8;

    public float timePerVoteReveal;
    public float timeToRevealPlayerName, timeToRevealWasA, timeToRevealAlignment, timeToWaitAfterAlignment;
    public float timeToRevealTie, timeToWaitAfterTie;

    public bool isActive = false;
    public bool hasSentEndGame = false;

    private float timeTaken;
    
    private bool hasShownPlayerName = false, hasShownWasA = false, hasShownAlignment = false;
    private bool hasShownTie = false;
    private bool hasShownVotes = false;

    private bool someoneWasVotedOff = false;
    private bool workersWon = false;
    private int numberOfVotesShown = 0;
    private bool isInitialized = false;

    public Dictionary<ColourManager.BirdName, string> playerBirdNameMap = new Dictionary<ColourManager.BirdName, string>();
    private Dictionary<int, RevealVotesBird> revealVotesBirdsMap;

    private void Start()
    {
        if (!isInitialized)
        {
            initialize();
        }
    }

    public void initialize()
    {
        revealVotesBirdsMap = new Dictionary<int, RevealVotesBird>();
        foreach (RevealVotesBird revealVotesBird in revealVotesBirds)
        {
            revealVotesBirdsMap.Add(revealVotesBird.index, revealVotesBird);
        }
    }

    public override void StartRound()
    {
        base.StartRound();
        isActive = true;
        numberOfVotesShown = 0;
        foreach (KeyValuePair<int, RevealVotesBird> revealVotesBird in revealVotesBirdsMap)
        {
            if (revealVotesBird.Value.isInitialized)
            {
                foreach (KeyValuePair<int, Image> voteImage in revealVotesBird.Value.voteImageMap)
                {
                    voteImage.Value.gameObject.SetActive(false);
                }
            }
        }
        revealPlayerObject.SetActive(false);
        playerNameText.gameObject.SetActive(false);
        prefixText.gameObject.SetActive(false);
        alignmentText.gameObject.SetActive(false);
        resultText.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isActive)
        {
            if (!isInitialized)
            {
                initialize();
            }
            timeTaken += Time.deltaTime;

            if (!hasShownVotes)
            {
                if (timeTaken > timePerVoteReveal)
                {
                    bool hasAnotherVoteToShow = false;
                    //Show a new vote
                    foreach (KeyValuePair<int, RevealVotesBird> revealVotesBird in revealVotesBirdsMap)
                    {
                        if (!revealVotesBird.Value.isInitialized)
                        {
                            revealVotesBird.Value.initialize();
                        }
                        
                        if (revealVotesBird.Value.voteImageMap.ContainsKey(numberOfVotesShown + 1) && 
                            revealVotesBird.Value.numberOfVotes > numberOfVotesShown)
                        {
                            hasAnotherVoteToShow = true;
                            revealVotesBird.Value.voteImageMap[numberOfVotesShown + 1].gameObject.SetActive(true);
                        }
                    }
                    timeTaken = 0.0f;
                    numberOfVotesShown++;
                    if (!hasAnotherVoteToShow)
                    {
                        hasShownVotes = true;
                        if (someoneWasVotedOff)
                        {
                            revealPlayerObject.SetActive(true);
                        }
                        
                    }
                }
            }
            else
            {
                if (someoneWasVotedOff)
                {
                    if (timeTaken > timeToRevealPlayerName && !hasShownPlayerName)
                    {
                        playerNameText.gameObject.SetActive(true);
                        hasShownPlayerName = true;
                    }
                    else if (timeTaken > timeToRevealWasA && !hasShownWasA)
                    {
                        prefixText.gameObject.SetActive(true);
                        hasShownWasA = true;
                    }
                    else if (timeTaken > timeToRevealAlignment && !hasShownAlignment)
                    {
                        alignmentText.gameObject.SetActive(true);
                        hasShownAlignment = true;
                    }
                    else if (timeTaken > timeToWaitAfterAlignment)
                    {
                        isActive = false;
                        hasShownVotes = false;
                        hasShownWasA = false;
                        hasShownAlignment = false;
                        hasShownPlayerName = false;
                        hasShownTie = false;
                        timeTaken = 0.0f;
                    }
                }
                else
                {
                    if (timeTaken > timeToRevealTie && !hasShownTie)
                    {
                        resultText.gameObject.SetActive(true);
                        hasShownTie = true;
                    }
                    else if (timeTaken > timeToWaitAfterTie)
                    {
                        isActive = false;
                        hasShownVotes = false;
                        hasShownWasA = false;
                        hasShownAlignment = false;
                        hasShownPlayerName = false;
                        hasShownTie = false;
                        timeTaken = 0.0f;
                    }
                }
            }
        }
    }

    public void setAccusations(BirdName botcherBirdName, Dictionary<BirdName, Dictionary<int,BirdName>> accusationMap, Dictionary<int,BirdName> orderMap)
    {
        if (!playerFlowManager)
        {
            playerFlowManager = GameManager.Instance.playerFlowManager;
        }
        if (!isInitialized)
        {
            initialize();
        }

        int mostVotes = 0;
        List<BirdName> birdsWithMostVotes = new List<BirdName>();
        BirdName accusedBirdName;
        RevealVotesBird revealVoteRow;
        ResultVoteRow resultVoteRow;
        Dictionary<int, BirdName> votes;
        int currentVoteRound = GameManager.Instance.playerFlowManager.accusationsRound.currentVoteRound;


        foreach(RevealVotesBird revealVoteBird in revealVotesBirdsMap.Values)
        {
            revealVoteBird.gameObject.SetActive(false);
        }

        for (int i = 1; i <= accusationMap.Count; i++)
        {
            if (!playerFlowManager.resultsRound.isInitialized)
            {
                playerFlowManager.resultsRound.initialize();
            }
            accusedBirdName = orderMap[i];
            if (!revealVotesBirdsMap.ContainsKey(i))
            {
                Debug.LogError("Reveal votes bird map missing index ["+i.ToString()+"]");
                continue;
            }
            revealVoteRow = revealVotesBirdsMap[i];

            resultVoteRow = playerFlowManager.resultsRound.resultVoteRowMap[currentVoteRound][i];
            if (!revealVoteRow.isInitialized)
            {
                revealVoteRow.initialize();
            }
            if (!resultVoteRow.isInitialized)
            {
                resultVoteRow.initialize();
            }

            revealVoteRow.birdImage.sprite = ColourManager.Instance.birdMap[accusedBirdName].faceSprite;
            resultVoteRow.birdImage.sprite = ColourManager.Instance.birdMap[accusedBirdName].faceSprite;
            votes = accusationMap[accusedBirdName];
            revealVoteRow.numberOfVotes = votes.Count;
            resultVoteRow.numberOfVotes = votes.Count;

            if(revealVoteRow.numberOfVotes == mostVotes)
            {
                birdsWithMostVotes.Add(accusedBirdName);
            }
            else if(revealVoteRow.numberOfVotes > mostVotes)
            {
                mostVotes = revealVoteRow.numberOfVotes;
                birdsWithMostVotes.Clear();
                birdsWithMostVotes.Add(accusedBirdName);
            }
            revealVoteRow.isActive = true;
            revealVoteRow.gameObject.SetActive(true);
            resultVoteRow.isActive = true;
            resultVoteRow.gameObject.SetActive(true);

            for(int j = 1; j <= votes.Count; j++)
            {
                revealVoteRow.voteImageMap[j].sprite = ColourManager.Instance.birdMap[votes[j]].faceSprite;
                resultVoteRow.voteImageMap[j].sprite = ColourManager.Instance.birdMap[votes[j]].faceSprite;
                resultVoteRow.voteImageMap[j].gameObject.SetActive(true);
            }

            resultVoteRow.birdName = accusedBirdName;
            resultVoteRow.playerNameText.text = playerFlowManager.playerNameMap[accusedBirdName];
            resultVoteRow.playerNameText.color = ColourManager.Instance.birdMap[accusedBirdName].colour;
            resultVoteRow.roleText.text = botcherBirdName == accusedBirdName ? "Botcher" : "Worker";
            resultVoteRow.roleText.color = botcherBirdName == accusedBirdName ? GameManager.Instance.traitorColour : GameManager.Instance.workerColour;
        }

        int increment = accusationMap.Count + 1;
        foreach(KeyValuePair<BirdName,string> birdName in playerBirdNameMap)
        {
            if (!accusationMap.ContainsKey(birdName.Key))
            {
                resultVoteRow = playerFlowManager.resultsRound.resultVoteRowMap[currentVoteRound][increment];
                resultVoteRow.birdImage.sprite = ColourManager.Instance.birdMap[birdName.Key].faceSprite;
                resultVoteRow.birdImage.sprite = ColourManager.Instance.birdMap[birdName.Key].faceSprite;
                resultVoteRow.playerNameText.text = birdName.Value;
                resultVoteRow.playerNameText.color = ColourManager.Instance.birdMap[birdName.Key].colour;
                resultVoteRow.roleText.text = botcherBirdName == birdName.Key ? "Botcher" : "Worker";
                resultVoteRow.roleText.color = botcherBirdName == birdName.Key ? GameManager.Instance.traitorColour : GameManager.Instance.workerColour;
                resultVoteRow.isActive = true;
                resultVoteRow.gameObject.SetActive(true);

                increment++;
            }
        }

        //Set whether there's a tie or not
        if (birdsWithMostVotes.Count > 1)
        {
            someoneWasVotedOff = false;
        }
        else if(birdsWithMostVotes.Count == 1)
        {
            someoneWasVotedOff = true;
            revealPlayerObject.GetComponent<Image>().sprite = ColourManager.Instance.birdMap[birdsWithMostVotes[0]].faceSprite;
            if (playerBirdNameMap.ContainsKey(birdsWithMostVotes[0]))
            {
                playerNameText.color = ColourManager.Instance.birdMap[birdsWithMostVotes[0]].colour;
                playerNameText.text = playerBirdNameMap[birdsWithMostVotes[0]];
                if (playerFlowManager.resultsRound.resultVoteRowMap[currentVoteRound].Where(rvr => rvr.Value.birdName == birdsWithMostVotes[0]).Count() == 1)
                {
                    playerFlowManager.resultsRound.resultVoteRowMap[currentVoteRound].Single(rvr => rvr.Value.birdName == birdsWithMostVotes[0]).Value.votedOffImage.SetActive(true);
                }

                if(birdsWithMostVotes[0] == botcherBirdName)
                {
                    alignmentText.text = "the Botcher";
                    alignmentText.color = GameManager.Instance.traitorColour;
                }
                else
                {
                    alignmentText.text = "a Worker";
                    alignmentText.color = GameManager.Instance.workerColour;
                }
            }
        }

        if (!PhotonNetwork.IsMasterClient)
        {
            GameNetwork.Instance.ToServerQueue.Add("transition_condition" + GameDelim.BASE + "accusations_loaded:" + SettingsManager.Instance.birdName);
        }

    }

    public void addPlayerName(ColourManager.BirdName inBirdName, string playerName)
    {
        if (playerBirdNameMap.ContainsKey(inBirdName))
        {
            return;
        }
        else
        {
            playerBirdNameMap.Add(inBirdName, playerName);
        }
    }
}
