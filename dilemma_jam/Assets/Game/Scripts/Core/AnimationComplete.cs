﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationComplete : MonoBehaviour
{
    public List<GameObject> objectsToShow;
    public List<GameObject> objectsToHide;
}
