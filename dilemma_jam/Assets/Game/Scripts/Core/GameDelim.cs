﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class GameDelim
{
    public const string BASE = "':'";
    public const string SUB = "','";
    public const string NAME = "'~'";
    public const string LINE = "'newl'";
    public const string SUBLINE = "'subL'";
    public const string POINT = "'newp'";
    public const string CONT = "'c'";
    public const string ITEM = "'item'";
    public const string RATING = "'r'";

    public static string stripGameDelims(string inString)
    {
        inString = inString.Replace(BASE, ":");
        inString = inString.Replace(SUB, ",");
        inString = inString.Replace(NAME, "~");
        inString = inString.Replace(LINE, "newl");
        inString = inString.Replace(SUBLINE, "subL");
        inString = inString.Replace(POINT, "newp");
        inString = inString.Replace(ITEM, "item");
        inString = inString.Replace(RATING, "r");

        return inString;
    }
}

